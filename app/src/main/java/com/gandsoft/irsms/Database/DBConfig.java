package com.gandsoft.irsms.Database;

import com.gandsoft.irsms.IrsmsApp;

/**
 * Created by gleen on 09/02/18.
 */

public interface DBConfig {

    String DATABASE_NAME = "subdistrict.db";
    String DATABASE_PATH = "/data/data/" + IrsmsApp.getContext().getPackageName() +"/databases/";
    int DATABASE_VERSION = 1;
}
