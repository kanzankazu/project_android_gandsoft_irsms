package com.gandsoft.irsms.Database;

import com.gandsoft.irsms.model.RealmDbModel.RequestRealmDbObject;

import io.realm.Realm;
import io.realm.RealmObject;

/**
 * Created by gleen on 13/02/18.
 */

public class RealmDatabaseHelper {

    protected static Realm realm = Realm.getDefaultInstance();

    public static Realm getRealmObject() {
        return realm;
    }

    //bugged as https://github.com/realm/realm-java/issues/993
    @Deprecated
    public static RealmObject saveToRealm(RealmObject object) {
        realm.beginTransaction();
        RealmObject obj = realm.copyToRealm(object);
        realm.commitTransaction();
        return obj;
    }

    public static int getNextPk(Realm relm, final Class realmModel, final String primaryKey) {
        final int[] nextpk = {0};
        relm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Number currentIdNum = realm.where(realmModel).max(primaryKey);
                if (currentIdNum == null) {
                    nextpk[0] = 1;
                } else {
                    nextpk[0] = currentIdNum.intValue() + 1;
                }
            }
        });
        return nextpk[0];
    }


    /*size*/
    public int sizeAccident(int id) {
        return realm.where(RequestRealmDbObject.class).equalTo("LocalID", id).findFirst().getSetAccidentRequestModel().size();
    }

    public int sizeDriver(int id) {
        return realm.where(RequestRealmDbObject.class).equalTo("LocalID", id).findFirst().getSetDriverRequestModels().size();
    }

    public int sizePassanger(int id) {
        return realm.where(RequestRealmDbObject.class).equalTo("LocalID", id).findFirst().getSetPassangerRequestModels().size();
    }

    public int sizePedestrian(int id) {
        return realm.where(RequestRealmDbObject.class).equalTo("LocalID", id).findFirst().getSetPedestrianRequestModels().size();
    }

    public int sizeWitness(int id) {
        return realm.where(RequestRealmDbObject.class).equalTo("LocalID", id).findFirst().getSetWitnessRequestModels().size();
    }


}
