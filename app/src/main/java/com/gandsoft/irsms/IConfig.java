package com.gandsoft.irsms;

/**
 * Created by glenn on 1/25/18.
 */


public interface IConfig {

    String PACKAGE_NAME = IrsmsApp.getContext().getPackageName();
    String API_BASE_URL = "http://irsmsapi.gandsoft.com/api/";

    int API_CALLER_LOGIN_CODE = 11001;
    int API_CALLER_FASWORD_CODE = 11002;

    int API_CALLER_UIITEM_CODE = 21001;
    int API_CALLER_LOGOUT_CODE = 21002;

    int API_CALLER_SET_ACCIDENT = 31001;

    String RETURN_STATUS_DONE = "success";

    String DATABASE_PATH = "";
    String DATABASE_NAME = "subdistrict.db";

    int versionCode = BuildConfig.VERSION_CODE;
    String versionName = BuildConfig.VERSION_NAME;

}
