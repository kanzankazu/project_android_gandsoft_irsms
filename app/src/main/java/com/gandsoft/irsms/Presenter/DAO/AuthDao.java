package com.gandsoft.irsms.Presenter.DAO;

import com.gandsoft.irsms.API.API;
import com.gandsoft.irsms.model.RequestModel.Auth.ChangePasswordRequestModel;
import com.gandsoft.irsms.model.RequestModel.Auth.LoginRequestModel;
import com.gandsoft.irsms.model.RequestModel.Auth.LogoutRequestModel;
import com.gandsoft.irsms.model.RequestModel.Auth.ResetPasswordRequestModel;

import com.gandsoft.irsms.API.API;
import com.gandsoft.irsms.model.RequestModel.Auth.ChangePasswordRequestModel;
import com.gandsoft.irsms.model.RequestModel.Auth.LoginRequestModel;
import com.gandsoft.irsms.model.RequestModel.Auth.LogoutRequestModel;
import com.gandsoft.irsms.model.RequestModel.Auth.ResetPasswordRequestModel;

import java.security.PublicKey;

import app.beelabs.com.codebase.base.BaseActivity;
import app.beelabs.com.codebase.base.BaseDao;
import retrofit2.Callback;

/**
 * Created by glenn on 1/25/18.
 */

public class AuthDao extends BaseDao {

    public AuthDao(Object obj) {
        super(obj);
    }

    public void doLoginDao(BaseActivity ac, LoginRequestModel request, Callback callback) {
        API.doLogin(ac, request, callback);
    }

    public void doChangePasswordDao(BaseActivity ac, ChangePasswordRequestModel request, Callback callback) {
        API.doChangePassword(ac, request, callback);
    }

    public void doResetPasswordDao(BaseActivity ac, ResetPasswordRequestModel request, Callback callback) {
        API.doResetPassword(ac, request, callback);
    }

    public void doLogOutDao(BaseActivity ac, LogoutRequestModel request, Callback callback) {
        API.doLogOut(ac, request, callback);
    }

    public void doCheckVersionDao(BaseActivity ac, Callback callback) {
        API.doCheckVersion(ac, callback);
    }

    public void doGetuserInfoDao(BaseActivity ac, Callback callback) {
        API.doGetuserInfo(ac, callback);
    }


}
