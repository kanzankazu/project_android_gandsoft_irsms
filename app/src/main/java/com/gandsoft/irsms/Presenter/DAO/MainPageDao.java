package com.gandsoft.irsms.Presenter.DAO;

import com.gandsoft.irsms.API.API;
import com.gandsoft.irsms.model.RequestModel.AccidentForm.PolresRequestModel;
import com.gandsoft.irsms.model.RequestModel.Auth.LoginRequestModel;
import com.gandsoft.irsms.model.RequestModel.MainPage.PostAccidentRequestModel;
import com.gandsoft.irsms.model.RequestModel.MainPage.SearchAccidentRequestModel;

import com.gandsoft.irsms.API.API;
import com.gandsoft.irsms.model.RequestModel.MainPage.PostAccidentRequestModel;
import com.gandsoft.irsms.model.RequestModel.MainPage.SearchAccidentRequestModel;

import app.beelabs.com.codebase.base.BaseActivity;
import app.beelabs.com.codebase.base.BaseDao;
import retrofit2.Callback;

/**
 * Created by glenn on 1/25/18.
 */

public class MainPageDao extends BaseDao {

    public MainPageDao(Object obj) {
        super(obj);
    }

    public void doSearchAccidentDao(BaseActivity ac, SearchAccidentRequestModel request, Callback callback) {
        API.doSearchAccident(ac, request, callback);
    }

    public void doPostAccidentDao(BaseActivity ac, PostAccidentRequestModel request, Callback callback) {
        API.doPostAccident(ac, request, callback);
    }

    public void doGetUiItemDao(BaseActivity ac, Callback callback) {
        API.doGetUiItem(ac, callback);
    }
}
