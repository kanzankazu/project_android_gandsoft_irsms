package com.gandsoft.irsms.Presenter.SeasonManager;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.gandsoft.irsms.IConfig;
import com.gandsoft.irsms.ISeasonConfig;
import com.gandsoft.irsms.IrsmsApp;
import com.gandsoft.irsms.Support.Util.SystemUtil;
import com.gandsoft.irsms.model.ObjectModel.UserDataModel;
import com.gandsoft.irsms.model.ResponseModel.BaseResponseModel;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import app.beelabs.com.codebase.base.response.BaseResponse;

/**
 * Created by glenn on 1/26/18.
 */

public class SessionManager implements ISeasonConfig {
    private static SharedPreferences pref;
    private static SharedPreferences.Editor editor;

    Context mContext;
    int PRIVATE_MODE = 0;

    public SessionManager() {
        this.mContext = IrsmsApp.getContext();
        pref = mContext.getSharedPreferences(IConfig.PACKAGE_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public static boolean isLoggedIn() {
        pref = IrsmsApp.getContext().getSharedPreferences(IConfig.PACKAGE_NAME, 0);
        editor = pref.edit();
        return loadUserData() != null;
    }

    public static void createLoginSession(String name) {

        editor.putBoolean(KEY_IS_LOGIN_KEY, true);
        editor.putString(KEY_USERNAME, name);
        editor.commit();
    }

    public static boolean saveUserData(UserDataModel model) {
        if (pref == null) {
            new SessionManager();
        }
        createLoginSession(model.getFirst_name() + " " + model.getLast_name());
        try {
            saveModelAsGson(KEY_USERDATA, model);
            return true;
        } catch (Exception e) {
            //Log.e("usersave err", e.getMessage());
            return false;
        }
    }

    public static boolean saveData(Object model, String key) {
        if (pref == null) {
            new SessionManager();
        }
        try {
            saveModelAsGson(key, model);
        } catch (Exception e) {
            //Log.e("save err: ", e.getMessage());
            return false;
        } finally {
            return true;
        }
    }

    private static void saveModelAsGson(String key, Object model) {
        pref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(model);
        editor.putString(key, json);
        editor.commit();
    }

    public static String loadData(String key) {
        return pref.getString(key, ERROR_RETRIEVAL);
    }

    public static boolean checkIfExist(String key) {
        return pref != null ? pref.contains(key) : false;
    }

    public static UserDataModel loadUserData() {
        try {
            Gson gson = new Gson();
            String json = pref.getString(KEY_USERDATA, "");
            UserDataModel model = gson.fromJson(json, UserDataModel.class);
            return model;
        } catch (Exception e) {
            return null;
        }
    }

    public static void logoutUser() {
        try {
            editor.remove(KEY_USERDATA);
            editor.remove(KEY_IS_LOGIN_KEY);
            editor.remove(KEY_USERNAME);
//            editor.remove(KEY_UITEM);
            editor.commit();
            editor = null;
            pref = null;
        } catch (Exception e) {

        }
    }

    public static void purgeUImodel() {
        editor.remove(KEY_UITEM).apply();
        //editor.commit();
    }

    public static void checkForNullKey(String key) {
        if (key == null) {
            throw new NullPointerException();
        }
    }

    public static void checkForNullValue(String value) {
        if (value == null) {
            throw new NullPointerException();
        }
    }

    //
    public static void saveInt(String keyUitemUpdate, int totalUi) {
        pref.edit();
        editor.putInt(keyUitemUpdate, totalUi);
        editor.commit();
    }

    public static int loadInt(String keyUitemUpdate) {
        pref.edit();
        return pref.getInt(keyUitemUpdate, 0);
    }

    public static void saveString(String keyUitemUpdate, String totalUi) {
        pref.edit();
        editor.putString(keyUitemUpdate, totalUi);
        editor.commit();
    }

    public static String loadString(String keyUitemUpdate, int totalUi) {
        pref.edit();
        return pref.getString(keyUitemUpdate, null);
    }

    public static void saveListString(String key, List<String> stringList) {
        checkForNullKey(key);
        String[] myStringList = stringList.toArray(new String[stringList.size()]);
        pref.edit().putString(key, TextUtils.join("‚‗‚", myStringList)).apply();
    }

    public static ArrayList<String> loadListString(String key) {
        return new ArrayList<String>(Arrays.asList(TextUtils.split(pref.getString(key, ""), "‚‗‚")));
    }

    public static void saveListInt(String key, ArrayList<Integer> intList) {
        checkForNullKey(key);
        Integer[] myIntList = intList.toArray(new Integer[intList.size()]);
        pref.edit().putString(key, TextUtils.join("‚‗‚", myIntList)).apply();
    }

    public static ArrayList<Integer> loadListInt(String key) {
        String[] myList = TextUtils.split(pref.getString(key, ""), "‚‗‚");
        ArrayList<String> arrayToList = new ArrayList<String>(Arrays.asList(myList));
        ArrayList<Integer> newList = new ArrayList<Integer>();

        for (String item : arrayToList)
            newList.add(Integer.parseInt(item));

        return newList;
    }

    //
    public static boolean isTotalDataUpdate(BaseResponse br) {
        int totalUi = ((BaseResponseModel) br).getTotal();
        return totalUi != SessionManager.loadInt(KEY_UITEM_TOTAL);
    }

    public static boolean isVersionDataUpdate(BaseResponse br) {
        int versionUi = ((BaseResponseModel) br).getVersion();
        return versionUi != SessionManager.loadInt(KEY_UITEM_VERSION);
    }

    public static void saveValidateData(BaseResponse br) {
        ArrayList<String> carCombine = new ArrayList<>();
        carCombine.addAll(SystemUtil.convertStringArrayToListString(((BaseResponseModel) br).getCar_default()));
        carCombine.addAll(SystemUtil.convertStringArrayToListString(((BaseResponseModel) br).getCar_relation()));
        saveListString(KEY_UITEM_CAR_DEFAULT, carCombine);
        Log.d("Lihat saveValidateData SessionManager", String.valueOf(carCombine));

        ArrayList<String> motorCombine = new ArrayList<>();
        motorCombine.addAll(SystemUtil.convertStringArrayToListString(((BaseResponseModel) br).getMotor_default()));
        motorCombine.addAll(SystemUtil.convertStringArrayToListString(((BaseResponseModel) br).getMotor_relation()));
        saveListString(KEY_UITEM_MOTOR_DEFAULT, motorCombine);
        Log.d("Lihat saveValidateData SessionManager", String.valueOf(motorCombine));

        ArrayList<String> safetyVehicleCarCombine = new ArrayList<>();
        safetyVehicleCarCombine.addAll(SystemUtil.convertStringArrayToListString(((BaseResponseModel) br).getSafety_vehicle_car_default()));
        safetyVehicleCarCombine.addAll(SystemUtil.convertStringArrayToListString(((BaseResponseModel) br).getSafety_vehicle_car_relation()));
        saveListString(KEY_UITEM_SAFETY_VEHICLE_CAR_DEFAULT, safetyVehicleCarCombine);
        Log.d("Lihat saveValidateData SessionManager", String.valueOf(safetyVehicleCarCombine));

        ArrayList<String> safetyVehicleMotorCombine = new ArrayList<>();
        safetyVehicleMotorCombine.addAll(SystemUtil.convertStringArrayToListString(((BaseResponseModel) br).getSafety_vehicle_motor_default()));
        safetyVehicleMotorCombine.addAll(SystemUtil.convertStringArrayToListString(((BaseResponseModel) br).getSafety_vehicle_motor_relation()));
        saveListString(KEY_UITEM_SAFETY_VEHICLE_MOTOR_DEFAULT, safetyVehicleMotorCombine);
        Log.d("Lihat saveValidateData SessionManager", String.valueOf(safetyVehicleMotorCombine));

        ArrayList<String> safetyDriverVehicleCarCombine = new ArrayList<>();
        safetyDriverVehicleCarCombine.addAll(SystemUtil.convertStringArrayToListString(((BaseResponseModel) br).getSafety_driver_car_default()));
        safetyDriverVehicleCarCombine.addAll(SystemUtil.convertStringArrayToListString(((BaseResponseModel) br).getSafety_driver_car_relation()));
        saveListString(KEY_UITEM_SAFETY_DRIVER_CAR_DEFAULT, safetyDriverVehicleCarCombine);
        Log.d("Lihat saveValidateData SessionManager", String.valueOf(safetyDriverVehicleCarCombine));

        ArrayList<String> safetyDriverVehicleMotorCombine = new ArrayList<>();
        safetyDriverVehicleMotorCombine.addAll(SystemUtil.convertStringArrayToListString(((BaseResponseModel) br).getSafety_driver_motor_default()));
        safetyDriverVehicleMotorCombine.addAll(SystemUtil.convertStringArrayToListString(((BaseResponseModel) br).getSafety_driver_motor_relation()));
        saveListString(KEY_UITEM_SAFETY_DRIVER_MOTOR_DEFAULT, safetyDriverVehicleMotorCombine);
        Log.d("Lihat saveValidateData SessionManager", String.valueOf(safetyDriverVehicleMotorCombine));

        ArrayList<String> safetyPassVehicleCarCombine = new ArrayList<>();
        safetyPassVehicleCarCombine.addAll(SystemUtil.convertStringArrayToListString(((BaseResponseModel) br).getSafety_pass_car_default()));
        safetyPassVehicleCarCombine.addAll(SystemUtil.convertStringArrayToListString(((BaseResponseModel) br).getSafety_pass_car_relation()));
        saveListString(KEY_UITEM_SAFETY_PASS_CAR_DEFAULT, safetyPassVehicleCarCombine);
        Log.d("Lihat saveValidateData SessionManager", String.valueOf(safetyPassVehicleCarCombine));

        ArrayList<String> safetyPassVehicleMotorCombine = new ArrayList<>();
        safetyPassVehicleMotorCombine.addAll(SystemUtil.convertStringArrayToListString(((BaseResponseModel) br).getSafety_pass_motor_default()));
        safetyPassVehicleMotorCombine.addAll(SystemUtil.convertStringArrayToListString(((BaseResponseModel) br).getSafety_pass_motor_relation()));
        saveListString(KEY_UITEM_SAFETY_PASS_MOTOR_DEFAULT, safetyPassVehicleMotorCombine);
        Log.d("Lihat saveValidateData SessionManager", String.valueOf(safetyPassVehicleMotorCombine));

    }
}
