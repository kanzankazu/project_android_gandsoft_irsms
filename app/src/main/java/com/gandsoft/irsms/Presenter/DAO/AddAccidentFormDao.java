package com.gandsoft.irsms.Presenter.DAO;

import com.gandsoft.irsms.API.API;
import com.gandsoft.irsms.model.RequestModel.AccidentForm.PolresRequestModel;
import com.gandsoft.irsms.model.RequestModel.AccidentForm.SetAccidentRequestModel;
import com.gandsoft.irsms.model.RequestModel.AccidentForm.SetCheckAccidentRequestModel;
import com.gandsoft.irsms.model.RequestModel.AccidentForm.UploadPhotoRequestModel;

import com.gandsoft.irsms.API.API;
import com.gandsoft.irsms.model.RequestModel.AccidentForm.PolresRequestModel;
import com.gandsoft.irsms.model.RequestModel.AccidentForm.SetAccidentRequestModel;
import com.gandsoft.irsms.model.RequestModel.AccidentForm.UploadPhotoRequestModel;
import com.gandsoft.irsms.model.RequestModel.Auth.LogoutRequestModel;

import app.beelabs.com.codebase.base.BaseActivity;
import app.beelabs.com.codebase.base.BaseDao;
import retrofit2.Callback;

/**
 * Created by glenn on 1/25/18.
 */

public class AddAccidentFormDao extends BaseDao {

    public AddAccidentFormDao(Object obj) {
        super(obj);
    }

    public void doSetAccidentDao(BaseActivity ac, SetAccidentRequestModel request, Callback callback) {
        API.doSetAccident(ac, request, callback);
    }

    public void doGetPoldaDao(BaseActivity ac, Callback callback) {
        API.doGetPolda(ac, callback);
    }

    public void doGetPolresDao(BaseActivity ac, PolresRequestModel request, Callback callback) {
        API.doGetPolres(ac, request, callback);
    }

    public void doUploadPhotoDao(BaseActivity ac, UploadPhotoRequestModel request, Callback callback){
        API.doUploadPhoto(ac, request, callback);
    }

    public void doCheckAccidentDao(BaseActivity ac, SetCheckAccidentRequestModel request, Callback callback) {
        API.docheckAccident(ac, request, callback);
    }
}
