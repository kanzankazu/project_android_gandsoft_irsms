package com.gandsoft.irsms.Presenter.DAO;

import com.gandsoft.irsms.API.API;
import com.gandsoft.irsms.model.RequestModel.VehicleForm.GetVehicleDetailRequestModel;
import com.gandsoft.irsms.model.RequestModel.VehicleForm.GetVehicleIDRequestModel;
import com.gandsoft.irsms.model.RequestModel.VehicleForm.GetVehicleTypeSubRequestModel;
import com.gandsoft.irsms.model.RequestModel.WitnessForm.GetWitnessDetailRequestModel;
import com.gandsoft.irsms.model.RequestModel.DiagramMappingForm.SetDiagramMappingRequestModel;
import com.gandsoft.irsms.model.RequestModel.VehicleForm.SetDriverRequestModel;
import com.gandsoft.irsms.model.RequestModel.PassangerForm.SetPassangerRequestModel;
import com.gandsoft.irsms.model.RequestModel.PedestrianForm.SetPedestrianRequestModel;
import com.gandsoft.irsms.model.RequestModel.WitnessForm.SetWitnessRequestModel;

import com.gandsoft.irsms.API.API;
import com.gandsoft.irsms.model.RequestModel.DiagramMappingForm.SetDiagramMappingRequestModel;
import com.gandsoft.irsms.model.RequestModel.PassangerForm.SetPassangerRequestModel;
import com.gandsoft.irsms.model.RequestModel.PedestrianForm.SetPedestrianRequestModel;
import com.gandsoft.irsms.model.RequestModel.VehicleForm.GetVehicleDetailRequestModel;
import com.gandsoft.irsms.model.RequestModel.VehicleForm.GetVehicleIDRequestModel;
import com.gandsoft.irsms.model.RequestModel.VehicleForm.GetVehicleTypeSubRequestModel;
import com.gandsoft.irsms.model.RequestModel.VehicleForm.SetDriverRequestModel;
import com.gandsoft.irsms.model.RequestModel.WitnessForm.GetWitnessDetailRequestModel;
import com.gandsoft.irsms.model.RequestModel.WitnessForm.SetWitnessRequestModel;

import app.beelabs.com.codebase.base.BaseActivity;
import app.beelabs.com.codebase.base.BaseDao;
import retrofit2.Callback;

/**
 * Created by glenn on 1/25/18.
 */

public class AddDriverDao extends BaseDao {

    public AddDriverDao(Object obj) {
        super(obj);
    }

    public void doSetDriverDao(BaseActivity ac, SetDriverRequestModel request, Callback callback) {
        API.doSetDriver(ac, request, callback);
    }

    public void doGetVehicleDetailDao(BaseActivity ac, GetVehicleDetailRequestModel request, Callback callback) {
        API.doGetVehicleDetail(ac, request, callback);
    }

    public void doGetWitnessDetailDao(BaseActivity ac, GetWitnessDetailRequestModel request, Callback callback) {
        API.doGetWitnessDetail(ac, request, callback);
    }

    public void doGetVehicleIDDao(BaseActivity ac, GetVehicleIDRequestModel request, Callback callback) {
        API.doGetVehicleID(ac, request, callback);
    }

    public void doSetPassengerDao(BaseActivity ac, SetPassangerRequestModel request, Callback callback) {
        API.doSetPassenger(ac, request, callback);
    }

    public void doSetPedestrianDao(BaseActivity ac, SetPedestrianRequestModel request, Callback callback) {
        API.doSetPedestrian(ac, request, callback);
    }

    public void doSetWitnessDao(BaseActivity ac, SetWitnessRequestModel request, Callback callback) {
        API.doSetWitness(ac, request, callback);
    }

    public void doSetDiagramMappingDao(BaseActivity ac, SetDiagramMappingRequestModel request, Callback callback) {
        API.doSetDiagramMapping(ac, request, callback);
    }

    public void doGetVehicleTypeSubDao(BaseActivity ac, GetVehicleTypeSubRequestModel request, Callback callback) {
        API.doGetVehicleTypeSub(ac, request, callback);
    }

}
