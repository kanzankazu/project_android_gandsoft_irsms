package com.gandsoft.irsms.Support.Util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.media.ExifInterface;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.gandsoft.irsms.IrsmsApp;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static android.content.Context.ACTIVITY_SERVICE;


/**
 * Created by glenn on 9/6/17.
 * modified at 7/2/18 for irsms
 */

public class SystemUtil {

    public static Intent getCropperIntent(Context mContext, Uri imageUri, @Nullable Integer sizex, @Nullable Integer sizey) {
        Intent CropIntent = null;

        if (sizex == null) {
            sizex = 800;
        }
        if (sizey == null) {
            sizey = 900;
        }
        if (sizex > 1500) {
            sizex = 1500;
        }
        if (sizey > 1500) {
            sizey = 1500;
        }

        try {
            CropIntent = new Intent("com.android.camera.action.CROP");
            CropIntent.setDataAndType(imageUri, "image/*");
            CropIntent.putExtra("crop", "true");
            CropIntent.putExtra("outputX", sizex);
            CropIntent.putExtra("outputY", sizey);
            CropIntent.putExtra("aspectX", 4);
            CropIntent.putExtra("aspectY", 4);
            CropIntent.putExtra("scaleUpIfNeeded", true);
            CropIntent.putExtra("return-data", true);
            return CropIntent;
        } catch (ActivityNotFoundException e) {
            return CropIntent;
        }
    }

    private static List<Intent> addIntentsToList(Context context, List<Intent> list, Intent intent) {
        List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resInfo) {
            String packageName = resolveInfo.activityInfo.packageName;
            Intent targetedIntent = new Intent(intent);
            targetedIntent.setPackage(packageName);
            list.add(targetedIntent);
        }
        return list;
    }

    public static Bitmap resizeImage(Bitmap bitmap, int newSize) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        int newWidth = 0;
        int newHeight = 0;

        if (width > height) {
            newWidth = newSize;
            newHeight = (newSize * height) / width;
        } else if (width < height) {
            newHeight = newSize;
            newWidth = (newSize * width) / height;
        } else if (width == height) {
            newHeight = newSize;
            newWidth = newSize;
        }

        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
    }

    public static Uri getUriFromResult(Context context, int resultCode, Uri jink) {
        InputStream in = null;
        Uri selectedImage = null;
        if (resultCode == Activity.RESULT_OK) {

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), jink);
                in = context.getContentResolver().openInputStream(jink);
                ExifInterface exifInterface = new ExifInterface(in);

                int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);

                switch (orientation) {

                    case ExifInterface.ORIENTATION_ROTATE_90:
                        selectedImage = rotateImage(bitmap, 90, context);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        selectedImage = rotateImage(bitmap, 180, context);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        selectedImage = rotateImage(bitmap, 270, context);
                        break;

                    case ExifInterface.ORIENTATION_NORMAL:
                    default:
                        selectedImage = jink;
                        break;
                }

            } catch (IOException e) {
                // Handle any errors
                return null;
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException ignored) {
                    }
                }
                return selectedImage;
            }
        } else {
            return selectedImage;
        }
    }

    private static File getTempFile(Context context) {
        File imageFile = new File(context.getExternalCacheDir(), "Tempe");
        imageFile.getParentFile().mkdirs();
        return imageFile;
    }

    public static String bitmapToBase64(Bitmap bitmap, Bitmap.CompressFormat format, int compression) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(format, compression, stream);
        byte[] byteArray = stream.toByteArray();
        try {
            stream.close();
            stream = null;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return "data:image/jpeg;base64," + Base64.encodeToString(byteArray, Base64.NO_WRAP);
        }
    }

    public static Bitmap Base64ToBitmap(String base64) {
        byte[] decodedString = Base64.decode(base64.replace("data:image/jpeg;base64,", ""), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

    public static Uri rotateImage(Bitmap source, float angle, Context mContext) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        Bitmap temp = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        temp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(mContext.getContentResolver(), temp, "Title", null);
        return Uri.parse(path);

    }

    public static Bitmap rotateImage(Bitmap source, float angle, Context mContext, Bitmap.CompressFormat format, int compressionLevel) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        Bitmap temp = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        temp.compress(format, compressionLevel, bytes);
        return temp;
    }

    @SuppressLint("MissingPermission")
    public static String getImei(Context cuntext) {
        TelephonyManager manager = (TelephonyManager) cuntext.getSystemService(cuntext.TELEPHONY_SERVICE);
        if (isKitkat()) {
            return manager.getDeviceId();
        } else {
            if (ActivityCompat.checkSelfPermission(cuntext, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    return manager.getImei();
                }
                return manager.getDeviceId();
            } else {
                Toast.makeText(cuntext, "No Permission", Toast.LENGTH_SHORT).show();
                return "NO PERMISSION";
            }
        }
    }

    /**
     * for beta testing
     *
     * @return
     */
    /*public static String getImei() {
        return "123456789";
    }*/
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void validationIdentityByType(EditText editText, String id, ArrayList<String> idIdentityTypeIdLv, ArrayList<String> idIdentityTypeMaxLenghtLv) {
        //editText.setText("");
        String s = editText.getText().toString();
        int i = s.length();

        for (int j = 0; j < idIdentityTypeIdLv.size(); j++) {
            if (id.equalsIgnoreCase("G0400")) {//TIDAK DI KETAHUI
                editText.setVisibility(View.INVISIBLE);
                editText.setText("");
            } else if (id.equalsIgnoreCase("G0401")) {//KTP
                int q = getPosStringInList(idIdentityTypeIdLv, "G0401");
                editText.setVisibility(View.VISIBLE);
                editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Integer.parseInt(idIdentityTypeMaxLenghtLv.get(q)))});
                if (i > Integer.parseInt(idIdentityTypeMaxLenghtLv.get(q))) {
                    editText.setText("");
                }
            } else if (id.equalsIgnoreCase("G0402")) {//PASPOR
                int q = getPosStringInList(idIdentityTypeIdLv, "G0402");
                editText.setVisibility(View.VISIBLE);
                editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
                editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Integer.parseInt(idIdentityTypeMaxLenghtLv.get(q)))});
                if (i > Integer.parseInt(idIdentityTypeMaxLenghtLv.get(q))) {
                    editText.setText("");
                }
            } else if (id.equalsIgnoreCase("G0403")) {//SIM
                int q = getPosStringInList(idIdentityTypeIdLv, "G0403");
                editText.setVisibility(View.VISIBLE);
                editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Integer.parseInt(idIdentityTypeMaxLenghtLv.get(q)))});
                if (i > Integer.parseInt(idIdentityTypeMaxLenghtLv.get(q))) {
                    editText.setText("");
                }
            } else if (id.equalsIgnoreCase("G0404")) {//SIM RUSAK
                editText.setVisibility(View.INVISIBLE);
                editText.setText("");
            } else if (id.equalsIgnoreCase(idIdentityTypeIdLv.get(j))) {
                editText.setVisibility(View.VISIBLE);
                //editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
                editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Integer.parseInt(idIdentityTypeMaxLenghtLv.get(j)))});
                if (i > Integer.parseInt(idIdentityTypeMaxLenghtLv.get(j))) {
                    editText.setText("");
                }
            }
        }

        /*if (id.equalsIgnoreCase("G0400")) {//TIDAK DI KETAHUI
            editText.setVisibility(View.INVISIBLE);
            editText.setText("");
        } else if (id.equalsIgnoreCase("G0401")) {//KTP
            editText.setVisibility(View.VISIBLE);
            editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(16)});
            if (i > 16) {
                editText.setText("");
            }
        } else if (id.equalsIgnoreCase("G0402")) {//PASPOR
            editText.setVisibility(View.VISIBLE);
            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(9)});
            if (i > 9) {
                editText.setText("");
            }
        } else if (id.equalsIgnoreCase("G0403")) {//SIM
            editText.setVisibility(View.VISIBLE);
            editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(12)});
            if (i > 12) {
                editText.setText("");
            }
        } else if (id.equalsIgnoreCase("G0404")) {//SIM RUSAK
            editText.setVisibility(View.INVISIBLE);
            editText.setText("");
        } else {
            editText.setVisibility(View.VISIBLE);
            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});
            if (i > 50) {
                editText.setText("");
            }
        }*/
    }

    public static String getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();

        return ageS;
    }

    public static void textCaps(EditText editText) {
        editText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
    }


    /*LIST & ARRAY*/
    public static Integer[] convertListIntegertToIntegerArray(List<Integer> result) {
        Integer[] finalResult = new Integer[result.size()];
        return result.toArray(finalResult);
    }

    public static ArrayList<String> convertStringArrayToListString(String[] strings) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < strings.length; i++) {
            arrayList.add(strings[i]);
        }
        return arrayList;
    }

    public static boolean isListContainInt(final int[] array, final int key) {
        Arrays.sort(array);
        return Arrays.binarySearch(array, key) >= 0;
    }

    public static boolean isListContainString(List<String> sourceList, String s) {
        return sourceList.contains(s);
    }

    public static boolean isListContainStringArray(List<String> sourceList, String stringArray) {
        String[] strings = stringArray.split(",");
        List<String> stringList = Arrays.asList(strings);
        return sourceList.containsAll(stringList);
    }

    public static boolean isListContainStringList(List<String> sourceList, List<String> stringList) {
        return sourceList.containsAll(stringList);
    }

    public static List<Integer> getAllSizeStringList(List<String> sourceList) {
        List<Integer> integers = new ArrayList<>();
        for (int i = 0; i < sourceList.size(); i++) {
            integers.add(i);
        }
        return integers;
    }

    public static int getPosStringInList(List<String> sourceList, String s) {
        int position = -1;
        for (int i = 0; i < sourceList.size(); i++) {
            if (sourceList.get(i).equalsIgnoreCase(s)) {
                position = i;
            }
        }
        return position;
    }

    public static List<Integer> getPosListStringInList(List<String> sourceList, String s) {
        List<Integer> integers = new ArrayList<>();
        for (int i = 0; i < sourceList.size(); i++) {
            if (sourceList.get(i).equalsIgnoreCase(s)) {
                integers.add(i);
            }
        }
        return integers;
    }

    public static List<Integer> getInvPosListStringInList(List<String> sourceList, String s) {
        List<Integer> integers = new ArrayList<>();
        for (int i = 0; i < sourceList.size(); i++) {
            if (!sourceList.get(i).equalsIgnoreCase(s)) {
                integers.add(i);
            }
        }
        return integers;
    }

    public static List<Integer> getPosListStringIn2List(List<String> sourceList, List<String> s) {
        List<Integer> integers = new ArrayList<>();
        for (int i = 0; i < sourceList.size(); i++) {
            for (String ss : s) {
                if (sourceList.get(i).equalsIgnoreCase(ss)) {
                    integers.add(i);
                }
            }

        }
        return integers;
    }

    public static List<Integer> getInvPosListStringIn2List(List<String> sourceList, List<String> s) {
        List<Integer> integers = new ArrayList<>();
        for (int i = 0; i < sourceList.size(); i++) {
            for (String ss : s) {
                if (!sourceList.get(i).equalsIgnoreCase(ss)) {
                    integers.add(i);
                }
            }

        }
        return integers;
    }

    /*Clear Data & Clear Cache*/

    public static void clearAppData(Context context) {
        File cacheDirectory = context.getCacheDir();
        File applicationDirectory = new File(cacheDirectory.getParent());
        if (applicationDirectory.exists()) {
            String[] fileNames = applicationDirectory.list();
            for (String fileName : fileNames) {
                if (!fileName.equals("lib")) {
                    deleteFile(new File(applicationDirectory, fileName));
                }
            }
        }
    }

    private static boolean deleteFile(File file) {
        boolean deletedAll = true;
        if (file != null) {
            if (file.isDirectory()) {
                String[] children = file.list();
                for (int i = 0; i < children.length; i++) {
                    deletedAll = deleteFile(new File(file, children[i])) && deletedAll;
                }
            } else {
                deletedAll = file.delete();
            }
        }

        return deletedAll;
    }

    public static void clearCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    private static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public static long checkMaxMemory() {
        Runtime rt = Runtime.getRuntime();
        long maxMemory = rt.maxMemory();
        return maxMemory;
    }

    public static int getMemoryClass() {
        ActivityManager am = (ActivityManager) IrsmsApp.getContext().getSystemService(ACTIVITY_SERVICE);
        int memoryClass = am.getMemoryClass();
        return memoryClass;
    }

    /*OTHER*/

    public static boolean isKitkat() {
        return Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT;
    }
}
