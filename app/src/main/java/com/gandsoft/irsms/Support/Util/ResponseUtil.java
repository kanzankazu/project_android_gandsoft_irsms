package com.gandsoft.irsms.Support.Util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.widget.Toast;

import com.gandsoft.irsms.Activity.Login.LoginActivity;
import com.gandsoft.irsms.IConfig;
import com.gandsoft.irsms.ISeasonConfig;
import com.gandsoft.irsms.Presenter.SeasonManager.SessionManager;
import com.gandsoft.irsms.model.uiModel.FormdataLoader;

import app.beelabs.com.codebase.base.response.BaseResponse;

public class ResponseUtil implements ISeasonConfig, IConfig {

    public static void addFormData(BaseResponse br, Context context) {
        SessionManager.saveData(br, ISeasonConfig.KEY_UITEM);
        FormdataLoader.initFormData();
        //isUiReady = true;
        Toast.makeText(context, "Data selesai di tambah, aplikasi siap digunakan", Toast.LENGTH_SHORT).show();
    }

    public static void replaceFormData(BaseResponse br, Context context) {
        SessionManager.purgeUImodel();
        SessionManager.saveData(br, ISeasonConfig.KEY_UITEM);
        FormdataLoader.initFormData();
        //isUiReady = true;
        Toast.makeText(context, "Data selesai di perbaharui, aplikasi akan memulai ulang", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            public void run() {
                //code here
                Intent mStartActivity = new Intent(context, LoginActivity.class);
                int mPendingIntentId = 123456;
                PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
                System.exit(0);
            }
        }, 2000);

    }
}
