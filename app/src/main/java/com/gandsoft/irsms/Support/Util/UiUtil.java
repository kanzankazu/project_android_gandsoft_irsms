package com.gandsoft.irsms.Support.Util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

/**
 * Created by glenn on 1/26/18.
 */

public class UiUtil {

    /**
     * @param context           activity context
     * @param dialogContentView what view on dialog, set it's content beforehand
     */
    public static void showDialog(Context context, View dialogContentView) {
        if (!(context instanceof Activity)) {
            throw new IllegalStateException("Context should be an activity");
        }
        Dialog dialog = new Dialog((Activity) context);
        dialog.setContentView(dialogContentView);
        dialog.show();
    }

    public static String getIDRadio(LinearLayout view) {
        return view.findViewById(((RadioGroup) ((LinearLayout) view.getChildAt(0)).getChildAt(1)).getCheckedRadioButtonId()).getTag().toString();
    }

}
