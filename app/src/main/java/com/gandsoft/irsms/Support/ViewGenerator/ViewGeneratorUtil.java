package com.gandsoft.irsms.Support.ViewGenerator;

import com.gandsoft.irsms.Presenter.witget.CheckBoxReactiveLinearLayout;
import com.gandsoft.irsms.Presenter.witget.ToggleRadioButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.gandsoft.irsms.Presenter.witget.CheckBoxReactiveLinearLayout;
import com.gandsoft.irsms.Presenter.witget.ToggleRadioButton;

import java.util.List;

/**
 * Created by gleen on 08/02/18.
 */

public class ViewGeneratorUtil {

    public static Integer getSelectedRadioIndex(ViewGroup guk) {
        try {
            return ((RadioGroup) ((LinearLayout) guk.getChildAt(0)).getChildAt(1))
                    .indexOfChild(((RadioGroup) ((LinearLayout) guk.getChildAt(0)).getChildAt(1))
                            .findViewById(((RadioGroup) ((LinearLayout) guk.getChildAt(0))
                                    .getChildAt(1)).getCheckedRadioButtonId()));
        } catch (Exception e) {
            //Log.e("guk1", e.getMessage());
            return null;
        }

    }

    public static String getSelectedRadioValue(ViewGroup guk) {
        try {
            return (String) ((RadioGroup) ((LinearLayout) guk.getChildAt(0)).getChildAt(1))
                    .findViewById(((RadioGroup) ((LinearLayout) guk.getChildAt(0))
                            .getChildAt(1)).getCheckedRadioButtonId()).getTag();
        } catch (Exception e) {
            //Log.e("guk2", e.getMessage());
            return null;
        }
    }

    public static String getSelectedRadioTextValue(ViewGroup guk) {
        try {
//            ToggleRadioButton x = ((RadioGroup) ((LinearLayout) guk.getChildAt(0)).getChildAt(1)).findViewById(((RadioGroup) ((LinearLayout) guk.getChildAt(0)).getChildAt(1)).getCheckedRadioButtonId());
            return (String) ((ToggleRadioButton) ((RadioGroup) ((LinearLayout) guk.getChildAt(0)).getChildAt(1))
                    .findViewById(((RadioGroup) ((LinearLayout) guk.getChildAt(0))
                            .getChildAt(1)).getCheckedRadioButtonId())).getText();
        } catch (Exception e) {
            //Log.e("guk3", e.getMessage());
            return null;
        }
    }

    public static void setRadioCheckedAt(ViewGroup guk, Integer index) {
        try {
            if (index != null && index >= 0) {
                ((AppCompatRadioButton) ((RadioGroup) ((LinearLayout) guk.getChildAt(0)).getChildAt(1)).getChildAt(0)).setChecked(false);
                ((AppCompatRadioButton) ((RadioGroup) ((LinearLayout) guk.getChildAt(0)).getChildAt(1)).getChildAt(index)).setChecked(true);
            } else {
                Log.d("guk4", "");
            }
        } catch (Exception e) {
            //Log.e("guk5", e.getMessage());
        }
    }

    public static void setRadioUnCheckedAt(ViewGroup guk, Integer index) {
        try {
            if (index != null && index >= 0) {
                ((AppCompatRadioButton) ((RadioGroup) ((LinearLayout) guk.getChildAt(0)).getChildAt(1)).getChildAt(0)).setChecked(true);
                ((AppCompatRadioButton) ((RadioGroup) ((LinearLayout) guk.getChildAt(0)).getChildAt(1)).getChildAt(index)).setChecked(false);
            } else {
                Log.d("guk4", "");
            }
        } catch (Exception e) {
            //Log.e("guk5", e.getMessage());
        }
    }

    public static void setRadioDisabledAt(ViewGroup meong, Integer[] indexes) {
        if (indexes != null) {
            for (int index : indexes) {
                ((AppCompatRadioButton) ((RadioGroup) ((LinearLayout) meong.getChildAt(0)).getChildAt(1)).getChildAt(index)).setEnabled(false);
            }
        }
    }

    public static void setRadioEnabledAt(ViewGroup meong, Integer[] indexes) {
        if (indexes != null) {
            for (int index : indexes) {
                ((AppCompatRadioButton) ((RadioGroup) ((LinearLayout) meong.getChildAt(0)).getChildAt(1)).getChildAt(index)).setEnabled(true);
            }
        }
    }

    //CheckBox
    public static Integer[] getSelectedCheckboxIndex(ViewGroup meong) {
        return ((CheckBoxReactiveLinearLayout) ((LinearLayout) meong.getChildAt(0))
                .getChildAt(1)).getSelectedCheckboxIndex();
    }

    public static List<Integer> getSelectedCheckboxIndexArrayList(ViewGroup meong) {
        return ((CheckBoxReactiveLinearLayout) ((LinearLayout) meong.getChildAt(0))
                .getChildAt(1)).getSelectedCheckboxIndexArrayList();
    }

    public static String getSelectedCheckboxValue(ViewGroup meong) {
        return ((CheckBoxReactiveLinearLayout) ((LinearLayout) meong.getChildAt(0))
                .getChildAt(1)).getSelectedCheckbox();
    }

    public static void setCheckboxCheckedAt(ViewGroup meong, Integer[] indexes) {
        if (indexes != null) {
            for (int index : indexes) {
                ((AppCompatCheckBox) ((CheckBoxReactiveLinearLayout) ((LinearLayout) meong.getChildAt(0)).getChildAt(1)).getChildAt(index)).setChecked(true);
            }
        }
    }

    public static void setCheckboxUnCheckedAt(ViewGroup meong, Integer[] indexes) {
        if (indexes != null) {
            for (int index :
                    indexes) {
                ((AppCompatCheckBox) ((CheckBoxReactiveLinearLayout) ((LinearLayout) meong.getChildAt(0))
                        .getChildAt(1)).getChildAt(index)).setChecked(false);
            }
        }
    }

    public static void setCheckboxDisabledAt(ViewGroup meong, Integer[] indexes) {
        if (indexes != null) {
            for (int index : indexes) {
                ((AppCompatCheckBox) ((CheckBoxReactiveLinearLayout) ((LinearLayout) meong.getChildAt(0)).getChildAt(1)).getChildAt(index)).setEnabled(false);
            }
        }
    }

    public static void setCheckboxEnabledAt(ViewGroup meong, Integer[] indexes) {
        if (indexes != null) {
            for (int index :
                    indexes) {
                ((AppCompatCheckBox) ((CheckBoxReactiveLinearLayout) ((LinearLayout) meong.getChildAt(0))
                        .getChildAt(1)).getChildAt(index)).setEnabled(true);
            }
        }
    }

    //Spinner
    public static int getSelectedSpinnerIndex(ViewGroup mbek) {
        return ((Spinner) ((LinearLayout) mbek.getChildAt(0)).getChildAt(1)).getSelectedItemPosition();
    }

    public static String getSelectedSpinnerValue(ViewGroup mbek) {
        return ((ViewGeneratorSpinnerAdapter) ((Spinner) ((LinearLayout) mbek.getChildAt(0))
                .getChildAt(1)).getAdapter()).getItem(((Spinner) ((LinearLayout) mbek.getChildAt(0))
                .getChildAt(1)).getSelectedItemPosition()).getViewID();
    }

    public static String getSelectedSpinnerTextValue(ViewGroup mbek) {
        return ((ViewGeneratorSpinnerAdapter) ((Spinner) ((LinearLayout) mbek.getChildAt(0))
                .getChildAt(1)).getAdapter()).getItem(((Spinner) ((LinearLayout) mbek.getChildAt(0))
                .getChildAt(1)).getSelectedItemPosition()).getTextContent();
    }

    public static ViewGeneratorSpinnerAdapter getSelectedSpinnerAdapter(ViewGroup mbek) {
        return ((ViewGeneratorSpinnerAdapter) ((Spinner) ((LinearLayout) mbek.getChildAt(0))
                .getChildAt(1)).getAdapter());

    }

    public static void notifyDatasetChangeOnSelectedSpinnerAdapter(ViewGroup mbek) {
        ((Spinner) ((LinearLayout) mbek.getChildAt(0))
                .getChildAt(1)).setSelection(0);
        ((ViewGeneratorSpinnerAdapter) ((Spinner) ((LinearLayout) mbek.getChildAt(0))
                .getChildAt(1)).getAdapter()).notifyDataSetChanged();
    }

    public static void selectSpinnerDropdownAt(ViewGroup mbek, int index) {
        try {
            ((Spinner) ((LinearLayout) mbek.getChildAt(0))
                    .getChildAt(1)).setSelection(index, true);
        } catch (Exception e) {
            //Log.e("", "selectSpinnerDropdownAt: " + e.getMessage());
        }

    }

    public static Spinner selectSpinnerView(ViewGroup mbek) {
        return ((Spinner) ((LinearLayout) mbek.getChildAt(0))
                .getChildAt(1));
    }
}
