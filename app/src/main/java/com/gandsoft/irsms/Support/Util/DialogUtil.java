package com.gandsoft.irsms.Support.Util;

import android.app.Dialog;
import com.gandsoft.irsms.R;
import com.gandsoft.irsms.model.uiModel.InfoDialogModel;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;

/**
 * Created by glenn on 1/29/18.
 */

public class DialogUtil {

    /**
     * generate small dialog from util
     * this dialog are meant for small message like server fail/success responses
     * @param context activity context
     * @param view the view wanted to be displayed
     **/
    public static void generateCustomDialogInfo(Context context, View view){
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        dialog.show();
    }

    /**
     * generate small dialog from util
     * this dialog are meant for small message like server fail/success responses
     * this have negative and positive button
     * @param context activity title
     * @param view set customview
     * @param title title of the alert dialog
     * @param twoButton if the button have positive and negative button, default it shows only negative if false
     * @param positiveListener nullable, set listener for positive button
     */
    public static void generateCustomAlertDialog(Context context, View view, String title, Boolean twoButton, @Nullable DialogInterface.OnClickListener positiveListener){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(view);
        builder.setTitle(title);
        if(twoButton) {
            builder.setPositiveButton(context.getText(com.gandsoft.irsms.R.string.dialog_btn_positive), positiveListener != null ? positiveListener : new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
        }
        builder.setNegativeButton(context.getText(com.gandsoft.irsms.R.string.dialog_btn_negative), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
    }
}
