
package com.gandsoft.irsms.Support.ViewGenerator;


import com.gandsoft.irsms.Presenter.witget.CheckBoxReactiveLinearLayout;
import com.gandsoft.irsms.Presenter.witget.ToggleRadioButton;
import com.gandsoft.irsms.model.uiModel.ViewModel;
import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.gandsoft.irsms.model.uiModel.ViewModel;

/**
 * Created by glenn on 1/17/18.
 *
 * @version 1.0.irsms
 *          fitted for irsms apps
 */

public class ViewGenerator {

    /**
     * Generate a page based on data given on ViewModel
     *
     * @param mContext activity context
     * @param models   ViewModel Object
     * @param parent   where you want to put the view
     * @return view based on data in models
     * @see ViewModel ;
     */
    public static View generateViewBasedOnModel(Context mContext, ViewModel models, ViewGroup parent) {
        ViewGroup mainView = parent;

        switch (models.getViewType()) {
            case 0:
                //generate subObject
                //disable to prevent loop
                //mainView.addView(generateViewBasedOnModel(mContext, models, mainView));
                break;
            case 1:
                mainView.addView(generateTextview(mContext, models));
                break;
            case 2:
                mainView.addView(generateEditText(mContext, models));
                break;
            case 3:
                mainView.addView(generateCheckboxGroup(mContext, models));
                break;
            case 4:
                break;
            case 5:
                mainView.addView(generateRadioGroup(mContext, models));
                break;
            case 6:
                break;
            case 7:
                mainView.addView(generateSpinner(mContext, models));
                break;
            default:
                break;
        }
        return mainView;
    }

    //view generator, make sure UI have correct id
    private static View generateTextview(Context mContext, ViewModel model) {
        ViewGroup container = (ViewGroup) LayoutInflater.from(mContext).inflate(model.getItemResourceID(), null);
        TextView titleUIObject = (TextView) container.getChildAt(0);
        TextView contentUIObject = (TextView) container.getChildAt(1);
        if (model.getTitle() != null) {
            titleUIObject.setText(model.getTitle());
        } else {
            titleUIObject.setVisibility(View.GONE);
        }

        contentUIObject.setTag(model.getViewID());
        contentUIObject.setText(model.getTextContent());
        if (model.getItemDecoratorID() != 0) {
            contentUIObject.setBackgroundResource(model.getItemDecoratorID());
        }
        return container;
    }

    //view generator, make sure UI have correct id
    private static View generateEditText(Context mContext, ViewModel model) {
        ViewGroup container = (ViewGroup) LayoutInflater.from(mContext).inflate(model.getItemResourceID(), null);
        TextView titleUIObject = (TextView) container.getChildAt(0);
        EditText contentUIObject = (EditText) container.getChildAt(1);

        if (model.getTitle() != null) {
            titleUIObject.setText(model.getTitle());
        } else {
            titleUIObject.setVisibility(View.GONE);
        }

        contentUIObject.setTag(model.getViewID());
        contentUIObject.setHint(model.getTextContent());
        if (model.getEditTextInputType() != 0) {
            contentUIObject.setInputType(model.getEditTextInputType());
        }

        if (model.getItemDecoratorID() != 0) {
            contentUIObject.setBackgroundResource(model.getItemDecoratorID());
        }
        return container;
    }

    //view generator, make sure UI have correct id
    private static View generateRadioGroup(Context mContext, ViewModel model) {
        ViewGroup container = (ViewGroup) LayoutInflater.from(mContext).inflate(model.getItemResourceID(), null);
        TextView titleUIObject = (TextView) container.getChildAt(0);
        RadioGroup contentUIObject = (RadioGroup) container.getChildAt(1);

        if (model.getTitle() != null) {
            titleUIObject.setText(model.getTitle());
        } else {
            titleUIObject.setVisibility(View.GONE);
        }
        contentUIObject.setTag(model.getViewID());

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );

        params.setMargins(16, 0, 16, 0);

        for (ViewModel radioItems :
                model.getViewModelGroup()) {
            ToggleRadioButton child = (ToggleRadioButton) LayoutInflater.from(mContext).inflate(radioItems.getItemResourceID(), null);
            child.setTag(radioItems.getViewID());
            child.setText(radioItems.getTextContent());
            child.setPadding(4, 4, 0, 4);
            contentUIObject.addView(child, params);
        }
        return container;
    }

    private static View generateCheckboxGroup(Context mContext, ViewModel model) {
        ViewGroup container = (ViewGroup) LayoutInflater.from(mContext).inflate(model.getItemResourceID(), null);
        TextView titleUIObject = (TextView) container.getChildAt(0);
        CheckBoxReactiveLinearLayout contentUIObject = (CheckBoxReactiveLinearLayout) container.getChildAt(1);

        if (model.getTitle() != null) {
            titleUIObject.setText(model.getTitle());
        } else {
            titleUIObject.setVisibility(View.GONE);
        }

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        for (ViewModel CheckboxItem : model.getViewModelGroup()) {
            params.setMargins(16, 4, 16, 4);
            AppCompatCheckBox child = (AppCompatCheckBox) LayoutInflater.from(mContext).inflate(CheckboxItem.getItemResourceID(), null);
            child.setTag(CheckboxItem.getViewID());
            child.setText(CheckboxItem.getTextContent());
            contentUIObject.addChildRefID(CheckboxItem.getViewID());
            //contentUIObject.addChildRefID(child.getId());
            contentUIObject.addView(child, params);
        }
        return container;
    }

    private static View generateSpinner(Context mContext, ViewModel model) {
        ViewGroup container = (ViewGroup) LayoutInflater.from(mContext).inflate(model.getItemResourceID(), null);
        TextView titleUIObject = (TextView) container.getChildAt(0);
        Spinner contentUIObject = (Spinner) container.getChildAt(1);
        ViewGeneratorSpinnerAdapter adapter = new ViewGeneratorSpinnerAdapter(mContext, model.getViewModelGroup());
        contentUIObject.setAdapter(adapter);

        if (model.getTitle() != null) {
            titleUIObject.setText(model.getTitle());
        } else {
            titleUIObject.setVisibility(View.GONE);
        }

        return container;
    }
}

