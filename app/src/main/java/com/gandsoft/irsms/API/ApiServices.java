package com.gandsoft.irsms.API;

import com.gandsoft.irsms.model.RequestModel.AccidentForm.PolresRequestModel;
import com.gandsoft.irsms.model.RequestModel.AccidentForm.SetAccidentRequestModel;
import com.gandsoft.irsms.model.RequestModel.AccidentForm.SetCheckAccidentRequestModel;
import com.gandsoft.irsms.model.RequestModel.AccidentForm.UploadPhotoRequestModel;
import com.gandsoft.irsms.model.RequestModel.Auth.ChangePasswordRequestModel;
import com.gandsoft.irsms.model.RequestModel.Auth.LoginRequestModel;
import com.gandsoft.irsms.model.RequestModel.Auth.LogoutRequestModel;
import com.gandsoft.irsms.model.RequestModel.Auth.ResetPasswordRequestModel;
import com.gandsoft.irsms.model.RequestModel.DiagramMappingForm.SetDiagramMappingRequestModel;
import com.gandsoft.irsms.model.RequestModel.MainPage.PostAccidentRequestModel;
import com.gandsoft.irsms.model.RequestModel.MainPage.SearchAccidentRequestModel;
import com.gandsoft.irsms.model.RequestModel.PassangerForm.SetPassangerRequestModel;
import com.gandsoft.irsms.model.RequestModel.PedestrianForm.SetPedestrianRequestModel;
import com.gandsoft.irsms.model.RequestModel.VehicleForm.GetVehicleDetailRequestModel;
import com.gandsoft.irsms.model.RequestModel.VehicleForm.GetVehicleIDRequestModel;
import com.gandsoft.irsms.model.RequestModel.VehicleForm.GetVehicleTypeSubRequestModel;
import com.gandsoft.irsms.model.RequestModel.VehicleForm.SetDriverRequestModel;
import com.gandsoft.irsms.model.RequestModel.WitnessForm.GetWitnessDetailRequestModel;
import com.gandsoft.irsms.model.RequestModel.WitnessForm.SetWitnessRequestModel;
import com.gandsoft.irsms.model.ResponseModel.AccidentForm.GETPoldaResponseModel;
import com.gandsoft.irsms.model.ResponseModel.AccidentForm.PolresResponseModel;
import com.gandsoft.irsms.model.ResponseModel.AccidentForm.SetAccidentResponseModel;
import com.gandsoft.irsms.model.ResponseModel.AccidentForm.SetCheckAccidentResponseModel;
import com.gandsoft.irsms.model.ResponseModel.AccidentForm.UploadPhotoResponseModel;
import com.gandsoft.irsms.model.ResponseModel.Auth.ChangePasswordResponseModel;
import com.gandsoft.irsms.model.ResponseModel.Auth.LoginResponseModel;
import com.gandsoft.irsms.model.ResponseModel.Auth.LogoutResponseModel;
import com.gandsoft.irsms.model.ResponseModel.Auth.ResetPasswordResponseModel;
import com.gandsoft.irsms.model.ResponseModel.FormDataLoader.FormDataResponseModel;
import com.gandsoft.irsms.model.ResponseModel.MainPage.PostAccidentResponseModel;
import com.gandsoft.irsms.model.ResponseModel.MainPage.SearchAccidentResponseModel;
import com.gandsoft.irsms.model.ResponseModel.UserInfo.GETUserInfoResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.GetVehicleDetailResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.GetVehicleIDResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.GetVehicleTypeSubResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.GetWitnessDetailResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.SetDiagramMappingResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.SetDriverResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.SetPassangerResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.SetPedestrianResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.SetWitnessResponseModel;
import com.gandsoft.irsms.model.ResponseModel.Version.GETVersionResponseModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by glenn on 1/25/18.
 */

public interface ApiServices {

    @POST("login")
    Call<LoginResponseModel> logIn(
            @Body LoginRequestModel model
    );

    @POST("checkAccident")
    Call<SetCheckAccidentResponseModel> checkAccident(
            @Body SetCheckAccidentRequestModel model
    );

    @POST("logOut")
    Call<LogoutResponseModel> logout(
            @Body LogoutRequestModel model
    );

    @POST("changePassword")
    Call<ChangePasswordResponseModel> changePassword(
            @Body ChangePasswordRequestModel model
    );

    @POST("forgotPassword")
    Call<ResetPasswordResponseModel> resetPassword(
            @Body ResetPasswordRequestModel model
    );

    @POST("searchAccident")
    Call<SearchAccidentResponseModel> searchAccident(
            @Body SearchAccidentRequestModel model
    );

    @POST("accident")
    Call<PostAccidentResponseModel> postAccident(
            @Body PostAccidentRequestModel model
    );

    @POST("uploadPhoto")
    Call<UploadPhotoResponseModel> postUploadPhoto(
            @Body UploadPhotoRequestModel model
    );

    @POST("setAccident")
    Call<SetAccidentResponseModel> setAccident(
            @Body SetAccidentRequestModel model
    );

    @POST("getPolres1")
    Call<PolresResponseModel> getPolres(
            @Body PolresRequestModel model
    );

    @POST("setDriver")
    Call<SetDriverResponseModel> setDriver(
            @Body SetDriverRequestModel model
    );

    @POST("getWitnessDetail")
    Call<GetWitnessDetailResponseModel> getWitnessDetail(
            @Body GetWitnessDetailRequestModel model
    );

    @POST("getVehicleId")
    Call<GetVehicleIDResponseModel> getVehicleID(
            @Body GetVehicleIDRequestModel model
    );

    @POST("getVehicleDetail")
    Call<GetVehicleDetailResponseModel> getVehicleDetail(
            @Body GetVehicleDetailRequestModel model
    );

    @POST("setPassenger")
    Call<SetPassangerResponseModel> setPassenger(
            @Body SetPassangerRequestModel model
    );

    @POST("setPedestrian")
    Call<SetPedestrianResponseModel> setPedestrian(
            @Body SetPedestrianRequestModel model
    );

    @POST("setWitness")
    Call<SetWitnessResponseModel> setWitness(
            @Body SetWitnessRequestModel model
    );

    @POST("diagram-mapping")
    Call<SetDiagramMappingResponseModel> setDiagramMaping(
            @Body SetDiagramMappingRequestModel model
    );

    @POST("getVehicleTypeSub")
    Call<GetVehicleTypeSubResponseModel> getVehicleTypeSub(
            @Body GetVehicleTypeSubRequestModel model
    );

    @GET("getCheckVersion")
    Call<GETVersionResponseModel> getCheckVersion();

    @GET("getuserInfo")
    Call<GETUserInfoResponseModel> getUserInfo();

    @GET("getPolda")
    Call<GETPoldaResponseModel> getPolda();

    @GET("getTipe")
    Call<FormDataResponseModel> getUiItem();

    //Call<GetPolres> getPolres();
    //    @GET("getPolres")


}
