package com.gandsoft.irsms.API;

import android.content.Context;

import com.gandsoft.irsms.IConfig;
import com.gandsoft.irsms.IrsmsApp;
import com.gandsoft.irsms.model.RequestModel.AccidentForm.PolresRequestModel;
import com.gandsoft.irsms.model.RequestModel.AccidentForm.SetAccidentRequestModel;
import com.gandsoft.irsms.model.RequestModel.AccidentForm.SetCheckAccidentRequestModel;
import com.gandsoft.irsms.model.RequestModel.AccidentForm.UploadPhotoRequestModel;
import com.gandsoft.irsms.model.RequestModel.Auth.ChangePasswordRequestModel;
import com.gandsoft.irsms.model.RequestModel.Auth.LoginRequestModel;
import com.gandsoft.irsms.model.RequestModel.Auth.LogoutRequestModel;
import com.gandsoft.irsms.model.RequestModel.Auth.ResetPasswordRequestModel;
import com.gandsoft.irsms.model.RequestModel.DiagramMappingForm.SetDiagramMappingRequestModel;
import com.gandsoft.irsms.model.RequestModel.MainPage.PostAccidentRequestModel;
import com.gandsoft.irsms.model.RequestModel.MainPage.SearchAccidentRequestModel;
import com.gandsoft.irsms.model.RequestModel.PassangerForm.SetPassangerRequestModel;
import com.gandsoft.irsms.model.RequestModel.PedestrianForm.SetPedestrianRequestModel;
import com.gandsoft.irsms.model.RequestModel.VehicleForm.GetVehicleDetailRequestModel;
import com.gandsoft.irsms.model.RequestModel.VehicleForm.GetVehicleIDRequestModel;
import com.gandsoft.irsms.model.RequestModel.VehicleForm.GetVehicleTypeSubRequestModel;
import com.gandsoft.irsms.model.RequestModel.VehicleForm.SetDriverRequestModel;
import com.gandsoft.irsms.model.RequestModel.WitnessForm.GetWitnessDetailRequestModel;
import com.gandsoft.irsms.model.RequestModel.WitnessForm.SetWitnessRequestModel;
import com.gandsoft.irsms.model.ResponseModel.AccidentForm.GETPoldaResponseModel;
import com.gandsoft.irsms.model.ResponseModel.AccidentForm.PolresResponseModel;
import com.gandsoft.irsms.model.ResponseModel.AccidentForm.SetAccidentResponseModel;
import com.gandsoft.irsms.model.ResponseModel.AccidentForm.SetCheckAccidentResponseModel;
import com.gandsoft.irsms.model.ResponseModel.AccidentForm.UploadPhotoResponseModel;
import com.gandsoft.irsms.model.ResponseModel.Auth.ChangePasswordResponseModel;
import com.gandsoft.irsms.model.ResponseModel.Auth.LoginResponseModel;
import com.gandsoft.irsms.model.ResponseModel.Auth.LogoutResponseModel;
import com.gandsoft.irsms.model.ResponseModel.Auth.ResetPasswordResponseModel;
import com.gandsoft.irsms.model.ResponseModel.FormDataLoader.FormDataResponseModel;
import com.gandsoft.irsms.model.ResponseModel.MainPage.PostAccidentResponseModel;
import com.gandsoft.irsms.model.ResponseModel.MainPage.SearchAccidentResponseModel;
import com.gandsoft.irsms.model.ResponseModel.UserInfo.GETUserInfoResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.GetVehicleDetailResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.GetVehicleIDResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.GetVehicleTypeSubResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.GetWitnessDetailResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.SetDiagramMappingResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.SetDriverResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.SetPassangerResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.SetPedestrianResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.SetWitnessResponseModel;
import com.gandsoft.irsms.model.ResponseModel.Version.GETVersionResponseModel;

import app.beelabs.com.codebase.base.BaseApi;
import retrofit2.Callback;

/**
 * Created by glenn on 1/25/18.
 */

public class API extends BaseApi implements IConfig {

    synchronized private static ApiServices initApiDomain(Context context) {
        setApiDomain(API_BASE_URL);
        return (ApiServices) setupApi(IrsmsApp.getAppComponent(), ApiServices.class);
    }

    synchronized public static void doLogin(Context context, LoginRequestModel request, Callback callback) {
        initApiDomain(context).logIn(request).enqueue((Callback<LoginResponseModel>) callback);
    }

    synchronized public static void doChangePassword(Context context, ChangePasswordRequestModel request, Callback callback) {
        initApiDomain(context).changePassword(request).enqueue((Callback<ChangePasswordResponseModel>) callback);
    }

    synchronized public static void doResetPassword(Context context, ResetPasswordRequestModel request, Callback callback) {
        initApiDomain(context).resetPassword(request).enqueue((Callback<ResetPasswordResponseModel>) callback);
    }

    synchronized public static void doSearchAccident(Context context, SearchAccidentRequestModel request, Callback callback) {
        initApiDomain(context).searchAccident(request).enqueue((Callback<SearchAccidentResponseModel>) callback);
    }

    synchronized public static void doPostAccident(Context context, PostAccidentRequestModel request, Callback callback) {
        initApiDomain(context).postAccident(request).enqueue((Callback<PostAccidentResponseModel>) callback);
    }

    synchronized public static void doSetAccident(Context context, SetAccidentRequestModel request, Callback callback) {
        initApiDomain(context).setAccident(request).enqueue((Callback<SetAccidentResponseModel>) callback);
    }

    synchronized public static void doUploadPhoto(Context context, UploadPhotoRequestModel request, Callback callback) {
        initApiDomain(context).postUploadPhoto(request).enqueue((Callback<UploadPhotoResponseModel>) callback);
    }

    synchronized public static void doLogOut(Context context, LogoutRequestModel request, Callback callback) {
        initApiDomain(context).logout(request).enqueue((Callback<LogoutResponseModel>) callback);
    }

    synchronized public static void doCheckVersion(Context context, Callback callback) {
        initApiDomain(context).getCheckVersion().enqueue((Callback<GETVersionResponseModel>) callback);
    }

    synchronized public static void doGetuserInfo(Context context, Callback callback) {
        initApiDomain(context).getUserInfo().enqueue((Callback<GETUserInfoResponseModel>) callback);
    }

    synchronized public static void doGetPolda(Context context, Callback callback) {
        initApiDomain(context).getPolda().enqueue((Callback<GETPoldaResponseModel>) callback);
    }

    synchronized public static void doGetPolres(Context context, PolresRequestModel request, Callback callback) {
        initApiDomain(context).getPolres(request).enqueue((Callback<PolresResponseModel>) callback);
    }

    synchronized public static void doGetUiItem(Context context, Callback callback) {
        initApiDomain(context).getUiItem().enqueue((Callback<FormDataResponseModel>) callback);
    }

    synchronized public static void doSetDriver(Context context, SetDriverRequestModel request, Callback callback) {
        initApiDomain(context).setDriver(request).enqueue((Callback<SetDriverResponseModel>) callback);
    }

    synchronized public static void doGetWitnessDetail(Context context, GetWitnessDetailRequestModel request, Callback callback) {
        initApiDomain(context).getWitnessDetail(request).enqueue((Callback<GetWitnessDetailResponseModel>) callback);
    }

    synchronized public static void doGetVehicleID(Context context, GetVehicleIDRequestModel request, Callback callback) {
        initApiDomain(context).getVehicleID(request).enqueue((Callback<GetVehicleIDResponseModel>) callback);
    }

    synchronized public static void doGetVehicleDetail(Context context, GetVehicleDetailRequestModel request, Callback callback) {
        initApiDomain(context).getVehicleDetail(request).enqueue((Callback<GetVehicleDetailResponseModel>) callback);
    }

    synchronized public static void doSetPassenger(Context context, SetPassangerRequestModel request, Callback callback) {
        initApiDomain(context).setPassenger(request).enqueue((Callback<SetPassangerResponseModel>) callback);
    }

    synchronized public static void doSetPedestrian(Context context, SetPedestrianRequestModel request, Callback callback) {
        initApiDomain(context).setPedestrian(request).enqueue((Callback<SetPedestrianResponseModel>) callback);
    }

    synchronized public static void doSetWitness(Context context, SetWitnessRequestModel request, Callback callback) {
        initApiDomain(context).setWitness(request).enqueue((Callback<SetWitnessResponseModel>) callback);
    }

    synchronized public static void doSetDiagramMapping(Context context, SetDiagramMappingRequestModel request, Callback callback) {
        initApiDomain(context).setDiagramMaping(request).enqueue((Callback<SetDiagramMappingResponseModel>) callback);
    }

    synchronized public static void doGetVehicleTypeSub(Context context, GetVehicleTypeSubRequestModel request, Callback callback) {
        initApiDomain(context).getVehicleTypeSub(request).enqueue((Callback<GetVehicleTypeSubResponseModel>) callback);
    }

    synchronized public static void docheckAccident(Context context, SetCheckAccidentRequestModel request, Callback callback) {
        initApiDomain(context).checkAccident(request).enqueue((Callback<SetCheckAccidentResponseModel>) callback);
    }

}
