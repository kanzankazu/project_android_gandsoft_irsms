package com.gandsoft.irsms.Activity.summary.sub;

/**
 * Created by gleen on 14/02/18.
 */

public interface WitnessObjectHookInteface {

    void deleteWitnessRealmModelAt(int id);

    void editWitnessRealmModelAt(int id);

}
