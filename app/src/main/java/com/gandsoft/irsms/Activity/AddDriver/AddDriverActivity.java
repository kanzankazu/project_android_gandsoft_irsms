package com.gandsoft.irsms.Activity.AddDriver;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gandsoft.irsms.Activity.AddAccident.LocalConfig;
import com.gandsoft.irsms.Activity.LocalBaseActivity;
import com.gandsoft.irsms.ISeasonConfig;
import com.gandsoft.irsms.Presenter.SeasonManager.SessionManager;
import com.gandsoft.irsms.Presenter.witget.CheckBoxReactiveFrameLayout;
import com.gandsoft.irsms.Presenter.witget.ToggleRadioButton;
import com.gandsoft.irsms.R;
import com.gandsoft.irsms.Support.Util.SystemUtil;
import com.gandsoft.irsms.Support.ViewGenerator.ViewGenerator;
import com.gandsoft.irsms.Support.ViewGenerator.ViewGeneratorSpinnerAdapter;
import com.gandsoft.irsms.Support.ViewGenerator.ViewGeneratorUtil;
import com.gandsoft.irsms.model.RealmDbModel.RequestRealmDbObject;
import com.gandsoft.irsms.model.RealmDbModel.VehicleForm.AddDriverStateModel;
import com.gandsoft.irsms.model.RealmDbModel.VehicleForm.DriverDBModel;
import com.gandsoft.irsms.model.uiModel.FormdataLoader;
import com.gandsoft.irsms.model.uiModel.ViewModel;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

import static com.gandsoft.irsms.Support.Util.SystemUtil.convertListIntegertToIntegerArray;
import static com.gandsoft.irsms.Support.Util.SystemUtil.getAge;
import static com.gandsoft.irsms.Support.Util.SystemUtil.getAllSizeStringList;
import static com.gandsoft.irsms.Support.Util.SystemUtil.getInvPosListStringIn2List;
import static com.gandsoft.irsms.Support.Util.SystemUtil.getPosListStringIn2List;
import static com.gandsoft.irsms.Support.Util.SystemUtil.getPosStringInList;
import static com.gandsoft.irsms.Support.Util.SystemUtil.isKitkat;
import static com.gandsoft.irsms.Support.Util.SystemUtil.isListContainString;
import static com.gandsoft.irsms.Support.Util.SystemUtil.isListContainStringArray;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.generateDynamicDBitems;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getAccident_after_id;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getAccident_before_id;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getAccident_end_id;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getAlcohol_id;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getDisita;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getDriver_behavior;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getDriver_law;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getEducation_id;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getGender_id;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getIdModel;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getId_identity_type_id_lv;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getId_license_class_id_lv;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getId_safety_device_lv;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getId_safety_lv;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getIdentity_type_id;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getKabupaten;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getKecamatan;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getKelurahan;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getKerusakan;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getLicense_class_id;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getNationality_id;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getPemudik;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getPlate_color_id;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getProfession_id;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getProvince;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getReligion_id;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getSafety;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getSafety_device;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getSpecial_id;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getVehicle_brand_id;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getVehicle_brand_motor_id;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getVehicle_color_id;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getVehicle_design_id;
import static com.gandsoft.irsms.model.uiModel.FormdataLoader.getVehicle_type_id;

public class AddDriverActivity extends LocalBaseActivity implements DatePickerDialog.OnDateSetListener, LocalConfig {

    private LinearLayout llDataKendaraanTitlefvbi;
    private View cvDataKendaraanKeluarfvbi, cvDataKendaraanSimpanfvbi;
    //Spinner
    private LinearLayout llDataKendaraanWarnafvbi, llDataKendaraanWarnaTNKBfvbi, llDataKendaraanJenisfvbi, llDataKendaraanMotorMerkfvbi, llDataKendaraanMerkfvbi, llDataKendaraanSTNKProvinsifvbi, llDataKendaraanSTNKKotafvbi, llDataKendaraanSTNKKecamatanfvbi, llDataKendaraanSTNKKelurahanfvbi, llDataPengemudiAgamafvbi, llDataPengemudiAlamatKecamatanfvbi, llDataPengemudiAlamatKelurahanfvbi, llDataPengemudiAlamatKotaKabupatenfvbi, llDataPengemudiAlamatProvinsifvbi, llDataPengemudiJenisIdentitasfvbi, llDataPengemudiKebangsaanfvbi, llDataPengemudiGolonganSimfvbi, llDataPengemudiPekerjaanfvbi, llDataPengemudiPendidikanfvbi;
    //RadioButton
    private LinearLayout llviewDataKendaraanPeruntukanfvbi, llviewDataKendaraanPergerakanSebelumTabrakanfvbi, llviewDataKendaraanPergerakanSesudahTabrakanfvbi, llviewDataKendaraanKedudukanAkhirKendaraanfvbi, llviewDataKendaraanKerusakanKhususfvbi, llviewDataKendaraanTipePemudikfvbi, llviewDataPengemudiJenisKelaminfvbi, llviewDataPengemudiTingkatLukafvbi, llviewDataPengemudiDipengaruhiObatAlkoholfvbi, llviewDataPengemudiDiTahanfvbi, llviewDataPengemudiPelakuSementarafvbi;
    //CheckBox
    private LinearLayout llviewDataKendaraanAlatKeselamatanfvbi, llviewDataKendaraanBarangSitafvbi, llviewDataKendaraanKerusakanAwalfvbi, llDataPengemudiTanggalLahir, llDataPengemudiTanggalTerbitSIM, llviewDataPengemudiPenggunaanAlatKeselamatanfvbi;

    private LinearLayout llcontentRadio;
    private TextView tvDataPengemudiJenisPelanggaranfvbi, tvDataPengemudiPerilakuPengemudifvbi, tvDataPengemudiTanggalTerbitSIM, tvDataPengemudiTanggalLahir;
    private EditText etDataKendaraanNoMesinfvbi, etDataKendaraanVolMesinfvbi, etDataKendaraanNoRangkafvbi, etDataKendaraanOdometerfvbi, etDataKendaraanSTNKNofvbi, etDataKendaraanTNKBNofvbi, etDataKendaraanPanjangJarakRemfvbi, etDataKendaraanSTNKfvbi, etDataKendaraanSTNKJalanfvbi, etDataKendaraanSTNKRtfvbi, etDataKendaraanSTNKRwfvbi, etDataKendaraanTNKBfvbi, etDataKendaraanTotalPenumpangfvbi, etDataPengemudiJalanfvbi, etDataPengemudiNamaBelakangfvbi, etDataPengemudiNamaDepanfvbi, etDataPengemudiNoIdentitasfvbi, etDataPengemudiRTfvbi, etDataPengemudiRWfvbi, etDataPengemudiUmurfvbi;
    private Button bDataPengemudiJenisPelanggaranfvbi, bDataPengemudiPerilakuPengemudifvbi;
    private ImageView ivDataKendaraanTitikKerusakanfvbi, ivDataPengemudiTanggalLahirClearfvbi, ivDataPengemudiTanggalLahirfvbi, ivDataPengemudiTanggalTerbitSIMfvbi;
    private RadioGroup llviewDataPengemudiTingkatLukaRadioGroup, llviewDataPengemudiDeadSpot;
    private CheckBoxReactiveFrameLayout cbrDataKendaraanTitikKerusakanfvbi;
    private Spinner spDataPengemudiJenisIdentitasfvbi;

    private Realm realm;
    private RequestRealmDbObject models;

    private int localId;
    private int orderId;

    private Integer[] sv1, sv2;
    //private List<String> typeVehicleOther = Arrays.asList("V02A00", "V02A01", "V02A03", "V02A06", "V02A19", "V02A20", "V02A31", "V02A34");
    //private List<String> typeVehicleR2 = Arrays.asList("V02A05", "V02A14", "V02B15");
    private List<String> typeVehicleR4 = SessionManager.loadListString(ISeasonConfig.KEY_UITEM_CAR_DEFAULT);
    private List<String> typeVehicleR2 = SessionManager.loadListString(ISeasonConfig.KEY_UITEM_MOTOR_DEFAULT);

    //private String r2AlatKeselamatan = "V0402";
    private List<String> r4AlatKeselamatan = SessionManager.loadListString(ISeasonConfig.KEY_UITEM_SAFETY_VEHICLE_CAR_DEFAULT);
    private List<String> r2AlatKeselamatan = SessionManager.loadListString(ISeasonConfig.KEY_UITEM_SAFETY_VEHICLE_MOTOR_DEFAULT);

    //private List<String> r4PenggunaAlatKeselamatan = Arrays.asList("D1003", "D1004");
    //private List<String> r2PenggunaAlatKeselamatan = Arrays.asList("D1001", "D1002", "D1003");
    private List<String> r4PenggunaAlatKeselamatan = SessionManager.loadListString(ISeasonConfig.KEY_UITEM_SAFETY_DRIVER_CAR_DEFAULT);
    private List<String> r2PenggunaAlatKeselamatan = SessionManager.loadListString(ISeasonConfig.KEY_UITEM_SAFETY_DRIVER_MOTOR_DEFAULT);

    private String simTidakTiketahui = "D0800";
    private String simTidakAda = "D0801";
    private List<String> valSim = Arrays.asList("D0800", "D0801");

    private String identityTypeSim = "G0403";
    private String identityTypeSimRusak = "G0404";
    private List<String> valIdenType = Arrays.asList("G0403", "G0404");

    private DatePickerDialog dpd;

    private boolean dpdSelector = false;
    private boolean isLoadedFromState = false;
    private String isMotorMobil = "other";
    private boolean isTnkbMandatoryEmpty = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.gandsoft.irsms.R.layout.activity_form_driver);

        Intent bundle = getIntent();
        if (bundle.hasExtra("Local_ID")) {
            localId = bundle.getIntExtra("Local_ID", 0);
        }

        if (bundle.hasExtra("Order_ID")) {
            orderId = bundle.getIntExtra("Order_ID", 0);
            isLoadedFromState = true;
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        initComponent();
        initContent();

        if (isLoadedFromState) {
            loadState();
        }
    }

    private void initComponent() {

        llDataKendaraanTitlefvbi = findViewById(R.id.llDataKendaraanTitle);

        cvDataKendaraanKeluarfvbi = findViewById(R.id.cvDataKendaraanKeluar);
        cvDataKendaraanSimpanfvbi = findViewById(R.id.cvDataKendaraanSimpan);

        /*kendaraan*/
        etDataKendaraanNoMesinfvbi = findViewById(R.id.etDataKendaraanNoMesin);
        etDataKendaraanVolMesinfvbi = findViewById(R.id.etDataKendaraanVolMesin);
        etDataKendaraanNoRangkafvbi = findViewById(R.id.etDataKendaraanNoRangka);
        etDataKendaraanOdometerfvbi = findViewById(R.id.etDataKendaraanOdometer);
        etDataKendaraanPanjangJarakRemfvbi = findViewById(R.id.etDataKendaraanPanjangJarakRem);
        etDataKendaraanSTNKNofvbi = findViewById(R.id.etDataKendaraanNoSTNK);
        etDataKendaraanSTNKJalanfvbi = findViewById(R.id.etDataKendaraanSTNKJalan);
        etDataKendaraanSTNKRtfvbi = findViewById(R.id.etDataKendaraanSTNKRt);
        etDataKendaraanSTNKRwfvbi = findViewById(R.id.etDataKendaraanSTNKRw);
        etDataKendaraanTNKBNofvbi = findViewById(R.id.etDataKendaraanTNKBNo);
        etDataKendaraanTotalPenumpangfvbi = findViewById(R.id.etDataKendaraanTotalPenumpang);
        //sp
        llDataKendaraanWarnaTNKBfvbi = findViewById(R.id.llDataKendaraanWarnaTNKB);
        llDataKendaraanJenisfvbi = findViewById(R.id.llDataKendaraanJenis);
        llDataKendaraanMerkfvbi = findViewById(R.id.llDataKendaraanMerk);
        llDataKendaraanMotorMerkfvbi = findViewById(R.id.llDataKendaraanMotorMerk);
        llDataKendaraanWarnafvbi = findViewById(R.id.llDataKendaraanWarna);
        //
        llDataKendaraanSTNKProvinsifvbi = findViewById(R.id.llDataKendaraanSTNKProvinsi);
        llDataKendaraanSTNKKotafvbi = findViewById(R.id.llDataKendaraanSTNKKota);
        llDataKendaraanSTNKKecamatanfvbi = findViewById(R.id.llDataKendaraanSTNKKecamatan);
        llDataKendaraanSTNKKelurahanfvbi = findViewById(R.id.llDataKendaraanSTNKKelurahan);
        /*rb*/
        llviewDataKendaraanPeruntukanfvbi = findViewById(R.id.llviewDataKendaraanPeruntukan);
        llviewDataKendaraanPergerakanSebelumTabrakanfvbi = findViewById(R.id.llviewDataKendaraanPergerakanSebelumTabrakan);
        llviewDataKendaraanPergerakanSesudahTabrakanfvbi = findViewById(R.id.llviewDataKendaraanPergerakanSesudahTabrakan);
        llviewDataKendaraanKedudukanAkhirKendaraanfvbi = findViewById(R.id.llviewDataKendaraanKedudukanAkhirKendaraan);
        llviewDataKendaraanKerusakanKhususfvbi = findViewById(R.id.llviewDataKendaraanKerusakanKhusus);
        llviewDataKendaraanTipePemudikfvbi = findViewById(R.id.llviewDataKendaraanTipePemudik);
        /*cb*/
        llviewDataKendaraanAlatKeselamatanfvbi = findViewById(R.id.llviewDataKendaraanAlatKeselamatan);
        llviewDataKendaraanKerusakanAwalfvbi = findViewById(R.id.llviewDataKendaraanKerusakanAwal);
        llviewDataKendaraanBarangSitafvbi = findViewById(R.id.llviewDataKendaraanBarangSita);

        etDataPengemudiNamaBelakangfvbi = findViewById(R.id.etDataPengemudiNamaBelakang);
        /*pengemudi*/
        ivDataPengemudiTanggalLahirfvbi = findViewById(R.id.ivDataPengemudiTanggalLahir);
        ivDataPengemudiTanggalLahirClearfvbi = findViewById(R.id.ivDataPengemudiTanggalLahirClear);
        ivDataPengemudiTanggalTerbitSIMfvbi = findViewById(R.id.ivDataPengemudiTanggalTerbitSIM);

        bDataPengemudiJenisPelanggaranfvbi = findViewById(R.id.bDataPengemudiJenisPelanggaran);
        bDataPengemudiPerilakuPengemudifvbi = findViewById(R.id.bDataPengemudiPerilakuPengemudi);
        tvDataPengemudiJenisPelanggaranfvbi = findViewById(R.id.tvDataPengemudiJenisPelanggaran);
        tvDataPengemudiPerilakuPengemudifvbi = findViewById(R.id.tvDataPengemudiPerilakuPengemudi);

        etDataPengemudiJalanfvbi = findViewById(R.id.etDataPengemudiJalan);
        etDataPengemudiNamaDepanfvbi = findViewById(R.id.etDataPengemudiNamaDepan);
        etDataPengemudiNoIdentitasfvbi = findViewById(R.id.etDataPengemudiNoIdentitas);
        etDataPengemudiRTfvbi = findViewById(R.id.etDataPengemudiRT);
        etDataPengemudiRWfvbi = findViewById(R.id.etDataPengemudiRW);
        etDataPengemudiUmurfvbi = findViewById(R.id.etDataPengemudiUmur);
        //sp
        llDataPengemudiAgamafvbi = findViewById(R.id.llDataPengemudiAgama);
        llDataPengemudiKebangsaanfvbi = findViewById(R.id.llDataPengemudiKebangsaan);
        llDataPengemudiPekerjaanfvbi = findViewById(R.id.llDataPengemudiPekerjaan);
        llDataPengemudiPendidikanfvbi = findViewById(R.id.llDataPengemudiPendidikan);
        llDataPengemudiGolonganSimfvbi = findViewById(R.id.llDataPengemudiGolonganSim);
        llDataPengemudiJenisIdentitasfvbi = findViewById(R.id.llDataPengemudiJenisIdentitas);
        spDataPengemudiJenisIdentitasfvbi = findViewById(R.id.spDataPengemudiJenisIdentitas);
        //
        llDataPengemudiAlamatKecamatanfvbi = findViewById(R.id.llDataPengemudiAlamatKecamatan);
        llDataPengemudiAlamatKelurahanfvbi = findViewById(R.id.llDataPengemudiAlamatKelurahan);
        llDataPengemudiAlamatKotaKabupatenfvbi = findViewById(R.id.llDataPengemudiAlamatKotaKabupaten);
        llDataPengemudiAlamatProvinsifvbi = findViewById(R.id.llDataPengemudiAlamatProvinsi);
        /*rb*/
        llviewDataPengemudiJenisKelaminfvbi = findViewById(R.id.llviewDataPengemudiJenisKelamin);
        llviewDataPengemudiTingkatLukafvbi = findViewById(R.id.llviewDataPengemudiTingkatLuka);
        llviewDataPengemudiDipengaruhiObatAlkoholfvbi = findViewById(R.id.llviewDataPengemudiDipengaruhiObatAlkohol);
        llviewDataPengemudiDiTahanfvbi = findViewById(R.id.llviewDataPengemudiDiTahan);
        llviewDataPengemudiPelakuSementarafvbi = findViewById(R.id.llviewDataPengemudiPelakuSementara);
        /*cb*/
        llviewDataPengemudiPenggunaanAlatKeselamatanfvbi = findViewById(R.id.llviewDataPengemudiPenggunaanAlatKeselamatan);

        llDataPengemudiTanggalTerbitSIM = findViewById(R.id.llDataPengemudiTanggalTerbitSIM);
        tvDataPengemudiTanggalTerbitSIM = findViewById(R.id.tvDataPengemudiTanggalTerbitSIM);
        llDataPengemudiTanggalLahir = findViewById(R.id.llDataPengemudiTanggalLahir);
        tvDataPengemudiTanggalLahir = findViewById(R.id.tvDataPengemudiTanggalLahir);

        cbrDataKendaraanTitikKerusakanfvbi = findViewById(R.id.cbrDataKendaraanTitikKerusakan);
        ivDataKendaraanTitikKerusakanfvbi = findViewById(R.id.ivDataKendaraanTitikKerusakan);
    }

    private void initContent() {

        if (isKitkat()) {
            llDataKendaraanTitlefvbi.bringToFront();
        }

        realm = Realm.getDefaultInstance();
        realm.refresh();

        models = realm.where(RequestRealmDbObject.class).sort("Local_ID").equalTo("Local_ID", localId).findFirst();

        String[] informasiKhusus = models.getSetAccidentRequestModel().get(0).getInformasi_khusus().split(",");
        List<String> ikList = Arrays.asList(informasiKhusus);
        String s = etDataKendaraanTNKBNofvbi.getText().toString();
        if (ikList.contains("A0601")) {
            isTnkbMandatoryEmpty = false;
        } else {
            isTnkbMandatoryEmpty = true;
        }

        cvDataKendaraanKeluarfvbi.setOnClickListener(v -> onBackPressed());
        cvDataKendaraanSimpanfvbi.setOnClickListener(v -> {
            saveOverwrite();
        });

        bDataPengemudiPerilakuPengemudifvbi.setOnClickListener(v -> {
            dialogCB(tvDataPengemudiPerilakuPengemudifvbi, getDriver_behavior(), sv1, true).show();
        });
        bDataPengemudiJenisPelanggaranfvbi.setOnClickListener(v -> {
            dialogCB(tvDataPengemudiJenisPelanggaranfvbi, getDriver_law(), sv2, false).show();
        });

        /*Kendaraan*/
        /*SP*/
        ViewGenerator.generateViewBasedOnModel(this, getPlate_color_id(), llDataKendaraanWarnaTNKBfvbi);
        ViewGenerator.generateViewBasedOnModel(this, getVehicle_type_id(), llDataKendaraanJenisfvbi);
        ViewGenerator.generateViewBasedOnModel(this, getVehicle_brand_id(), llDataKendaraanMerkfvbi);
        ViewGenerator.generateViewBasedOnModel(this, getVehicle_brand_motor_id(), llDataKendaraanMotorMerkfvbi);
        ViewGenerator.generateViewBasedOnModel(this, getVehicle_color_id(), llDataKendaraanWarnafvbi);
        /*RB*/
        ViewGenerator.generateViewBasedOnModel(this, getVehicle_design_id(), llviewDataKendaraanPeruntukanfvbi);
        ViewGenerator.generateViewBasedOnModel(this, getAccident_before_id(), llviewDataKendaraanPergerakanSebelumTabrakanfvbi);
        ViewGenerator.generateViewBasedOnModel(this, getAccident_after_id(), llviewDataKendaraanPergerakanSesudahTabrakanfvbi);
        ViewGenerator.generateViewBasedOnModel(this, getAccident_end_id(), llviewDataKendaraanKedudukanAkhirKendaraanfvbi);
        ViewGenerator.generateViewBasedOnModel(this, getSpecial_id(), llviewDataKendaraanKerusakanKhususfvbi);
        ViewGenerator.generateViewBasedOnModel(this, getPemudik(), llviewDataKendaraanTipePemudikfvbi);
        /*CB*/
        ViewGenerator.generateViewBasedOnModel(this, getSafety_device(), llviewDataKendaraanAlatKeselamatanfvbi);
        ViewGenerator.generateViewBasedOnModel(this, getKerusakan(), llviewDataKendaraanKerusakanAwalfvbi);
        ViewGenerator.generateViewBasedOnModel(this, getDisita(), llviewDataKendaraanBarangSitafvbi);

        ViewGenerator.generateViewBasedOnModel(this, getProvince(), llDataKendaraanSTNKProvinsifvbi);
        generateDynamicDBitems(0, ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanSTNKProvinsifvbi));
        ViewGenerator.generateViewBasedOnModel(this, getKabupaten(), llDataKendaraanSTNKKotafvbi);
        generateDynamicDBitems(1, ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanSTNKKotafvbi));
        ViewGenerator.generateViewBasedOnModel(this, getKecamatan(), llDataKendaraanSTNKKecamatanfvbi);
        generateDynamicDBitems(2, ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanSTNKKecamatanfvbi));
        ViewGenerator.generateViewBasedOnModel(this, getKelurahan(), llDataKendaraanSTNKKelurahanfvbi);
        ((Spinner) ((ViewGroup) llDataKendaraanSTNKProvinsifvbi.getChildAt(0)).getChildAt(1)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                generateDynamicDBitems(0, ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanSTNKProvinsifvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(llDataKendaraanSTNKKotafvbi).replaceModels(getKabupaten().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(llDataKendaraanSTNKKotafvbi);

                generateDynamicDBitems(1, ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanSTNKKotafvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(llDataKendaraanSTNKKecamatanfvbi).replaceModels(getKecamatan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(llDataKendaraanSTNKKecamatanfvbi);

                generateDynamicDBitems(2, ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanSTNKKecamatanfvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(llDataKendaraanSTNKKelurahanfvbi).replaceModels(getKelurahan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(llDataKendaraanSTNKKelurahanfvbi);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ((Spinner) ((ViewGroup) llDataKendaraanSTNKKotafvbi.getChildAt(0)).getChildAt(1)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                generateDynamicDBitems(1, ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanSTNKKotafvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(llDataKendaraanSTNKKecamatanfvbi).replaceModels(getKecamatan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(llDataKendaraanSTNKKecamatanfvbi);

                generateDynamicDBitems(2, ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanSTNKKecamatanfvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(llDataKendaraanSTNKKelurahanfvbi).replaceModels(getKelurahan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(llDataKendaraanSTNKKelurahanfvbi);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ((Spinner) ((ViewGroup) llDataKendaraanSTNKKecamatanfvbi.getChildAt(0)).getChildAt(1)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                generateDynamicDBitems(2, ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanSTNKKecamatanfvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(llDataKendaraanSTNKKelurahanfvbi).replaceModels(getKelurahan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(llDataKendaraanSTNKKelurahanfvbi);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*Pengemudi*/
        /*SP*/
        ViewGenerator.generateViewBasedOnModel(this, getReligion_id(), llDataPengemudiAgamafvbi);
        ViewGenerator.generateViewBasedOnModel(this, getLicense_class_id(), llDataPengemudiGolonganSimfvbi);
        ViewGenerator.generateViewBasedOnModel(this, getNationality_id(), llDataPengemudiKebangsaanfvbi);
        ViewGenerator.generateViewBasedOnModel(this, getProfession_id(), llDataPengemudiPekerjaanfvbi);
        ViewGenerator.generateViewBasedOnModel(this, getEducation_id(), llDataPengemudiPendidikanfvbi);
        ViewGeneratorSpinnerAdapter adapterIdentity = new ViewGeneratorSpinnerAdapter(this, getIdentity_type_id().getViewModelGroup()) {
            @Override
            public boolean isEnabled(int position) {
                int posSim = ViewGeneratorUtil.getSelectedSpinnerIndex(llDataPengemudiGolonganSimfvbi);
                //return posSim > 1 || position < 3);
                return !getPosListStringIn2List(getId_license_class_id_lv(), valSim).contains(posSim) || !getPosListStringIn2List(getId_identity_type_id_lv(), valIdenType).contains(position);
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                ViewModel model = getIdentity_type_id().getViewModelGroup().get(position);
                LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(model.getItemResourceID(), null);
                TextView tv = (TextView) ((ViewGroup) convertView).getChildAt(1);
                tv.setText(model.getTextContent());
                int posSim = ViewGeneratorUtil.getSelectedSpinnerIndex(llDataPengemudiGolonganSimfvbi);

                if (!getPosListStringIn2List(getId_license_class_id_lv(), valSim).contains(posSim) || !getPosListStringIn2List(getId_identity_type_id_lv(), valIdenType).contains(position)) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.BLACK);
                } else {
                    tv.setTextColor(Color.GRAY);
                }
                return convertView;
            }
        };
        spDataPengemudiJenisIdentitasfvbi.setAdapter(adapterIdentity);
        /*RB*/
        ViewGenerator.generateViewBasedOnModel(this, getGender_id(), llviewDataPengemudiJenisKelaminfvbi);
        ViewGenerator.generateViewBasedOnModel(this, getAlcohol_id(), llviewDataPengemudiDipengaruhiObatAlkoholfvbi);
        ViewGenerator.generateViewBasedOnModel(this, getAlcohol_id(), llviewDataPengemudiDiTahanfvbi);
        ViewGenerator.generateViewBasedOnModel(this, getAlcohol_id(), llviewDataPengemudiPelakuSementarafvbi);

        View tot = LayoutInflater.from(this).inflate(R.layout.widget_item_injury, null, false);
        llviewDataPengemudiTingkatLukafvbi.addView(tot);
        llviewDataPengemudiTingkatLukaRadioGroup = llviewDataPengemudiTingkatLukafvbi.findViewById(R.id.kucing);
        llviewDataPengemudiTingkatLukaRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.meong4) {
                    group.findViewById(R.id.hundx).setVisibility(View.VISIBLE);
                } else {
                    group.findViewById(R.id.hundx).setVisibility(View.GONE);
                }
            }
        });
        llviewDataPengemudiDeadSpot = llviewDataPengemudiTingkatLukafvbi.findViewById(R.id.lukasub);
        /*CB*/
        ViewGenerator.generateViewBasedOnModel(this, getSafety(), llviewDataPengemudiPenggunaanAlatKeselamatanfvbi);

        ViewGenerator.generateViewBasedOnModel(this, getProvince(), llDataPengemudiAlamatProvinsifvbi);
        generateDynamicDBitems(0, ViewGeneratorUtil.getSelectedSpinnerValue(llDataPengemudiAlamatProvinsifvbi));
        ViewGenerator.generateViewBasedOnModel(this, getKabupaten(), llDataPengemudiAlamatKotaKabupatenfvbi);
        generateDynamicDBitems(1, ViewGeneratorUtil.getSelectedSpinnerValue(llDataPengemudiAlamatKotaKabupatenfvbi));
        ViewGenerator.generateViewBasedOnModel(this, getKecamatan(), llDataPengemudiAlamatKecamatanfvbi);
        generateDynamicDBitems(2, ViewGeneratorUtil.getSelectedSpinnerValue(llDataPengemudiAlamatKecamatanfvbi));
        ViewGenerator.generateViewBasedOnModel(this, getKelurahan(), llDataPengemudiAlamatKelurahanfvbi);
        ((Spinner) ((ViewGroup) llDataPengemudiAlamatProvinsifvbi.getChildAt(0)).getChildAt(1)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                generateDynamicDBitems(0, ViewGeneratorUtil.getSelectedSpinnerValue(llDataPengemudiAlamatProvinsifvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(llDataPengemudiAlamatKotaKabupatenfvbi).replaceModels(getKabupaten().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(llDataPengemudiAlamatKotaKabupatenfvbi);

                generateDynamicDBitems(1, ViewGeneratorUtil.getSelectedSpinnerValue(llDataPengemudiAlamatKotaKabupatenfvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(llDataPengemudiAlamatKecamatanfvbi).replaceModels(getKecamatan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(llDataPengemudiAlamatKecamatanfvbi);

                generateDynamicDBitems(2, ViewGeneratorUtil.getSelectedSpinnerValue(llDataPengemudiAlamatKecamatanfvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(llDataPengemudiAlamatKelurahanfvbi).replaceModels(getKelurahan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(llDataPengemudiAlamatKelurahanfvbi);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ((Spinner) ((ViewGroup) llDataPengemudiAlamatKotaKabupatenfvbi.getChildAt(0)).getChildAt(1)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                generateDynamicDBitems(1, ViewGeneratorUtil.getSelectedSpinnerValue(llDataPengemudiAlamatKotaKabupatenfvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(llDataPengemudiAlamatKecamatanfvbi).replaceModels(getKecamatan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(llDataPengemudiAlamatKecamatanfvbi);

                generateDynamicDBitems(2, ViewGeneratorUtil.getSelectedSpinnerValue(llDataPengemudiAlamatKecamatanfvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(llDataPengemudiAlamatKelurahanfvbi).replaceModels(getKelurahan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(llDataPengemudiAlamatKelurahanfvbi);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ((Spinner) ((ViewGroup) llDataPengemudiAlamatKecamatanfvbi.getChildAt(0)).getChildAt(1)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                generateDynamicDBitems(2, ViewGeneratorUtil.getSelectedSpinnerValue(llDataPengemudiAlamatKecamatanfvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(llDataPengemudiAlamatKelurahanfvbi).replaceModels(getKelurahan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(llDataPengemudiAlamatKelurahanfvbi);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Calendar now = Calendar.getInstance();
        dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setAccentColor(ContextCompat.getColor(this, R.color.colorGreenButton));

        ivDataPengemudiTanggalLahirfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dpd = DatePickerDialog.newInstance(AddDriverActivity.this::onDateSet, now.get(Calendar.YEAR) - 17, now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
                dpd.setAccentColor(ContextCompat.getColor(AddDriverActivity.this, R.color.colorGreenButton));
                dpd.show(getFragmentManager(), "dpdborn");
                dpd.setAccentColor(ContextCompat.getColor(AddDriverActivity.this, R.color.colorGreenButton));
                dpdSelector = false;
            }
        });

        ivDataPengemudiTanggalTerbitSIMfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dpd = DatePickerDialog.newInstance(AddDriverActivity.this::onDateSet, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
                dpd.setAccentColor(ContextCompat.getColor(AddDriverActivity.this, R.color.colorGreenButton));
                dpd.show(getFragmentManager(), "dpdsim");
                dpd.setAccentColor(ContextCompat.getColor(AddDriverActivity.this, R.color.colorGreenButton));
                dpdSelector = true;
            }
        });
        ivDataPengemudiTanggalLahirClearfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivDataPengemudiTanggalLahirClearfvbi.setVisibility(View.GONE);
                tvDataPengemudiTanggalLahir.setText("");
                etDataPengemudiUmurfvbi.setEnabled(true);
                etDataPengemudiUmurfvbi.setText("");
            }
        });


        cbrDataKendaraanTitikKerusakanfvbi.addChildRefID("V0702");
        cbrDataKendaraanTitikKerusakanfvbi.addChildRefID("V0703");
        cbrDataKendaraanTitikKerusakanfvbi.addChildRefID("V0704");
        cbrDataKendaraanTitikKerusakanfvbi.addChildRefID("V0701");
        cbrDataKendaraanTitikKerusakanfvbi.addChildRefID("V0709");
        cbrDataKendaraanTitikKerusakanfvbi.addChildRefID("V0705");
        cbrDataKendaraanTitikKerusakanfvbi.addChildRefID("V0708");
        cbrDataKendaraanTitikKerusakanfvbi.addChildRefID("V0707");
        cbrDataKendaraanTitikKerusakanfvbi.addChildRefID("V0706");

        ArrayList<String> idSafetyDeviceLv = getId_safety_device_lv();
        ArrayList<String> idSafetyLv = getId_safety_lv();
        ViewGeneratorUtil.selectSpinnerView(llDataKendaraanJenisfvbi).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("NewApi")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (isListContainString(typeVehicleR4, ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanJenisfvbi))) {
                    if (isKitkat()) {
                        ivDataKendaraanTitikKerusakanfvbi.setImageResource(R.drawable.img_car_top);
                    } else {
                        ivDataKendaraanTitikKerusakanfvbi.setImageDrawable(getDrawable(R.drawable.img_car_top));
                    }
                    cbrDataKendaraanTitikKerusakanfvbi.setVisibility(View.VISIBLE);
                    llviewDataKendaraanAlatKeselamatanfvbi.setVisibility(View.VISIBLE);
                    llviewDataPengemudiPenggunaanAlatKeselamatanfvbi.setVisibility(View.VISIBLE);

                    //ViewGeneratorUtil.setCheckboxUnCheckedAt(llviewDataKendaraanAlatKeselamatanfvbi, r2AlatKeselamatan);
                    ViewGeneratorUtil.setCheckboxEnabledAt(llviewDataKendaraanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getAllSizeStringList(idSafetyDeviceLv)));

                    ViewGeneratorUtil.setCheckboxUnCheckedAt(llviewDataPengemudiPenggunaanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getPosListStringIn2List(idSafetyLv, r2PenggunaAlatKeselamatan)));
                    ViewGeneratorUtil.setCheckboxDisabledAt(llviewDataPengemudiPenggunaanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getPosListStringIn2List(idSafetyLv, r2PenggunaAlatKeselamatan)));
                    ViewGeneratorUtil.setCheckboxEnabledAt(llviewDataPengemudiPenggunaanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getPosListStringIn2List(idSafetyLv, r4PenggunaAlatKeselamatan)));

                    llDataKendaraanMerkfvbi.setVisibility(View.VISIBLE);
                    llDataKendaraanMotorMerkfvbi.setVisibility(View.GONE);
                    isMotorMobil = "mobil";
                } else if (isListContainString(typeVehicleR2, ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanJenisfvbi))) {
                    if (isKitkat()) {
                        ivDataKendaraanTitikKerusakanfvbi.setImageResource(R.drawable.img_motorcycle_top);
                    } else {
                        ivDataKendaraanTitikKerusakanfvbi.setImageDrawable(getDrawable(R.drawable.img_motorcycle_top));
                    }
                    cbrDataKendaraanTitikKerusakanfvbi.setVisibility(View.VISIBLE);
                    llDataKendaraanMerkfvbi.setVisibility(View.GONE);
                    llDataKendaraanMotorMerkfvbi.setVisibility(View.VISIBLE);
                    /*if (ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanJenisfvbi).equalsIgnoreCase("V02A14")) {
                        cbrDataKendaraanTitikKerusakanfvbi.setVisibility(View.INVISIBLE);
                        llDataKendaraanMerkfvbi.setVisibility(View.GONE);
                        llDataKendaraanMotorMerkfvbi.setVisibility(View.INVISIBLE);
                    } else {
                        cbrDataKendaraanTitikKerusakanfvbi.setVisibility(View.VISIBLE);
                        llDataKendaraanMerkfvbi.setVisibility(View.GONE);
                        llDataKendaraanMotorMerkfvbi.setVisibility(View.VISIBLE);
                    }*/
                    llviewDataKendaraanAlatKeselamatanfvbi.setVisibility(View.VISIBLE);
                    llviewDataPengemudiPenggunaanAlatKeselamatanfvbi.setVisibility(View.VISIBLE);

                    ViewGeneratorUtil.setCheckboxUnCheckedAt(llviewDataKendaraanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getInvPosListStringIn2List(idSafetyDeviceLv, r2AlatKeselamatan)));
                    ViewGeneratorUtil.setCheckboxDisabledAt(llviewDataKendaraanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getInvPosListStringIn2List(idSafetyDeviceLv, r2AlatKeselamatan)));
                    ViewGeneratorUtil.setCheckboxEnabledAt(llviewDataKendaraanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getPosListStringIn2List(idSafetyDeviceLv, r2AlatKeselamatan)));

                    ViewGeneratorUtil.setCheckboxUnCheckedAt(llviewDataPengemudiPenggunaanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getPosListStringIn2List(idSafetyLv, r4PenggunaAlatKeselamatan)));
                    ViewGeneratorUtil.setCheckboxDisabledAt(llviewDataPengemudiPenggunaanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getPosListStringIn2List(idSafetyLv, r4PenggunaAlatKeselamatan)));
                    ViewGeneratorUtil.setCheckboxEnabledAt(llviewDataPengemudiPenggunaanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getPosListStringIn2List(idSafetyLv, r2PenggunaAlatKeselamatan)));

                    isMotorMobil = "motor";
                } else {
                    cbrDataKendaraanTitikKerusakanfvbi.setVisibility(View.INVISIBLE);
                    llviewDataKendaraanAlatKeselamatanfvbi.setVisibility(View.INVISIBLE);
                    llviewDataPengemudiPenggunaanAlatKeselamatanfvbi.setVisibility(View.INVISIBLE);

                    ViewGeneratorUtil.setCheckboxUnCheckedAt(llviewDataKendaraanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getAllSizeStringList(idSafetyDeviceLv)));
                    ViewGeneratorUtil.setCheckboxEnabledAt(llviewDataKendaraanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getAllSizeStringList(idSafetyDeviceLv)));

                    ViewGeneratorUtil.setCheckboxUnCheckedAt(llviewDataPengemudiPenggunaanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getAllSizeStringList(idSafetyLv)));
                    ViewGeneratorUtil.setCheckboxEnabledAt(llviewDataPengemudiPenggunaanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getAllSizeStringList(idSafetyLv)));

                    llDataKendaraanMerkfvbi.setVisibility(View.GONE);
                    llDataKendaraanMotorMerkfvbi.setVisibility(View.GONE);
                    isMotorMobil = "other";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ViewGeneratorUtil.selectSpinnerView(llDataPengemudiGolonganSimfvbi).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int posIdentity = spDataPengemudiJenisIdentitasfvbi.getSelectedItemPosition();
                if (getPosListStringIn2List(getId_license_class_id_lv(), valSim).contains(position)) {
                    tvDataPengemudiTanggalTerbitSIM.setText("");
                    llDataPengemudiTanggalTerbitSIM.setVisibility(View.INVISIBLE);
                    if (getPosListStringIn2List(getId_identity_type_id_lv(), valIdenType).contains(posIdentity)) {
                        spDataPengemudiJenisIdentitasfvbi.setSelection(0, true);
                    }
                } else {
                    spDataPengemudiJenisIdentitasfvbi.setSelection(getPosStringInList(getId_identity_type_id_lv(), identityTypeSim), true);
                    llDataPengemudiTanggalTerbitSIM.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spDataPengemudiJenisIdentitasfvbi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //SystemUtil.validationIdentityByType(etDataPengemudiNoIdentitasfvbi, position);
                //validationIdentityByType(etDataPengemudiNoIdentitasfvbi, ((ViewGeneratorSpinnerAdapter) spDataPengemudiJenisIdentitasfvbi.getAdapter()).getModelViewID(spDataPengemudiJenisIdentitasfvbi.getSelectedItemPosition()));
                SystemUtil.validationIdentityByType(etDataPengemudiNoIdentitasfvbi, ((ViewGeneratorSpinnerAdapter) spDataPengemudiJenisIdentitasfvbi.getAdapter()).getModelViewID(spDataPengemudiJenisIdentitasfvbi.getSelectedItemPosition()), FormdataLoader.getId_identity_type_id_lv(), FormdataLoader.getId_identity_type_max_lenght_lv());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etDataKendaraanPanjangJarakRemfvbi.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = etDataKendaraanPanjangJarakRemfvbi.getText().toString();
                if (!string.matches("")) {
                    int i = Integer.valueOf(string);
                    if (i > 100 && !string.isEmpty()) {
                        etDataKendaraanPanjangJarakRemfvbi.setText("100");
                        etDataKendaraanPanjangJarakRemfvbi.setError("Max: 100 Meters");
                    }
                } else {

                }
            }
        });
        etDataKendaraanTNKBNofvbi.setFilters(new InputFilter[]{new InputFilter.LengthFilter(12), new InputFilter.AllCaps()});
        etDataPengemudiJalanfvbi.setFilters(new InputFilter[]{new InputFilter.LengthFilter(255)});
        etDataKendaraanSTNKJalanfvbi.setFilters(new InputFilter[]{new InputFilter.LengthFilter(255)});
        etDataPengemudiUmurfvbi.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String sLahir = tvDataPengemudiTanggalLahir.getText().toString();
                String sUmur = etDataPengemudiUmurfvbi.getText().toString();
                if (sUmur.isEmpty()) {
                    llDataPengemudiTanggalLahir.setVisibility(View.VISIBLE);
                } else {
                    if (sLahir.isEmpty()) {
                        llDataPengemudiTanggalLahir.setVisibility(View.GONE);
                    } else {
                        llDataPengemudiTanggalLahir.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

    }

    private void loadState() {
        realm = Realm.getDefaultInstance();
        AddDriverStateModel vehicleStateModel = realm.where(AddDriverStateModel.class).equalTo("Local_ID", localId).equalTo("vehicle_Dbid", orderId).findFirst();

        checkData(vehicleStateModel);

        //kendaraan
        /*//sp
        ViewGeneratorUtil.selectSpinnerDropdownAt(llDataKendaraanWarnaTNKBfvbi, vehicleStateModel.getPlate_color_id());
        vehicleTypeIdSpinnerHandler(vehicleStateModel);
        ViewGeneratorUtil.selectSpinnerDropdownAt(llDataKendaraanWarnafvbi, vehicleStateModel.getVehicle_color_id());

        //rb
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataKendaraanPeruntukanfvbi, vehicleStateModel.getVehicle_design_id());
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataKendaraanPergerakanSebelumTabrakanfvbi, vehicleStateModel.getAccident_before_id());
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataKendaraanPergerakanSesudahTabrakanfvbi, vehicleStateModel.getAccident_after_id());
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataKendaraanKedudukanAkhirKendaraanfvbi, vehicleStateModel.getAccident_end_id());
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataKendaraanKerusakanKhususfvbi, vehicleStateModel.getSpecial_damage_id());
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataKendaraanTipePemudikfvbi, vehicleStateModel.getPemudik());

        //cb
        ViewGeneratorUtil.setCheckboxCheckedAt(llviewDataKendaraanKerusakanAwalfvbi, vehicleStateModel.getKerusakan());
        ViewGeneratorUtil.setCheckboxCheckedAt(llviewDataKendaraanBarangSitafvbi, vehicleStateModel.getDisita());*/

        /*ET*/
        etDataKendaraanNoMesinfvbi.setText(vehicleStateModel.getEngine_no());
        etDataKendaraanVolMesinfvbi.setText(vehicleStateModel.getEngine_capacity());
        etDataKendaraanNoRangkafvbi.setText(vehicleStateModel.getFrame_no());
        etDataKendaraanOdometerfvbi.setText(vehicleStateModel.getOdometer());
        etDataKendaraanPanjangJarakRemfvbi.setText(vehicleStateModel.getBrake_footprint());
        etDataKendaraanSTNKNofvbi.setText(vehicleStateModel.getRoad_number_stnk());
        etDataKendaraanSTNKJalanfvbi.setText(vehicleStateModel.getRoad_name_stnk());
        etDataKendaraanSTNKRtfvbi.setText(vehicleStateModel.getRt_stnk());
        etDataKendaraanSTNKRwfvbi.setText(vehicleStateModel.getRw_stnk());
        etDataKendaraanTNKBNofvbi.setText(vehicleStateModel.getPlate_no());
        etDataKendaraanTotalPenumpangfvbi.setText(vehicleStateModel.getTotal_penumpang());

        vehicleStnkSpinnerHandler(vehicleStateModel);

        //Pengmudi
        /*//sp
        ViewGeneratorUtil.selectSpinnerDropdownAt(llDataPengemudiAgamafvbi, vehicleStateModel.getReligion_id());
        ViewGeneratorUtil.selectSpinnerDropdownAt(llDataPengemudiGolonganSimfvbi, vehicleStateModel.getLicense_class_id());
        ViewGeneratorUtil.selectSpinnerDropdownAt(llDataPengemudiKebangsaanfvbi, vehicleStateModel.getNationality_id());
        ViewGeneratorUtil.selectSpinnerDropdownAt(llDataPengemudiPekerjaanfvbi, vehicleStateModel.getProfession_id());
        ViewGeneratorUtil.selectSpinnerDropdownAt(llDataPengemudiPendidikanfvbi, vehicleStateModel.getEducation_id());
        driverIdentityTypeSpinnerHandler(vehicleStateModel);


        //rb
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataPengemudiJenisKelaminfvbi, vehicleStateModel.getGender_id());
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataPengemudiTingkatLukafvbi, vehicleStateModel.getInjury_id());
        if (vehicleStateModel.getDead_spot_id() >= 0) {
            ((ToggleRadioButton) llviewDataPengemudiDeadSpot.getChildAt(vehicleStateModel.getDead_spot_id())).setChecked(true);
        }
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataPengemudiDipengaruhiObatAlkoholfvbi, vehicleStateModel.getAlcohol_id());
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataPengemudiDiTahanfvbi, vehicleStateModel.getArrested_id());
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataPengemudiPelakuSementarafvbi, vehicleStateModel.getSuspected_id());
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataPengemudiTingkatLukafvbi, vehicleStateModel.getInjury_id());*/

        //cb
        /*sv1 = vehicleStateModel.getDriver_behavior_asArray();
        Log.d("Lihat loadState AddDriverActivity", String.valueOf(sv1));
        bDataPengemudiPerilakuPengemudifvbi.setOnClickListener(v -> {
            dialogCB(tvDataPengemudiPerilakuPengemudifvbi, FormdataLoader.getDriver_behavior(), sv1, true).show();
        });
        sv2 = vehicleStateModel.getDriver_law_asArray();
        Log.d("Lihat loadState AddDriverActivity", String.valueOf(sv2));
        bDataPengemudiJenisPelanggaranfvbi.setOnClickListener(v -> {
            dialogCB(tvDataPengemudiJenisPelanggaranfvbi, FormdataLoader.getDriver_law(), sv2, false).show();
        });*/

        /*ET*/
        etDataPengemudiJalanfvbi.setText(vehicleStateModel.getRoad_name());
        etDataPengemudiNamaDepanfvbi.setText(vehicleStateModel.getFirst_name());
        etDataPengemudiNamaBelakangfvbi.setText(vehicleStateModel.getLast_name());
        etDataPengemudiRTfvbi.setText(vehicleStateModel.getRt());
        etDataPengemudiRWfvbi.setText(vehicleStateModel.getRw());

        tvDataPengemudiTanggalLahir.setText(vehicleStateModel.getBirth_date());
        tvDataPengemudiTanggalTerbitSIM.setText(vehicleStateModel.getLicense_pub_date());

        /*bDataPengemudiPerilakuPengemudifvbi.setOnClickListener(null);
        tvDataPengemudiPerilakuPengemudifvbi.setText(vehicleStateModel.getDehavior_text());
        bDataPengemudiJenisPelanggaranfvbi.setOnClickListener(null);
        tvDataPengemudiJenisPelanggaranfvbi.setText(vehicleStateModel.getDaw_text());*/

        vehicleDriverSpinnerHandler(vehicleStateModel);
        vehicleDateAgeSpinnerHandler(vehicleStateModel);

        if (!realm.isClosed()) {
            realm.close();
        }
    }

    private DriverDBModel generateRequestModel(@Nullable DriverDBModel driverDBModel) {
        if (driverDBModel == null) {
            driverDBModel = new DriverDBModel();
            driverDBModel.setLocal_id(localId);
            int dataCount = realm.where(AddDriverStateModel.class).equalTo("Local_ID", localId).findAll().size();
            if (dataCount > 0) {
                driverDBModel.setVehicle_Dbid(realm.where(AddDriverStateModel.class).equalTo("Local_ID", localId).findAll().get(dataCount - 1).getVehicle_Dbid() + 1);
            } else {
                driverDBModel.setVehicle_Dbid(1);
            }
        }

        driverDBModel.setPlate_no(etDataKendaraanTNKBNofvbi.getText().toString());

        /*kendaraan*/
        /*SP*/
        driverDBModel.setPlate_color_id(ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanWarnaTNKBfvbi));
        driverDBModel.setPropinsi_stnk(ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanSTNKProvinsifvbi));
        driverDBModel.setKabupaten_stnk(ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanSTNKKotafvbi));
        driverDBModel.setKecamatan_stnk(ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanSTNKKecamatanfvbi));
        driverDBModel.setKelurahan_stnk(ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanSTNKKelurahanfvbi));

        if (isMotorMobil.equalsIgnoreCase("motor")) {
            driverDBModel.setVehicle_brand_id(ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanMotorMerkfvbi));
            driverDBModel.setVehicle_brand_text(ViewGeneratorUtil.getSelectedSpinnerTextValue(llDataKendaraanMotorMerkfvbi));
        } else if (isMotorMobil.equalsIgnoreCase("mobil")) {
            driverDBModel.setVehicle_brand_id(ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanMerkfvbi));
            driverDBModel.setVehicle_brand_text(ViewGeneratorUtil.getSelectedSpinnerTextValue(llDataKendaraanMerkfvbi));
        } else {
            driverDBModel.setVehicle_brand_id("");
            driverDBModel.setVehicle_brand_text("");
        }

        driverDBModel.setVehicle_type_id(ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanJenisfvbi));
        driverDBModel.setVehicle_color_id(ViewGeneratorUtil.getSelectedSpinnerValue(llDataKendaraanWarnafvbi));

        /*ET*/
        driverDBModel.setEngine_no(etDataKendaraanNoMesinfvbi.getText().toString());
        driverDBModel.setEngine_capacity(etDataKendaraanVolMesinfvbi.getText().toString());
        driverDBModel.setFrame_no(etDataKendaraanNoRangkafvbi.getText().toString());
        driverDBModel.setOdometer(etDataKendaraanOdometerfvbi.getText().toString());
        driverDBModel.setBrake_footprint(etDataKendaraanPanjangJarakRemfvbi.getText().toString());
        driverDBModel.setRoad_number_stnk(etDataKendaraanSTNKNofvbi.getText().toString() + "");
        driverDBModel.setRoad_name_stnk(etDataKendaraanSTNKJalanfvbi.getText().toString());
        driverDBModel.setRt_stnk(etDataKendaraanSTNKRtfvbi.getText().toString());
        driverDBModel.setRw_stnk(etDataKendaraanSTNKRwfvbi.getText().toString());
        driverDBModel.setPlate_no(etDataKendaraanTNKBNofvbi.getText().toString());
        driverDBModel.setTotal_penumpang(etDataKendaraanTotalPenumpangfvbi.getText().toString());

        /*RB*/
        driverDBModel.setVehicle_design_id(ViewGeneratorUtil.getSelectedRadioValue(llviewDataKendaraanPeruntukanfvbi));
        driverDBModel.setAccident_before_id(ViewGeneratorUtil.getSelectedRadioValue(llviewDataKendaraanPergerakanSebelumTabrakanfvbi));
        driverDBModel.setAccident_after_id(ViewGeneratorUtil.getSelectedRadioValue(llviewDataKendaraanPergerakanSesudahTabrakanfvbi));
        driverDBModel.setAccident_end_id(ViewGeneratorUtil.getSelectedRadioValue(llviewDataKendaraanKedudukanAkhirKendaraanfvbi));
        driverDBModel.setSpecial_damage_id(ViewGeneratorUtil.getSelectedRadioValue(llviewDataKendaraanKerusakanKhususfvbi));
        driverDBModel.setPemudik(ViewGeneratorUtil.getSelectedRadioValue(llviewDataKendaraanTipePemudikfvbi));

        /*CB*/
        driverDBModel.setSafety_device(ViewGeneratorUtil.getSelectedCheckboxValue(llviewDataKendaraanAlatKeselamatanfvbi));
        driverDBModel.setKerusakan(ViewGeneratorUtil.getSelectedCheckboxValue(llviewDataKendaraanKerusakanAwalfvbi));
        driverDBModel.setDisita(ViewGeneratorUtil.getSelectedCheckboxValue(llviewDataKendaraanBarangSitafvbi));

        /*pengemudiRB*/
        /*SP*/
        driverDBModel.setReligion_id(ViewGeneratorUtil.getSelectedSpinnerValue(llDataPengemudiAgamafvbi));
        driverDBModel.setLicense_class_id(ViewGeneratorUtil.getSelectedSpinnerValue(llDataPengemudiGolonganSimfvbi));
        driverDBModel.setNationality_id(ViewGeneratorUtil.getSelectedSpinnerValue(llDataPengemudiKebangsaanfvbi));
        driverDBModel.setIdentity_type_id(((ViewGeneratorSpinnerAdapter) spDataPengemudiJenisIdentitasfvbi.getAdapter()).getModelViewID(spDataPengemudiJenisIdentitasfvbi.getSelectedItemPosition()));
        driverDBModel.setPropinsi(ViewGeneratorUtil.getSelectedSpinnerValue(llDataPengemudiAlamatProvinsifvbi));
        driverDBModel.setKabupaten(ViewGeneratorUtil.getSelectedSpinnerValue(llDataPengemudiAlamatKotaKabupatenfvbi));
        driverDBModel.setKecamatan(ViewGeneratorUtil.getSelectedSpinnerValue(llDataPengemudiAlamatKecamatanfvbi));
        driverDBModel.setKelurahan(ViewGeneratorUtil.getSelectedSpinnerValue(llDataPengemudiAlamatKelurahanfvbi));
        driverDBModel.setProfession_id(ViewGeneratorUtil.getSelectedSpinnerValue(llDataPengemudiPekerjaanfvbi));
        driverDBModel.setEducation_id(ViewGeneratorUtil.getSelectedSpinnerValue(llDataPengemudiPendidikanfvbi));

        /*ET*/
        driverDBModel.setRoad_name(etDataPengemudiJalanfvbi.getText().toString());
        driverDBModel.setFirst_name(etDataPengemudiNamaDepanfvbi.getText().toString());
        driverDBModel.setLast_name(etDataPengemudiNamaBelakangfvbi.getText().toString());
        driverDBModel.setIdentity_no(etDataPengemudiNoIdentitasfvbi.getText().toString());
        driverDBModel.setRt(etDataPengemudiRTfvbi.getText().toString());
        driverDBModel.setRw(etDataPengemudiRWfvbi.getText().toString());
        driverDBModel.setAge(etDataPengemudiUmurfvbi.getText().toString());

        /*RB*/
        driverDBModel.setGender_id(ViewGeneratorUtil.getSelectedRadioValue(llviewDataPengemudiJenisKelaminfvbi));
        driverDBModel.setAlcohol_id(ViewGeneratorUtil.getSelectedRadioValue(llviewDataPengemudiDipengaruhiObatAlkoholfvbi));
        driverDBModel.setArrested_id(ViewGeneratorUtil.getSelectedRadioValue(llviewDataPengemudiDiTahanfvbi));
        driverDBModel.setSuspected_id(ViewGeneratorUtil.getSelectedRadioValue(llviewDataPengemudiPelakuSementarafvbi));
        driverDBModel.setInjury_id(ViewGeneratorUtil.getSelectedRadioValue(llviewDataPengemudiTingkatLukafvbi));

        try {
            driverDBModel.setDead_spot_id((llviewDataPengemudiDeadSpot.findViewById(llviewDataPengemudiDeadSpot.getCheckedRadioButtonId())).getTag().toString());
            Log.d("Lihat generateRequestModel AddDriverActivity", (llviewDataPengemudiDeadSpot.findViewById(llviewDataPengemudiDeadSpot.getCheckedRadioButtonId())).getTag().toString());
            driverDBModel.setDead_spot_id_text((llviewDataPengemudiDeadSpot.findViewById(llviewDataPengemudiDeadSpot.getCheckedRadioButtonId())).getTag().toString());
        } catch (Exception e) {
            //Log.e("DeadSpot: ", e.getMessage());
            driverDBModel.setDead_spot_id("");
        }

        /*CB*/
        driverDBModel.setSafety(ViewGeneratorUtil.getSelectedCheckboxValue(llviewDataPengemudiPenggunaanAlatKeselamatanfvbi));
        driverDBModel.setTitik_kerusakan(cbrDataKendaraanTitikKerusakanfvbi.getSelectedCheckbox());
        //drvModel.setVehicle_brand_text(ViewGeneratorUtil.getSelectedSpinnerTextValue(llDataKendaraanMerkfvbi));
        //drvModel.setVehicle_brand_text(ViewGeneratorUtil.getSelectedSpinnerTextValue(llDataKendaraanMerkfvbi));
        driverDBModel.setVehicle_type_text(ViewGeneratorUtil.getSelectedSpinnerTextValue(llDataKendaraanJenisfvbi));
        driverDBModel.setInjury_id_text(ViewGeneratorUtil.getSelectedRadioTextValue(llviewDataPengemudiTingkatLukafvbi));

        if (tvDataPengemudiTanggalLahir.getTag() != null) {
            driverDBModel.setBirth_date(tvDataPengemudiTanggalLahir.getTag().toString());
        }

        if (tvDataPengemudiTanggalTerbitSIM.getTag() != null) {
            driverDBModel.setLicense_pub_date(tvDataPengemudiTanggalTerbitSIM.getTag().toString());
        }

        driverDBModel.setVehicle_color_text(ViewGeneratorUtil.getSelectedSpinnerTextValue(llDataKendaraanWarnafvbi));
        try {
            driverDBModel.setDriver_law((String) tvDataPengemudiJenisPelanggaranfvbi.getTag());

        } catch (Exception e) {
            Log.e("", "drvstmdl: " + e.getMessage());
        }

        try {
            driverDBModel.setDriver_behavior((String) tvDataPengemudiPerilakuPengemudifvbi.getTag());

        } catch (Exception e) {
            Log.e("", "drvtbhvr: " + e.getMessage());
        }

        return driverDBModel;
    }

    private AddDriverStateModel generateStateModel(@Nullable AddDriverStateModel driverStateModel) {
        if (driverStateModel == null) {
            driverStateModel = new AddDriverStateModel();
            driverStateModel.setLocal_id(localId);
            int dataCount = realm.where(AddDriverStateModel.class).equalTo("Local_ID", localId).findAll().size();
            if (dataCount > 0) {
                driverStateModel.setVehicle_Dbid(realm.where(AddDriverStateModel.class).equalTo("Local_ID", localId).findAll().get(dataCount - 1).getVehicle_Dbid() + 1);
            } else {
                driverStateModel.setVehicle_Dbid(1);
            }
        }

        /*kendaraan*/
        /*SP*/
        driverStateModel.setPlate_no(etDataKendaraanTNKBNofvbi.getText().toString());
        driverStateModel.setPlate_color_id(ViewGeneratorUtil.getSelectedSpinnerIndex(llDataKendaraanWarnaTNKBfvbi));
        driverStateModel.setPropinsi_stnk(ViewGeneratorUtil.getSelectedSpinnerIndex(llDataKendaraanSTNKProvinsifvbi));
        driverStateModel.setKabupaten_stnk(ViewGeneratorUtil.getSelectedSpinnerIndex(llDataKendaraanSTNKKotafvbi));
        driverStateModel.setKecamatan_stnk(ViewGeneratorUtil.getSelectedSpinnerIndex(llDataKendaraanSTNKKecamatanfvbi));
        driverStateModel.setKelurahan_stnk(ViewGeneratorUtil.getSelectedSpinnerIndex(llDataKendaraanSTNKKelurahanfvbi));
        driverStateModel.setVehicle_type_id(ViewGeneratorUtil.getSelectedSpinnerIndex(llDataKendaraanJenisfvbi));
        if (isMotorMobil.equalsIgnoreCase("motor")) {
            driverStateModel.setVehicle_brand_id(ViewGeneratorUtil.getSelectedSpinnerIndex(llDataKendaraanMotorMerkfvbi));
        } else if (isMotorMobil.equalsIgnoreCase("mobil")) {
            driverStateModel.setVehicle_brand_id(ViewGeneratorUtil.getSelectedSpinnerIndex(llDataKendaraanMerkfvbi));
        }
        driverStateModel.setVehicle_color_id(ViewGeneratorUtil.getSelectedSpinnerIndex(llDataKendaraanWarnafvbi));

        /*ET*/
        driverStateModel.setEngine_no(etDataKendaraanNoMesinfvbi.getText().toString());
        driverStateModel.setEngine_capacity(etDataKendaraanVolMesinfvbi.getText().toString());
        driverStateModel.setFrame_no(etDataKendaraanNoRangkafvbi.getText().toString());
        driverStateModel.setOdometer(etDataKendaraanOdometerfvbi.getText().toString());
        driverStateModel.setBrake_footprint(etDataKendaraanPanjangJarakRemfvbi.getText().toString());
        driverStateModel.setRoad_number_stnk(etDataKendaraanSTNKNofvbi.getText().toString() + "");
        driverStateModel.setRoad_name_stnk(etDataKendaraanSTNKJalanfvbi.getText().toString());
        driverStateModel.setRt_stnk(etDataKendaraanSTNKRtfvbi.getText().toString());
        driverStateModel.setRw_stnk(etDataKendaraanSTNKRwfvbi.getText().toString());
        driverStateModel.setPlate_no(etDataKendaraanTNKBNofvbi.getText().toString());
        driverStateModel.setTotal_penumpang(etDataKendaraanTotalPenumpangfvbi.getText().toString());

        /*RB*/
        driverStateModel.setVehicle_design_id(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataKendaraanPeruntukanfvbi));
        driverStateModel.setAccident_before_id(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataKendaraanPergerakanSebelumTabrakanfvbi));
        driverStateModel.setAccident_after_id(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataKendaraanPergerakanSesudahTabrakanfvbi));
        driverStateModel.setAccident_end_id(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataKendaraanKedudukanAkhirKendaraanfvbi));
        driverStateModel.setSpecial_damage_id(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataKendaraanKerusakanKhususfvbi));
        driverStateModel.setPemudik(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataKendaraanTipePemudikfvbi));

        /*CB*/
        driverStateModel.setSafety_device(ViewGeneratorUtil.getSelectedCheckboxIndex(llviewDataKendaraanAlatKeselamatanfvbi));
        driverStateModel.setKerusakan(ViewGeneratorUtil.getSelectedCheckboxIndex(llviewDataKendaraanKerusakanAwalfvbi));
        driverStateModel.setDisita(ViewGeneratorUtil.getSelectedCheckboxIndex(llviewDataKendaraanBarangSitafvbi));

        /*pengemudiRB*/
        /*SP*/
        driverStateModel.setReligion_id(ViewGeneratorUtil.getSelectedSpinnerIndex(llDataPengemudiAgamafvbi));
        driverStateModel.setLicense_class_id(ViewGeneratorUtil.getSelectedSpinnerIndex(llDataPengemudiGolonganSimfvbi));
        driverStateModel.setNationality_id(ViewGeneratorUtil.getSelectedSpinnerIndex(llDataPengemudiKebangsaanfvbi));
        driverStateModel.setIdentity_type_id(spDataPengemudiJenisIdentitasfvbi.getSelectedItemPosition());
        driverStateModel.setPropinsi(ViewGeneratorUtil.getSelectedSpinnerIndex(llDataPengemudiAlamatProvinsifvbi));
        driverStateModel.setKabupaten(ViewGeneratorUtil.getSelectedSpinnerIndex(llDataPengemudiAlamatKotaKabupatenfvbi));
        driverStateModel.setKecamatan(ViewGeneratorUtil.getSelectedSpinnerIndex(llDataPengemudiAlamatKecamatanfvbi));
        driverStateModel.setKelurahan(ViewGeneratorUtil.getSelectedSpinnerIndex(llDataPengemudiAlamatKelurahanfvbi));
        driverStateModel.setProfession_id(ViewGeneratorUtil.getSelectedSpinnerIndex(llDataPengemudiPekerjaanfvbi));
        driverStateModel.setEducation_id(ViewGeneratorUtil.getSelectedSpinnerIndex(llDataPengemudiPendidikanfvbi));

        /*ET*/
        driverStateModel.setRoad_name(etDataPengemudiJalanfvbi.getText().toString());
        driverStateModel.setFirst_name(etDataPengemudiNamaDepanfvbi.getText().toString());
        driverStateModel.setLast_name(etDataPengemudiNamaBelakangfvbi.getText().toString());
        driverStateModel.setIdentity_no(etDataPengemudiNoIdentitasfvbi.getText().toString());
        driverStateModel.setRt(etDataPengemudiRTfvbi.getText().toString());
        driverStateModel.setRw(etDataPengemudiRWfvbi.getText().toString());
        driverStateModel.setAge(etDataPengemudiUmurfvbi.getText().toString());

        /*RB*/
        driverStateModel.setGender_id(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataPengemudiJenisKelaminfvbi));
        driverStateModel.setInjury_id(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataPengemudiTingkatLukafvbi));
        driverStateModel.setDead_spot_id(llviewDataPengemudiDeadSpot.indexOfChild(llviewDataPengemudiDeadSpot.findViewById(llviewDataPengemudiDeadSpot.getCheckedRadioButtonId())));
        Log.d("Lihat generateStateModel AddDriverActivity", String.valueOf(llviewDataPengemudiDeadSpot.indexOfChild(llviewDataPengemudiDeadSpot.findViewById(llviewDataPengemudiDeadSpot.getCheckedRadioButtonId()))));
        driverStateModel.setAlcohol_id(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataPengemudiDipengaruhiObatAlkoholfvbi));
        driverStateModel.setArrested_id(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataPengemudiDiTahanfvbi));
        driverStateModel.setSuspected_id(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataPengemudiPelakuSementarafvbi));

        /*CB*/
        driverStateModel.setSafety(ViewGeneratorUtil.getSelectedCheckboxIndex(llviewDataPengemudiPenggunaanAlatKeselamatanfvbi));
        driverStateModel.setTitik_kerusakan(cbrDataKendaraanTitikKerusakanfvbi.getSelectedCheckboxIndex());
        try {
            driverStateModel.setDriver_law(new RealmList<>((Integer[]) tvDataPengemudiJenisPelanggaranfvbi.getTag(tvDataPengemudiJenisPelanggaranfvbi.getId())));

        } catch (Exception e) {
            //Log.e("", "drvstmdl: " + e.getMessage());
        }

        try {
            driverStateModel.setDriver_behavior(new RealmList<>((Integer[]) tvDataPengemudiPerilakuPengemudifvbi.getTag(tvDataPengemudiPerilakuPengemudifvbi.getId())));

        } catch (Exception e) {
            //Log.e("", "drvtbhvr: " + e.getMessage());
        }

        driverStateModel.setBirth_date(tvDataPengemudiTanggalLahir.getText().toString());
        driverStateModel.setLicense_pub_date(tvDataPengemudiTanggalTerbitSIM.getText().toString());

        driverStateModel.setDehavior_text(tvDataPengemudiPerilakuPengemudifvbi.getText().toString());
        driverStateModel.setDaw_text(tvDataPengemudiJenisPelanggaranfvbi.getText().toString());
        return driverStateModel;
    }

    private void saveOverwrite() {
        String s = etDataKendaraanTNKBNofvbi.getText().toString();
        if (s.isEmpty()) {
            if (isTnkbMandatoryEmpty) {
                Toast.makeText(getApplicationContext(), "No Tnkb Harus Diisi", Toast.LENGTH_SHORT).show();
                etDataKendaraanTNKBNofvbi.setError("No Tnkb Harus Diisi");
                etDataKendaraanTNKBNofvbi.requestFocus();
            } else {
                if (isLoadedFromState) {
                    overwriteData();
                    finish();
                } else {
                    if (saveData()) {
                        finish();
                    }
                }
            }
        } else {
            if (isLoadedFromState) {
                overwriteData();
                finish();
            } else {
                if (saveData()) {
                    finish();
                }
            }
        }
    }

    private boolean saveData() {
        realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RequestRealmDbObject poolRequestModel = realm.where(RequestRealmDbObject.class).sort("Local_ID").equalTo("Local_ID", localId).findFirst();
                poolRequestModel.getSetDriverRequestModels().add(generateRequestModel(null));
                poolRequestModel.getSetDriverStateModels().add(generateStateModel(null));
                realm.copyToRealmOrUpdate(poolRequestModel);
            }
        });
        realm.refresh();
        realm.close();
        return true;
    }

    private boolean overwriteData() {
        realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm -> {
            generateRequestModel(realm.where(DriverDBModel.class).equalTo("Local_ID", localId).equalTo("vehicle_Dbid", orderId).findFirst());
            generateStateModel(realm.where(AddDriverStateModel.class).equalTo("Local_ID", localId).equalTo("vehicle_Dbid", orderId).findFirst());
        });
        realm.refresh();
        realm.close();
        return true;
    }

    private void checkData(AddDriverStateModel driverStateModel) {
        realm = Realm.getDefaultInstance();
        DriverDBModel driverDBModel = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst().getSetDriverRequestModels().get(0);

        List<String> idModel1 = getIdModel();
        Log.d("Lihat checkData AddAccidentActivity1", String.valueOf(driverDBModel));
        Log.d("Lihat checkData AddAccidentActivity4", String.valueOf(idModel1));

        //Kendaraan
        //SP
        if (driverDBModel.getPlate_color_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getPlate_color_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(llDataKendaraanWarnaTNKBfvbi, driverStateModel.getPlate_color_id());
            }
        }
        if (driverDBModel.getVehicle_type_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getVehicle_type_id())) {
                vehicleTypeIdSpinnerHandler(driverStateModel);
            }
        }
        if (driverDBModel.getVehicle_color_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getVehicle_color_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(llDataKendaraanWarnafvbi, driverStateModel.getVehicle_color_id());
            }
        }
        //RB
        if (driverDBModel.getVehicle_design_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getVehicle_design_id())) {
                ViewGeneratorUtil.setRadioCheckedAt(llviewDataKendaraanPeruntukanfvbi, driverStateModel.getVehicle_design_id());
            }
        }
        if (driverDBModel.getAccident_before_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getAccident_before_id())) {
                ViewGeneratorUtil.setRadioCheckedAt(llviewDataKendaraanPergerakanSebelumTabrakanfvbi, driverStateModel.getAccident_before_id());
            }
        }
        if (driverDBModel.getAccident_after_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getAccident_after_id())) {
                ViewGeneratorUtil.setRadioCheckedAt(llviewDataKendaraanPergerakanSesudahTabrakanfvbi, driverStateModel.getAccident_after_id());
            }
        }
        if (driverDBModel.getAccident_end_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getAccident_end_id())) {
                ViewGeneratorUtil.setRadioCheckedAt(llviewDataKendaraanKedudukanAkhirKendaraanfvbi, driverStateModel.getAccident_end_id());
            }
        }
        if (driverDBModel.getSpecial_damage_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getSpecial_damage_id())) {
                ViewGeneratorUtil.setRadioCheckedAt(llviewDataKendaraanKerusakanKhususfvbi, driverStateModel.getSpecial_damage_id());
            }
        }
        if (driverDBModel.getPemudik() != null) {
            if (isListContainString(idModel1, driverDBModel.getPemudik())) {
                ViewGeneratorUtil.setRadioCheckedAt(llviewDataKendaraanTipePemudikfvbi, driverStateModel.getPemudik());
            }
        }
        //CB
        if (driverDBModel.getKerusakan() != null) {
            if (isListContainStringArray(idModel1, driverDBModel.getKerusakan())) {
                ViewGeneratorUtil.setCheckboxCheckedAt(llviewDataKendaraanKerusakanAwalfvbi, driverStateModel.getKerusakan());
            }
        }
        if (driverDBModel.getDisita() != null) {
            if (isListContainStringArray(idModel1, driverDBModel.getDisita())) {
                ViewGeneratorUtil.setCheckboxCheckedAt(llviewDataKendaraanBarangSitafvbi, driverStateModel.getDisita());
            }
        }
        //pengemudi
        //sp
        if (driverDBModel.getReligion_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getReligion_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(llDataPengemudiAgamafvbi, driverStateModel.getReligion_id());
            }
        }
        if (driverDBModel.getLicense_class_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getLicense_class_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(llDataPengemudiGolonganSimfvbi, driverStateModel.getLicense_class_id());
            }
        }
        if (driverDBModel.getNationality_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getNationality_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(llDataPengemudiKebangsaanfvbi, driverStateModel.getNationality_id());
            }
        }
        if (driverDBModel.getIdentity_type_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getIdentity_type_id())) {
                driverIdentityTypeSpinnerHandler(driverStateModel, driverDBModel);
            }
        }
        if (driverDBModel.getProfession_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getProfession_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(llDataPengemudiPekerjaanfvbi, driverStateModel.getProfession_id());
            }
        }
        if (driverDBModel.getEducation_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getEducation_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(llDataPengemudiPendidikanfvbi, driverStateModel.getEducation_id());
            }
        }
        //rb
        if (driverDBModel.getGender_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getGender_id())) {
                ViewGeneratorUtil.setRadioCheckedAt(llviewDataPengemudiJenisKelaminfvbi, driverStateModel.getGender_id());
            }
        }
        if (driverDBModel.getInjury_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getInjury_id())) {
                ViewGeneratorUtil.setRadioCheckedAt(llviewDataPengemudiTingkatLukafvbi, driverStateModel.getInjury_id());
            }
        }
        if (driverDBModel.getDead_spot_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getDead_spot_id())) {
                ((ToggleRadioButton) llviewDataPengemudiDeadSpot.getChildAt(driverStateModel.getDead_spot_id())).setChecked(true);
            }
        }
        if (driverDBModel.getAlcohol_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getAlcohol_id())) {
                ViewGeneratorUtil.setRadioCheckedAt(llviewDataPengemudiDipengaruhiObatAlkoholfvbi, driverStateModel.getAlcohol_id());
            }
        }
        if (driverDBModel.getArrested_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getArrested_id())) {
                ViewGeneratorUtil.setRadioCheckedAt(llviewDataPengemudiDiTahanfvbi, driverStateModel.getArrested_id());
            }
        }
        if (driverDBModel.getSuspected_id() != null) {
            if (isListContainString(idModel1, driverDBModel.getSuspected_id())) {
                ViewGeneratorUtil.setRadioCheckedAt(llviewDataPengemudiPelakuSementarafvbi, driverStateModel.getSuspected_id());
            }
        }
        //cb
        if (driverDBModel.getDriver_behavior() != null) {
            if (isListContainStringArray(idModel1, driverDBModel.getDriver_behavior())) {
                sv1 = driverStateModel.getDriver_behavior_asArray();
                bDataPengemudiPerilakuPengemudifvbi.setOnClickListener(v -> {
                    dialogCB(tvDataPengemudiPerilakuPengemudifvbi, getDriver_behavior(), sv1, true).show();
                });
                tvDataPengemudiPerilakuPengemudifvbi.setText(driverStateModel.getDehavior_text());
            }
        }
        if (driverDBModel.getDriver_law() != null) {
            if (isListContainStringArray(idModel1, driverDBModel.getDriver_law())) {
                sv2 = driverStateModel.getDriver_law_asArray();
                bDataPengemudiJenisPelanggaranfvbi.setOnClickListener(v -> {
                    dialogCB(tvDataPengemudiJenisPelanggaranfvbi, getDriver_law(), sv2, false).show();
                });
                tvDataPengemudiJenisPelanggaranfvbi.setText(driverStateModel.getDaw_text());
            }
        }

    }

    private Dialog dialogCB(TextView textViewTarget, ViewModel data, @Nullable Integer[] currstate, Boolean type) {
        View internalView = LayoutInflater.from(this).inflate(com.gandsoft.irsms.R.layout.dialog_checkbox_selector, null);
        LinearLayout contentView = internalView.findViewById(com.gandsoft.irsms.R.id.asdfqwer);
        ViewGenerator.generateViewBasedOnModel(this, data, contentView);
        Log.d("Lihat dialogCB AddVehicleActivitydata", String.valueOf(data));

        if (currstate != null) {
            ViewGeneratorUtil.setCheckboxCheckedAt(contentView, currstate);
        } else {
            ViewGeneratorUtil.setCheckboxCheckedAt(contentView, null);
        }
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams layoutparams = new WindowManager.LayoutParams();
        layoutparams.width = WindowManager.LayoutParams.MATCH_PARENT;//ukuran lebar layout
        layoutparams.height = WindowManager.LayoutParams.WRAP_CONTENT;//ukuran tinggi layout
        dialog.getWindow().setAttributes(layoutparams);
        dialog.setContentView(internalView);
        dialog.setOnDismissListener(dialog1 -> {
            String cuk = "";
            for (int c : ViewGeneratorUtil.getSelectedCheckboxIndex(contentView)) {
                cuk += ", " + c;
            }
            if (cuk.length() != 0) {
                String value;
                if (ViewGeneratorUtil.getSelectedCheckboxValue(contentView) != null) {
                    value = ViewGeneratorUtil.getSelectedCheckboxValue(contentView);
                } else {
                    value = null;
                }
                cuk = cuk.substring(1);
                Log.d("Lihat dialogCB AddVehicleActivityval", value);
                Log.d("Lihat dialogCB AddVehicleActivitycuk", cuk);
                textViewTarget.setText(cuk);
                //textViewTarget.setText(value);
                textViewTarget.setTag(value);
                textViewTarget.setTag(textViewTarget.getId(), ViewGeneratorUtil.getSelectedCheckboxIndex(contentView));
                if (type) {
                    sv1 = ViewGeneratorUtil.getSelectedCheckboxIndex(contentView);
                } else {
                    sv2 = ViewGeneratorUtil.getSelectedCheckboxIndex(contentView);
                }
            } else {
                textViewTarget.setText("-");
                textViewTarget.setTag(null);
                if (type) {
                    sv1 = ViewGeneratorUtil.getSelectedCheckboxIndex(contentView);
                } else {
                    sv2 = ViewGeneratorUtil.getSelectedCheckboxIndex(contentView);
                }
            }
        });
        return dialog;
    }

    private void vehicleDateAgeSpinnerHandler(AddDriverStateModel vehicleStateModel) {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                //code here
                etDataPengemudiUmurfvbi.setText(vehicleStateModel.getAge());
                if (!tvDataPengemudiTanggalLahir.getText().toString().isEmpty()) {
                    llDataPengemudiTanggalLahir.setVisibility(View.VISIBLE);
                    ivDataPengemudiTanggalLahirClearfvbi.setVisibility(View.VISIBLE);
                    etDataPengemudiUmurfvbi.setEnabled(false);
                } else {
                    llDataPengemudiTanggalLahir.setVisibility(View.GONE);
                    ivDataPengemudiTanggalLahirClearfvbi.setVisibility(View.GONE);
                    etDataPengemudiUmurfvbi.setEnabled(true);
                }
            }
        }, 200);
    }

    private void vehicleTypeIdSpinnerHandler(AddDriverStateModel driverStateModel) {
        Handler localHandler = new Handler();

        ViewGeneratorUtil.selectSpinnerDropdownAt(llDataKendaraanJenisfvbi, driverStateModel.getVehicle_type_id());
        localHandler.postDelayed((Runnable) () -> {
            if (isMotorMobil.equalsIgnoreCase("mobil")) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(llDataKendaraanMerkfvbi, driverStateModel.getVehicle_brand_id());
            } else if (isMotorMobil.equalsIgnoreCase("motor")) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(llDataKendaraanMotorMerkfvbi, driverStateModel.getVehicle_brand_id());
            }
            ViewGeneratorUtil.setCheckboxCheckedAt(llviewDataKendaraanAlatKeselamatanfvbi, driverStateModel.getSafety_device());
            ViewGeneratorUtil.setCheckboxCheckedAt(llviewDataPengemudiPenggunaanAlatKeselamatanfvbi, driverStateModel.getSafety());
            try {
                Integer[] x = driverStateModel.getTitik_kerusakan();
                final int childCount = cbrDataKendaraanTitikKerusakanfvbi.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    if (cbrDataKendaraanTitikKerusakanfvbi.getChildAt(i) instanceof CheckBox) {
                        try {
                            for (Integer j : x) {
                                j = j + 1;
                                if (j.equals(i)) {
                                    ((CheckBox) cbrDataKendaraanTitikKerusakanfvbi.getChildAt(i)).setChecked(true);
                                }
                            }
                        } catch (Exception e) {
                            //Log.e("MEONG", e.getMessage());
                        }
                    }
                }
            } catch (Exception e) {
                //Log.e("MEONG2", e.getMessage());
            }
        }, 500);
    }

    private void driverIdentityTypeSpinnerHandler(AddDriverStateModel vehicleStateModel, DriverDBModel driverDBModel) {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                //code here
                spDataPengemudiJenisIdentitasfvbi.setSelection(vehicleStateModel.getIdentity_type_id());
                etDataPengemudiNoIdentitasfvbi.setText(vehicleStateModel.getIdentity_no());
                String vecTypeId = driverDBModel.getVehicle_type_id();
                if (isListContainString(typeVehicleR4, vecTypeId)) {
                    isMotorMobil = "mobil";
                } else if (isListContainString(typeVehicleR2, vecTypeId)) {
                    isMotorMobil = "motor";
                } else {
                    isMotorMobil = "other";
                }
            }
        }, 200);
    }

    private void vehicleStnkSpinnerHandler(AddDriverStateModel driverStateModel) {
        Handler localHandler = new Handler();

        ViewGeneratorUtil.selectSpinnerDropdownAt(llDataKendaraanSTNKProvinsifvbi, driverStateModel.getPropinsi_stnk());
        localHandler.postDelayed(() -> {
            ViewGeneratorUtil.selectSpinnerDropdownAt(llDataKendaraanSTNKKotafvbi, driverStateModel.getKabupaten_stnk());
            localHandler.postDelayed(() -> {
                ViewGeneratorUtil.selectSpinnerDropdownAt(llDataKendaraanSTNKKecamatanfvbi, driverStateModel.getKecamatan_stnk());
                localHandler.postDelayed(() -> ViewGeneratorUtil.selectSpinnerDropdownAt(llDataKendaraanSTNKKelurahanfvbi, driverStateModel.getKelurahan_stnk()), 200);
            }, 200);
        }, 200);
    }

    private void vehicleDriverSpinnerHandler(AddDriverStateModel driverStateModel) {
        Handler localHandler = new Handler();

        ViewGeneratorUtil.selectSpinnerDropdownAt(llDataPengemudiAlamatProvinsifvbi, driverStateModel.getPropinsi());
        localHandler.postDelayed(() -> {
            ViewGeneratorUtil.selectSpinnerDropdownAt(llDataPengemudiAlamatKotaKabupatenfvbi, driverStateModel.getKabupaten());
            localHandler.postDelayed(() -> {
                ViewGeneratorUtil.selectSpinnerDropdownAt(llDataPengemudiAlamatKecamatanfvbi, driverStateModel.getKecamatan());
                localHandler.postDelayed(() -> ViewGeneratorUtil.selectSpinnerDropdownAt(llDataPengemudiAlamatKelurahanfvbi, driverStateModel.getKelurahan()), 200);
            }, 200);
        }, 200);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String s = String.format("%02d", dayOfMonth) + "-" + String.format("%02d", (monthOfYear + 1)) + "-" + year;
        String d = year + "-" + String.format("%02d", (monthOfYear + 1)) + "-" + String.format("%02d", dayOfMonth);

        Calendar now = Calendar.getInstance();

        if (dpdSelector) {
            tvDataPengemudiTanggalTerbitSIM.setText(s);
            tvDataPengemudiTanggalTerbitSIM.setTag(d);
        } else {
            tvDataPengemudiTanggalLahir.setText(s);
            tvDataPengemudiTanggalLahir.setTag(d);
            ivDataPengemudiTanggalLahirClearfvbi.setVisibility(View.VISIBLE);
            etDataPengemudiUmurfvbi.setEnabled(false);
            etDataPengemudiUmurfvbi.setText(getAge(year, monthOfYear, dayOfMonth));
        }
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        saveOverwrite();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        finish();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(AddDriverActivity.this);
        builder.setMessage("Simpan data sebelum keluar?").setPositiveButton("Ya", dialogClickListener)
                .setNegativeButton("Tidak", dialogClickListener).show();
    }
}

