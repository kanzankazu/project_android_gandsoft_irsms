package com.gandsoft.irsms.Activity;

/**
 * Created by gleen on 13/03/18.
 */

public interface ActivityCallbackListenerInterface {

    void onAdapterCallback(int id, Object value);
}
