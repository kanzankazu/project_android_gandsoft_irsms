package com.gandsoft.irsms.Activity.summary.sub;

import android.app.Activity;
import android.app.AlertDialog;
import com.gandsoft.irsms.Activity.AddWitness.AddWitnessActivity;
import com.gandsoft.irsms.Activity.LocalBaseActivity;
import com.gandsoft.irsms.Support.Util.SystemUtil;
import com.gandsoft.irsms.model.RealmDbModel.RequestRealmDbObject;
import com.gandsoft.irsms.model.uiModel.ViewModel;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import java.util.List;

import io.realm.Realm;

/**
 * Created by kanzan on 2/6/2018.
 */

public class WitnessSummaryActivity extends LocalBaseActivity implements WitnessObjectHookInteface {
    private View cvSummaryDataSaksiKeluarfvbi;
    private View cvSummaryDataSaksiTambahSaksifvbi;
    private RecyclerView rvSummaryDataSaksifvbi;
    private WitnessItemRecyclerviewAdapter rvAdaptSummaryDataSaksifvbi;
    private List<ViewModel> saksis;
    private int localId;
    private Realm realm;
    private RequestRealmDbObject models;
    private LinearLayout llSummaryDataTitlefvbi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.gandsoft.irsms.R.layout.activity_summary_witness);

        Intent bundle = getIntent();
        if (bundle.hasExtra("Local_ID")) {
            localId = bundle.getIntExtra("Local_ID", 0);
        }

        Log.d("WitnessSummaryActivity", String.valueOf(localId));

        initComponent();
        initContent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        rvAdaptSummaryDataSaksifvbi.notifyDataSetChanged();
    }

    private void initComponent() {
        llSummaryDataTitlefvbi = findViewById(com.gandsoft.irsms.R.id.llSummaryDataTitle);

        cvSummaryDataSaksiKeluarfvbi = findViewById(com.gandsoft.irsms.R.id.cvSummaryDataSaksiKeluar);
        cvSummaryDataSaksiTambahSaksifvbi = findViewById(com.gandsoft.irsms.R.id.cvSummaryDataSaksiTambahSaksi);

        rvSummaryDataSaksifvbi = findViewById(com.gandsoft.irsms.R.id.rvSummaryDataSaksi);
    }

    private void initContent() {
        if (SystemUtil.isKitkat()){
            llSummaryDataTitlefvbi.bringToFront();
        }

        cvSummaryDataSaksiKeluarfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        cvSummaryDataSaksiTambahSaksifvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WitnessSummaryActivity.this, AddWitnessActivity.class);
                intent.putExtra("Local_ID", localId);
                intent.putExtra("Order_ID", -1);
                startActivity(intent);
            }
        });

        realm = Realm.getDefaultInstance();
        realm.refresh();

        models = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst();

        rvAdaptSummaryDataSaksifvbi = new WitnessItemRecyclerviewAdapter(this, models.getSetWitnessRequestModels());
        rvSummaryDataSaksifvbi.setLayoutManager(new LinearLayoutManager(this, LinearLayout.VERTICAL, false));
        rvSummaryDataSaksifvbi.setAdapter(rvAdaptSummaryDataSaksifvbi);
    }

    @Override
    public void deleteWitnessRealmModelAt(final int id) {
        final int internalId = id;
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        try {
                            realm = Realm.getDefaultInstance();
                            realm.executeTransactionAsync(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    RequestRealmDbObject model = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst();
                                    model.getSetWitnessRequestModels().remove(internalId);
                                    model.getSetWitnessStateModels().remove(internalId);
                                    realm.copyToRealmOrUpdate(model);
                                }
                            }, new Realm.Transaction.OnSuccess() {
                                @Override
                                public void onSuccess() {
                                    rvAdaptSummaryDataSaksifvbi.notifyDataSetChanged();
                                }
                            });
                            realm.refresh();
                        } finally {
                            if (realm != null) {
                                realm.close();
                            }
                        }
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Hapus laporan ini?").
                setPositiveButton("Ya", dialogClickListener)
                .setNegativeButton("Tidak", dialogClickListener).show();
    }

    @Override
    public void editWitnessRealmModelAt(int id) {
        Intent editIntent = new Intent(this, AddWitnessActivity.class);
        editIntent.putExtra("Local_ID", localId);
        editIntent.putExtra("Order_ID", id);
        startActivity(editIntent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }


}
