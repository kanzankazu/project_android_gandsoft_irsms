package com.gandsoft.irsms.Activity.Sync;

/**
 * Created by gleen on 27/02/18.
 */

public interface SyncHookInterface {

    void nextStep(int localID);

}
