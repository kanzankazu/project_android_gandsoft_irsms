package com.gandsoft.irsms.Activity.AddWitness;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.gandsoft.irsms.Activity.LocalBaseActivity;
import com.gandsoft.irsms.R;
import com.gandsoft.irsms.Support.Util.SystemUtil;
import com.gandsoft.irsms.Support.ViewGenerator.ViewGenerator;
import com.gandsoft.irsms.Support.ViewGenerator.ViewGeneratorUtil;
import com.gandsoft.irsms.model.RealmDbModel.RequestRealmDbObject;
import com.gandsoft.irsms.model.RealmDbModel.WitnessForm.AddWitnessStateModel;
import com.gandsoft.irsms.model.RealmDbModel.WitnessForm.SetWitnessDBModel;
import com.gandsoft.irsms.model.uiModel.FormdataLoader;

import java.util.List;

import io.realm.Realm;

import static com.gandsoft.irsms.Support.Util.SystemUtil.isKitkat;
import static com.gandsoft.irsms.Support.Util.SystemUtil.isListContainString;

/**
 * Created by kanzan on 2/6/2018.
 */

public class AddWitnessActivity extends LocalBaseActivity {
    private LinearLayout llDataSaksiTitlefvbi;

    private View cvDataSaksiKeluarfvbi, cvDataSaksiSimpanfvbi;
    private LinearLayout spDataSaksiAgamafvbi, spDataSaksiPekerjaanfvbi, spDataSaksiJenisIdentitasfvbi;
    private LinearLayout rbviewDataSaksiJenisKelaminfvbi;
    private EditText etDataSaksiNamaDepanfvbi, etDataSaksiNamaBelakangfvbi, etDataSaksiUmurfvbi, etDataSaksiNoIdentitasfvbi, etDataSaksiAlamatfvbi;
    private int localId;
    private int orderId;
    private boolean loadedFromState;
    private Realm realm;
    private boolean chckr = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_witness);

        initComponent();
        initContent();

        Intent bundle = getIntent();
        if (bundle.hasExtra("Local_ID")) {
            localId = bundle.getIntExtra("Local_ID", 0);
        }
        if (bundle.hasExtra("Order_ID")) {
            orderId = bundle.getIntExtra("Order_ID", 0);
        }

        loadedFromState = orderId > -1;
        if (loadedFromState) {
            loadState();
        }
        Log.d("Lihat", localId + " , " + orderId);
    }

    private void initComponent() {
        llDataSaksiTitlefvbi = findViewById(R.id.llDataSaksiTitle);

        etDataSaksiNamaDepanfvbi = findViewById(R.id.etDataSaksiNamaDepan);
        etDataSaksiNamaBelakangfvbi = findViewById(R.id.etDataSaksiNamaBelakang);
        etDataSaksiUmurfvbi = findViewById(R.id.etDataSaksiUmur);
        etDataSaksiAlamatfvbi = findViewById(R.id.etDataSaksiAlamat);
        etDataSaksiNoIdentitasfvbi = findViewById(R.id.etDataSaksiNoIdentitas);

        cvDataSaksiKeluarfvbi = findViewById(R.id.cvDataSaksiKeluar);
        cvDataSaksiSimpanfvbi = findViewById(R.id.cvDataSaksiSimpan);

        spDataSaksiAgamafvbi = findViewById(R.id.spDataSaksiAgama);
        spDataSaksiPekerjaanfvbi = findViewById(R.id.spDataSaksiPekerjaan);
        spDataSaksiJenisIdentitasfvbi = findViewById(R.id.spDataSaksiJenisIdentitas);

        rbviewDataSaksiJenisKelaminfvbi = findViewById(R.id.rbviewDataSaksiJenisKelamin);
    }

    private void initContent() {
        if (isKitkat()){
            llDataSaksiTitlefvbi.bringToFront();
        }

        /*SP*/
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getReligion_id(), spDataSaksiAgamafvbi);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getProfession_id(), spDataSaksiPekerjaanfvbi);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getIdentity_type_id(), spDataSaksiJenisIdentitasfvbi);

        /*RB*/
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getGender_id(), rbviewDataSaksiJenisKelaminfvbi);

        cvDataSaksiKeluarfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        cvDataSaksiSimpanfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveOverwriteData();
            }
        });

        ViewGeneratorUtil.selectSpinnerView(spDataSaksiJenisIdentitasfvbi).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SystemUtil.validationIdentityByType(etDataSaksiNoIdentitasfvbi,
                        ViewGeneratorUtil.getSelectedSpinnerValue(spDataSaksiJenisIdentitasfvbi),
                        FormdataLoader.getId_identity_type_id_lv(),
                        FormdataLoader.getId_identity_type_max_lenght_lv());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etDataSaksiAlamatfvbi.setFilters(new InputFilter[]{new InputFilter.LengthFilter(255)});
    }

    private void loadState() {
        realm = Realm.getDefaultInstance();

        AddWitnessStateModel state = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst().getSetWitnessStateModels().get(orderId);

        checkData(state);

        /*SP*//*
        ViewGeneratorUtil.selectSpinnerDropdownAt(spDataSaksiAgamafvbi, state.getReligion_id());
        ViewGeneratorUtil.selectSpinnerDropdownAt(spDataSaksiPekerjaanfvbi, state.getProfession_id());
        ViewGeneratorUtil.selectSpinnerDropdownAt(spDataSaksiJenisIdentitasfvbi, state.getIdentity_type_id());
        *//*RB*//*
        ViewGeneratorUtil.setRadioCheckedAt(rbviewDataSaksiJenisKelaminfvbi, state.getGender_id());*/

        /*ET*/
        etDataSaksiNamaDepanfvbi.setText(state.getFirst_name());
        etDataSaksiNamaBelakangfvbi.setText(state.getLast_name());
        etDataSaksiUmurfvbi.setText(state.getAge());
        etDataSaksiAlamatfvbi.setText(state.getRoad_name());
        etDataSaksiNoIdentitasfvbi.setText(state.getIdentity_no());

        if (state.getIdentity_type_id() == 4) {
            etDataSaksiNoIdentitasfvbi.setVisibility(View.INVISIBLE);
        }
    }

    private SetWitnessDBModel generateRequestModel(@Nullable SetWitnessDBModel requestModel) {
        if (requestModel == null) {
            requestModel = new SetWitnessDBModel();
        }

        /*SP*/
        requestModel.setReligion_id(ViewGeneratorUtil.getSelectedSpinnerValue(spDataSaksiAgamafvbi));
        requestModel.setProfession_id(ViewGeneratorUtil.getSelectedSpinnerValue(spDataSaksiPekerjaanfvbi));
        requestModel.setIdentity_type_id(ViewGeneratorUtil.getSelectedSpinnerValue(spDataSaksiJenisIdentitasfvbi));
        /*ET*/
        requestModel.setFirst_name(etDataSaksiNamaDepanfvbi.getText().toString());
        requestModel.setLast_name(etDataSaksiNamaBelakangfvbi.getText().toString());
        requestModel.setAge(etDataSaksiUmurfvbi.getText().toString());
        requestModel.setRoad_name(etDataSaksiAlamatfvbi.getText().toString());
        requestModel.setIdentity_no(etDataSaksiNoIdentitasfvbi.getText().toString() + "");
        //Toast.makeText(this, etDataSaksiNoIdentitasfvbi.getText().toString(), Toast.LENGTH_SHORT).show();

        /*RB*/
        requestModel.setGender_id(ViewGeneratorUtil.getSelectedRadioValue(rbviewDataSaksiJenisKelaminfvbi));

        requestModel.setGender_id_text(ViewGeneratorUtil.getSelectedRadioTextValue(rbviewDataSaksiJenisKelaminfvbi));

        return requestModel;
    }

    private AddWitnessStateModel generateStateModel(@Nullable AddWitnessStateModel stateModel) {
        if (stateModel == null) {
            stateModel = new AddWitnessStateModel();
        }

        /*SP*/
        stateModel.setReligion_id(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataSaksiAgamafvbi));
        stateModel.setProfession_id(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataSaksiPekerjaanfvbi));
        stateModel.setIdentity_type_id(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataSaksiJenisIdentitasfvbi));
        /*ET*/
        stateModel.setFirst_name(etDataSaksiNamaDepanfvbi.getText().toString());
        stateModel.setLast_name(etDataSaksiNamaBelakangfvbi.getText().toString());
        stateModel.setAge(etDataSaksiUmurfvbi.getText().toString());
        stateModel.setRoad_name(etDataSaksiAlamatfvbi.getText().toString());
        stateModel.setIdentity_no(etDataSaksiNoIdentitasfvbi.getText().toString());

        /*RB*/
        stateModel.setGender_id(ViewGeneratorUtil.getSelectedRadioIndex(rbviewDataSaksiJenisKelaminfvbi));

        return stateModel;
    }

    private void saveOverwriteData() {
        if (!loadedFromState) {
            if (saveData()) {
                Toast.makeText(getApplicationContext(), "Save Success", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Save Failed", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (overwriteData()) {
                Toast.makeText(getApplicationContext(), "Overwrite Success", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Overwrite Failed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean saveData() {
        realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                RequestRealmDbObject poolRequestModel = realm.where(RequestRealmDbObject.class).sort("Local_ID").equalTo("Local_ID", localId).findFirst();
                poolRequestModel.getSetWitnessRequestModels().add(generateRequestModel(null));
                poolRequestModel.getSetWitnessStateModels().add(generateStateModel(null));
                realm.copyToRealmOrUpdate(poolRequestModel);
            }
        });
        realm.refresh();
        realm.close();
        return true;
    }

    private boolean overwriteData() {
        realm = Realm.getDefaultInstance();
        final RequestRealmDbObject poolRequestModel = realm.where(RequestRealmDbObject.class).sort("Local_ID").equalTo("Local_ID", localId).findFirst();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                generateRequestModel(poolRequestModel.getSetWitnessRequestModels().get(orderId));
                generateStateModel(poolRequestModel.getSetWitnessStateModels().get(orderId));
            }
        });
        realm.refresh();
        realm.close();
        return true;
    }

    private void checkData(AddWitnessStateModel witnessStateModel) {
        realm = Realm.getDefaultInstance();
        SetWitnessDBModel witnessDBModel = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst().getSetWitnessRequestModels().get(0);

        List<String> idModel1 = FormdataLoader.getIdModel();
        Log.d("Lihat checkData AddAccidentActivity1", String.valueOf(witnessDBModel));
        Log.d("Lihat checkData AddAccidentActivity4", String.valueOf(idModel1));

        if (witnessDBModel.getReligion_id() != null) {
            if (isListContainString(idModel1, witnessDBModel.getReligion_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(spDataSaksiAgamafvbi, witnessStateModel.getReligion_id());
            }
        }
        if (witnessDBModel.getProfession_id() != null) {
            if (isListContainString(idModel1, witnessDBModel.getProfession_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(spDataSaksiPekerjaanfvbi, witnessStateModel.getProfession_id());
            }
        }
        if (witnessDBModel.getIdentity_type_id() != null) {
            if (isListContainString(idModel1, witnessDBModel.getIdentity_type_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(spDataSaksiJenisIdentitasfvbi, witnessStateModel.getIdentity_type_id());
            }
        }
        if (witnessDBModel.getGender_id() != null) {
            if (isListContainString(idModel1, witnessDBModel.getGender_id())) {
                ViewGeneratorUtil.setRadioCheckedAt(rbviewDataSaksiJenisKelaminfvbi, witnessStateModel.getGender_id());
            }
        }
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        saveOverwriteData();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        finish();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(AddWitnessActivity.this);
        builder.setMessage("Simpan data sebelum keluar?").setPositiveButton("Ya", dialogClickListener)
                .setNegativeButton("Tidak", dialogClickListener).show();
    }
}