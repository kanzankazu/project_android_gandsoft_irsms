package com.gandsoft.irsms.Activity.Sync;

import com.gandsoft.irsms.Activity.Dashboard.DashboardObjectHookInteface;
import com.gandsoft.irsms.R;
import com.gandsoft.irsms.model.uiModel.SyncViewModel;
import com.gandsoft.irsms.model.uiModel.ViewModel;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gandsoft.irsms.model.uiModel.SyncViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

/**
 * Created by gleen on 15/02/18.
 */

public class SycnRecyclerAdapter extends RecyclerView.Adapter<SycnRecyclerAdapter.ViewHolder> {
    private final Activity parent;
    private List<SyncViewModel> models = new ArrayList<>();
    private int currentPosition;

    public SycnRecyclerAdapter(Activity parent, List<SyncViewModel> models) {
        this.models = models;
        this.parent = parent;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageButton ibLvSyncStatus;
        private TextView tvLvSyncStatusTahapanProses, tvLvSyncStatusStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            tvLvSyncStatusStatus = itemView.findViewById(com.gandsoft.irsms.R.id.tvLvSyncStatusStatus);
            tvLvSyncStatusTahapanProses = itemView.findViewById(com.gandsoft.irsms.R.id.tvLvSyncStatusTahapanProses);
            ibLvSyncStatus = itemView.findViewById(com.gandsoft.irsms.R.id.ibLvSyncStatus);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(com.gandsoft.irsms.R.layout.list_layout_syncsstatus, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SyncViewModel item = models.get(position);
        String[] getStatus = holder.tvLvSyncStatusStatus.getText().toString().split(",");
        if (getStatus[0].equalsIgnoreCase("warning")){
            holder.ibLvSyncStatus.setVisibility(View.VISIBLE);
        }
        holder.tvLvSyncStatusTahapanProses.setText(item.getContent());
        holder.tvLvSyncStatusStatus.setText(item.getStatus());
        currentPosition = position;
        holder.ibLvSyncStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SyncHookInterface) parent).nextStep(item.getLocalID());
            }
        });
    }

    public void updateModel(int id, @Nullable String content, String status) {
        SyncViewModel model = models.get(id);
        if (content != null) {
            model.setContent(content);
        }
        model.setStatus(status);
    }

    public void updateModelAt(int id, String content, String status) {
        SyncViewModel model = null;
        int counter = 0;
        for (SyncViewModel jink :
                models
                ) {
            counter++;
            if (jink.getLocalID() == id) {
                model = jink;
                break;
            }
        }
        if (model != null) {
            model.setContent(content);
            model.setStatus(status);
        }
    }

    public void addModel(SyncViewModel model) {
        models.add(model);
    }

    public int getPosition() {
        return currentPosition;
    }

    @Override
    public int getItemCount() {
        return models.size();
    }


}
