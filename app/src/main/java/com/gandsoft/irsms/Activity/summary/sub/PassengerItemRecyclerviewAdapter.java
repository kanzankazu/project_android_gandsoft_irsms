package com.gandsoft.irsms.Activity.summary.sub;

import android.app.Activity;

import com.gandsoft.irsms.model.RealmDbModel.PassengerForm.PassangerDBModel;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class PassengerItemRecyclerviewAdapter extends RecyclerView.Adapter<PassengerItemRecyclerviewAdapter.viewHolder> {

    private List<PassangerDBModel> models;
    private PassengerObjectHookInteface parent;

    public PassengerItemRecyclerviewAdapter(Activity parent, List<PassangerDBModel> data) {
        models = data;
        try {
            this.parent = (PassengerObjectHookInteface) parent;
        } catch (Exception e) {
            this.parent = null;
        }
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(com.gandsoft.irsms.R.layout.list_layout_summary_passenger, parent, false);
        return new PassengerItemRecyclerviewAdapter.viewHolder(itemView);
    }

    public class viewHolder extends RecyclerView.ViewHolder {

        ImageView ivSummaryDataPenumpangfvbi;
        TextView tvSummaryDataPenumpangNofvbi, tvSummaryDataPenumpangNamafvbi, tvSummaryDataPenumpangUmurfvbi, tvSummaryDataPenumpangLukafvbi;
        ImageButton ibSummaryDataPenumpangDeletefvbi, ibSummaryDataPenumpangEditfvbi;

        public viewHolder(View view) {
            super(view);
            ivSummaryDataPenumpangfvbi = view.findViewById(com.gandsoft.irsms.R.id.ivSummaryDataPenumpang);
            tvSummaryDataPenumpangNofvbi = view.findViewById(com.gandsoft.irsms.R.id.tvSummaryDataPenumpangNo);
            tvSummaryDataPenumpangNamafvbi = view.findViewById(com.gandsoft.irsms.R.id.tvSummaryDataPenumpangNama);
            tvSummaryDataPenumpangUmurfvbi = view.findViewById(com.gandsoft.irsms.R.id.tvSummaryDataPenumpangUmur);
            tvSummaryDataPenumpangLukafvbi = view.findViewById(com.gandsoft.irsms.R.id.tvSummaryDataPenumpangLuka);
            ibSummaryDataPenumpangDeletefvbi = view.findViewById(com.gandsoft.irsms.R.id.ibSummaryDataPenumpangDelete);
            ibSummaryDataPenumpangEditfvbi = view.findViewById(com.gandsoft.irsms.R.id.ibSummaryDataPenumpangEdit);
        }
    }

    @Override
    public void onBindViewHolder(PassengerItemRecyclerviewAdapter.viewHolder holder, final int position) {
        final PassangerDBModel model = models.get(position);
        holder.tvSummaryDataPenumpangNofvbi.setText(String.valueOf(position + 1));
        holder.tvSummaryDataPenumpangNamafvbi.setText(model.getFirst_name() + " " + model.getLast_name());
        holder.tvSummaryDataPenumpangUmurfvbi.setText(model.getAge());
        holder.tvSummaryDataPenumpangLukafvbi.setText(model.getInjury_id_text());

        holder.ibSummaryDataPenumpangEditfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parent.editPassengerRealmModelAt(model.getItem_id());
            }
        });

        holder.ibSummaryDataPenumpangDeletefvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parent.deletePassengerRealmModelAt(model.getItem_id());
            }
        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public void updateModel(List<PassangerDBModel> models) {
        this.models = models;
    }


}
