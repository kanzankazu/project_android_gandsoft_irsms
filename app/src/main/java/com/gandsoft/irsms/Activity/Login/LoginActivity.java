package com.gandsoft.irsms.Activity.Login;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gandsoft.irsms.Activity.Dashboard.DashboardActivity;
import com.gandsoft.irsms.Activity.LocalBaseActivity;
import com.gandsoft.irsms.IConfig;
import com.gandsoft.irsms.ISeasonConfig;
import com.gandsoft.irsms.IrsmsApp;
import com.gandsoft.irsms.Presenter.DAO.AuthDao;
import com.gandsoft.irsms.Presenter.DAO.MainPageDao;
import com.gandsoft.irsms.Presenter.SeasonManager.SessionManager;
import com.gandsoft.irsms.Support.Util.DialogUtil;
import com.gandsoft.irsms.Support.Util.ResponseUtil;
import com.gandsoft.irsms.Support.Util.SystemUtil;
import com.gandsoft.irsms.model.ObjectModel.UserDataModel;
import com.gandsoft.irsms.model.RealmDbModel.UserData.UserData;
import com.gandsoft.irsms.model.RealmDbModel.UserData.UserDataRealmObject;
import com.gandsoft.irsms.model.RequestModel.Auth.LoginRequestModel;
import com.gandsoft.irsms.model.RequestModel.Auth.ResetPasswordRequestModel;
import com.gandsoft.irsms.model.ResponseModel.Auth.ChangePasswordResponseModel;
import com.gandsoft.irsms.model.ResponseModel.Auth.LoginResponseModel;
import com.gandsoft.irsms.model.ResponseModel.BaseResponseModel;

import java.util.regex.Pattern;

import app.beelabs.com.codebase.base.BaseDao;
import app.beelabs.com.codebase.base.response.BaseResponse;
import io.realm.Realm;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;
import retrofit2.Response;

/**
 * Created by glenn on 1/26/18.
 */

public class LoginActivity extends LocalBaseActivity {

    private TextView tvLogDatalaka, tvLogLogin, tvLogMasukanUsernamePassword, tvLogReloadUI, tvLogForgetPass, tvLogBtnLogin;
    private LinearLayout loginViewContainer, forgotPasswordViewContainer;
    private EditText username, password, email;
    private View submit, forgetpasswordca, getimeica;
    private Boolean isLoginPage = true;
    private Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.gandsoft.irsms.R.layout.activity_login);
        realm = Realm.getDefaultInstance();

        if (SessionManager.isLoggedIn()) {
            moveToDashboard();
        }

        initComponent();
        initContent();
        initPermissions();
    }

    @Override
    public void onBackPressed() {
        if (isLoginPage) {
            super.onBackPressed();
        } else {
            changeToLoginState();
        }
    }

    private void initComponent() {
        username = findViewById(com.gandsoft.irsms.R.id.eLogUsername);
        password = findViewById(com.gandsoft.irsms.R.id.eLogPassword);
        email = findViewById(com.gandsoft.irsms.R.id.edtChangePassword);

        submit = findViewById(com.gandsoft.irsms.R.id.cvLogLogin);
        forgetpasswordca = findViewById(com.gandsoft.irsms.R.id.tvLogForgetPass);
        getimeica = findViewById(com.gandsoft.irsms.R.id.tvLogDevReg);

        tvLogDatalaka = findViewById(com.gandsoft.irsms.R.id.tvLogDatalaka);
        tvLogLogin = findViewById(com.gandsoft.irsms.R.id.tvLogLogin);
        tvLogMasukanUsernamePassword = findViewById(com.gandsoft.irsms.R.id.tvLogMasukanUsernamePassword);
        tvLogForgetPass = findViewById(com.gandsoft.irsms.R.id.tvLogForgetPass);
        tvLogReloadUI = findViewById(com.gandsoft.irsms.R.id.tvLogReloadUI);
        tvLogBtnLogin = findViewById(com.gandsoft.irsms.R.id.tvLogBtnLogin);

        loginViewContainer = findViewById(com.gandsoft.irsms.R.id.container_loginview);
        forgotPasswordViewContainer = findViewById(com.gandsoft.irsms.R.id.container_lupapassword);
    }

    private void initContent() {

        getimeica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generateImeiDialog();
            }
        });

        tvLogReloadUI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemUtil.isNetworkAvailable(getBaseContext())) {
                    //callUiItemAPI();
                } else {
                    generateFailLoginDialog("Koneksi ke jaringan tidak terdeteksi, Coba lagi setelah Anda terhubung ke jaringan");
                }
            }
        });

        forgetpasswordca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLoginPage) {
                    changeToForgetPasswordState();
                } else {
                    changeToLoginState();
                }
            }
        });
        changeToLoginState();
    }

    private void generateImeiDialog() {
        TextView tv_content, tv_subtitle, tv_title;
        ImageView ellipse, dialog_image;
        View customView = LayoutInflater.from(this).inflate(com.gandsoft.irsms.R.layout.dialog_imei, null);
        ellipse = (ImageView) customView.findViewById(com.gandsoft.irsms.R.id.ellipse);
        dialog_image = (ImageView) customView.findViewById(com.gandsoft.irsms.R.id.dialog_image);
        tv_content = (TextView) customView.findViewById(com.gandsoft.irsms.R.id.tv_content);
        tv_subtitle = (TextView) customView.findViewById(com.gandsoft.irsms.R.id.tv_subtitle);
        tv_title = (TextView) customView.findViewById(com.gandsoft.irsms.R.id.tv_title);
        tv_content.setText(SystemUtil.getImei(this));
        DialogUtil.generateCustomDialogInfo(this, customView);
    }

    private void changeToForgetPasswordState() {
        isLoginPage = false;
        loginViewContainer.setVisibility(View.GONE);
        forgotPasswordViewContainer.setVisibility(View.VISIBLE);

        tvLogDatalaka.setText(getString(com.gandsoft.irsms.R.string.datalakastate2));
        tvLogLogin.setText(getString(com.gandsoft.irsms.R.string.loginstate2));
        tvLogMasukanUsernamePassword.setText(getString(com.gandsoft.irsms.R.string.usernamepasswordstate2));
        tvLogForgetPass.setText(getString(com.gandsoft.irsms.R.string.lupa_passwordstate2));
        tvLogBtnLogin.setText(getString(com.gandsoft.irsms.R.string.send));
    }

    private void changeToLoginState() {
        isLoginPage = true;
        loginViewContainer.setVisibility(View.VISIBLE);
        forgotPasswordViewContainer.setVisibility(View.GONE);

        tvLogDatalaka.setText(getString(com.gandsoft.irsms.R.string.datalaka));
        tvLogLogin.setText(getString(com.gandsoft.irsms.R.string.login));
        tvLogMasukanUsernamePassword.setText(getString(com.gandsoft.irsms.R.string.usernamepassword));
        tvLogForgetPass.setText(getString(com.gandsoft.irsms.R.string.lupa_password));
        tvLogBtnLogin.setText(getString(com.gandsoft.irsms.R.string.login));

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemUtil.isNetworkAvailable(getBaseContext())) {
                    if (validateForm()) {
                        if (isLoginPage) {
                            doLogin(username.getText().toString().trim(), password.getText().toString().trim(), SystemUtil.getImei(LoginActivity.this));
                        } else {
                            doChangePassword(email.getText().toString());
                        }
                    }
                } else {
                    generateFailLoginDialog("Koneksi ke jaringan tidak terdeteksi, Coba lagi setelah Anda terhubung ke jaringan");
                }
            }
        });
    }

    private void doLogin(String uName, String pWod, String imei) {
        //check if data is on realm
        LoginRequestModel request = new LoginRequestModel();
        request.setUsername(uName);
        request.setPassword(pWod);
        request.setImei(imei);
        //callUiItemAPI();
        if (!checkDBlogin(request)) {
            dologinApi(request);
            Toast.makeText(this, "logged from api", Toast.LENGTH_SHORT).show();
        } else {
            moveToDashboard();
            Toast.makeText(this, "logged from local db", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkDBlogin(LoginRequestModel request) {
        if (realm.where(UserDataRealmObject.class).equalTo("username", request.getUsername()).equalTo("password", request.getPassword()).findAll().size() > 0) {
            UserDataModel consumable = new UserDataModel();
            consumable.initFromDB(realm.where(UserDataRealmObject.class).equalTo("username", request.getUsername()).equalTo("password", request.getPassword()).findFirst());
            SessionManager.saveUserData(consumable);
            return true;
        } else {
            return false;
        }
    }

    private void doChangePassword(String email) {
        //Toast.makeText(this, "do changePassword: " + email, Toast.LENGTH_SHORT).show();
        ResetPasswordRequestModel request = new ResetPasswordRequestModel();
        request.setEmail(email);
        doChangePasswordApi(request);
    }

    private boolean validateForm() {
        boolean isValid = true;

        if (isLoginPage) {
            if (isValid &&
                    (username.getText().toString().trim().isEmpty() ||
                            username.getText().toString().trim().length() == 0 ||
                            username.getText().toString().trim().equals("") ||
                            username.getText().toString().trim() == null)) {
                username.setError(this.getString(com.gandsoft.irsms.R.string.dialog_tv_error_empty));
                isValid = false;
            }


            if (isValid &&
                    (password.getText().toString().trim().isEmpty() ||
                            password.getText().toString().trim().length() == 0 ||
                            password.getText().toString().trim().equals("") ||
                            password.getText().toString().trim() == null)) {
                password.setError(this.getString(com.gandsoft.irsms.R.string.dialog_tv_error_empty));
                isValid = false;
            }

            if (isValid &&
                    (password.getText().toString().trim().length() <= 8)) {
                password.setError(this.getString(com.gandsoft.irsms.R.string.dialog_tv_password_error_length));
                isValid = false;
            }


        } else {
            String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);

            if (isValid &&
                    (email.getText().toString().trim().isEmpty() ||
                            email.getText().toString().trim().length() == 0 ||
                            email.getText().toString().trim().equals("") ||
                            email.getText().toString().trim() == null)) {
                email.setError(this.getString(com.gandsoft.irsms.R.string.dialog_tv_error_empty));
                isValid = false;
            }

            if (isValid && !pattern.matcher(email.getText().toString()).matches()) {
                email.setError(this.getString(com.gandsoft.irsms.R.string.dialog_tv_username_error_mismatch));
                isValid = false;
            }
        }

        return isValid;

    }

    private void moveToDashboard() {
        Intent goToDashBoard = new Intent(this, DashboardActivity.class);
        startActivity(goToDashBoard);
        finish();
    }

    private void onLoginFail() {
        Toast.makeText(this, "login fail", Toast.LENGTH_SHORT).show();
    }

    private void dologinApi(final LoginRequestModel requestModel) {
        showApiProgressDialog(IrsmsApp.getAppComponent(), new AuthDao(this) {
            @Override
            public void call() {
                super.doLoginDao(LoginActivity.this, requestModel, BaseDao.getInstance(LoginActivity.this, IConfig.API_CALLER_LOGIN_CODE).callback);
            }
        });
    }

    private void doChangePasswordApi(final ResetPasswordRequestModel requestModel) {
        showApiProgressDialog(IrsmsApp.getAppComponent(), new AuthDao(this) {
            @Override
            public void call() {
                super.doResetPasswordDao(LoginActivity.this, requestModel, BaseDao.getInstance(LoginActivity.this, IConfig.API_CALLER_FASWORD_CODE).callback);
            }
        });
    }

    private void removeUser(String username) {
        try {
            realm.where(UserDataRealmObject.class).equalTo("username", username).findAll().deleteAllFromRealm();
            Toast.makeText(this, "deleted from db " + username, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            //Log.e("", "username not found");
        }
    }

    private void generatePositiveDialog(String title, String subtitle, String content) {
        TextView tv_content, tv_subtitle, tv_title;
        ImageView ellipse, dialog_image;
        View customView = LayoutInflater.from(this).inflate(com.gandsoft.irsms.R.layout.dialog_imei, null);
        ellipse = (ImageView) customView.findViewById(com.gandsoft.irsms.R.id.ellipse);
        dialog_image = (ImageView) customView.findViewById(com.gandsoft.irsms.R.id.dialog_image);
        tv_content = (TextView) customView.findViewById(com.gandsoft.irsms.R.id.tv_content);
        tv_subtitle = (TextView) customView.findViewById(com.gandsoft.irsms.R.id.tv_subtitle);
        tv_title = (TextView) customView.findViewById(com.gandsoft.irsms.R.id.tv_title);
        tv_content.setText(content);

        dialog_image.setImageResource(com.gandsoft.irsms.R.drawable.ic_check_88dp);
        ellipse.setImageResource(com.gandsoft.irsms.R.drawable.vector_ellipse_mossgreen);
        tv_title.setText(title);
        tv_title.setBackgroundColor(ContextCompat.getColor(this, com.gandsoft.irsms.R.color.colorMossGreen));
        tv_subtitle.setText(subtitle);
        DialogUtil.generateCustomDialogInfo(this, customView);
    }

    private void generateFailLoginDialog(String content) {
        TextView tv_content, tv_subtitle, tv_title;
        ImageView ellipse, dialog_image;
        View customView = LayoutInflater.from(this).inflate(com.gandsoft.irsms.R.layout.dialog_imei, null);
        ellipse = (ImageView) customView.findViewById(com.gandsoft.irsms.R.id.ellipse);
        dialog_image = (ImageView) customView.findViewById(com.gandsoft.irsms.R.id.dialog_image);
        tv_content = (TextView) customView.findViewById(com.gandsoft.irsms.R.id.tv_content);
        tv_subtitle = (TextView) customView.findViewById(com.gandsoft.irsms.R.id.tv_subtitle);
        tv_title = (TextView) customView.findViewById(com.gandsoft.irsms.R.id.tv_title);
        tv_content.setText(content);

        dialog_image.setImageResource(com.gandsoft.irsms.R.drawable.ic_error_outline_88dp);
        ellipse.setImageResource(com.gandsoft.irsms.R.drawable.vector_ellipse_yellow);
        tv_title.setText("Error");
        tv_title.setBackgroundColor(ContextCompat.getColor(this, com.gandsoft.irsms.R.color.colorYellow));

        if (isLoginPage) {
            tv_subtitle.setText("Gagal Login");
        } else {
            tv_subtitle.setText("Gagal Ubah Password");
        }
        DialogUtil.generateCustomDialogInfo(this, customView);
    }

    private void callUiItemAPI() {
        showApiProgressDialog(IrsmsApp.getAppComponent(), new MainPageDao(this) {
            @Override
            public void call() {
                super.doGetUiItemDao(LoginActivity.this, BaseDao.getInstance(LoginActivity.this, IConfig.API_CALLER_UIITEM_CODE).callback);
            }
        });
    }

    @Override
    protected void onApiResponseCallback(final BaseResponse br, int responseCode, Response response) {
        super.onApiResponseCallback(br, responseCode, response);
        if (response.isSuccessful()) {
            if (responseCode == IConfig.API_CALLER_LOGIN_CODE) {
                if (br.getStatus().equals(IConfig.RETURN_STATUS_DONE)) {
                    if (SessionManager.saveUserData(((LoginResponseModel) br).getData_user())) {
                        if (realm.where(UserDataRealmObject.class).findAll().size() <= 10) {
                            realm.executeTransactionAsync(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    UserDataRealmObject udro = new UserDataRealmObject();
                                    udro.setUsername(username.getText().toString());
                                    udro.setPassword(password.getText().toString());
                                    UserData udat = new UserData();
                                    udat.consume(((LoginResponseModel) br).getData_user());
                                    udro.setUserDetailData(udat);
                                    realm.copyToRealm(udro);
                                }
                            });
                        } else {
                            Toast.makeText(this, "user data is full, displacing oldest data", Toast.LENGTH_SHORT).show();
                            realm.executeTransactionAsync(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.where(UserDataRealmObject.class).findFirst().deleteFromRealm();
                                    UserDataRealmObject udro = new UserDataRealmObject();
                                    udro.setUsername(username.getText().toString());
                                    udro.setPassword(password.getText().toString());
                                    UserData udat = new UserData();
                                    udat.consume(((LoginResponseModel) br).getData_user());
                                    udro.setUserDetailData(udat);
                                    realm.copyToRealm(udro);
                                }
                            });
                        }
                        moveToDashboard();
                    } else {
                        onLoginFail();
                        generateFailLoginDialog("Gagal Menyimpan Data");
                    }

                } else {
                    generateFailLoginDialog(((BaseResponseModel) br).getMessage());
                }
            } else if (responseCode == IConfig.API_CALLER_FASWORD_CODE) {
                try {
                    if (br.getStatus().equals(IConfig.RETURN_STATUS_DONE)) {
                        generatePositiveDialog("Reset Password", "Berhasil", "Request Reset Password dikirim ke email");
                        removeUser(((ChangePasswordResponseModel) br).getUsername());
                    } else {
                        //generateFailLoginDialog(br.getStatus());
                        generateFailLoginDialog(((BaseResponseModel) br).getMessage());
                    }
                } catch (Exception e) {
                    generateFailLoginDialog("Error Tidak di Ketahui");
                }
            } else if (responseCode == IConfig.API_CALLER_UIITEM_CODE) {
                if (br.getStatus().equals(IConfig.RETURN_STATUS_DONE)) {
                    SessionManager.saveValidateData(((BaseResponseModel) br));
                    int totalUi = ((BaseResponseModel) br).getTotal();
                    Log.d("Lihat onApiResponseCallback LoginActivity", String.valueOf(totalUi));
                    int versionUi = ((BaseResponseModel) br).getVersion();
                    Log.d("Lihat onApiResponseCallback LoginActivity", String.valueOf(versionUi));
                    if (SessionManager.checkIfExist(ISeasonConfig.KEY_UITEM)) {
                        if (SessionManager.checkIfExist(ISeasonConfig.KEY_UITEM_VERSION) && !SessionManager.isVersionDataUpdate(br)) {
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_TOTAL, totalUi);
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_VERSION, versionUi);
                            Toast.makeText(getBaseContext(), "Data sekarang sudah yang terbaru, silahkan gunakan aplikasi", Toast.LENGTH_SHORT).show();
                        } else if (!SessionManager.checkIfExist(ISeasonConfig.KEY_UITEM_VERSION) && SessionManager.isVersionDataUpdate(br)) {
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_TOTAL, totalUi);
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_VERSION, versionUi);
                            ResponseUtil.replaceFormData(br, getBaseContext());
                        } else if (SessionManager.checkIfExist(ISeasonConfig.KEY_UITEM_VERSION) && SessionManager.isVersionDataUpdate(br)) {
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_TOTAL, totalUi);
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_VERSION, versionUi);
                            ResponseUtil.replaceFormData(br, getBaseContext());
                        } else if (!SessionManager.checkIfExist(ISeasonConfig.KEY_UITEM_VERSION) && !SessionManager.isVersionDataUpdate(br)) {
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_TOTAL, totalUi);
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_VERSION, versionUi);
                        }
                    } else {
                        ResponseUtil.addFormData(br, LoginActivity.this);
                    }
                }
            } else {
                generateFailLoginDialog("Koneksi Tidak di Ketahui");
            }
        } else {
            generateFailLoginDialog("Gagal Terhubung ke Server");
        }
    }

    @Override
    protected void onApiFailureCallback(String message) {
        super.onApiFailureCallback(message);
    }

    private void initPermissions() {
        Nammu.init(getApplicationContext());

        if (!Nammu.checkPermission(Manifest.permission.READ_PHONE_STATE)) {
            Nammu.askForPermission(this, Manifest.permission.READ_PHONE_STATE, new PermissionCallback() {
                @Override
                public void permissionGranted() {

                }

                @Override
                public void permissionRefused() {
                    finish();
                }
            });
        }

        if (!Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            Nammu.askForPermission(this, Manifest.permission.ACCESS_FINE_LOCATION, new PermissionCallback() {
                @Override
                public void permissionGranted() {

                }

                @Override
                public void permissionRefused() {
                    finish();
                }
            });
        }

        if (!Nammu.checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            Nammu.askForPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION, new PermissionCallback() {
                @Override
                public void permissionGranted() {

                }

                @Override
                public void permissionRefused() {
                    finish();
                }
            });
        }

        if (!Nammu.checkPermission(Manifest.permission.CAMERA)) {
            Nammu.askForPermission(this, Manifest.permission.CAMERA, new PermissionCallback() {
                @Override
                public void permissionGranted() {

                }

                @Override
                public void permissionRefused() {
                    finish();
                }
            });
        }

        if (!Nammu.checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Nammu.askForPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE, new PermissionCallback() {
                @Override
                public void permissionGranted() {

                }

                @Override
                public void permissionRefused() {
                    finish();
                }
            });
        }

        if (!Nammu.checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Nammu.askForPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE, new PermissionCallback() {
                @Override
                public void permissionGranted() {

                }

                @Override
                public void permissionRefused() {
                    finish();
                }
            });
        }

        if (!Nammu.checkPermission(Manifest.permission.CAMERA)) {
            Nammu.askForPermission(this, Manifest.permission.CAMERA, new PermissionCallback() {
                @Override
                public void permissionGranted() {

                }

                @Override
                public void permissionRefused() {
                    finish();
                }
            });
        }

        if (!Nammu.checkPermission(Manifest.permission.ACCESS_NETWORK_STATE)) {
            Nammu.askForPermission(this, Manifest.permission.ACCESS_NETWORK_STATE, new PermissionCallback() {
                @Override
                public void permissionGranted() {

                }

                @Override
                public void permissionRefused() {

                }
            });
        }
    }


}
