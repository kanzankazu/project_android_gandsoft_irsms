package com.gandsoft.irsms.Activity.AddAccident.sub;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.gandsoft.irsms.R;
import com.gandsoft.irsms.Support.Util.GeocoderUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import app.beelabs.com.codebase.base.BaseActivity;

/**
 * Created by glenn on 2/6/18.
 */

public class SetAccidentMapActivity extends BaseActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private double lat, lng;
    private TextView tvKordintaLakaLatLng, tvKordintaLakaAddress;
    private View ivKordintaLakaClose, llKordintaLakaDone;
    private String addr;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_accident_maps);

        Intent intent = getIntent();
        lat = intent.getDoubleExtra("lat", 0);
        lng = intent.getDoubleExtra("lng", 0);
        addr = intent.getStringExtra("address");
        Log.d("Lihat onCreate SetAccidentMapActivity", String.valueOf(lat));
        Log.d("Lihat onCreate SetAccidentMapActivity", String.valueOf(lng));

        initComponent();
        initContent();
    }

    private void initComponent() {
        tvKordintaLakaAddress = findViewById(R.id.tvKordintaLakaAddress);
        tvKordintaLakaLatLng = findViewById(R.id.tvKordintaLakaLatLng);
        llKordintaLakaDone = findViewById(R.id.llKordintaLakaDone);
        ivKordintaLakaClose = findViewById(R.id.ivKordintaLakaClose);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void initContent() {
        tvKordintaLakaAddress.setText(addr);
        tvKordintaLakaLatLng.setText(String.valueOf(lat) + "," + String.valueOf(lng));

        View.OnClickListener onclklstnr = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent;
                switch (view.getId()) {
                    case R.id.ivKordintaLakaClose:
                        returnIntent = new Intent();
                        setResult(Activity.RESULT_CANCELED, returnIntent);
                        finish();
                        break;
                    case R.id.llKordintaLakaDone:
                        returnIntent = new Intent();
                        returnIntent.putExtra("lat", lat);
                        returnIntent.putExtra("lng", lng);
                        returnIntent.putExtra("address", tvKordintaLakaAddress.getText().toString());
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                        break;
                }
            }
        };
        llKordintaLakaDone.setOnClickListener(onclklstnr);
        ivKordintaLakaClose.setOnClickListener(onclklstnr);
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng curloc = new LatLng(lat, lng);
        mMap.addMarker(new MarkerOptions().position(curloc).title("Lokasi Dipilih").draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(curloc, 15));

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
                tvKordintaLakaAddress.setText("-");
                tvKordintaLakaLatLng.setText("-");
            }

            @Override
            public void onMarkerDrag(Marker marker) {
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                lat = marker.getPosition().latitude;
                lng = marker.getPosition().longitude;
                tvKordintaLakaAddress.setText(GeocoderUtil.getAddressFromlatlng(SetAccidentMapActivity.this, marker.getPosition().latitude, marker.getPosition().longitude));
                tvKordintaLakaLatLng.setText(String.valueOf(marker.getPosition().latitude) + "," + String.valueOf(marker.getPosition().longitude));
            }
        });
    }


}
