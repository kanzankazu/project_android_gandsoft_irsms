package com.gandsoft.irsms.Activity.summary.sub;

import android.app.Activity;

import com.gandsoft.irsms.model.RealmDbModel.WitnessForm.SetWitnessDBModel;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

/**
 * Created by glenn on 2/6/18.
 */

public class WitnessItemRecyclerviewAdapter extends RecyclerView.Adapter<WitnessItemRecyclerviewAdapter.viewHolder> {

    private List<SetWitnessDBModel> models;
    private WitnessObjectHookInteface parent;

    public WitnessItemRecyclerviewAdapter(Activity parent, List<SetWitnessDBModel> models) {
        this.models = models;
        try {
            this.parent = (WitnessObjectHookInteface) parent;
        } catch (Exception e) {
            this.parent = null;
        }
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(com.gandsoft.irsms.R.layout.list_layout_summary_witness, parent, false);
        return new WitnessItemRecyclerviewAdapter.viewHolder(itemView);
    }

    public class viewHolder extends RecyclerView.ViewHolder {

        private final TextView tvSummaryDataSaksiNamafvbi, tvSummaryDataSaksiJenisKelaminfvbi, tvSummaryDataSaksiUmurfvbi, no;
        private final ImageButton ibSummaryDataSaksiDeletefvbi, ibSummaryDataSaksiEditfvbi;

        public viewHolder(View view) {
            super(view);
            tvSummaryDataSaksiNamafvbi = view.findViewById(com.gandsoft.irsms.R.id.tvSummaryDataSaksiNama);
            tvSummaryDataSaksiJenisKelaminfvbi = view.findViewById(com.gandsoft.irsms.R.id.tvSummaryDataSaksiJenisKelamin);
            tvSummaryDataSaksiUmurfvbi = view.findViewById(com.gandsoft.irsms.R.id.tvSummaryDataSaksiUmur);
            ibSummaryDataSaksiDeletefvbi = view.findViewById(com.gandsoft.irsms.R.id.ibSummaryDataSaksiDelete);
            ibSummaryDataSaksiEditfvbi = view.findViewById(com.gandsoft.irsms.R.id.ibSummaryDataSaksiEdit);
            no = view.findViewById(com.gandsoft.irsms.R.id.no);
        }
    }

    @Override
    public void onBindViewHolder(WitnessItemRecyclerviewAdapter.viewHolder holder, final int position) {
        SetWitnessDBModel model = models.get(position);
        holder.no.setText((position + 1) + ".");
        holder.tvSummaryDataSaksiNamafvbi.setText(model.getFirst_name() + " " + model.getLast_name());
        holder.tvSummaryDataSaksiJenisKelaminfvbi.setText(model.getGender_id_text());
        holder.tvSummaryDataSaksiUmurfvbi.setText(model.getAge());
        holder.ibSummaryDataSaksiEditfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parent.editWitnessRealmModelAt(position);
            }
        });
        holder.ibSummaryDataSaksiDeletefvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parent.deleteWitnessRealmModelAt(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public void updateModel(List<SetWitnessDBModel> models) {
        this.models = models;
    }


}
