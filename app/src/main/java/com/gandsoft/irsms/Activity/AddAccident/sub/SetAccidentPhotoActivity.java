package com.gandsoft.irsms.Activity.AddAccident.sub;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.gandsoft.irsms.Activity.AddAccident.LocalConfig;
import com.gandsoft.irsms.Support.Util.SystemUtil;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import app.beelabs.com.codebase.base.BaseActivity;

/**
 * Created by glenn on 2/6/18.
 */

public class SetAccidentPhotoActivity extends BaseActivity implements LocalConfig {

    private View ivUploadFotoClose, llUploadFotoDone;
    private View llUploadFotoGaleri, llUploadFotoTakePhoto;

    private ImageView ivUploadFotoMain;
    private ImageView ivUploadFoto1, ivUploadFoto2, ivUploadFoto3, ivUploadFoto4, ivUploadFoto5;
    private View ivUploadFoto1del, ivUploadFoto2del, ivUploadFoto3del, ivUploadFoto4del, ivUploadFoto5del;
    private ArrayList<String> Base64Store = new ArrayList<>();

    private boolean isActive = false;
    private String imageFilePath;
    private File imageFile;
    private Uri imageFileUri;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (!isActive) {
            super.onCreate(savedInstanceState);
            setContentView(com.gandsoft.irsms.R.layout.activity_form_accident_upload_photo);
            initComponent();
            Intent state = getIntent();
            if (state.hasExtra("shyu")) {
                String[] imgstate = state.getExtras().getStringArray("shyu");
                for (String base64 :
                        imgstate) {
                    Base64Store.add(base64);
                }
                CheckForSlot();
                ivUploadFotoMain.setImageBitmap(SystemUtil.Base64ToBitmap(Base64Store.get(0)));
            }

            initContent();
            isActive = true;
        }
    }

    private void initComponent() {
        ivUploadFotoClose = findViewById(com.gandsoft.irsms.R.id.ivUploadFotoClose);
        llUploadFotoDone = findViewById(com.gandsoft.irsms.R.id.llUploadFotoDone);
        llUploadFotoGaleri = findViewById(com.gandsoft.irsms.R.id.llUploadFotoGaleri);
        llUploadFotoTakePhoto = findViewById(com.gandsoft.irsms.R.id.llUploadFotoTakePhoto);

        ivUploadFotoMain = findViewById(com.gandsoft.irsms.R.id.ivUploadFotoMain);
        ivUploadFoto1 = findViewById(com.gandsoft.irsms.R.id.ivUploadFoto1);
        ivUploadFoto1.setTag(true);
        ivUploadFoto2 = findViewById(com.gandsoft.irsms.R.id.ivUploadFoto2);
        ivUploadFoto2.setTag(true);
        ivUploadFoto3 = findViewById(com.gandsoft.irsms.R.id.ivUploadFoto3);
        ivUploadFoto3.setTag(true);
        ivUploadFoto4 = findViewById(com.gandsoft.irsms.R.id.ivUploadFoto4);
        ivUploadFoto4.setTag(true);
        ivUploadFoto5 = findViewById(com.gandsoft.irsms.R.id.ivUploadFoto5);
        ivUploadFoto5.setTag(true);

        ivUploadFoto1del = findViewById(com.gandsoft.irsms.R.id.ivUploadFoto1del);
        ivUploadFoto2del = findViewById(com.gandsoft.irsms.R.id.ivUploadFoto2del);
        ivUploadFoto3del = findViewById(com.gandsoft.irsms.R.id.ivUploadFoto3del);
        ivUploadFoto4del = findViewById(com.gandsoft.irsms.R.id.ivUploadFoto4del);
        ivUploadFoto5del = findViewById(com.gandsoft.irsms.R.id.ivUploadFoto5del);
    }

    private void initContent() {
        View.OnClickListener clklistnr = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent;
                switch (view.getId()) {
                    case com.gandsoft.irsms.R.id.ivUploadFotoClose:
                        returnIntent = new Intent();
                        setResult(Activity.RESULT_CANCELED, returnIntent);
                        finish();
                        break;
                    case com.gandsoft.irsms.R.id.llUploadFotoDone:
                        returnIntent = new Intent();
                        String[] Base64Ar = new String[Base64Store.size()];
                        Base64Ar = Base64Store.toArray(Base64Ar);
                        Log.d("Lihat onClick SetAccidentPhotoActivity", Arrays.toString(Base64Ar));
                        returnIntent.putExtra("jink", Base64Ar);
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                        break;
                    case com.gandsoft.irsms.R.id.llUploadFotoGaleri:
                        if (Base64Store.size() < 5) {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECTOR_IMAGE_INTENT_ID);
                        } else {
                            Toast.makeText(SetAccidentPhotoActivity.this, "Slot penyimpanan gambar telah habis," +
                                    "\n hapus gambar yg tidak dibutuhkan untuk menambah kembail gambar", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case com.gandsoft.irsms.R.id.llUploadFotoTakePhoto:
                        if (Base64Store.size() < 5) {
                            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                                imageFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/picture.jpg";
                                imageFile = new File(imageFilePath);
                                imageFileUri = Uri.fromFile(imageFile); // convert path to Uri
                                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                cameraIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, imageFileUri);
                                startActivityForResult(cameraIntent, TAKE_PHOTO_INTENT_ID_KITKAT);
                            } else {
                                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, TAKE_PHOTO_INTENT_ID);
                            }
                        } else {
                            Toast.makeText(SetAccidentPhotoActivity.this, "Slot penyimpanan gambar telah habis," +
                                    "\n hapus gambar yg tidak dibutuhkan untuk menambah kembail gambar", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case com.gandsoft.irsms.R.id.ivUploadFoto1del:
                        if (Base64Store.size() >= 1) {
                            Base64Store.remove(0);
                            ClearSlot();
                            CheckForSlot();
                        }
                        break;
                    case com.gandsoft.irsms.R.id.ivUploadFoto2del:
                        if (Base64Store.size() >= 2) {
                            Base64Store.remove(1);
                            ClearSlot();
                            CheckForSlot();
                        }
                        break;
                    case com.gandsoft.irsms.R.id.ivUploadFoto3del:
                        if (Base64Store.size() >= 3) {
                            Base64Store.remove(2);
                            ClearSlot();
                            CheckForSlot();
                        }
                        break;
                    case com.gandsoft.irsms.R.id.ivUploadFoto4del:
                        if (Base64Store.size() >= 4) {
                            Base64Store.remove(3);
                            ClearSlot();
                            CheckForSlot();
                        }
                        break;
                    case com.gandsoft.irsms.R.id.ivUploadFoto5del:
                        if (Base64Store.size() >= 5) {
                            Base64Store.remove(4);
                            ClearSlot();
                            CheckForSlot();
                        }
                        break;
                    case com.gandsoft.irsms.R.id.ivUploadFoto1:
                        if (Base64Store.size() >= 1) {
                            ivUploadFotoMain.setImageBitmap(SystemUtil.Base64ToBitmap(Base64Store.get(0)));
                        }
                        break;
                    case com.gandsoft.irsms.R.id.ivUploadFoto2:
                        if (Base64Store.size() >= 2) {
                            ivUploadFotoMain.setImageBitmap(SystemUtil.Base64ToBitmap(Base64Store.get(1)));
                        }
                        break;
                    case com.gandsoft.irsms.R.id.ivUploadFoto3:
                        if (Base64Store.size() >= 3) {
                            ivUploadFotoMain.setImageBitmap(SystemUtil.Base64ToBitmap(Base64Store.get(2)));
                        }
                        break;
                    case com.gandsoft.irsms.R.id.ivUploadFoto4:
                        if (Base64Store.size() >= 4) {
                            ivUploadFotoMain.setImageBitmap(SystemUtil.Base64ToBitmap(Base64Store.get(3)));
                        }
                        break;
                    case com.gandsoft.irsms.R.id.ivUploadFoto5:
                        if (Base64Store.size() >= 5) {
                            ivUploadFotoMain.setImageBitmap(SystemUtil.Base64ToBitmap(Base64Store.get(4)));
                        }
                        break;
                    case com.gandsoft.irsms.R.id.ivUploadFotoMain:

                        break;
                    default:
                        returnIntent = new Intent();
                        setResult(Activity.RESULT_CANCELED, returnIntent);
                        finish();
                        break;
                }
            }
        };

        ivUploadFotoClose.setOnClickListener(clklistnr);
        llUploadFotoDone.setOnClickListener(clklistnr);
        llUploadFotoGaleri.setOnClickListener(clklistnr);
        llUploadFotoTakePhoto.setOnClickListener(clklistnr);

        ivUploadFoto1.setOnClickListener(clklistnr);
        ivUploadFoto2.setOnClickListener(clklistnr);
        ivUploadFoto3.setOnClickListener(clklistnr);
        ivUploadFoto4.setOnClickListener(clklistnr);
        ivUploadFoto5.setOnClickListener(clklistnr);

        ivUploadFoto1del.setOnClickListener(clklistnr);
        ivUploadFoto2del.setOnClickListener(clklistnr);
        ivUploadFoto3del.setOnClickListener(clklistnr);
        ivUploadFoto4del.setOnClickListener(clklistnr);
        ivUploadFoto5del.setOnClickListener(clklistnr);
    }

    private void ClearSlot() {
        ivUploadFoto1.setImageResource(com.gandsoft.irsms.R.drawable.img_placeholder);
        ivUploadFoto1.setTag(true);
        ivUploadFoto2.setImageResource(com.gandsoft.irsms.R.drawable.img_placeholder);
        ivUploadFoto2.setTag(true);
        ivUploadFoto3.setImageResource(com.gandsoft.irsms.R.drawable.img_placeholder);
        ivUploadFoto3.setTag(true);
        ivUploadFoto4.setImageResource(com.gandsoft.irsms.R.drawable.img_placeholder);
        ivUploadFoto4.setTag(true);
        ivUploadFoto5.setImageResource(com.gandsoft.irsms.R.drawable.img_placeholder);
        ivUploadFoto5.setTag(true);
        ivUploadFoto1del.setVisibility(View.GONE);
        ivUploadFoto2del.setVisibility(View.GONE);
        ivUploadFoto3del.setVisibility(View.GONE);
        ivUploadFoto4del.setVisibility(View.GONE);
        ivUploadFoto5del.setVisibility(View.GONE);
        ivUploadFotoMain.setImageResource(com.gandsoft.irsms.R.drawable.img_placeholder);
    }

    private void CheckForSlot() {
        if ((boolean) ivUploadFoto1.getTag() && Base64Store.size() >= 1) {
            ivUploadFoto1.setImageBitmap(SystemUtil.Base64ToBitmap(Base64Store.get(0)));
            ivUploadFoto1.setTag(false);
            ivUploadFoto1del.setVisibility(View.VISIBLE);
        }
        if ((boolean) ivUploadFoto2.getTag() && Base64Store.size() >= 2) {
            ivUploadFoto2.setImageBitmap(SystemUtil.Base64ToBitmap(Base64Store.get(1)));
            ivUploadFoto2.setTag(false);
            ivUploadFoto2del.setVisibility(View.VISIBLE);
        }
        if ((boolean) ivUploadFoto3.getTag() && Base64Store.size() >= 3) {
            ivUploadFoto3.setImageBitmap(SystemUtil.Base64ToBitmap(Base64Store.get(2)));
            ivUploadFoto3.setTag(false);
            ivUploadFoto3del.setVisibility(View.VISIBLE);
        }
        if ((boolean) ivUploadFoto4.getTag() && Base64Store.size() >= 4) {
            ivUploadFoto4.setImageBitmap(SystemUtil.Base64ToBitmap(Base64Store.get(3)));
            ivUploadFoto4.setTag(false);
            ivUploadFoto4del.setVisibility(View.VISIBLE);
        }
        if ((boolean) ivUploadFoto5.getTag() && Base64Store.size() >= 5) {
            ivUploadFoto5.setImageBitmap(SystemUtil.Base64ToBitmap(Base64Store.get(4)));
            ivUploadFoto5.setTag(false);
            ivUploadFoto5del.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList("bod@", Base64Store);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            Base64Store = savedInstanceState.getStringArrayList("bod@");
            CheckForSlot();
            ivUploadFotoMain.setImageBitmap(SystemUtil.Base64ToBitmap(Base64Store.get(0)));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri selectedImage;
        int sizePic = 150;
        int compressPic = 100;
        switch (requestCode) {
            case SELECTOR_IMAGE_INTENT_ID:
                if (resultCode == RESULT_OK) {
                    Uri selectedImageUri = data.getData();
                    if (Build.VERSION.SDK_INT < 19) {
                        String selectedImagePath = getPath(selectedImageUri);
                        Bitmap bitmap = BitmapFactory.decodeFile(selectedImagePath);

                        Bitmap storedata = SystemUtil.resizeImage(bitmap, sizePic);
                        ClearSlot();
                        //ivUploadFotoMain.setImageBitmap(bitmap);
                        ivUploadFotoMain.setImageBitmap(storedata);
                        Base64Store.add(SystemUtil.bitmapToBase64(storedata, Bitmap.CompressFormat.PNG, compressPic));
                        CheckForSlot();
                    } else {
                        ParcelFileDescriptor parcelFileDescriptor;
                        try {
                            parcelFileDescriptor = getContentResolver().openFileDescriptor(selectedImageUri, "r");
                            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
                            Bitmap bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor);
                            parcelFileDescriptor.close();

                            Bitmap storedata = SystemUtil.resizeImage(bitmap, sizePic);
                            ClearSlot();
                            //ivUploadFotoMain.setImageBitmap(bitmap);
                            ivUploadFotoMain.setImageBitmap(storedata);
                            Base64Store.add(SystemUtil.bitmapToBase64(storedata, Bitmap.CompressFormat.PNG, compressPic));
                            CheckForSlot();

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }
                break;
            case TAKE_PHOTO_INTENT_ID:
                selectedImage = SystemUtil.getUriFromResult(this, resultCode, data.getData());
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    Bitmap storedata = SystemUtil.resizeImage(bitmap, sizePic);
                    ClearSlot();
                    //ivUploadFotoMain.setImageBitmap(bitmap);
                    ivUploadFotoMain.setImageBitmap(storedata);
                    Base64Store.add(SystemUtil.bitmapToBase64(storedata, Bitmap.CompressFormat.PNG, compressPic));
                    CheckForSlot();
                } catch (Exception e) {
                    Log.d("Lihat onActivityResult SetAccidentPhotoActivity", String.valueOf(e.getMessage()));
                    //Log.e("TOD", e.getMessage());
                }
                break;
            case TAKE_PHOTO_INTENT_ID_KITKAT:
                if (resultCode == RESULT_OK) {
                    // Decode it for real
                    BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                    bmpFactoryOptions.inJustDecodeBounds = false;

                    //imageFilePath image path which you pass with intent
                    Bitmap bitmap = BitmapFactory.decodeFile(imageFilePath, bmpFactoryOptions);


                    Bitmap storedata = SystemUtil.resizeImage(bitmap, sizePic);
                    ClearSlot();
                    //ivUploadFotoMain.setImageBitmap(bitmap);
                    ivUploadFotoMain.setImageBitmap(storedata);
                    Base64Store.add(SystemUtil.bitmapToBase64(storedata, Bitmap.CompressFormat.PNG, compressPic));
                    CheckForSlot();
                }
                break;
        }
    }

    public String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }
}
