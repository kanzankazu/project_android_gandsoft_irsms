package com.gandsoft.irsms.Activity.AddAccident;

import android.app.Activity;

import com.gandsoft.irsms.Activity.AddAccident.sub.SetAccidentTypeInterface;
import com.gandsoft.irsms.model.uiModel.ViewModel;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

/**
 * Created by glenn on 2/6/18.
 */

public class AccidentTypeSelectorRecyclerviewAdapter extends RecyclerView.Adapter<AccidentTypeSelectorRecyclerviewAdapter.viewHolder> {

    private List<ViewModel> models;
    private SetAccidentTypeInterface parent;
    private boolean isMain = true;
    private int measurement = 0;

    public AccidentTypeSelectorRecyclerviewAdapter(Activity parent, List<ViewModel> models, boolean ismain) {
        this.models = models;
        try {
            this.parent = (SetAccidentTypeInterface) parent;
        } catch (Exception e) {
            this.parent = null;
        }
        isMain = ismain;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(com.gandsoft.irsms.R.layout.component_rv_addaccident_type, parent, false);
        return new viewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(viewHolder holder, int position) {
        final ViewModel model = models.get(position);
        final int pos = position;
        holder.pic.setImageResource(model.getItemResourceID());
        holder.pic.getLayoutParams().height = 200;
        holder.pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isMain) {
                    parent.setSelectedMainTypeItem(pos);
                    parent.closeDialogForResult(model.getItemResourceID(), model.getViewModelGroup().get(0).getItemResourceID(), model.getViewModelGroup().get(0).getViewID(), isMain);
                } else {
                    parent.closeDialogForResult(0, model.getItemResourceID(), model.getViewID(), isMain);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        public ImageView pic;

        public viewHolder(View view) {
            super(view);
            pic = view.findViewById(com.gandsoft.irsms.R.id.acc_img);
        }
    }

    public void updateModel(List<ViewModel> models) {
        this.models = models;
    }
}
