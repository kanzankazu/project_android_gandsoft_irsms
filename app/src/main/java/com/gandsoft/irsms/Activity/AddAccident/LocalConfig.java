package com.gandsoft.irsms.Activity.AddAccident;

/**
 * Created by gleen on 08/02/18.
 */

public interface LocalConfig {

    int ADDACCIDENT_PHOTO_UPLOAD_CODE = 200;
    int ADDACCIDENT_ACCTYPE_CODE = 202;
    int ADDACCIDENT_MAP_CODE = 203;

    int SELECTOR_IMAGE_INTENT_ID = 210;
    int TAKE_PHOTO_INTENT_ID = 220;
    int SELECTOR_IMAGE_INTENT_ID_KITKAT = 310;
    int TAKE_PHOTO_INTENT_ID_KITKAT = 320;

    int CROPPER_INTENT_ID = 211;
}
