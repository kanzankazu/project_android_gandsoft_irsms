package com.gandsoft.irsms.Activity.AddPedestrian;

import com.gandsoft.irsms.model.uiModel.ViewModel;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.gandsoft.irsms.model.uiModel.ViewModel;

import java.util.List;

/**
 * Created by gleen on 23/03/18.
 */

public class pedestrianSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

    private Context mContext;
    private List<ViewModel> models;


    public pedestrianSpinnerAdapter(Context mContext, List<ViewModel> models) {
        this.mContext = mContext;
        this.models = models;
    }

    @Override
    public int getCount() {
        return models != null ? models.size() : 0;
    }

    @Override
    public ViewModel getItem(int position) {
        return models.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ViewModel model = models.get(position);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(model.getItemResourceID(), null);
        TextView tv = (TextView) ((ViewGroup) convertView).getChildAt(1);
        tv.setText(model.getTextContent());
        return convertView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewModel model = models.get(position);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(model.getItemContainerID(), null);
        TextView tv = (TextView) ((LinearLayout) ((ViewGroup) convertView).getChildAt(0)).getChildAt(0);
        tv.setText(model.getTextContent());
        return convertView;
    }

    public void replaceModels(List<ViewModel> jink) {
        models = jink;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}