package com.gandsoft.irsms.Activity.summary;

import android.app.Activity;
import com.gandsoft.irsms.model.RealmDbModel.RequestRealmDbObject;
import com.gandsoft.irsms.model.RealmDbModel.VehicleForm.DriverDBModel;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import io.realm.Realm;

/**
 * Created by glenn on 2/6/18.
 */

public class AccidentRecyclerviewDriverAdapter extends RecyclerView.Adapter<AccidentRecyclerviewDriverAdapter.viewHolder> {

    private List<DriverDBModel> models;
    private SummaryHookInterface parent;
    private Realm realm;
    private int localID;

    public AccidentRecyclerviewDriverAdapter(Activity parent, List<DriverDBModel> models, int localID) {
        this.models = models;
        realm = Realm.getDefaultInstance();
        try {
            this.parent = (SummaryHookInterface) parent;
        } catch (Exception e) {
            this.parent = null;
        }
        this.localID = localID;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(com.gandsoft.irsms.R.layout.list_layout_summary_driver, parent, false);
        return new AccidentRecyclerviewDriverAdapter.viewHolder(itemView);
    }

    public class viewHolder extends RecyclerView.ViewHolder {

        private final ImageView ivSummaryDataKendaraanfvbi;
        private final TextView tvSummaryDataKendaraanNoRegfvbi, tvSummaryDataKendaraanKendaraanfvbi, tvSummaryDataKendaraanLukafvbi, tvSummaryDataKendaraanDataPenumpangfvbi, tvSummaryDataKendaraanNo;
        private final ImageButton ibSummaryDataKendaraanDeletefvbi, ibSummaryDataKendaraanEditfvbi;
        private final CardView tvSummaryDataKendaraanDataPenumpang_btn;

        public viewHolder(View view) {
            super(view);
            tvSummaryDataKendaraanNo = view.findViewById(com.gandsoft.irsms.R.id.tvSummaryDataKendaraanNo);
            ivSummaryDataKendaraanfvbi = view.findViewById(com.gandsoft.irsms.R.id.ibSummaryDataKendaraanEdit);
            tvSummaryDataKendaraanNoRegfvbi = view.findViewById(com.gandsoft.irsms.R.id.tvSummaryDataKendaraanNoReg);
            tvSummaryDataKendaraanKendaraanfvbi = view.findViewById(com.gandsoft.irsms.R.id.tvSummaryDataKendaraanKendaraan);
            tvSummaryDataKendaraanLukafvbi = view.findViewById(com.gandsoft.irsms.R.id.tvSummaryDataKendaraanLuka);
            tvSummaryDataKendaraanDataPenumpangfvbi = view.findViewById(com.gandsoft.irsms.R.id.tvSummaryDataKendaraanDataPenumpang);
            ibSummaryDataKendaraanDeletefvbi = view.findViewById(com.gandsoft.irsms.R.id.ibSummaryDataKendaraanDelete);
            ibSummaryDataKendaraanEditfvbi = view.findViewById(com.gandsoft.irsms.R.id.ibSummaryDataKendaraanEdit);
            tvSummaryDataKendaraanDataPenumpang_btn = view.findViewById(com.gandsoft.irsms.R.id.tvSummaryDataKendaraanDataPenumpang_btn);
        }
    }

    @Override
    public void onBindViewHolder(AccidentRecyclerviewDriverAdapter.viewHolder holder, int position) {

        DriverDBModel model = models.get(position);
        holder.tvSummaryDataKendaraanNoRegfvbi.setText(model.getPlate_no());
        holder.tvSummaryDataKendaraanKendaraanfvbi.setText(model.getVehicle_brand_text() + " " + model.getVehicle_type_text());
        holder.tvSummaryDataKendaraanLukafvbi.setText(model.getInjury_id_text());
        holder.tvSummaryDataKendaraanDataPenumpangfvbi.setText(realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localID).findFirst().getSetPassangerRequestModels().where().equalTo("vehicle_SubID", model.getVehicle_Dbid()).findAll().size() + "");
        holder.tvSummaryDataKendaraanNo.setText((position + 1) + ".");
        holder.ibSummaryDataKendaraanDeletefvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parent.deleteVehicleItemAt(model.getVehicle_Dbid());
            }
        });
        holder.ibSummaryDataKendaraanEditfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parent.editVehicleItemAt(model.getVehicle_Dbid());
            }
        });
        holder.tvSummaryDataKendaraanDataPenumpang_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parent.moveToPassangerSummary(model.getVehicle_Dbid());
            }
        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }
}
