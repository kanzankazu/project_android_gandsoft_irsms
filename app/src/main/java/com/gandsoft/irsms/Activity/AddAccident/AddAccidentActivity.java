package com.gandsoft.irsms.Activity.AddAccident;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gandsoft.irsms.Activity.AddAccident.sub.SetAccidentMapActivity;
import com.gandsoft.irsms.Activity.AddAccident.sub.SetAccidentPhotoActivity;
import com.gandsoft.irsms.Activity.AddAccident.sub.SetAccidentTypeActivity;
import com.gandsoft.irsms.Activity.LocalBaseActivity;
import com.gandsoft.irsms.Activity.summary.AccidentSummaryActivity;
import com.gandsoft.irsms.Presenter.SeasonManager.SessionManager;
import com.gandsoft.irsms.R;
import com.gandsoft.irsms.Support.Util.GeocoderUtil;
import com.gandsoft.irsms.Support.Util.SystemUtil;
import com.gandsoft.irsms.Support.ViewGenerator.ViewGenerator;
import com.gandsoft.irsms.Support.ViewGenerator.ViewGeneratorUtil;
import com.gandsoft.irsms.model.ObjectModel.UserDataModel;
import com.gandsoft.irsms.model.RealmDbModel.AccidentForm.AccidentDBModel;
import com.gandsoft.irsms.model.RealmDbModel.AccidentForm.AddAccidentStateModel;
import com.gandsoft.irsms.model.RealmDbModel.RequestRealmDbObject;
import com.gandsoft.irsms.model.uiModel.FormdataLoader;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import pl.tajchert.nammu.Nammu;

import static com.gandsoft.irsms.Support.Util.SystemUtil.isKitkat;
import static com.gandsoft.irsms.Support.Util.SystemUtil.isListContainString;
import static com.gandsoft.irsms.Support.Util.SystemUtil.isListContainStringArray;

/**
 * Created by glenn on 2/1/18.
 */

public class AddAccidentActivity extends LocalBaseActivity implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener, LocalConfig {

    private static final long MIN_TIME_BW_UPDATES = 1;
    private static final float MIN_DISTANCE_CHANGE_FOR_UPDATES = 1;
    private UserDataModel currentUser;
    private String[] base64;
    private LocationManager locationManager;
    private GpsStatus gpsStatus;
    private LinearLayout llDataLakaTitlefvbi;
    private LinearLayout llDataLakaKecelkaanMenonjol;
    private DatePickerDialog dpd;
    private TimePickerDialog tpd;
    private TextView etDataLakaTglLaka, etDataLakaJamLaka;
    private TextView tvDataLakaNamaPetugas;
    private TextView tvDataLakaNRP, tvDataLakaPolda, tvDataLakaPolres, tvDataLakaTipeLaka;
    private TextView tvDataKoordinatSat, tvDataKoordinatAcc, tvDataKoordinatLint, tvDataKoordinatBujur;
    private EditText etDataKoordinatNamaJalan;
    private TextView tvDataLakaTglLap, tvDataLakaJamLap;
    private ImageView ivDataLakaTipeLaka;
    private ImageView ivFotoLaka1, ivFotoLaka2, ivFotoLaka3, ivFotoLaka4, ivFotoLaka5;
    private EditText etDataKerusakanRugiMaterialKendaraan, etDataKerusakanRugiMaterialNonKendaraan;
    private int selected_lakatype_resid = R.drawable.img_accident_sub_a0700, selected_lakatypemain_resid, selected_lakatypemain_posid;

    private double lat, lng;
    private boolean isLocationFound;

    //nav
    private View cvDataLakaKeluarfvbi, cvDataLakaSimpanfvbi;
    private View ivFotoLaka;

    //radio
    private LinearLayout rvDataLakaKondisiCahaya, rvDataLakaKondisiCuaca,
            rvDataJalanFungsi, rvDataJalanKelas, rvDataJalanTipe, rvDataJalanBentuk,
            rvDataJalanKondisi, rvDataJalanBatasKecepatan, rvDataJalanKemiringan,
            rvDataJalanStatus, rvDataKerusakanPenyebab;

    //checkbox
    private LinearLayout rvDataLakaInfoKhusus, rvDataJalanPengaturanSimpang,
            rvDataKerusakanMaterial;

    //unique element
    private View dataLakaTipeLakaCa, bDataKoordinat;

    private int localId;
    private boolean loadedFromState = false;
    private Realm realm;

    private GoogleApiClient mGoogleApiClent;
    private boolean isGPSEnabled;
    private boolean isNetworkEnabled;
    private Location location;
    private double latitude;
    private double longitude;
    private ImageView imgvgps;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_accident);
        Nammu.init(getApplicationContext());

        if (mGoogleApiClent == null) {
            mGoogleApiClent = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(@Nullable Bundle bundle) {

                        }

                        @Override
                        public void onConnectionSuspended(int i) {

                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                        }
                    }).build();
            mGoogleApiClent.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            builder.setAlwaysShow(true);
            builder.setAlwaysShow(true);
            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClent, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                status.startResolutionForResult(AddAccidentActivity.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }
                }
            });
        }

        mGoogleApiClent = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {

                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                })
                .addOnConnectionFailedListener(connectionResult -> {

                }).build();

        localId = getIntent().getIntExtra("Local_ID", 0);
        loadedFromState = (localId > 0) ? true : false;
        Log.d("Lihat onCreate AddAccidentActivity", String.valueOf(loadedFromState));

        try {
            currentUser = SessionManager.loadUserData();
        } catch (Exception e) {
            //Log.e("load fail", e.getMessage());
            //Log.e("load fail", e.getCause().toString());
        }

        initComponent();
        initContent();

        if (loadedFromState) {
            loadState();
            initDateTimeSetting();
        } else {
            initFirstDateTime();
            initDateTimeSetting();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        localId = getIntent().getIntExtra("Local_ID", 0);
        loadedFromState = (localId > 0) ? true : false;

        if (loadedFromState) {
            loadState();
        }
    }

    private void initComponent() {
        llDataLakaTitlefvbi = (LinearLayout) findViewById(R.id.llDataLakaTitle);

        ivFotoLaka = findViewById(R.id.ivFotoLaka);
        ivFotoLaka1 = findViewById(R.id.ivFotoLaka1);
        ivFotoLaka2 = findViewById(R.id.ivFotoLaka2);
        ivFotoLaka3 = findViewById(R.id.ivFotoLaka3);
        ivFotoLaka4 = findViewById(R.id.ivFotoLaka4);
        ivFotoLaka5 = findViewById(R.id.ivFotoLaka5);

        cvDataLakaKeluarfvbi = findViewById(R.id.cvDataLakaKeluar);
        cvDataLakaSimpanfvbi = findViewById(R.id.cvDataLakaSimpan);

        etDataLakaTglLaka = findViewById(R.id.etDataLakaTglLaka);
        etDataLakaJamLaka = findViewById(R.id.etDataLakaJamLaka);

        tvDataLakaTglLap = findViewById(R.id.tvDataLakaTglLap);
        tvDataLakaJamLap = findViewById(R.id.tvDataLakaJamLap);

        tvDataLakaNamaPetugas = findViewById(R.id.tvDataLakaNamaPetugas);
        tvDataLakaNRP = findViewById(R.id.tvDataLakaNRP);
        tvDataLakaPolda = findViewById(R.id.tvDataLakaPolda);
        tvDataLakaPolres = findViewById(R.id.tvDataLakaPolres);

        llDataLakaKecelkaanMenonjol = findViewById(R.id.llDataLakaKecelkaanMenonjol);
        rvDataLakaKondisiCahaya = findViewById(R.id.rvDataLakaKondisiCahaya);
        rvDataLakaKondisiCuaca = findViewById(R.id.rvDataLakaKondisiCuaca);
        rvDataJalanFungsi = findViewById(R.id.rvDataJalanFungsi);
        rvDataJalanKelas = findViewById(R.id.rvDataJalanKelas);
        rvDataJalanTipe = findViewById(R.id.rvDataJalanTipe);
        rvDataJalanBentuk = findViewById(R.id.rvDataJalanBentuk);
        rvDataJalanKondisi = findViewById(R.id.rvDataJalanKondisi);
        rvDataJalanBatasKecepatan = findViewById(R.id.rvDataJalanBatasKecepatan);
        rvDataJalanKemiringan = findViewById(R.id.rvDataJalanKemiringan);
        rvDataJalanStatus = findViewById(R.id.rvDataJalanStatus);
        rvDataKerusakanPenyebab = findViewById(R.id.rvDataKerusakanPenyebab);

        rvDataLakaInfoKhusus = findViewById(R.id.rvDataLakaInfoKhusus);
        rvDataJalanPengaturanSimpang = findViewById(R.id.rvDataJalanPengaturanSimpang);
        rvDataKerusakanMaterial = findViewById(R.id.rvDataKerusakanMaterial);

        dataLakaTipeLakaCa = findViewById(R.id.dataLakaTipeLakaCa);
        tvDataLakaTipeLaka = findViewById(R.id.tvDataLakaTipeLaka);
        ivDataLakaTipeLaka = findViewById(R.id.ivDataLakaTipeLaka);

        bDataKoordinat = findViewById(R.id.bDataKoordinat);
        imgvgps = findViewById(R.id.imgvgps);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        tvDataKoordinatSat = findViewById(R.id.tvDataKoordinatSat);
        tvDataKoordinatAcc = findViewById(R.id.tvDataKoordinatAcc);
        tvDataKoordinatLint = findViewById(R.id.tvDataKoordinatLint);
        tvDataKoordinatBujur = findViewById(R.id.tvDataKoordinatBujur);
        etDataKoordinatNamaJalan = findViewById(R.id.etDataKoordinatNamaJalan);

        etDataKerusakanRugiMaterialKendaraan = findViewById(R.id.etDataKerusakanRugiMaterialKendaraan);
        etDataKerusakanRugiMaterialNonKendaraan = findViewById(R.id.etDataKerusakanRugiMaterialNonKendaraan);
    }

    private void initContent() {

        if (isKitkat()) {
            llDataLakaTitlefvbi.bringToFront();
        }

        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getKecelakaan_menonjol(), llDataLakaKecelkaanMenonjol);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getKondisi_cahaya(), rvDataLakaKondisiCahaya);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getCuaca(), rvDataLakaKondisiCuaca);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getFungsi_jalan(), rvDataJalanFungsi);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getKelas_jalan(), rvDataJalanKelas);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getTipe_jalan(), rvDataJalanTipe);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getBentuk_geometri(), rvDataJalanBentuk);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getKondisi_permukaan(), rvDataJalanKondisi);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getBatas_kecepatan(), rvDataJalanBatasKecepatan);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getKemiringan_jalan(), rvDataJalanKemiringan);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getStatus_jalan(), rvDataJalanStatus);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getPenyebab(), rvDataKerusakanPenyebab);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getInformasi_khusus(), rvDataLakaInfoKhusus);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getPengaturan_simpang(), rvDataJalanPengaturanSimpang);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getKermat(), rvDataKerusakanMaterial);

        cvDataLakaKeluarfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //finish();
                onBackPressed();
            }
        });
        cvDataLakaSimpanfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (localId != 0) {
                    overwriteData();
                    finish();
                } else {
                    if (saveData()) {
                        Intent summaryIntent = new Intent(AddAccidentActivity.this, AccidentSummaryActivity.class);
                        summaryIntent.putExtra("Local_ID", localId);
                        startActivity(summaryIntent);
                        finish();
                    }
                }*/
                saveOverwrite();
            }
        });

        try {
            tvDataLakaNamaPetugas.setText(currentUser.getFirst_name() + " " + currentUser.getLast_name());
            tvDataLakaNRP.setText(currentUser.getOfficer_id() + "(" + currentUser.getFirst_name() + ")");
            tvDataLakaPolda.setText(currentUser.getPolda_name());
            tvDataLakaPolres.setText(currentUser.getPolres_name());
        } catch (Exception ignored) {

        }

        ivFotoLaka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToImageUpload();
            }
        });

        etDataLakaTglLaka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dpd.show(getFragmentManager(), "jngprd");
            }
        });
        etDataLakaJamLaka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tpd.show(getFragmentManager(), "cngpd");
            }
        });

        ViewGeneratorUtil.setRadioCheckedAt(llDataLakaKecelkaanMenonjol, 1);

        dataLakaTipeLakaCa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToSetAccidentType();
            }
        });

        bDataKoordinat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLocationFound) {
                    moveToSetMap();
                }
            }
        });
        imgvgps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLocationFound) {
                    moveToSetMap();
                }
            }
        });

        if (!loadedFromState) {
            getGpsLocation();
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private void loadState() {
        realm = Realm.getDefaultInstance();

        AddAccidentStateModel state = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst().getAddAccidentStateModel().get(0);

        checkData(state);
        /*//rb
        ViewGeneratorUtil.setRadioCheckedAt(llDataLakaKecelkaanMenonjol, state.getKecelakaan_menonjol());
        ViewGeneratorUtil.setRadioCheckedAt(rvDataLakaKondisiCahaya, state.getKondisi_cahaya());
        ViewGeneratorUtil.setRadioCheckedAt(rvDataLakaKondisiCuaca, state.getCuaca());
        ViewGeneratorUtil.setRadioCheckedAt(rvDataJalanFungsi, state.getFungsi_jalan());
        ViewGeneratorUtil.setRadioCheckedAt(rvDataJalanKelas, state.getKelas_jalan());
        ViewGeneratorUtil.setRadioCheckedAt(rvDataJalanTipe, state.getTipe_jalan());
        ViewGeneratorUtil.setRadioCheckedAt(rvDataJalanBentuk, state.getBentuk_geometri());
        ViewGeneratorUtil.setRadioCheckedAt(rvDataJalanKondisi, state.getKondisi_permukaan());
        ViewGeneratorUtil.setRadioCheckedAt(rvDataJalanBatasKecepatan, state.getBatas_kecepatan());
        ViewGeneratorUtil.setRadioCheckedAt(rvDataJalanKemiringan, state.getKemiringan_jalan());
        ViewGeneratorUtil.setRadioCheckedAt(rvDataJalanStatus, state.getStatus_jalan());
        ViewGeneratorUtil.setRadioCheckedAt(rvDataKerusakanPenyebab, state.getPenyebab());

        //cb
        ViewGeneratorUtil.setCheckboxCheckedAt(rvDataLakaInfoKhusus, state.getInformasi_khusus());
        ViewGeneratorUtil.setCheckboxCheckedAt(rvDataJalanPengaturanSimpang, state.getPengaturan_simpang());
        ViewGeneratorUtil.setCheckboxCheckedAt(rvDataKerusakanMaterial, state.getKerusakan_material());*/

        //et
        etDataLakaTglLaka.setText(state.getTanggal_kejadian());
        etDataLakaJamLaka.setText(state.getJam_kejadian());
        tvDataLakaTglLap.setText(state.getTanggal_dilaporkan());
        tvDataLakaJamLap.setText(state.getJam_dilaporkan());
        base64 = state.getPhotoBase64();
        etDataKerusakanRugiMaterialKendaraan.setText(state.getTotal_nilai_rugi_kendaraan());
        etDataKerusakanRugiMaterialNonKendaraan.setText(state.getTotal_nilai_rugi_non_kendaraan());

        tvDataLakaTipeLaka.setText(state.getTipe_kecelakaan());
        ivDataLakaTipeLaka.setImageResource(state.getTipe_kecelakaan_id());

        etDataKoordinatNamaJalan.setText(state.getNama_jalan());
        try {
            tvDataKoordinatLint.setText(state.getLatitude().toString());
            tvDataKoordinatBujur.setText(state.getLongitude().toString());
            lat = state.getGps()[0];
            lng = state.getGps()[1];
            isLocationFound = true;
        } catch (Exception e) {
            //Log.e("TOT", e.getMessage());
            getGpsLocation();
        }

        if (!isLocationFound) {
            getGpsLocation();
        }

        selected_lakatype_resid = state.getTipe_kecelakaan_id();
        selected_lakatypemain_resid = state.getTipe_kecelakaan_mainid();
        selected_lakatypemain_posid = state.getTipe_kecelakaan_posid();

        refreshFotolakaSlot(base64);
    }

    private AccidentDBModel generateRequestModel(@Nullable AccidentDBModel requestModel) {
        if (requestModel == null) {
            requestModel = new AccidentDBModel();
        }
        requestModel.setImei(SystemUtil.getImei(AddAccidentActivity.this));
        //radiogroup
        requestModel.setPetugas_pelapor(currentUser.getOfficer_id() + "");
        requestModel.setCreated_by(currentUser.getUser_id() + "");
        requestModel.setPolres(currentUser.getPolres_id());
        requestModel.setTanggal_kejadian(etDataLakaTglLaka.getText().toString());
        requestModel.setJam_kejadian(etDataLakaJamLaka.getText().toString());
        requestModel.setTanggal_dilaporkan(tvDataLakaTglLap.getText().toString());
        requestModel.setJam_dilaporkan(tvDataLakaJamLap.getText().toString());
        requestModel.setInformasi_khusus(ViewGeneratorUtil.getSelectedCheckboxValue(rvDataLakaInfoKhusus));
        requestModel.setTipe_kecelakaan(tvDataLakaTipeLaka.getText().toString());
        requestModel.setKecelakaan_menonjol(ViewGeneratorUtil.getSelectedRadioValue(llDataLakaKecelkaanMenonjol));
        requestModel.setKondisi_cahaya(ViewGeneratorUtil.getSelectedRadioValue(rvDataLakaKondisiCahaya));
        requestModel.setCuaca(ViewGeneratorUtil.getSelectedRadioValue(rvDataLakaKondisiCuaca));
        requestModel.setLatitude(tvDataKoordinatLint.getText().toString());
        requestModel.setLongitude(tvDataKoordinatBujur.getText().toString());
        requestModel.setNama_jalan(etDataKoordinatNamaJalan.getText().toString());
        requestModel.setFungsi_jalan(ViewGeneratorUtil.getSelectedRadioValue(rvDataJalanFungsi));
        requestModel.setKelas_jalan(ViewGeneratorUtil.getSelectedRadioValue(rvDataJalanKelas));
        requestModel.setTipe_jalan(ViewGeneratorUtil.getSelectedRadioValue(rvDataJalanTipe));
        requestModel.setBentuk_geometri(ViewGeneratorUtil.getSelectedRadioValue(rvDataJalanBentuk));
        requestModel.setKondisi_permukaan(ViewGeneratorUtil.getSelectedRadioValue(rvDataJalanKondisi));
        requestModel.setBatas_kecepatan(ViewGeneratorUtil.getSelectedRadioValue(rvDataJalanBatasKecepatan));
        requestModel.setKemiringan_jalan(ViewGeneratorUtil.getSelectedRadioValue(rvDataJalanKemiringan));
        requestModel.setPengaturan_simpang(ViewGeneratorUtil.getSelectedCheckboxValue(rvDataJalanPengaturanSimpang));
        requestModel.setStatus_jalan(ViewGeneratorUtil.getSelectedRadioValue(rvDataJalanStatus));
        requestModel.setKerusakan_material(ViewGeneratorUtil.getSelectedCheckboxValue(rvDataKerusakanMaterial));
        requestModel.setPenyebab(ViewGeneratorUtil.getSelectedRadioValue(rvDataKerusakanPenyebab));
        requestModel.setTotal_nilai_rugi_kendaraan(etDataKerusakanRugiMaterialKendaraan.getText().toString());
        requestModel.setTotal_nilai_rugi_non_kendaraan(etDataKerusakanRugiMaterialNonKendaraan.getText().toString());

        return requestModel;
    }

    private AddAccidentStateModel generateStateModel(@Nullable AddAccidentStateModel stateModel) {

        if (stateModel == null) {
            stateModel = new AddAccidentStateModel();
        }

        //radiogroup
        stateModel.setPolres(tvDataLakaPolres.getText().toString());

        stateModel.setKecelakaan_menonjol(ViewGeneratorUtil.getSelectedRadioIndex(llDataLakaKecelkaanMenonjol));
        stateModel.setKondisi_cahaya(ViewGeneratorUtil.getSelectedRadioIndex(rvDataLakaKondisiCahaya));
        stateModel.setCuaca(ViewGeneratorUtil.getSelectedRadioIndex(rvDataLakaKondisiCuaca));
        stateModel.setFungsi_jalan(ViewGeneratorUtil.getSelectedRadioIndex(rvDataJalanFungsi));
        stateModel.setKelas_jalan(ViewGeneratorUtil.getSelectedRadioIndex(rvDataJalanKelas));
        stateModel.setTipe_jalan(ViewGeneratorUtil.getSelectedRadioIndex(rvDataJalanTipe));
        stateModel.setBentuk_geometri(ViewGeneratorUtil.getSelectedRadioIndex(rvDataJalanBentuk));
        stateModel.setKondisi_permukaan(ViewGeneratorUtil.getSelectedRadioIndex(rvDataJalanKondisi));
        stateModel.setBatas_kecepatan(ViewGeneratorUtil.getSelectedRadioIndex(rvDataJalanBatasKecepatan));
        stateModel.setKemiringan_jalan(ViewGeneratorUtil.getSelectedRadioIndex(rvDataJalanKemiringan));
        stateModel.setStatus_jalan(ViewGeneratorUtil.getSelectedRadioIndex(rvDataJalanStatus));
        stateModel.setPenyebab(ViewGeneratorUtil.getSelectedRadioIndex(rvDataKerusakanPenyebab));

        //checkbx
        stateModel.setInformasi_khusus(ViewGeneratorUtil.getSelectedCheckboxIndex(rvDataLakaInfoKhusus));
        stateModel.setPengaturan_simpang(ViewGeneratorUtil.getSelectedCheckboxIndex(rvDataJalanPengaturanSimpang));
        stateModel.setKerusakan_material(ViewGeneratorUtil.getSelectedCheckboxIndex(rvDataKerusakanMaterial));
        //edtxt
        stateModel.setTanggal_kejadian(etDataLakaTglLaka.getText().toString());
        stateModel.setJam_kejadian(etDataLakaJamLaka.getText().toString());
        stateModel.setTanggal_dilaporkan(tvDataLakaTglLap.getText().toString());
        stateModel.setJam_dilaporkan(tvDataLakaJamLap.getText().toString());
        //others
        stateModel.setTotal_nilai_rugi_kendaraan(etDataKerusakanRugiMaterialKendaraan.getText().toString());
        stateModel.setTotal_nilai_rugi_non_kendaraan(etDataKerusakanRugiMaterialNonKendaraan.getText().toString());
        stateModel.setPhotoBase64(base64);

        stateModel.setTipe_kecelakaan(tvDataLakaTipeLaka.getText().toString());
        stateModel.setTipe_kecelakaan_id(selected_lakatype_resid);
        stateModel.setTipe_kecelakaan_mainid(selected_lakatypemain_resid);
        stateModel.setTipe_kecelakaan_posid(selected_lakatypemain_posid);

        try {
            stateModel.setLatitude(Double.valueOf(tvDataKoordinatLint.getText().toString()));
            stateModel.setLongitude(Double.valueOf(tvDataKoordinatBujur.getText().toString()));
            stateModel.setNama_jalan(etDataKoordinatNamaJalan.getText().toString());
            Double[] latlang = {
                    lat, lng
            };
            stateModel.setGps(latlang);

        } catch (Exception e) {
            //Log.e("", "generateStateModel: " + e.getMessage());
        }
        return stateModel;
    }

    private void saveOverwrite() {
        if (loadedFromState) {
            overwriteData();
            finish();
        } else {
            if (saveData()) {
                Intent summaryIntent = new Intent(AddAccidentActivity.this, AccidentSummaryActivity.class);
                summaryIntent.putExtra("Local_ID", localId);
                startActivity(summaryIntent);
                finish();
            }
        }
    }

    private boolean saveData() {
        realm = Realm.getDefaultInstance();
        final Number currentIdNum = realm.where(RequestRealmDbObject.class).max("Local_ID");
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                RequestRealmDbObject poolRequestModel = new RequestRealmDbObject();
                int local_val = (currentIdNum != null) ? (currentIdNum.intValue() + 1) : 1;
                poolRequestModel.setUser_id(currentUser.getUser_id());
                poolRequestModel.setLocal_ID(local_val);

                RealmList<AccidentDBModel> tot = new RealmList<>();
                tot.add(generateRequestModel(null));
                poolRequestModel.setSetAccidentRequestModel(tot);

                RealmList<AddAccidentStateModel> en = new RealmList<>();
                en.add(generateStateModel(null));
                poolRequestModel.setAddAccidentStateModel(en);

                localId = poolRequestModel.getLocal_ID();
                realm.copyToRealm(poolRequestModel);
            }
        });
        realm.refresh();
        realm.close();
        return true;
    }

    private void overwriteData() {
        realm = Realm.getDefaultInstance();
        realm.refresh();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RequestRealmDbObject poolRequestModel = realm.where(RequestRealmDbObject.class).sort("Local_ID").equalTo("Local_ID", localId).findFirst();
                generateRequestModel(poolRequestModel.getSetAccidentRequestModel().first());
                generateStateModel(poolRequestModel.getAddAccidentStateModel().first());
            }
        });
        realm.close();
    }

    private void initFirstDateTime() {

        Date now = Calendar.getInstance().getTime();
        tvDataLakaTglLap.setText(new SimpleDateFormat("dd-MM-yyyy").format(now));
        tvDataLakaJamLap.setText(new SimpleDateFormat("HH:mm").format(now));
        etDataLakaTglLaka.setText(new SimpleDateFormat("dd-MM-yyyy").format(now));
        etDataLakaJamLaka.setText(new SimpleDateFormat("HH:mm").format(now));
    }

    private void initDateTimeSetting() {
        Calendar now = Calendar.getInstance();
        dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setAccentColor(ContextCompat.getColor(this, R.color.colorGreenButton));
        dpd.setMaxDate(now);

        tpd = TimePickerDialog.newInstance(
                this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                true
        );
        tpd.setAccentColor(ContextCompat.getColor(this, R.color.colorGreenButton));

        String[] splitDateLap = tvDataLakaTglLap.getText().toString().split("-");
        int hariLap = Integer.parseInt(splitDateLap[0]);
        int bulanLap = Integer.parseInt(splitDateLap[1]);
        int tahunLap = Integer.parseInt(splitDateLap[2]);

        String[] splitDateLak = etDataLakaTglLaka.getText().toString().split("-");
        int hariLak = Integer.parseInt(splitDateLak[0]);
        int bulanLak = Integer.parseInt(splitDateLak[1]);
        int tahunLak = Integer.parseInt(splitDateLak[2]);

        if (hariLak == hariLap && bulanLak == bulanLap && tahunLak == tahunLap) {
            String[] splitTimeLap = tvDataLakaJamLap.getText().toString().split(":");
            int jam = Integer.parseInt(splitTimeLap[0]);
            int menit = Integer.parseInt(splitTimeLap[1]);
            tpd.setMaxTime(jam, menit, 00);
        }
    }

    private void moveToSetAccidentType() {
        Intent jinktent = new Intent(this, SetAccidentTypeActivity.class);
        if (loadedFromState) {
            jinktent.putExtra("Selected_item_a", selected_lakatypemain_resid);
            jinktent.putExtra("Selected_item_b", selected_lakatype_resid);
            jinktent.putExtra("Selected_item_main_position", selected_lakatypemain_posid);
        }
        startActivityForResult(jinktent, ADDACCIDENT_ACCTYPE_CODE);
    }

    private void moveToSetMap() {
        Intent jinktent = new Intent(this, SetAccidentMapActivity.class);
        jinktent.putExtra("lat", Double.valueOf(tvDataKoordinatLint.getText().toString()));
        jinktent.putExtra("lng", Double.valueOf(tvDataKoordinatBujur.getText().toString()));
        jinktent.putExtra("address", etDataKoordinatNamaJalan.getText().toString());
        startActivityForResult(jinktent, ADDACCIDENT_MAP_CODE);
    }

    private void moveToImageUpload() {
        Intent jinktent = new Intent(this, SetAccidentPhotoActivity.class);
        if (base64 != null && base64.length > 0) {
            jinktent.putExtra("shyu", base64);
        }
        startActivityForResult(jinktent, ADDACCIDENT_PHOTO_UPLOAD_CODE);
    }

    private void refreshFotolakaSlot(String[] base64) {
        ivFotoLaka1.setImageResource(R.drawable.img_placeholder);
        ivFotoLaka2.setImageResource(R.drawable.img_placeholder);
        ivFotoLaka3.setImageResource(R.drawable.img_placeholder);
        ivFotoLaka4.setImageResource(R.drawable.img_placeholder);
        ivFotoLaka5.setImageResource(R.drawable.img_placeholder);
        try {
            ivFotoLaka1.setImageBitmap(SystemUtil.Base64ToBitmap(base64[0]));
            ivFotoLaka2.setImageBitmap(SystemUtil.Base64ToBitmap(base64[1]));
            ivFotoLaka3.setImageBitmap(SystemUtil.Base64ToBitmap(base64[2]));
            ivFotoLaka4.setImageBitmap(SystemUtil.Base64ToBitmap(base64[3]));
            ivFotoLaka5.setImageBitmap(SystemUtil.Base64ToBitmap(base64[4]));
        } catch (Exception e) {
            //Log.e("ERR", e.getMessage());
        }

    }

    private void getGpsLocation() {
        LocationListener clubbing = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                tvDataKoordinatAcc.setText("Acc : " + location.getAccuracy());
                lat = location.getLatitude();
                lng = location.getLongitude();
                tvDataKoordinatLint.setText(location.getLatitude() + "");
                tvDataKoordinatBujur.setText(location.getLongitude() + "");
                locationManager.removeUpdates(this);
                etDataKoordinatNamaJalan.setText(GeocoderUtil.getAddressFromlatlng(AddAccidentActivity.this, lat, lng));
                Toast.makeText(AddAccidentActivity.this, R.string.gps_found, Toast.LENGTH_LONG).show();

                isLocationFound = true;
                int i = 1;

                if (gpsStatus != null) {
                    Iterable<GpsSatellite> satellites = gpsStatus.getSatellites();
                    Iterator<GpsSatellite> sat = satellites.iterator();
                    i--;
                    while (sat.hasNext()) {
                        sat.next();
                        i++;
                    }
                }
                tvDataKoordinatSat.setText("Sat : " + i);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
//                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        };

        if (Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION) && Nammu.checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            try {
            } finally {
                /*
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, clubbing);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, clubbing);
                */
                // getting GPS status
                isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

                // getting network status
                isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                if (!isGPSEnabled && !isNetworkEnabled) {
                    // no network provider is enabled
                    Toast.makeText(getApplicationContext(), "no network provider is enabled", Toast.LENGTH_SHORT).show();
                    Log.d("Lihat getGpsLocation AddAccidentActivity", "no network provider is enabled");
                    isLocationFound = true;
                } else {
                    //this.canGetLocation = true;
                    if (isNetworkEnabled) {
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, clubbing);
                        Log.d("Network", "Network");
                        if (locationManager != null) {
                            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            } else {
                                Toast.makeText(getApplicationContext(), "Can't Reach GPS", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Can't Reach GPS", Toast.LENGTH_SHORT).show();
                        }
                    }
                    // if GPS Enabled get lat/long using GPS Services
                    if (isGPSEnabled) {
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, clubbing);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            } else {
                                Toast.makeText(getApplicationContext(), "Can't Reach GPS", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Can't Reach GPS", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                gpsStatus = locationManager.getGpsStatus(null);
            }
        }
    }

    private void checkData(AddAccidentStateModel accidentStateModel) {
        realm = Realm.getDefaultInstance();
        AccidentDBModel accidentDBModel = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst().getSetAccidentRequestModel().get(0);

        List<String> idModel1 = FormdataLoader.getIdModel();
        Log.d("Lihat checkData AddAccidentActivity1", String.valueOf(accidentDBModel));
        Log.d("Lihat checkData AddAccidentActivity4", String.valueOf(idModel1));

        if (accidentDBModel.getKondisi_cahaya() != null) {
            if (isListContainString(idModel1, accidentDBModel.getKondisi_cahaya())) {
                ViewGeneratorUtil.setRadioCheckedAt(rvDataLakaKondisiCahaya, accidentStateModel.getKondisi_cahaya());
            }
        }
        if (accidentDBModel.getCuaca() != null) {
            if (isListContainString(idModel1, accidentDBModel.getCuaca())) {
                ViewGeneratorUtil.setRadioCheckedAt(rvDataLakaKondisiCuaca, accidentStateModel.getCuaca());
            }
        }
        if (accidentDBModel.getFungsi_jalan() != null) {
            if (isListContainString(idModel1, accidentDBModel.getFungsi_jalan())) {
                ViewGeneratorUtil.setRadioCheckedAt(rvDataJalanFungsi, accidentStateModel.getFungsi_jalan());
            }
        }
        if (accidentDBModel.getKelas_jalan() != null) {
            if (isListContainString(idModel1, accidentDBModel.getKelas_jalan())) {
                ViewGeneratorUtil.setRadioCheckedAt(rvDataJalanKelas, accidentStateModel.getKelas_jalan());
            }
        }
        if (accidentDBModel.getTipe_jalan() != null) {
            if (isListContainString(idModel1, accidentDBModel.getTipe_jalan())) {
                ViewGeneratorUtil.setRadioCheckedAt(rvDataJalanTipe, accidentStateModel.getTipe_jalan());
            }
        }
        if (accidentDBModel.getBentuk_geometri() != null) {
            if (isListContainString(idModel1, accidentDBModel.getBentuk_geometri())) {
                ViewGeneratorUtil.setRadioCheckedAt(rvDataJalanBentuk, accidentStateModel.getBentuk_geometri());
            }
        }
        if (accidentDBModel.getKondisi_permukaan() != null) {
            if (isListContainString(idModel1, accidentDBModel.getKondisi_permukaan())) {
                ViewGeneratorUtil.setRadioCheckedAt(rvDataJalanKondisi, accidentStateModel.getKondisi_permukaan());
            }
        }
        if (accidentDBModel.getBatas_kecepatan() != null) {
            if (isListContainString(idModel1, accidentDBModel.getBatas_kecepatan())) {
                ViewGeneratorUtil.setRadioCheckedAt(rvDataJalanBatasKecepatan, accidentStateModel.getBatas_kecepatan());
            }
        }
        if (accidentDBModel.getKemiringan_jalan() != null) {
            if (isListContainString(idModel1, accidentDBModel.getKemiringan_jalan())) {
                ViewGeneratorUtil.setRadioCheckedAt(rvDataJalanKemiringan, accidentStateModel.getKemiringan_jalan());
            }
        }
        if (accidentDBModel.getStatus_jalan() != null) {
            if (isListContainString(idModel1, accidentDBModel.getStatus_jalan())) {
                ViewGeneratorUtil.setRadioCheckedAt(rvDataJalanStatus, accidentStateModel.getStatus_jalan());
            }
        }
        if (accidentDBModel.getPenyebab() != null) {
            if (isListContainString(idModel1, accidentDBModel.getPenyebab())) {
                ViewGeneratorUtil.setRadioCheckedAt(rvDataKerusakanPenyebab, accidentStateModel.getPenyebab());
            }
        }
        if (accidentDBModel.getPengaturan_simpang() != null) {
            if (isListContainStringArray(idModel1, accidentDBModel.getPengaturan_simpang())) {
                ViewGeneratorUtil.setCheckboxCheckedAt(rvDataJalanPengaturanSimpang, accidentStateModel.getPengaturan_simpang());
            }
        }
        if (accidentDBModel.getKerusakan_material() != null) {
            if (isListContainStringArray(idModel1, accidentDBModel.getKerusakan_material())) {
                ViewGeneratorUtil.setCheckboxCheckedAt(rvDataKerusakanMaterial, accidentStateModel.getKerusakan_material());
            }
        }
        if (accidentDBModel.getInformasi_khusus() != null) {
            if (isListContainStringArray(idModel1, accidentDBModel.getInformasi_khusus())) {
                ViewGeneratorUtil.setCheckboxCheckedAt(rvDataLakaInfoKhusus, accidentStateModel.getInformasi_khusus());
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ADDACCIDENT_ACCTYPE_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    tvDataLakaTipeLaka.setText(data.getStringExtra("result"));
                    ivDataLakaTipeLaka.setImageResource(data.getIntExtra("imgidsub", R.drawable.img_accident_sub_a0700));
                    selected_lakatype_resid = data.getIntExtra("imgidsub", R.drawable.img_accident_sub_a0700);
                    selected_lakatypemain_resid = data.getIntExtra("imgidmain", R.drawable.img_accident_a0700);
                    selected_lakatypemain_posid = data.getIntExtra("imgposmain", 0);
                }
                break;
            case ADDACCIDENT_MAP_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    tvDataKoordinatLint.setText(String.valueOf(data.getDoubleExtra("lat", 0)));
                    tvDataKoordinatBujur.setText(String.valueOf(data.getDoubleExtra("lng", 0)));
                    etDataKoordinatNamaJalan.setText(data.getStringExtra("address"));
                }
                break;
            case ADDACCIDENT_PHOTO_UPLOAD_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    base64 = data.getStringArrayExtra("jink");
                    refreshFotolakaSlot(base64);
                }
                break;
            default:

                break;
        }
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        try {
            etDataLakaJamLaka.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
        } finally {

        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String day = dayOfMonth + "";
        String month = (monthOfYear + 1) + "";
        if (dayOfMonth < 10) {
            day = "0" + dayOfMonth;
        }
        if (monthOfYear < 10) {
            month = "0" + (monthOfYear + 1);
        }
        etDataLakaTglLaka.setText(day + "-" + month + "-" + year);

        String[] dateItems = etDataLakaTglLaka.getText().toString().split("-");
        Calendar now = Calendar.getInstance();
        if (Integer.parseInt(dateItems[0]) == now.get(Calendar.DAY_OF_MONTH) && Integer.parseInt(dateItems[1]) == (now.get(Calendar.MONTH) + 1) && Integer.parseInt(dateItems[2]) == now.get(Calendar.YEAR)) {
            //tpd.setMaxTime(now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), 0);
            initDateTimeSetting();
            etDataLakaJamLaka.setText("00:01");
        } else {
            tpd.setMaxTime(23, 59, 0);
        }
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        saveOverwrite();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        finish();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(AddAccidentActivity.this);
        builder.setMessage("Simpan data sebelum keluar?").setPositiveButton("Ya", dialogClickListener)
                .setNegativeButton("Tidak", dialogClickListener).show();
    }
}

