package com.gandsoft.irsms.Activity.Dashboard;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.gandsoft.irsms.Presenter.SeasonManager.SessionManager;
import com.gandsoft.irsms.model.ObjectModel.UserDataModel;
import com.gandsoft.irsms.model.RealmDbModel.RequestRealmDbObject;

import java.util.List;

import io.realm.OrderedRealmCollection;

/**
 * Created by gleen on 14/02/18.
 */

public class DashboardItemRecyclerAdapter extends RecyclerView.Adapter<DashboardItemRecyclerAdapter.ViewHolder> {

    List<RequestRealmDbObject> models;
    private Activity parent;
    private UserDataModel currentUser = SessionManager.loadUserData();

    public DashboardItemRecyclerAdapter(Activity parent, @Nullable OrderedRealmCollection<RequestRealmDbObject> data) {
        models = data;
        this.parent = parent;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(com.gandsoft.irsms.R.layout.list_layout_dashboard, parent, false);
        return new ViewHolder(itemView);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView ivListLaka;
        TextView numbers, tvListLakaTanggal, tvListLakaJam, tvListLakaPolres, tvListLakaVehicle;

        ImageButton ibSummaryDataKendaraanEdit, ibSummaryDataKendaraanDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            ivListLaka = (ImageView) itemView.findViewById(com.gandsoft.irsms.R.id.ivListLaka);
            tvListLakaTanggal = (TextView) itemView.findViewById(com.gandsoft.irsms.R.id.tvListLakaTanggal);
            numbers = (TextView) itemView.findViewById(com.gandsoft.irsms.R.id.numbers);
            tvListLakaJam = (TextView) itemView.findViewById(com.gandsoft.irsms.R.id.tvListLakaJam);
            tvListLakaPolres = (TextView) itemView.findViewById(com.gandsoft.irsms.R.id.tvListLakaPolres);
            tvListLakaVehicle = (TextView) itemView.findViewById(com.gandsoft.irsms.R.id.tvListLakaVehicle);
            ibSummaryDataKendaraanEdit = (ImageButton) itemView.findViewById(com.gandsoft.irsms.R.id.ibSummaryDataKendaraanEdit);
            ibSummaryDataKendaraanDelete = (ImageButton) itemView.findViewById(com.gandsoft.irsms.R.id.ibSummaryDataKendaraanDelete);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RequestRealmDbObject model = models.get(position);
        //holder.numbers.setText((position + 1) + ".");
        holder.numbers.setText(model.getLocal_ID() + ".");
        holder.ivListLaka.setImageResource(model.getAddAccidentStateModel().get(0).getTipe_kecelakaan_id());
        holder.tvListLakaTanggal.setText(model.getSetAccidentRequestModel().get(0).getTanggal_kejadian());
        holder.tvListLakaJam.setText(model.getSetAccidentRequestModel().get(0).getJam_kejadian());
        holder.tvListLakaPolres.setText(model.getAddAccidentStateModel().get(0).getPolres());
        holder.tvListLakaVehicle.setText("-");

        if (model.getSetDriverRequestModels().size() > 0) {
            String[] split1 = model.getSetDriverRequestModels().get(0).getVehicleCompoundKey().split("/")[1].split("\\|");
            holder.tvListLakaVehicle.setText(split1[1].toUpperCase() + " " + split1[2]);
        }

        int id = model.getLocal_ID();
        holder.ibSummaryDataKendaraanEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DashboardObjectHookInteface) parent).editRealmModelAt(id);
            }
        });

        holder.ibSummaryDataKendaraanDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DashboardObjectHookInteface) parent).deleteRealmModelAt(id);
            }
        });

        if (currentUser.getUser_id() != model.getUser_id()) {
            holder.ibSummaryDataKendaraanEdit.setVisibility(View.INVISIBLE);
            holder.ibSummaryDataKendaraanDelete.setVisibility(View.INVISIBLE);
            holder.ibSummaryDataKendaraanEdit.setClickable(false);
            holder.ibSummaryDataKendaraanDelete.setClickable(false);
        }
    }

    @Override
    public int getItemCount() {
        return models != null ? models.size() : 0;
    }
}
