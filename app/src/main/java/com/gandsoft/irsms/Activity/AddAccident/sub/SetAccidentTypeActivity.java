package com.gandsoft.irsms.Activity.AddAccident.sub;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.gandsoft.irsms.Activity.AddAccident.AccidentTypeSelectorRecyclerviewAdapter;
import com.gandsoft.irsms.Activity.LocalBaseActivity;
import com.gandsoft.irsms.model.uiModel.ViewModel;

import java.util.List;

/**
 * Created by glenn on 2/6/18.
 */

public class SetAccidentTypeActivity extends LocalBaseActivity implements SetAccidentTypeInterface {
    private List<ViewModel> accidentTypeContainer;
    private List<ViewModel> accidentTypeContainerSub;

    private Dialog setMainitemDialog;
    private Dialog setSubitemDialog;

    private AccidentTypeSelectorRecyclerviewAdapter accidentMainAdapter;
    private AccidentTypeSelectorRecyclerviewAdapter accidentSubAdapter;

    private ImageView ivTipeLakaMainItem, ivTipeLakaSubItem, ivTipeLakaClose;
    private TextView tvTipeLakaPilihItem, tvTipeLakaCancel;

    private int selectedMainTypePos = 0;
    private int mainTypeHook;
    private int subTypeItemHook;
    private int selectedMainTypeHook;

    private String result = "A0700";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.gandsoft.irsms.R.layout.activity_form_accident_type);
        Intent bundle = this.getIntent();

        if (bundle.hasExtra("Selected_item_a")) {
            mainTypeHook = bundle.getIntExtra("Selected_item_a", 0);
            subTypeItemHook = bundle.getIntExtra("Selected_item_b", com.gandsoft.irsms.R.drawable.img_accident_sub_a0700);
            selectedMainTypeHook = bundle.getIntExtra("Selected_item_main_position", 0);
        }

        Log.d("Lihat onCreate SetAccidentTypeActivity", String.valueOf(mainTypeHook + "  " + subTypeItemHook));

        initComponent();
        initContent();
        if (mainTypeHook != 0) {
            loadState();
        }

    }

    @Override
    public void setSelectedMainTypeItem(int i) {
        try {
            selectedMainTypePos = i;
            accidentTypeContainerSub = accidentTypeContainer.get(selectedMainTypePos).getViewModelGroup();
            accidentSubAdapter.updateModel(accidentTypeContainerSub);
            accidentSubAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            //Log.e("err", "ssmti: " + e.getMessage());
        }
    }

    @Override
    public void closeDialogForResult(int res, int res2, String resid, boolean ismain) {
        if (ismain) {
            closeMainDialog();
            ivTipeLakaMainItem.setImageResource(res);
            ivTipeLakaSubItem.setImageResource(res2);
            result = resid;
            mainTypeHook = res;
        } else {
            closeSubDialog();
            ivTipeLakaSubItem.setImageResource(res2);
            result = resid;
        }
        subTypeItemHook = res2;
    }

    private void initComponent() {
        accidentTypeContainer = AccidentTypeBuilder.getAccidentTypeList();
        accidentTypeContainerSub = accidentTypeContainer.get(selectedMainTypePos).getViewModelGroup();

        accidentMainAdapter = new AccidentTypeSelectorRecyclerviewAdapter(this, accidentTypeContainer, true);
        accidentSubAdapter = new AccidentTypeSelectorRecyclerviewAdapter(this, accidentTypeContainerSub, false);

        ivTipeLakaClose = findViewById(com.gandsoft.irsms.R.id.ivTipeLakaClose);
        ivTipeLakaMainItem = findViewById(com.gandsoft.irsms.R.id.ivTipeLakaMainItem);
        ivTipeLakaSubItem = findViewById(com.gandsoft.irsms.R.id.ivTipeLakaSubItem);

        tvTipeLakaPilihItem = findViewById(com.gandsoft.irsms.R.id.tvTipeLakaPilihItem);
        tvTipeLakaCancel = findViewById(com.gandsoft.irsms.R.id.tvTipeLakaCancel);

        setMainitemDialog = generateDialog(accidentMainAdapter);
        setSubitemDialog = generateDialog(accidentSubAdapter);
    }

    private void initContent() {
        ivTipeLakaMainItem.setImageResource(com.gandsoft.irsms.R.drawable.img_accident_a0700);
        ivTipeLakaSubItem.setImageResource(com.gandsoft.irsms.R.drawable.img_accident_sub_a0700);

        View.OnClickListener onclklstnr = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent;
                switch (view.getId()) {
                    case com.gandsoft.irsms.R.id.ivTipeLakaClose:
                        returnIntent = new Intent();
                        setResult(Activity.RESULT_CANCELED, returnIntent);
                        finish();
                        break;
                    case com.gandsoft.irsms.R.id.ivTipeLakaMainItem:
                        setMainitemDialog.show();
                        break;
                    case com.gandsoft.irsms.R.id.ivTipeLakaSubItem:
                        setSubitemDialog.show();
                        break;
                    case com.gandsoft.irsms.R.id.tvTipeLakaCancel:
                        returnIntent = new Intent();
                        setResult(Activity.RESULT_CANCELED, returnIntent);
                        finish();
                        break;
                    case com.gandsoft.irsms.R.id.tvTipeLakaPilihItem:
                        returnIntent = new Intent();
                        returnIntent.putExtra("result", result);
                        returnIntent.putExtra("imgidmain", mainTypeHook);
                        returnIntent.putExtra("imgidsub", subTypeItemHook);
                        returnIntent.putExtra("imgposmain", selectedMainTypePos);
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                        break;
                }
            }
        };
        ivTipeLakaClose.setOnClickListener(onclklstnr);
        ivTipeLakaMainItem.setOnClickListener(onclklstnr);
        ivTipeLakaSubItem.setOnClickListener(onclklstnr);
        tvTipeLakaPilihItem.setOnClickListener(onclklstnr);
        tvTipeLakaCancel.setOnClickListener(onclklstnr);
    }

    private Dialog generateDialog(AccidentTypeSelectorRecyclerviewAdapter adapter) {
        Dialog dialogView = new Dialog(this);
        dialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lWindowParams = new WindowManager.LayoutParams();
        lWindowParams.copyFrom(dialogView.getWindow().getAttributes());
        lWindowParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialogView.getWindow().setAttributes(lWindowParams);
        dialogView.setContentView(com.gandsoft.irsms.R.layout.dialog_accident_type);
        RecyclerView rv = dialogView.findViewById(com.gandsoft.irsms.R.id.rv_dialog);
        GridLayoutManager lm = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        rv.setLayoutManager(lm);
        rv.setAdapter(adapter);
        return dialogView;
    }

    private void loadState() {
        setSelectedMainTypeItem(selectedMainTypeHook);
        ivTipeLakaMainItem.setImageResource(mainTypeHook);
        ivTipeLakaSubItem.setImageResource(subTypeItemHook);

    }

    public void closeMainDialog() {
        if (setMainitemDialog != null) {
            if (setMainitemDialog.isShowing()) {
                setMainitemDialog.dismiss();
            }
        }
    }

    public void closeSubDialog() {
        if (setSubitemDialog != null) {
            if (setSubitemDialog.isShowing()) {
                setSubitemDialog.dismiss();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

}
