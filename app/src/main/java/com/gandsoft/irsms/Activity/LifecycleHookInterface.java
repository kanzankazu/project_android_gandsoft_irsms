package com.gandsoft.irsms.Activity;

/**
 * Created by glenn on 1/30/18.
 */

public interface LifecycleHookInterface {

    void onActivityFinish();

    void onActivityDestroy();
}
