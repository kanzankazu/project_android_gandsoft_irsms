package com.gandsoft.irsms.Activity.summary;

import javax.annotation.Nullable;

/**
 * Created by gleen on 27/02/18.
 */

public interface SummaryHookInterface {

    void deleteVehicleItemAt(int id);

    void editVehicleItemAt(int id);

    void deletePedestrianRealmModelAt(int id);

    void editPedestrianRealmModelAt(int id);

    void moveToPassangerSummary(int id);

}
