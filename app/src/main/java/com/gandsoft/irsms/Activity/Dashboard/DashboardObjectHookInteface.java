package com.gandsoft.irsms.Activity.Dashboard;

/**
 * Created by gleen on 14/02/18.
 */

public interface DashboardObjectHookInteface {

    void deleteRealmModelAt(int id);

    void editRealmModelAt(int id);

}
