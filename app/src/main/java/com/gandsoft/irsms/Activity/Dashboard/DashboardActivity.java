package com.gandsoft.irsms.Activity.Dashboard;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gandsoft.irsms.Activity.AddAccident.AddAccidentActivity;
import com.gandsoft.irsms.Activity.LocalBaseActivity;
import com.gandsoft.irsms.Activity.Login.LoginActivity;
import com.gandsoft.irsms.Activity.Sync.SyncActivity;
import com.gandsoft.irsms.Activity.summary.AccidentSummaryActivity;
import com.gandsoft.irsms.IConfig;
import com.gandsoft.irsms.ISeasonConfig;
import com.gandsoft.irsms.IrsmsApp;
import com.gandsoft.irsms.Presenter.DAO.AuthDao;
import com.gandsoft.irsms.Presenter.DAO.MainPageDao;
import com.gandsoft.irsms.Presenter.SeasonManager.SessionManager;
import com.gandsoft.irsms.R;
import com.gandsoft.irsms.Support.Util.DialogUtil;
import com.gandsoft.irsms.Support.Util.ResponseUtil;
import com.gandsoft.irsms.Support.Util.SystemUtil;
import com.gandsoft.irsms.model.ObjectModel.UserDataModel;
import com.gandsoft.irsms.model.RealmDbModel.PassengerForm.AddPassangerStateModel;
import com.gandsoft.irsms.model.RealmDbModel.PassengerForm.PassangerDBModel;
import com.gandsoft.irsms.model.RealmDbModel.PedestrianForm.AddPedestrianStateModel;
import com.gandsoft.irsms.model.RealmDbModel.PedestrianForm.PedestrianDBModel;
import com.gandsoft.irsms.model.RealmDbModel.RequestRealmDbObject;
import com.gandsoft.irsms.model.RealmDbModel.VehicleForm.AddDriverStateModel;
import com.gandsoft.irsms.model.RealmDbModel.VehicleForm.DriverDBModel;
import com.gandsoft.irsms.model.RealmDbModel.WitnessForm.AddWitnessStateModel;
import com.gandsoft.irsms.model.RealmDbModel.WitnessForm.SetWitnessDBModel;
import com.gandsoft.irsms.model.RequestModel.Auth.LogoutRequestModel;
import com.gandsoft.irsms.model.ResponseModel.BaseResponseModel;
import com.gandsoft.irsms.model.uiModel.FormdataLoader;

import app.beelabs.com.codebase.base.BaseDao;
import app.beelabs.com.codebase.base.response.BaseResponse;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Response;


public class DashboardActivity extends LocalBaseActivity implements IConfig, DashboardObjectHookInteface, NavigationView.OnNavigationItemSelectedListener {

    private LinearLayout llLogoutBerandafvbi;

    private UserDataModel currentUser;
    private View cvBerandaTambahLaka, cvBerandaSync;
    private RecyclerView rvBerandaListLaka;

    private ImageView drawer_personphoto;
    private TextView drawer_username, version;

    private Realm realm;
    private DashboardItemRecyclerAdapter dbItemAdapter;

    private Boolean isUiReady = false;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.gandsoft.irsms.R.layout.activity_dashboard_drawerlayout);
        initGlobalElement();
        initComponent();
        initContent();
        checkSession();
        //Log.e("bf:", "--[----->+<]>----.-.+.+++++++.[--->+<]>---.[->+++<]>+.+++++++++++++.----.-.+++++.-------.-[--->+<]>--.+[----->+<]>.------------.--[--->+<]>-.-----------.[--->+<]>---.++[--->++<]>.+++++++++++.+[---->+<]>++.---[->++++<]>-.++++[->+++<]>.++++++++.++++++++.[->+++<]>++.");
    }

    private void initComponent() {
        currentUser = SessionManager.loadUserData();
        Toolbar toolbar = findViewById(com.gandsoft.irsms.R.id.toolbar);
        drawer = findViewById(com.gandsoft.irsms.R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, com.gandsoft.irsms.R.string.navigation_drawer_open, com.gandsoft.irsms.R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(com.gandsoft.irsms.R.id.nav_view);
        View headerLayout = navigationView.inflateHeaderView(com.gandsoft.irsms.R.layout.activity_dashboard_nav_header);
        navigationView.setNavigationItemSelectedListener(this);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        Log.d("Lihat initComponent DashboardActivity", String.valueOf(SystemUtil.getMemoryClass()));
        Log.d("Lihat initComponent DashboardActivity", String.valueOf(SystemUtil.checkMaxMemory()));

        llLogoutBerandafvbi = (LinearLayout) findViewById(R.id.llLogoutBeranda);

        cvBerandaTambahLaka = findViewById(com.gandsoft.irsms.R.id.cvBerandaTambahLaka);
        cvBerandaSync = findViewById(com.gandsoft.irsms.R.id.cvBerandaSync);
        rvBerandaListLaka = findViewById(com.gandsoft.irsms.R.id.rvBerandaListLaka);
        drawer_personphoto = headerLayout.findViewById(com.gandsoft.irsms.R.id.drawer_personphoto);
        drawer_username = headerLayout.findViewById(com.gandsoft.irsms.R.id.drawer_username);
        version = findViewById(com.gandsoft.irsms.R.id.version);
    }

    private void initContent() {

        if (SystemUtil.isKitkat()) {
            llLogoutBerandafvbi.bringToFront();
        }

        llLogoutBerandafvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START);
            }
        });

        realm = Realm.getDefaultInstance();
        realm.refresh();
        checkVersion();

        drawer_username.setText(currentUser.getFirst_name() + " " + currentUser.getLast_name());

        cvBerandaTambahLaka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isUiReady) {
                    Intent addAccidentIntent = new Intent(DashboardActivity.this, AddAccidentActivity.class);
                    startActivity(addAccidentIntent);
                } else {
                    Toast.makeText(DashboardActivity.this, "Data belum siap, cobalah sebentar lagi", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cvBerandaSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sycnIntent = new Intent(DashboardActivity.this, SyncActivity.class);
                startActivityForResult(sycnIntent, 888);
            }
        });

        RealmResults<RequestRealmDbObject> models = realm.where(RequestRealmDbObject.class).sort("Local_ID", Sort.ASCENDING).findAll();
        dbItemAdapter = new DashboardItemRecyclerAdapter(this, models);
        rvBerandaListLaka.setLayoutManager(new LinearLayoutManager(this, LinearLayout.VERTICAL, false));
        rvBerandaListLaka.setAdapter(dbItemAdapter);

        if (realm.where(RequestRealmDbObject.class).findAll().size() >= 100) {
            //handle realm full
            Toast.makeText(this, "Kapasitas Data Laka Telah penuh, mohon lakukan sinkronisasi untuk mengirim data terlebih dahulu", Toast.LENGTH_LONG).show();
            cvBerandaSync.setVisibility(View.GONE);
        }
    }

    private void initGlobalElement() {
        if (SessionManager.isLoggedIn()) {
            if (!SessionManager.checkIfExist(ISeasonConfig.KEY_UITEM)) {
                try {
                    callUiItemAPIDash();
                } catch (Exception e) {
                    //Log.e("load fail", e.getMessage());
                    generateErrorDialog(e.getMessage());
                }
            } else {
                Log.d("Continues", "Data already exist, continuing");
                FormdataLoader.initFormData();
                isUiReady = true;
            }
        } else {
            //user somehow access this activity without login creds
            logoutUser();
        }
    }

    private void callUiItemAPIDash() {
        showApiProgressDialog(IrsmsApp.getAppComponent(), new MainPageDao(this) {
            @Override
            public void call() {
                super.doGetUiItemDao(DashboardActivity.this, BaseDao.getInstance(DashboardActivity.this, IConfig.API_CALLER_UIITEM_CODE).callback);
            }
        });
    }

    private void callLogoutAPI(final LogoutRequestModel request) {
        new AuthDao(this) {
            @Override
            public void call() {
                super.doLogOutDao(DashboardActivity.this, request, BaseDao.getInstance(DashboardActivity.this, IConfig.API_CALLER_LOGOUT_CODE).callback);
            }
        };
        /*API.doLogOut(DashboardActivity.this, request, new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                SessionManager.logoutUser();
                Intent gotoLogin = new Intent(DashboardActivity.this, LoginActivity.class);
                startActivity(gotoLogin);
                finish();
            }

            @Override
            public void onFailure(Call call, Throwable t) {

            }
        });*/
    }

    private void moveToLogin() {
        Intent gotoLogin = new Intent(this, LoginActivity.class);
        this.startActivity(gotoLogin);
        finish();
    }

    private void generateErrorDialog(String content) {
        TextView tv_content, tv_subtitle, tv_title;
        ImageView ellipse, dialog_image;
        View customView = LayoutInflater.from(this).inflate(com.gandsoft.irsms.R.layout.dialog_imei, null);
        ellipse = (ImageView) customView.findViewById(com.gandsoft.irsms.R.id.ellipse);
        dialog_image = (ImageView) customView.findViewById(com.gandsoft.irsms.R.id.dialog_image);
        tv_content = (TextView) customView.findViewById(com.gandsoft.irsms.R.id.tv_content);
        tv_subtitle = (TextView) customView.findViewById(com.gandsoft.irsms.R.id.tv_subtitle);
        tv_title = (TextView) customView.findViewById(com.gandsoft.irsms.R.id.tv_title);
        tv_content.setText(content);

        dialog_image.setImageResource(com.gandsoft.irsms.R.drawable.ic_error_outline_88dp);
        ellipse.setImageResource(com.gandsoft.irsms.R.drawable.vector_ellipse_yellow);
        tv_title.setText("Error");
        tv_title.setBackgroundColor(ContextCompat.getColor(this, com.gandsoft.irsms.R.color.colorYellow));
        tv_subtitle.setText("System Error");
        DialogUtil.generateCustomDialogInfo(this, customView);
    }

    @SuppressLint("SetTextI18n")
    private void checkVersion() {
        if (SessionManager.checkIfExist(ISeasonConfig.KEY_UITEM_TOTAL)) {
            version.setText("Version : " + versionName + "." + versionCode + "." + SessionManager.loadInt(ISeasonConfig.KEY_UITEM_TOTAL));
        } else {
            version.setText("Version : " + versionName + "." + versionCode);
        }
    }

    private void checkSession() {
        if (SessionManager.checkIfExist(ISeasonConfig.KEY_USERDATA)) {
            if (SessionManager.loadUserData() == null) {
                logoutUser();
                return;
            }
        } else {
            Toast.makeText(this, "no userdata", Toast.LENGTH_SHORT).show();
            logoutUser();
            return;
        }

        if (!SessionManager.checkIfExist(ISeasonConfig.KEY_UITEM)) {
            Toast.makeText(this, "no UI item, reloading ui data", Toast.LENGTH_SHORT).show();
            callUiItemAPIDash();
            return;
        }
    }

    private void logoutUser() {
        LogoutRequestModel lgoutmdl = new LogoutRequestModel();
        lgoutmdl.setImei(SystemUtil.getImei(DashboardActivity.this));
        callLogoutAPI(lgoutmdl);
        SessionManager.logoutUser();
        moveToLogin();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        realm = Realm.getDefaultInstance();
        dbItemAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {

        super.onResume();
        if (realm.where(RequestRealmDbObject.class).findAll().size() < 100) {
            cvBerandaSync.setVisibility(View.VISIBLE);
        }
        checkVersion();
        checkSession();
    }

    @Override
    protected void onPause() {
        if (!realm.isClosed()) {
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (!realm.isClosed()) {
            realm.close();
        }
        super.onDestroy();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {
            case com.gandsoft.irsms.R.id.nav_home:
                break;
            case com.gandsoft.irsms.R.id.nav_reloadUi:
                if (SystemUtil.isNetworkAvailable(getBaseContext())) {
                    callUiItemAPIDash();
                } else {
                    Toast.makeText(getApplicationContext(), "Koneksi ke jaringan tidak terdeteksi, Coba lagi setelah Anda terhubung ke jaringan", Toast.LENGTH_SHORT).show();
                }
                break;
            case com.gandsoft.irsms.R.id.nav_logout:
                logoutUser();
                break;
            default:
                break;
        }
        DrawerLayout drawer = findViewById(com.gandsoft.irsms.R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (dbItemAdapter != null) {
            dbItemAdapter.notifyDataSetChanged();
        }
        checkVersion();
    }

    @Override
    protected void onApiResponseCallback(BaseResponse br, int responseCode, Response response) {
        super.onApiResponseCallback(br, responseCode, response);

        if (response.isSuccessful()) {
            if (responseCode == IConfig.API_CALLER_UIITEM_CODE) {
                if (br.getStatus().equals(IConfig.RETURN_STATUS_DONE)) {

                    SessionManager.saveValidateData(((BaseResponseModel) br));

                    int totalUi = ((BaseResponseModel) br).getTotal();
                    int versionUi = ((BaseResponseModel) br).getVersion();
                    if (SessionManager.checkIfExist(ISeasonConfig.KEY_UITEM)) {
                        if (SessionManager.checkIfExist(ISeasonConfig.KEY_UITEM_VERSION) && !SessionManager.isVersionDataUpdate(br)) {
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_TOTAL, totalUi);
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_VERSION, versionUi);
                            Toast.makeText(getBaseContext(), "Data sekarang sudah yang terbaru, silahkan gunakan aplikasi", Toast.LENGTH_SHORT).show();
                        } else if (!SessionManager.checkIfExist(ISeasonConfig.KEY_UITEM_VERSION) && SessionManager.isVersionDataUpdate(br)) {
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_TOTAL, totalUi);
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_VERSION, versionUi);
                            ResponseUtil.replaceFormData(br, getBaseContext());
                        } else if (SessionManager.checkIfExist(ISeasonConfig.KEY_UITEM_VERSION) && SessionManager.isVersionDataUpdate(br)) {
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_TOTAL, totalUi);
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_VERSION, versionUi);
                            ResponseUtil.replaceFormData(br, getBaseContext());
                        } else if (!SessionManager.checkIfExist(ISeasonConfig.KEY_UITEM_VERSION) && !SessionManager.isVersionDataUpdate(br)) {
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_TOTAL, totalUi);
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_VERSION, versionUi);
                        }
                    } else {
                        ResponseUtil.addFormData(br, DashboardActivity.this);
                    }
                }
            } else if (responseCode == IConfig.API_CALLER_LOGOUT_CODE) {
                SessionManager.logoutUser();
                moveToLogin();
            } else {
                generateErrorDialog("Koneksi Tidak di Ketahui");
            }
        } else {
            generateErrorDialog("Gagal Terhubung ke Server");
        }
    }

    @Override
    protected void onApiFailureCallback(String message) {
        super.onApiFailureCallback(message);
    }

    @Override
    public void deleteRealmModelAt(int id) {
        final int internalId = id;
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        try {
                            realm = Realm.getDefaultInstance();
                            realm.executeTransactionAsync(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.where(DriverDBModel.class).equalTo("Local_ID", id).findAll().deleteAllFromRealm();
                                    realm.where(AddDriverStateModel.class).equalTo("Local_ID", id).findAll().deleteAllFromRealm();

                                    realm.where(PassangerDBModel.class).equalTo("Local_ID", id).findAll().deleteAllFromRealm();
                                    realm.where(AddPassangerStateModel.class).equalTo("Local_ID", id).findAll().deleteAllFromRealm();

                                    realm.where(PedestrianDBModel.class).equalTo("Local_ID", id).findAll().deleteAllFromRealm();
                                    realm.where(AddPedestrianStateModel.class).equalTo("Local_ID", id).findAll().deleteAllFromRealm();

                                    realm.where(SetWitnessDBModel.class).equalTo("Local_ID", id).findAll().deleteAllFromRealm();
                                    realm.where(AddWitnessStateModel.class).equalTo("Local_ID", id).findAll().deleteAllFromRealm();

                                    realm.where(RequestRealmDbObject.class).equalTo("Local_ID", id).findFirst().getSetAccidentRequestModel().deleteAllFromRealm();
                                    realm.where(RequestRealmDbObject.class).equalTo("Local_ID", id).findFirst().getAddAccidentStateModel().deleteAllFromRealm();
                                    realm.where(RequestRealmDbObject.class).equalTo("Local_ID", id).findFirst().getAddDiagramMappingStateModel().deleteAllFromRealm();
                                    realm.where(RequestRealmDbObject.class).equalTo("Local_ID", id).findFirst().deleteFromRealm();
                                }
                            }, new Realm.Transaction.OnSuccess() {
                                @Override
                                public void onSuccess() {
                                    dbItemAdapter.notifyDataSetChanged();
                                    /*finish();
                                    startActivity(getIntent());*/
                                }
                            });
                            realm.refresh();
                        } finally {
                            if (realm != null) {
                                realm.close();
                            }
                        }
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Hapus laporan ini?")
                .setPositiveButton("Ya", dialogClickListener)
                .setNegativeButton("Tidak", dialogClickListener).show();
    }

    @Override
    public void editRealmModelAt(int id) {
        Intent editIntent = new Intent(this, AccidentSummaryActivity.class);
        editIntent.putExtra("Local_ID", id);
        startActivity(editIntent);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(com.gandsoft.irsms.R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            finish();
        }
    }
}
