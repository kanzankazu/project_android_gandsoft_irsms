package com.gandsoft.irsms.Activity.summary;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;

import com.gandsoft.irsms.Activity.AddAccident.AddAccidentActivity;
import com.gandsoft.irsms.Activity.AddPedestrian.AddPedestrianActivity;
import com.gandsoft.irsms.Activity.AddDriver.AddDriverActivity;
import com.gandsoft.irsms.Activity.DiagramMapping.AddDiagraMappingActivity;
import com.gandsoft.irsms.Activity.LocalBaseActivity;
import com.gandsoft.irsms.Activity.summary.sub.PassangerSummaryActivity;
import com.gandsoft.irsms.Activity.summary.sub.WitnessSummaryActivity;
import com.gandsoft.irsms.IConfig;
import com.gandsoft.irsms.R;
import com.gandsoft.irsms.Support.Util.SystemUtil;
import com.gandsoft.irsms.model.RealmDbModel.PassengerForm.AddPassangerStateModel;
import com.gandsoft.irsms.model.RealmDbModel.PassengerForm.PassangerDBModel;
import com.gandsoft.irsms.model.RealmDbModel.PedestrianForm.AddPedestrianStateModel;
import com.gandsoft.irsms.model.RealmDbModel.PedestrianForm.PedestrianDBModel;
import com.gandsoft.irsms.model.RealmDbModel.RequestRealmDbObject;
import com.gandsoft.irsms.model.RealmDbModel.VehicleForm.AddDriverStateModel;
import com.gandsoft.irsms.model.RealmDbModel.VehicleForm.DriverDBModel;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.realm.Realm;

import static com.gandsoft.irsms.Support.Util.SystemUtil.isKitkat;

/**
 * Created by kanzan on 2/6/2018.
 */

public class AccidentSummaryActivity extends LocalBaseActivity implements View.OnClickListener, SummaryHookInterface {


    private View cvSummaryKeluarfvbi, cvSummaryEditInfoAwalfvbi, cvSummaryDiagramMappingfvbi, cvSummaryDataSaksifvbi, cvSummaryDataPenumpangfvbi, cvSummaryTambahKendaraanfvbi, cvSummaryTambahPejalanKakifvbi;
    private ImageView ivSummaryDataKecelakaanGPSfvbi;
    private TextView tvSummaryDataKecelakaanTanggalfvbi, tvSummaryDataKecelakaanJamfvbi, tvSummaryDataKecelakaanPejalanKakifvbi, tvSummaryDataKecelakaanJumlahSaksifvbi;
    private ImageView ivSummaryFotoLaka1fvbi, ivSummaryFotoLaka2fvbi, ivSummaryFotoLaka3fvbi, ivSummaryFotoLaka4fvbi, ivSummaryFotoLaka5fvbi;
    private RecyclerView rvSummaryDataKendaraanfvbi, rvSummaryDataPejalanKakifvbi;
    private AccidentRecyclerviewDriverAdapter rvAdaptSummaryKendaraan;
    private AccidentRecyclerviewPedestrianAdapter rvAdaptSummaryPedestrian;

    private Realm realm;
    private RequestRealmDbObject models;

    private int localId;
    private boolean isPileup = false;
    private int totalVehicle;
    private TextView tvSummaryTitlefvbi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.gandsoft.irsms.R.layout.activity_summary_accident);

        Intent bundle = getIntent();
        if (bundle.hasExtra("Local_ID")) {
            localId = bundle.getIntExtra("Local_ID", 0);
        }

        initComponent();
        initContent();
    }

    private void initComponent() {

        tvSummaryTitlefvbi = findViewById(com.gandsoft.irsms.R.id.tvSummaryTitle);

        cvSummaryKeluarfvbi = findViewById(com.gandsoft.irsms.R.id.cvSummaryKeluar);
        cvSummaryEditInfoAwalfvbi = findViewById(com.gandsoft.irsms.R.id.cvSummaryEditInfoAwal);
        cvSummaryDiagramMappingfvbi = findViewById(com.gandsoft.irsms.R.id.cvSummaryDiagramMapping);
        cvSummaryDataSaksifvbi = findViewById(com.gandsoft.irsms.R.id.cvSummaryDataSaksi);
        cvSummaryDataPenumpangfvbi = findViewById(com.gandsoft.irsms.R.id.cvSummaryDataPenumpang);
        cvSummaryTambahKendaraanfvbi = findViewById(com.gandsoft.irsms.R.id.cvSummaryTambahKendaraan);
        cvSummaryTambahPejalanKakifvbi = findViewById(com.gandsoft.irsms.R.id.cvSummaryTambahPejalanKaki);

        ivSummaryDataKecelakaanGPSfvbi = findViewById(com.gandsoft.irsms.R.id.ivSummaryDataKecelakaanGPS);
        tvSummaryDataKecelakaanTanggalfvbi = findViewById(com.gandsoft.irsms.R.id.tvSummaryDataKecelakaanTanggal);
        tvSummaryDataKecelakaanJamfvbi = findViewById(com.gandsoft.irsms.R.id.tvSummaryDataKecelakaanJam);
        tvSummaryDataKecelakaanPejalanKakifvbi = findViewById(com.gandsoft.irsms.R.id.tvSummaryDataKecelakaanPejalanKaki);
        tvSummaryDataKecelakaanJumlahSaksifvbi = findViewById(com.gandsoft.irsms.R.id.tvSummaryDataKecelakaanJumlahSaksi);

        ivSummaryFotoLaka1fvbi = findViewById(com.gandsoft.irsms.R.id.ivSummaryFotoLaka1);
        ivSummaryFotoLaka2fvbi = findViewById(com.gandsoft.irsms.R.id.ivSummaryFotoLaka2);
        ivSummaryFotoLaka3fvbi = findViewById(com.gandsoft.irsms.R.id.ivSummaryFotoLaka3);
        ivSummaryFotoLaka4fvbi = findViewById(com.gandsoft.irsms.R.id.ivSummaryFotoLaka4);
        ivSummaryFotoLaka5fvbi = findViewById(com.gandsoft.irsms.R.id.ivSummaryFotoLaka5);

        rvSummaryDataKendaraanfvbi = findViewById(com.gandsoft.irsms.R.id.rvSummaryDataKendaraan);
        rvSummaryDataPejalanKakifvbi = findViewById(com.gandsoft.irsms.R.id.rvSummaryDataPejalanKaki);
    }

    @SuppressLint("NewApi")
    private void initContent() {

        if (SystemUtil.isKitkat()){
            tvSummaryTitlefvbi.bringToFront();
        }



        realm = Realm.getDefaultInstance();
        realm.refresh();

        models = realm.where(RequestRealmDbObject.class).sort("Local_ID").equalTo("Local_ID", localId).findFirst();

        String[] informasiKhusus = models.getSetAccidentRequestModel().get(0).getInformasi_khusus().split(",");
        List<String> sd = Arrays.asList(informasiKhusus);
        Log.wtf("Lihat initValidation AccidentSummaryActivitywtf", String.valueOf(sd));
        Log.wtf("Lihat initValidation AccidentSummaryActivitywtf", String.valueOf(informasiKhusus));
        Log.wtf("Lihat initValidation AccidentSummaryActivitywtf", String.valueOf(models.getSetAccidentRequestModel().get(0).getInformasi_khusus()));
        totalVehicle = models.getSetDriverRequestModels().size();

        if (sd.contains("A0602")) {
            cvSummaryDataSaksifvbi.setVisibility(View.INVISIBLE);
        } else {
            cvSummaryDataSaksifvbi.setVisibility(View.VISIBLE);
        }

        if (sd.contains("A0603")) {
            isPileup = true;
        } else {
            isPileup = false;

        }

        tvSummaryDataKecelakaanTanggalfvbi.setText(models.getSetAccidentRequestModel().get(0).getTanggal_kejadian());
        tvSummaryDataKecelakaanJamfvbi.setText(models.getSetAccidentRequestModel().get(0).getJam_kejadian());
        tvSummaryDataKecelakaanPejalanKakifvbi.setText(models.getSetPedestrianRequestModels() != null ? models.getSetPedestrianRequestModels().size() + "" : "0");
        tvSummaryDataKecelakaanJumlahSaksifvbi.setText(models.getSetWitnessRequestModels() != null ? models.getSetWitnessRequestModels().size() + "" : "0");
        String[] base64 = models.getAddAccidentStateModel().get(0).getPhotoBase64();

        try {
            ivSummaryFotoLaka1fvbi.setImageResource(com.gandsoft.irsms.R.drawable.img_placeholder);
            ivSummaryFotoLaka2fvbi.setImageResource(com.gandsoft.irsms.R.drawable.img_placeholder);
            ivSummaryFotoLaka3fvbi.setImageResource(com.gandsoft.irsms.R.drawable.img_placeholder);
            ivSummaryFotoLaka4fvbi.setImageResource(com.gandsoft.irsms.R.drawable.img_placeholder);
            ivSummaryFotoLaka5fvbi.setImageResource(com.gandsoft.irsms.R.drawable.img_placeholder);

            ivSummaryFotoLaka1fvbi.setImageBitmap(SystemUtil.Base64ToBitmap(base64[0]));
            ivSummaryFotoLaka2fvbi.setImageBitmap(SystemUtil.Base64ToBitmap(base64[1]));
            ivSummaryFotoLaka3fvbi.setImageBitmap(SystemUtil.Base64ToBitmap(base64[2]));
            ivSummaryFotoLaka4fvbi.setImageBitmap(SystemUtil.Base64ToBitmap(base64[3]));
            ivSummaryFotoLaka5fvbi.setImageBitmap(SystemUtil.Base64ToBitmap(base64[4]));
        } catch (Exception e) {
            //Log.e("ERR", e.getMessage());
        }

        rvAdaptSummaryKendaraan = new AccidentRecyclerviewDriverAdapter(this, models.getSetDriverRequestModels(), localId);
        rvSummaryDataKendaraanfvbi.setLayoutManager(new LinearLayoutManager(this, LinearLayout.VERTICAL, false));
        rvSummaryDataKendaraanfvbi.setAdapter(rvAdaptSummaryKendaraan);

        rvAdaptSummaryPedestrian = new AccidentRecyclerviewPedestrianAdapter(this, models.getSetPedestrianRequestModels());
        rvSummaryDataPejalanKakifvbi.setLayoutManager(new LinearLayoutManager(this, LinearLayout.VERTICAL, false));
        rvSummaryDataPejalanKakifvbi.setAdapter(rvAdaptSummaryPedestrian);

        List<DriverDBModel> driverLists = models.getSetDriverRequestModels();
        List<String> noTnkbLists = new ArrayList<>();
        for (DriverDBModel mdls :
                driverLists) {
            noTnkbLists.add(mdls.getPlate_no());
        }

        cvSummaryKeluarfvbi.setOnClickListener(this);
        cvSummaryEditInfoAwalfvbi.setOnClickListener(this);
        cvSummaryDiagramMappingfvbi.setOnClickListener(this);
        cvSummaryDataSaksifvbi.setOnClickListener(this);
        cvSummaryDataPenumpangfvbi.setOnClickListener(this);
        cvSummaryTambahKendaraanfvbi.setOnClickListener(this);
        cvSummaryTambahPejalanKakifvbi.setOnClickListener(this);

        String lat = models.getSetAccidentRequestModel().get(0).getLatitude();
        String lng = models.getSetAccidentRequestModel().get(0).getLongitude();
        boolean isLatLng = !lat.isEmpty() && !lng.isEmpty();
        if (isLatLng){
            if (isKitkat()) {
                ivSummaryDataKecelakaanGPSfvbi.setImageResource(R.drawable.img_markermaps);
            } else {
                ivSummaryDataKecelakaanGPSfvbi.setImageDrawable(getDrawable(R.drawable.img_markermaps));
            }
        }else {
            if (isKitkat()) {
                ivSummaryDataKecelakaanGPSfvbi.setImageResource(R.drawable.img_markermaps_merah);
            } else {
                ivSummaryDataKecelakaanGPSfvbi.setImageDrawable(getDrawable(R.drawable.img_markermaps_merah));
            }
        }
    }

    private void moveToAddAccident() {
        Intent addAccidentIntent = new Intent(AccidentSummaryActivity.this, AddAccidentActivity.class);
        addAccidentIntent.putExtra("Local_ID", localId);
        addAccidentIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(addAccidentIntent, IConfig.API_CALLER_SET_ACCIDENT);
    }

    private void moveToDashBoard() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
        /*Intent intent = new Intent(AccidentSummaryActivity.this, DashboardActivity.class);
        startActivity(intent);
        finish();*/
    }

    @Override
    public void onClick(View v) {
        if (v == cvSummaryKeluarfvbi) {
            onBackPressed();
        } else if (v == cvSummaryEditInfoAwalfvbi) {
            moveToAddAccident();
            //finish();
        } else if (v == cvSummaryDiagramMappingfvbi) {
            Intent intent = new Intent(AccidentSummaryActivity.this, AddDiagraMappingActivity.class);
            intent.putExtra("Local_ID", localId);
            startActivityForResult(intent,8);
        } else if (v == cvSummaryDataSaksifvbi) {
            Intent intent = new Intent(AccidentSummaryActivity.this, WitnessSummaryActivity.class);
            intent.putExtra("Local_ID", localId);
            startActivityForResult(intent,8);
        } else if (v == cvSummaryDataPenumpangfvbi) {
            Intent intent = new Intent(AccidentSummaryActivity.this, PassangerSummaryActivity.class);
            intent.putExtra("Local_ID", localId);
            startActivityForResult(intent,8);
        } else if (v == cvSummaryTambahKendaraanfvbi) {
            Intent intent = new Intent(AccidentSummaryActivity.this, AddDriverActivity.class);
            intent.putExtra("Local_ID", localId);
            startActivityForResult(intent, 8);
        } else if (v == cvSummaryTambahPejalanKakifvbi) {
            Intent intent = new Intent(AccidentSummaryActivity.this, AddPedestrianActivity.class);
            intent.putExtra("Local_ID", localId);
            startActivityForResult(intent, 8);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        initContent();
    }

    @Override
    public void deleteVehicleItemAt(final int positionId) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        try {
                            realm = Realm.getDefaultInstance();
                            realm.executeTransactionAsync(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    //deleteVehicle
                                    realm.where(DriverDBModel.class).equalTo("Local_ID", localId).equalTo("vehicle_Dbid", positionId).findAll().deleteAllFromRealm();
                                    realm.where(AddDriverStateModel.class).equalTo("Local_ID", localId).equalTo("vehicle_Dbid", positionId).findAll().deleteAllFromRealm();

                                    //deletePassanger
                                    realm.where(PassangerDBModel.class).equalTo("Local_ID", localId).equalTo("vehicle_SubID", positionId).findAll().deleteAllFromRealm();
                                    realm.where(AddPassangerStateModel.class).equalTo("Local_ID", localId).equalTo("vehicle_SubID", positionId).findAll().deleteAllFromRealm();

//                                  //deletePedestrian
                                    realm.where(PedestrianDBModel.class).equalTo("Local_ID", localId).equalTo("vehicle_id", String.valueOf(positionId)).findAll().deleteAllFromRealm();
                                    realm.where(AddPedestrianStateModel.class).equalTo("Local_ID", localId).equalTo("vehicle_id", String.valueOf(positionId)).findAll().deleteAllFromRealm();
                                }
                            }, new Realm.Transaction.OnSuccess() {
                                @Override
                                public void onSuccess() {
                                    initContent();
                                }
                            });
                            realm.refresh();
                        } finally {
                            if (realm != null) {
                                realm.close();
                            }
                        }
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Hapus laporan ini?")
                .setPositiveButton("Ya", dialogClickListener)
                .setNegativeButton("Tidak", dialogClickListener).show();
    }

    @Override
    public void editVehicleItemAt(int orderId) {
        Intent editIntent = new Intent(this, AddDriverActivity.class);
        editIntent.putExtra("Local_ID", localId);
        editIntent.putExtra("Order_ID", orderId);
        startActivityForResult(editIntent, 8);
    }

    @Override
    public void moveToPassangerSummary(int subID) {
        Intent intent = new Intent(AccidentSummaryActivity.this, PassangerSummaryActivity.class);
        intent.putExtra("Local_ID", localId);
        intent.putExtra("Local_SubID", subID);
        startActivityForResult(intent, 8);
    }

    @Override
    public void deletePedestrianRealmModelAt(int id) {
        final int internalId = id;
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        try {
                            realm = Realm.getDefaultInstance();
                            realm.executeTransactionAsync(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    RequestRealmDbObject model = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst();
                                    model.getSetPedestrianRequestModels().remove(internalId);
                                    model.getSetPedestrianStateModels().remove(internalId);
                                    realm.copyToRealmOrUpdate(model);

                                }
                            }, new Realm.Transaction.OnSuccess() {
                                @Override
                                public void onSuccess() {
                                    rvAdaptSummaryPedestrian.notifyDataSetChanged();
                                    Toast.makeText(getApplicationContext(), "Delete Success", Toast.LENGTH_SHORT).show();
                                }
                            });
                            realm.refresh();
                        } finally {
                            if (realm != null) {
                                realm.close();
                            }
                        }
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Hapus laporan ini?").
                setPositiveButton("Ya", dialogClickListener)
                .setNegativeButton("Tidak", dialogClickListener).show();
    }

    @Override
    public void editPedestrianRealmModelAt(int id) {
        Intent editIntent = new Intent(this, AddPedestrianActivity.class);
        editIntent.putExtra("Local_ID", localId);
        editIntent.putExtra("Order_ID", id);
        startActivityForResult(editIntent, 8);
    }

    @Override
    public void onBackPressed() {
        if (isPileup) {
            if (totalVehicle >= 3) {
                moveToDashBoard();
            } else {
                Toast.makeText(getApplicationContext(), "Kendaraan Kurang Dari 3", Toast.LENGTH_SHORT).show();
            }
        } else {
            moveToDashBoard();
        }
    }
}
