package com.gandsoft.irsms.Activity.DiagramMapping;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.gandsoft.irsms.Activity.LocalBaseActivity;
import com.gandsoft.irsms.Presenter.witget.ConstrainLayoutRadioGroup;
import com.gandsoft.irsms.R;
import com.gandsoft.irsms.Support.Util.SystemUtil;
import com.gandsoft.irsms.Support.ViewGenerator.ViewGeneratorSpinnerAdapter;
import com.gandsoft.irsms.model.RealmDbModel.AccidentForm.AddAccidentStateModel;
import com.gandsoft.irsms.model.RealmDbModel.DiagramMappingForm.AddDiagraMappingStateModel;
import com.gandsoft.irsms.model.RealmDbModel.DiagramMappingForm.DiagramMappingDBModel;
import com.gandsoft.irsms.model.RealmDbModel.PedestrianForm.PedestrianDBModel;
import com.gandsoft.irsms.model.RealmDbModel.RequestRealmDbObject;
import com.gandsoft.irsms.model.RealmDbModel.VehicleForm.DriverDBModel;
import com.gandsoft.irsms.model.uiModel.ViewModel;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by kanzan on 2/14/2018.
 */

public class AddDiagraMappingActivity extends LocalBaseActivity {

    private View cvDiagramMappingStartfvbi, cvDiagramMappingKeluarfvbi;
    private ImageView ivDataLakaTipeLakafvbi;
    private Spinner spDataMappingTerlibatAfvbi, spDataMappingTerlibatBfvbi;
    private ConstrainLayoutRadioGroup constrainRadioGroup;
    private View ccad;
    private Realm realm;
    private int localId;

    private boolean loadedFromState;
    private LinearLayout llDiagramMappingTitlefvbi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_diagram_mapping);

        Intent bundle = getIntent();
        if (bundle.hasExtra("Local_ID")) {
            localId = bundle.getIntExtra("Local_ID", 0);
        }
        initComponent();
        initContent();
        realm = Realm.getDefaultInstance();
        if (realm.where(RequestRealmDbObject.class).sort("Local_ID").equalTo("Local_ID", localId).findFirst().getSetDiagramMappingRequestModel().size() > 0) {
            loadedFromState = true;
            loadState();
        }

        if (!realm.isClosed()) {
            realm.close();
        }
    }

    private void initComponent() {
        llDiagramMappingTitlefvbi = findViewById(R.id.llDiagramMappingTitle);
        cvDiagramMappingKeluarfvbi = findViewById(R.id.cvDiagramMappingKeluar);
        cvDiagramMappingStartfvbi = findViewById(R.id.cvDiagramMappingStart);
        ivDataLakaTipeLakafvbi = findViewById(R.id.ivDataLakaTipeLaka);
        spDataMappingTerlibatAfvbi = findViewById(R.id.spDataMappingTerlibatA);
        spDataMappingTerlibatBfvbi = findViewById(R.id.spDataMappingTerlibatB);
        ccad = findViewById(R.id.cacd);
        constrainRadioGroup = findViewById(R.id.cingview);
        constrainRadioGroup.initiateViewOperation();

    }

    private void initContent() {
        if (SystemUtil.isKitkat()) {
            llDiagramMappingTitlefvbi.bringToFront();
        }

        cvDiagramMappingStartfvbi.setOnClickListener(v -> checkValidation());
        cvDiagramMappingKeluarfvbi.setOnClickListener(v -> onBackPressed());
        loadData();

    }

    private void loadData() {
        realm = Realm.getDefaultInstance();
        RequestRealmDbObject requestRealmDbObject = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst();
        int sizedriverdb = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst().getSetDriverRequestModels().size();
        int sizepedesdb = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst().getSetPedestrianRequestModels().size();
        AddAccidentStateModel addAccidentStateModel = requestRealmDbObject.getAddAccidentStateModel().get(0);
        ivDataLakaTipeLakafvbi.setImageResource(addAccidentStateModel.getTipe_kecelakaan_id());
        int accidentType = Integer.parseInt(addAccidentStateModel.getTipe_kecelakaan().substring(3, 4));

        List<DriverDBModel> driverModels = requestRealmDbObject.getSetDriverRequestModels();
        List<PedestrianDBModel> pedestrianModels = requestRealmDbObject.getSetPedestrianRequestModels();

        List<ViewModel> viewmodelA = new ArrayList<>();
        List<ViewModel> viewmodelB = new ArrayList<>();

        if (accidentType >= 0 && accidentType <= 1) {
            if ((sizepedesdb < 1 && sizedriverdb < 1)) {
                finish();
                Toast.makeText(getApplicationContext(), "Tipe Kecelakaan Kendaraan & Pejalan kaki\n\nData Kendaraan Masih Kurang Dari 1 &\nData Pejalan kaki Masih Kurang Dari 1", Toast.LENGTH_LONG).show();
            } else if ((sizepedesdb > 0 && sizedriverdb < 1) || (sizepedesdb < 1 && sizedriverdb > 0)) {
                finish();
                Toast.makeText(getApplicationContext(), "Tipe Kecelakaan Kendaraan & Pejalan kaki\n\nData Kendaraan Masih Kurang Dari 1 /\n Data Pejalan kaki Masih Kurang Dari 1", Toast.LENGTH_LONG).show();
            } else {
                for (DriverDBModel model : driverModels) {
                    ViewModel temp = new ViewModel();
                    temp.setViewID("vec" + model.getVehicle_Dbid());
                    String[] split1 = model.getVehicleCompoundKey().split("/")[1].split("\\|");
                    String contentDriver = "(" + split1[0].toUpperCase() + ") " + split1[2];
                    temp.setTextContent(contentDriver);
                    temp.setItemResourceID(R.layout.component_spinneritem_dropdown);
                    temp.setItemContainerID(R.layout.component_spinneritem_display);
                    viewmodelA.add(temp);
                }
                for (PedestrianDBModel model : pedestrianModels) {
                    ViewModel temp = new ViewModel();
                    temp.setViewID("ped" + model.getPedestrian_Dbid());
                    temp.setTextContent(model.getFirst_name().toUpperCase() + " " + model.getLast_name().toUpperCase());
                    temp.setItemResourceID(R.layout.component_spinneritem_dropdown);
                    temp.setItemContainerID(R.layout.component_spinneritem_display);
                    viewmodelB.add(temp);
                }
                ViewGeneratorSpinnerAdapter adapterLakaA = new ViewGeneratorSpinnerAdapter(this, viewmodelA);
                ViewGeneratorSpinnerAdapter adapterLakaB = new ViewGeneratorSpinnerAdapter(this, viewmodelB);
                spDataMappingTerlibatAfvbi.setAdapter(adapterLakaA);
                spDataMappingTerlibatBfvbi.setAdapter(adapterLakaB);
            }
        } else if (accidentType >= 2 && accidentType <= 3) {
            if (sizedriverdb < 1) {
                finish();
                Toast.makeText(getApplicationContext(), "Tipe Kecelakaan Kendaraan Tunggal\n\nData Kendaraan Masih Kurang Dari 1", Toast.LENGTH_LONG).show();
            } else {
                for (DriverDBModel model : driverModels) {
                    ViewModel temp = new ViewModel();
                    temp.setViewID("vec" + model.getVehicle_Dbid());
                    String[] split1 = model.getVehicleCompoundKey().split("/")[1].split("\\|");
                    String contentDriver = "(" + split1[0].toUpperCase() + ") " + split1[2];
                    temp.setTextContent(contentDriver);
                    temp.setItemResourceID(R.layout.component_spinneritem_dropdown);
                    temp.setItemContainerID(R.layout.component_spinneritem_display);
                    viewmodelA.add(temp);
                }
                ViewGeneratorSpinnerAdapter adapterLakaA = new ViewGeneratorSpinnerAdapter(this, viewmodelA);
                spDataMappingTerlibatAfvbi.setAdapter(adapterLakaA);
                ccad.setVisibility(View.INVISIBLE);
            }
        } else {
            if (sizedriverdb < 2) {
                finish();
                Toast.makeText(getApplicationContext(), "Tipe Kecelakaan Kendaraan & Kendaraan\n\nData Kendaraan Masih Kurang Dari 2", Toast.LENGTH_LONG).show();
            } else {
                for (DriverDBModel model : driverModels) {
                    ViewModel temp = new ViewModel();
                    temp.setViewID("vec" + model.getVehicle_Dbid());
                    String[] split1 = model.getVehicleCompoundKey().split("/")[1].split("\\|");
                    String contentDriver = "(" + split1[0].toUpperCase() + ") " + split1[2];
                    temp.setTextContent(contentDriver);
                    temp.setItemResourceID(R.layout.component_spinneritem_dropdown);
                    temp.setItemContainerID(R.layout.component_spinneritem_display);
                    viewmodelA.add(temp);
                }
                for (DriverDBModel model : driverModels) {
                    ViewModel temp = new ViewModel();
                    temp.setViewID("vec" + model.getVehicle_Dbid());
                    String[] split1 = model.getVehicleCompoundKey().split("/")[1].split("\\|");
                    String contentDriver = "(" + split1[0].toUpperCase() + ") " + split1[2];
                    temp.setTextContent(contentDriver);
                    temp.setItemResourceID(R.layout.component_spinneritem_dropdown);
                    temp.setItemContainerID(R.layout.component_spinneritem_display);
                    viewmodelB.add(temp);
                }
                ViewGeneratorSpinnerAdapter adapterLakaA = new ViewGeneratorSpinnerAdapter(this, viewmodelA) {
                    @Override
                    public boolean isEnabled(int position) {
                        return super.isEnabled(position);
                    }
                };
                ViewGeneratorSpinnerAdapter adapterLakaB = new ViewGeneratorSpinnerAdapter(this, viewmodelB);
                spDataMappingTerlibatAfvbi.setAdapter(adapterLakaA);
                spDataMappingTerlibatBfvbi.setAdapter(adapterLakaB);

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        //code here
                        spDataMappingTerlibatBfvbi.setSelection(1);
                    }
                }, 300);
            }
        }
    }

    private void loadState() {
        realm = Realm.getDefaultInstance();
        AddDiagraMappingStateModel state = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst().getAddDiagramMappingStateModel().where().findFirst();
        spDataMappingTerlibatAfvbi.setSelection(state.getAccident_a());
        spDataMappingTerlibatBfvbi.setSelection(state.getAccident_b());
        constrainRadioGroup.selectItem(state.getAccident_direction());
        if (!realm.isClosed()) {
            realm.close();
        }


    }

    private DiagramMappingDBModel generateRequestModel(@Nullable DiagramMappingDBModel requestModel) {
        if (requestModel == null) {
            requestModel = new DiagramMappingDBModel();
        }

        requestModel.setAccident_a(((ViewGeneratorSpinnerAdapter) spDataMappingTerlibatAfvbi.getAdapter()).getModelViewID(spDataMappingTerlibatAfvbi.getSelectedItemPosition()));
        try {
            requestModel.setAccident_b(((ViewGeneratorSpinnerAdapter) spDataMappingTerlibatBfvbi.getAdapter()).getModelViewID(spDataMappingTerlibatBfvbi.getSelectedItemPosition()));
        } catch (Exception e) {
            //only data a exist
        }

        requestModel.setAccident_direction(constrainRadioGroup.getSelectedTag());
        return requestModel;
    }

    private AddDiagraMappingStateModel generateStateModel(@Nullable AddDiagraMappingStateModel stateModel) {
        if (stateModel == null) {
            stateModel = new AddDiagraMappingStateModel();
        }

        stateModel.setAccident_id(localId);
        stateModel.setAccident_a(spDataMappingTerlibatAfvbi.getSelectedItemPosition());
        stateModel.setAccident_b(spDataMappingTerlibatBfvbi.getSelectedItemPosition());
        stateModel.setAccident_direction(constrainRadioGroup.getSelectedIndex());

        return stateModel;
    }

    private void saveOverwriteData() {
        realm = Realm.getDefaultInstance();
        if (!loadedFromState) {
            if (saveData()) {
                Toast.makeText(getApplicationContext(), "Save Success", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Save Failed", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (overwriteData()) {
                Toast.makeText(getApplicationContext(), "Overwrite Success", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Overwrite Failed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean saveData() {
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    RequestRealmDbObject poolRequestModel = realm.where(RequestRealmDbObject.class).sort("Local_ID").equalTo("Local_ID", localId).findFirst();
                    poolRequestModel.getSetDiagramMappingRequestModel().add(generateRequestModel(null));
                    poolRequestModel.getAddDiagramMappingStateModel().add(generateStateModel(null));
                    realm.copyToRealmOrUpdate(poolRequestModel);
                }
            });
            realm.refresh();
            realm.close();
            return true;
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    private boolean overwriteData() {
        try {
            realm = Realm.getDefaultInstance();
            final RequestRealmDbObject poolRequestModel = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst();

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    generateRequestModel(poolRequestModel.getSetDiagramMappingRequestModel().first());
                    generateStateModel(poolRequestModel.getAddDiagramMappingStateModel().first());
                }
            });
            realm.refresh();
            realm.close();
            return true;
        } catch (Exception e) {
            //Log.e("ERR", "saveData: " + e.getMessage());
            return false;
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    private void checkValidation() {
        realm = Realm.getDefaultInstance();
        RequestRealmDbObject requestRealmDbObject = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst();
        AddAccidentStateModel addAccidentStateModel = requestRealmDbObject.getAddAccidentStateModel().get(0);
        ivDataLakaTipeLakafvbi.setImageResource(addAccidentStateModel.getTipe_kecelakaan_id());
        int accidentType = Integer.parseInt(addAccidentStateModel.getTipe_kecelakaan().substring(3, 4));
        int posspA = spDataMappingTerlibatAfvbi.getSelectedItemPosition();
        int posspB = spDataMappingTerlibatBfvbi.getSelectedItemPosition();

        if (accidentType >= 4) {
            if (posspA != posspB) {
                saveOverwriteData();
            } else {
                Toast.makeText(getApplicationContext(), "Kendaraan antara laka A dan B tidak boleh sama", Toast.LENGTH_SHORT).show();
            }
        } else {
            saveOverwriteData();
        }
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        checkValidation();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        finish();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(AddDiagraMappingActivity.this);
        builder.setMessage("Simpan data sebelum keluar?").setPositiveButton("Ya", dialogClickListener)
                .setNegativeButton("Tidak", dialogClickListener).show();
    }
}
