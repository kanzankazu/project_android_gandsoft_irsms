package com.gandsoft.irsms.Activity.summary.sub;

/**
 * Created by gleen on 14/02/18.
 */

public interface PassengerObjectHookInteface {

    void deletePassengerRealmModelAt(int id);

    void editPassengerRealmModelAt(int id);

}
