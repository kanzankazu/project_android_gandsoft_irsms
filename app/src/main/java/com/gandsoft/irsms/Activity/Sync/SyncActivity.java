package com.gandsoft.irsms.Activity.Sync;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gandsoft.irsms.API.API;
import com.gandsoft.irsms.Activity.LocalBaseActivity;
import com.gandsoft.irsms.IConfig;
import com.gandsoft.irsms.ISeasonConfig;
import com.gandsoft.irsms.IrsmsApp;
import com.gandsoft.irsms.Presenter.DAO.AuthDao;
import com.gandsoft.irsms.Presenter.DAO.MainPageDao;
import com.gandsoft.irsms.Presenter.SeasonManager.SessionManager;
import com.gandsoft.irsms.R;
import com.gandsoft.irsms.Support.Util.DialogUtil;
import com.gandsoft.irsms.Support.Util.ResponseUtil;
import com.gandsoft.irsms.Support.Util.SystemUtil;
import com.gandsoft.irsms.model.ObjectModel.UserDataModel;
import com.gandsoft.irsms.model.RealmDbModel.AccidentForm.AccidentDBModel;
import com.gandsoft.irsms.model.RealmDbModel.AccidentForm.AddAccidentStateModel;
import com.gandsoft.irsms.model.RealmDbModel.DiagramMappingForm.DiagramMappingDBModel;
import com.gandsoft.irsms.model.RealmDbModel.PassengerForm.AddPassangerStateModel;
import com.gandsoft.irsms.model.RealmDbModel.PassengerForm.PassangerDBModel;
import com.gandsoft.irsms.model.RealmDbModel.PedestrianForm.AddPedestrianStateModel;
import com.gandsoft.irsms.model.RealmDbModel.PedestrianForm.PedestrianDBModel;
import com.gandsoft.irsms.model.RealmDbModel.RequestRealmDbObject;
import com.gandsoft.irsms.model.RealmDbModel.VehicleForm.AddDriverStateModel;
import com.gandsoft.irsms.model.RealmDbModel.VehicleForm.DriverDBModel;
import com.gandsoft.irsms.model.RealmDbModel.WitnessForm.AddWitnessStateModel;
import com.gandsoft.irsms.model.RealmDbModel.WitnessForm.SetWitnessDBModel;
import com.gandsoft.irsms.model.RequestModel.AccidentForm.SetAccidentRequestModel;
import com.gandsoft.irsms.model.RequestModel.AccidentForm.SetCheckAccidentRequestModel;
import com.gandsoft.irsms.model.RequestModel.AccidentForm.UploadPhotoRequestModel;
import com.gandsoft.irsms.model.RequestModel.Auth.LoginRequestModel;
import com.gandsoft.irsms.model.RequestModel.DiagramMappingForm.SetDiagramMappingRequestModel;
import com.gandsoft.irsms.model.RequestModel.PassangerForm.SetPassangerRequestModel;
import com.gandsoft.irsms.model.RequestModel.PedestrianForm.SetPedestrianRequestModel;
import com.gandsoft.irsms.model.RequestModel.VehicleForm.SetDriverRequestModel;
import com.gandsoft.irsms.model.RequestModel.WitnessForm.SetWitnessRequestModel;
import com.gandsoft.irsms.model.ResponseModel.AccidentForm.SetAccidentResponseModel;
import com.gandsoft.irsms.model.ResponseModel.AccidentForm.SetCheckAccidentResponseModel;
import com.gandsoft.irsms.model.ResponseModel.AccidentForm.UploadPhotoResponseModel;
import com.gandsoft.irsms.model.ResponseModel.BaseResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.SetDiagramMappingResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.SetDriverResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.SetPassangerResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.SetPedestrianResponseModel;
import com.gandsoft.irsms.model.ResponseModel.VehicleForm.SetWitnessResponseModel;
import com.gandsoft.irsms.model.uiModel.SyncViewModel;

import java.util.ArrayList;
import java.util.List;

import app.beelabs.com.codebase.base.BaseDao;
import app.beelabs.com.codebase.base.response.BaseResponse;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gleen on 15/02/18.
 */

public class SyncActivity extends LocalBaseActivity implements SyncHookInterface {

    private EditText etSyncPassword;
    private TextView tvPersonnelName, tvSyncStatus, totjing, tvSyncKesatuan, spSyncNRP;
    private RecyclerView lvSyncStatus;
    private View cvSyncKeluar, cvSyncStart;

    private SycnRecyclerAdapter adapter;
    private UserDataModel currentUser;

    private List<SyncViewModel> menuUi = new ArrayList<>();
    private List<RequestRealmDbObject> jenk = new ArrayList<>();

    private Realm realm;
    private boolean isRealmNull = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.gandsoft.irsms.R.layout.activity_form_sync);
        initComponent();
        initContent();
    }

    private void initComponent() {
        tvPersonnelName = (TextView) findViewById(com.gandsoft.irsms.R.id.tvPersonnelName);
        spSyncNRP = (TextView) findViewById(com.gandsoft.irsms.R.id.spSyncNRP);
        etSyncPassword = (EditText) findViewById(com.gandsoft.irsms.R.id.etSyncPassword);
        tvSyncStatus = (TextView) findViewById(com.gandsoft.irsms.R.id.tvSyncStatus);
        tvSyncKesatuan = (TextView) findViewById(com.gandsoft.irsms.R.id.tvSyncKesatuan);
        lvSyncStatus = (RecyclerView) findViewById(com.gandsoft.irsms.R.id.lvSyncStatus);
        cvSyncKeluar = (View) findViewById(com.gandsoft.irsms.R.id.cvSyncKeluar);
        cvSyncStart = (View) findViewById(com.gandsoft.irsms.R.id.cvSyncStart);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private void initContent() {
        currentUser = SessionManager.loadUserData();

        realm = Realm.getDefaultInstance();
        jenk = realm.where(RequestRealmDbObject.class).findAll();
        tvPersonnelName.setText(currentUser.getFirst_name() + " " + currentUser.getLast_name());
        lvSyncStatus.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        spSyncNRP.setText(currentUser.getOfficer_id() + "(" + currentUser.getFirst_name() + ")");
        adapter = new SycnRecyclerAdapter(this, menuUi);
        lvSyncStatus.setAdapter(adapter);
        tvSyncStatus.setText("Data Laka: " + jenk.size() + "");
        tvSyncKesatuan.setText(currentUser.getPolda_name() + " " + currentUser.getPolres_name());

        cvSyncStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (SystemUtil.isNetworkAvailable(SyncActivity.this)) {
                        if (jenk.size() != 0) {
                            callUiItemAPI();
                        } else {
                            onBackPressed();
                            Toast.makeText(getApplicationContext(), "Data kosong silahkan input data terlebih dahulu", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        generateErrorDialog("Error", "Tidak Ada Koneksi", "Koneksi ke jaringan tidak terdeteksi, Coba lagi setelah Anda terhubung ke jaringan");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        cvSyncKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void callUiItemAPI() {
        showApiProgressDialog(IrsmsApp.getAppComponent(), new MainPageDao(this) {
            @Override
            public void call() {
                super.doGetUiItemDao(SyncActivity.this, BaseDao.getInstance(SyncActivity.this, IConfig.API_CALLER_UIITEM_CODE).callback);
            }
        });
    }

    private void doLogin() {
        LoginRequestModel requestModel = new LoginRequestModel();
        requestModel.setImei(SystemUtil.getImei(SyncActivity.this));
        requestModel.setUsername(currentUser.getUsername());
        requestModel.setPassword(etSyncPassword.getText().toString());
        dologinApi(requestModel);
    }

    private void dologinApi(final LoginRequestModel requestModel) {
        showApiProgressDialog(IrsmsApp.getAppComponent(), new AuthDao(this) {
            @Override
            public void call() {
                super.doLoginDao(SyncActivity.this, requestModel, BaseDao.getInstance(SyncActivity.this, IConfig.API_CALLER_LOGIN_CODE).callback);
            }
        });
    }

    private void doSync() {
        realm = Realm.getDefaultInstance();
        List<RequestRealmDbObject> realmObjects = realm.where(RequestRealmDbObject.class).findAll();

        for (RequestRealmDbObject realmObject : realmObjects) {
            callSetAccidentAPI(realmObject.getLocal_ID(), realmObject);
            adapter.addModel(new SyncViewModel(realmObject.getLocal_ID(), "processing bundled data " + realmObject.getLocal_ID(), ""));
            adapter.notifyDataSetChanged();

        }
    }

    private void callSetAccidentAPI(int localID, RequestRealmDbObject dbObject) {

        adapter.updateModelAt(localID, "Sending Accident Data for local item " + localID, "On Progress");
        adapter.notifyDataSetChanged();

        /*if (dbObject.getSetDriverRequestModels().size() == 0) {
            adapter.updateModelAt(localID, "No Vehicle or Driver data " + localID, "canceled");
            adapter.notifyDataSetChanged();
        }*/

        AccidentDBModel accidentDBModel = dbObject.getSetAccidentRequestModel().first();
        AddAccidentStateModel accidentStateModel = dbObject.getAddAccidentStateModel().first();

        SetAccidentRequestModel request = new SetAccidentRequestModel(accidentDBModel);
        request.setReq_data(localID);
        request.setImei(SystemUtil.getImei(SyncActivity.this));

        API.doSetAccident(this, request, new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                try {
                    if (((SetAccidentResponseModel) response.body()).getStatus().equals("success")) {
                        adapter.updateModelAt(localID, "Sending Accident Data " + localID, "Success");
                        adapter.notifyDataSetChanged();

                        callUploadPhotoAPI(localID, ((SetAccidentResponseModel) response.body()).getAccident_id(), accidentStateModel, dbObject);
                    } else {
                        adapter.updateModelAt(localID, "Sending Accident Data " + localID, "Failed");
                        adapter.notifyDataSetChanged();
                        Log.d("Lihat onResponse SyncActivity", ((SetAccidentResponseModel) response.body()).getMessage());

                    }
                } catch (Exception e) {
                    adapter.updateModelAt(localID, "Error when upload accident Data " + localID, "Please check data & Reupload data again");
                    adapter.notifyDataSetChanged();
                    Log.d("Lihat onResponse SyncActivity", e.getMessage());
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                adapter.updateModelAt(localID, "Sending Accident Data " + localID + " were failed : ", t.getMessage());
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void callUploadPhotoAPI(int localID, String accidentID, AddAccidentStateModel model, RequestRealmDbObject dbObject) {
        UploadPhotoRequestModel request = new UploadPhotoRequestModel();
        request.setAccident_id(accidentID);
        String base64Request = "";
        if (model.getPhotoBase64() != null && model.getPhotoBase64().length > 0) {
            for (String base64 : model.getPhotoBase64()) {
                base64Request += "|" + base64;
            }
            try {
                base64Request.substring(1);
            } catch (Exception e) {
                adapter.updateModelAt(localID, "Sending Image Data " + localID, " Failed, No Data ");
                adapter.notifyDataSetChanged();
                callCheckAccidentAPI(accidentID, 0);
                return;
            }
            request.setPic(base64Request);
            adapter.updateModelAt(localID, "Sending Image Data " + localID, " On Progress");
            adapter.notifyDataSetChanged();

            API.doUploadPhoto(this, request, new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    try {
                        if (((UploadPhotoResponseModel) response.body()).getStatus().equals("success")) {
                            adapter.updateModelAt(localID, "Sending Image Data " + localID, "Success");
                            adapter.notifyDataSetChanged();
                            callSetWitnessAPI(localID, accidentID, dbObject.getSetWitnessRequestModels(), dbObject);
                        } else {
                            adapter.updateModelAt(localID, "Sending Image Data " + localID, "Failed");
                            adapter.notifyDataSetChanged();
                            callCheckAccidentAPI(accidentID, 0);
                            Log.d("Lihat onResponse SyncActivity", ((UploadPhotoResponseModel) response.body()).getMessage());
                        }
                    } catch (Exception e) {
                        adapter.updateModelAt(localID, "Error when upload Image Data " + localID, "Please check data & Reupload data again");
                        adapter.notifyDataSetChanged();
                        callCheckAccidentAPI(accidentID, 0);
                        Log.d("Lihat onResponse SyncActivity", e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    adapter.updateModelAt(localID, "Sending Image Data " + localID + " were failed : ", t.getMessage());
                    adapter.notifyDataSetChanged();
                    callCheckAccidentAPI(accidentID, 0);
                }
            });
        } else {
            callSetWitnessAPI(localID, accidentID, dbObject.getSetWitnessRequestModels(), dbObject);
        }
    }

    private void callSetWitnessAPI(final int localID, String accidentID, List<SetWitnessDBModel> witnessDBModels, RequestRealmDbObject dbObject) {
        int bundleCount = witnessDBModels.size();
        if (bundleCount > 0) {
            int counter = 1;
            adapter.updateModelAt(localID, "detected Witnesses Data " + localID + " with total of " + bundleCount, "On Progress");
            adapter.notifyDataSetChanged();
            for (SetWitnessDBModel model : witnessDBModels) {
                adapter.updateModelAt(localID, "Sending Witnesses Data " + localID + " " + counter + "/" + bundleCount, "On Progress");
                adapter.notifyDataSetChanged();
                SetWitnessRequestModel request = new SetWitnessRequestModel();
                request.consumeDBModel(model);
                request.setAccident_id(accidentID);
                final int finalCounter = counter;
                API.doSetWitness(this, request, new Callback() {
                    @Override
                    public void onResponse(Call call, Response response) {
                        try {
                            if (((SetWitnessResponseModel) response.body()).getStatus().equals("success")) {
                                adapter.updateModelAt(localID, "Sending Witnesses Data " + localID + " - " + finalCounter + "/" + bundleCount, "Success");
                                adapter.notifyDataSetChanged();

                                if (finalCounter == bundleCount) {

                                    try {
                                        Thread.sleep(200L);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                    if (dbObject.getSetDriverRequestModels().size() != 0) {
                                        callSetDriverAPI(localID, accidentID, dbObject);
                                    } else {
                                        deleteLocalDBat(localID);//setwitness
                                        callCheckAccidentAPI(accidentID, 1);//witness ada
                                    }
                                }
                            } else {
                                adapter.updateModelAt(localID, "Sending Witnesses Data " + localID + " - " + finalCounter + "/" + bundleCount, "Failed");
                                adapter.notifyDataSetChanged();
                                callCheckAccidentAPI(accidentID, 0);
                                Log.d("Lihat onResponse SyncActivity", ((SetWitnessResponseModel) response.body()).getMessage());
                            }
                        } catch (Exception e) {
                            adapter.updateModelAt(localID, "Error when upload witness Data " + localID + " - " + finalCounter + "/" + bundleCount, "Please check data & Reupload data again");
                            adapter.notifyDataSetChanged();
                            callCheckAccidentAPI(accidentID, 0);
                            Log.d("Lihat onResponse SyncActivity", e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {
                        adapter.updateModelAt(localID, "Sending Witnesses Data " + localID + " - " + finalCounter + "/" + bundleCount + " were failed : ", t.getMessage());
                        adapter.notifyDataSetChanged();
                        callCheckAccidentAPI(accidentID, 0);
                    }
                });
                counter++;
            }
        } else {
            if (dbObject.getSetDriverRequestModels().size() != 0) {
                callSetDriverAPI(localID, accidentID, dbObject);
            } else {
                deleteLocalDBat(localID);//setwitness
                callCheckAccidentAPI(accidentID, 1);//witness skip
            }
        }
    }

    private void callSetDriverAPI(int localID, String accidentID, RequestRealmDbObject dbObject) {
        List<DriverDBModel> driverDBModels = dbObject.getSetDriverRequestModels();
        int bundleCount = driverDBModels.size();

        SparseArray<String> driverIDstore = new SparseArray<>();

        if (bundleCount > 0) {
            int counter = 1;
            adapter.updateModelAt(localID, "detected driver data on " + localID + " with total of " + bundleCount, "On Progress");
            adapter.notifyDataSetChanged();
            for (DriverDBModel dbModel : driverDBModels) {
                adapter.updateModelAt(localID, "Sending driver Data for " + localID + " " + counter + "/" + bundleCount, "On Progress");
                adapter.notifyDataSetChanged();
                SetDriverRequestModel request = new SetDriverRequestModel(dbModel);
                request.setAccident_id(accidentID);
                final int finalCounter = counter;
                API.doSetDriver(this, request, new Callback() {
                    @Override
                    public void onResponse(Call call, Response response) {
                        try {
                            if (((SetDriverResponseModel) response.body()).getStatus().equals("success")) {
                                adapter.updateModelAt(localID, "Sending driver Data for " + localID + " - " + finalCounter + "/" + bundleCount, "Success");
                                adapter.notifyDataSetChanged();
                                driverIDstore.put(dbModel.getVehicle_Dbid(), ((SetDriverResponseModel) response.body()).getVehicle_id());

                                if (finalCounter == bundleCount) {

                                    try {
                                        Thread.sleep(200L);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                    int passangerSize = dbObject.getSetPassangerRequestModels().size();
                                    int pedestrianSize = dbObject.getSetPedestrianRequestModels().size();
                                    int diagramMappingSize = dbObject.getSetDiagramMappingRequestModel().size();

                                    if (passangerSize == 0 && pedestrianSize == 0 && diagramMappingSize == 0) {
                                        deleteLocalDBat(localID);//callsetdriver
                                        callCheckAccidentAPI(accidentID, 1);//driver
                                        //
                                    } else if (passangerSize != 0 && pedestrianSize != 0 && diagramMappingSize == 0) {//pass,pedes
                                        callSetPassangerAPI(localID, accidentID, dbObject, driverIDstore);
                                    } else if (passangerSize == 0 && pedestrianSize != 0 && diagramMappingSize != 0) {//pedes,diag
                                        callSetPedestrianAPI(localID, accidentID, dbObject, driverIDstore);
                                    } else if (passangerSize != 0 && pedestrianSize == 0 && diagramMappingSize != 0) {//pass,diag
                                        callSetPassangerAPI(localID, accidentID, dbObject, driverIDstore);
                                        //callDiagramMappingAPI(localID, accidentID, dbObject.getSetDiagramMappingRequestModel().first(), driverIDstore, null);//driver
                                    } else if (passangerSize != 0 && pedestrianSize == 0 && diagramMappingSize == 0) {
                                        callSetPassangerAPI(localID, accidentID, dbObject, driverIDstore);
                                    } else if (passangerSize == 0 && pedestrianSize != 0 && diagramMappingSize == 0) {//pedes
                                        callSetPedestrianAPI(localID, accidentID, dbObject, driverIDstore);
                                    } else if (passangerSize == 0 && pedestrianSize == 0 && diagramMappingSize != 0) {//diag
                                        callDiagramMappingAPI(localID, accidentID, dbObject.getSetDiagramMappingRequestModel().first(), driverIDstore, null);//driver
                                    } else if (passangerSize != 0 && pedestrianSize != 0 && diagramMappingSize != 0) {//pass,pedes,diag
                                        callSetPassangerAPI(localID, accidentID, dbObject, driverIDstore);
                                    }
                                }
                            } else {
                                adapter.updateModelAt(localID, "Sending driver Data for " + localID + " - " + finalCounter + "/" + bundleCount + " with message : " + ((SetDriverResponseModel) response.body()).getMessage(), "Failed ");
                                adapter.notifyDataSetChanged();
                                callCheckAccidentAPI(accidentID, 0);
                                Log.d("Lihat onResponse SyncActivity", ((SetDriverResponseModel) response.body()).getMessage());
                            }
                        } catch (Exception e) {
                            adapter.updateModelAt(localID, "Error when upload driver Data on " + localID + " - " + finalCounter + "/" + bundleCount, "Please check data & Reupload data again");
                            adapter.notifyDataSetChanged();
                            callCheckAccidentAPI(accidentID, 0);
                            Log.d("Lihat onResponse SyncActivity", e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {
                        adapter.updateModelAt(localID, "Sending driver Data for " + localID + " - " + finalCounter + "/" + bundleCount, "Failed, Upload Error " + t.getMessage());
                        adapter.notifyDataSetChanged();
                        callCheckAccidentAPI(accidentID, 0);
                    }
                });
                counter++;
            }
        } else {
            adapter.updateModelAt(localID, "Sending driver Data", " Failed, No Data");
            adapter.notifyDataSetChanged();
            callCheckAccidentAPI(accidentID, 0);
        }
    }

    private void callSetPassangerAPI(int localID, String accidentID, RequestRealmDbObject dbObject, SparseArray<String> driverIDstore) {
        List<PassangerDBModel> dbModels = dbObject.getSetPassangerRequestModels();

        int bundleCount = dbModels.size();
        if (bundleCount > 0) {
            adapter.updateModelAt(localID, "Sending Passenger Data " + localID, "On Progress");
            adapter.notifyDataSetChanged();
            int counter = 1;
            for (PassangerDBModel dbModel : dbModels) {
                SetPassangerRequestModel request = new SetPassangerRequestModel();
                int jink = dbModel.getVehicle_SubID();
                request.consumeDbModel(dbModel);
                request.setVehicle_id(driverIDstore.get(jink));
                int finalCounter = counter;

                API.doSetPassenger(this, request, new Callback() {
                    @Override
                    public void onResponse(Call call, Response response) {
                        try {
                            if (((SetPassangerResponseModel) response.body()).getStatus().equals("success")) {
                                adapter.updateModelAt(localID, "Sending Passenger Data for " + localID + " - " + finalCounter + "/" + bundleCount, "Success");
                                adapter.notifyDataSetChanged();
                                if (finalCounter == bundleCount) {

                                    try {
                                        Thread.sleep(200L);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                    if (dbObject.getSetPedestrianRequestModels().size() != 0) {
                                        callSetPedestrianAPI(localID, accidentID, dbObject, driverIDstore);
                                    } else {
                                        deleteLocalDBat(localID);//callsetpassanger
                                        callCheckAccidentAPI(accidentID, 1);//passanger
                                    }
                                }
                            } else {
                                adapter.updateModelAt(localID, "Sending Passenger Data for " + localID + " - " + finalCounter + "/" + bundleCount, "Failed");
                                adapter.notifyDataSetChanged();
                                callCheckAccidentAPI(accidentID, 0);
                                Log.d("Lihat onResponse SyncActivity", ((SetPassangerResponseModel) response.body()).getMessage());
                            }
                        } catch (Exception e) {
                            adapter.updateModelAt(localID, "Error when upload passenger Data for " + localID + " - " + finalCounter + "/" + bundleCount, "Please check data & Reupload data again");
                            adapter.notifyDataSetChanged();
                            callCheckAccidentAPI(accidentID, 0);
                            Log.d("Lihat onResponse SyncActivity", e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {
                        adapter.updateModelAt(localID, "Sending Passenger Data for " + localID + " - " + finalCounter + "/" + bundleCount + " were failed : ", t.getMessage());
                        adapter.notifyDataSetChanged();
                        callCheckAccidentAPI(accidentID, 0);
                    }
                });
                counter++;
            }
        } else {
            if (dbObject.getSetPedestrianRequestModels().size() != 0) {
                callSetPedestrianAPI(localID, accidentID, dbObject, driverIDstore);
            } else {
                deleteLocalDBat(localID);//callsetpassanger
                callCheckAccidentAPI(accidentID, 1);//passanger
            }
        }
        if (!realm.isClosed()) {
            realm.close();
        }
    }

    private void callSetPedestrianAPI(int localID, String accidentID, RequestRealmDbObject dbObject, SparseArray<String> driverIDstore) {
        adapter.updateModelAt(localID, "Sending Pedestrian Data " + localID, "On Progress");
        adapter.notifyDataSetChanged();
        realm = Realm.getDefaultInstance();
        List<PedestrianDBModel> models = dbObject.getSetPedestrianRequestModels();
        int bundleCount = models.size();
        if (bundleCount > 0) {
            SparseArray<String> pedestrianIDstore = new SparseArray<>();
            int counter = 1;
            for (PedestrianDBModel model :
                    models) {
                SetPedestrianRequestModel request = new SetPedestrianRequestModel();
                request.consumeDBModel(model);
                request.setVehicle_id(driverIDstore.get(Integer.parseInt(model.getVehicle_id())));
                int finalCounter = counter;
                API.doSetPedestrian(this, request, new Callback() {
                    @Override
                    public void onResponse(Call call, Response response) {
                        try {
                            if (((SetPedestrianResponseModel) response.body()).getStatus().equals("success")) {
                                adapter.updateModelAt(localID, "Sending Pedestrian Data for " + localID + " - " + finalCounter + "/" + bundleCount, "Success");
                                adapter.notifyDataSetChanged();
                                pedestrianIDstore.put(model.getPedestrian_Dbid(), ((SetPedestrianResponseModel) response.body()).getPedestrian_person_id());
                                if (finalCounter == bundleCount) {

                                    try {
                                        Thread.sleep(200L);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                    if (dbObject.getSetDiagramMappingRequestModel().size() != 0) {
                                        callDiagramMappingAPI(localID, accidentID, dbObject.getSetDiagramMappingRequestModel().first(), driverIDstore, pedestrianIDstore);//pedestrian
                                    } else {
                                        deleteLocalDBat(localID);//callsetpedestrian
                                        callCheckAccidentAPI(accidentID, 1);//pedestrian
                                    }
                                }
                            } else {
                                adapter.updateModelAt(localID, "Sending Pedestrian Data " + localID + " - " + finalCounter + "/" + bundleCount, "Failed");
                                adapter.notifyDataSetChanged();
                                callCheckAccidentAPI(accidentID, 0);
                                Log.d("Lihat onResponse SyncActivity", ((SetPedestrianResponseModel) response.body()).getMessage());
                            }
                        } catch (Exception e) {
                            adapter.updateModelAt(localID, "Error when upload pedestrian Data " + localID + " - " + finalCounter + "/" + bundleCount, "Please check data & Reupload data again");
                            adapter.notifyDataSetChanged();
                            callCheckAccidentAPI(accidentID, 0);
                            Log.d("Lihat onResponse SyncActivity", e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {
                        adapter.updateModelAt(localID, "Sending Pedestrian Data " + localID + " were failed : ", t.getMessage());
                        adapter.notifyDataSetChanged();
                        callCheckAccidentAPI(accidentID, 0);
                    }
                });
                counter++;
            }
        } else {
            if (dbObject.getSetDiagramMappingRequestModel().size() != 0) {
                callDiagramMappingAPI(localID, accidentID, dbObject.getSetDiagramMappingRequestModel().first(), driverIDstore, null);//pedestrian
            } else {
                deleteLocalDBat(localID);//callsetpedestrian
                callCheckAccidentAPI(accidentID, 1);//pedestrian
            }
        }
        if (!realm.isClosed()) {
            realm.close();
        }
    }

    private void callDiagramMappingAPI(final int localID, String accidentID, DiagramMappingDBModel model, SparseArray<String> driverIDstore, SparseArray<String> pedestrianIDstore) {
        adapter.updateModelAt(localID, "Sending DiagramMapping Data " + localID, "On Progress");
        adapter.notifyDataSetChanged();
        SetDiagramMappingRequestModel request = new SetDiagramMappingRequestModel();
        request.consumeDBModel(model);
        request.setAccident_id(accidentID);
        realm = Realm.getDefaultInstance();
        if (request.getAccident_a() != null || !request.getAccident_a().equals("")) {
            request.setAccident_a(getObjectID(request.getAccident_a(), driverIDstore, pedestrianIDstore));
        }
        if (request.getAccident_b() != null || !request.getAccident_b().equals("")) {
            try {
                request.setAccident_b(getObjectID(request.getAccident_b(), driverIDstore, pedestrianIDstore));
            } catch (Exception e) {
                //is null
                //Log.e("Lihat", String.valueOf(e));
            }
        }

        API.doSetDiagramMapping(this, request, new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                try {
                    if (((SetDiagramMappingResponseModel) response.body()).getStatus().equals("success")) {
                        adapter.updateModelAt(localID, "Sending DiagramMapping Data " + localID, "Success");
                        adapter.notifyDataSetChanged();
                        deleteLocalDBat(localID);//callsetdiagrammapping
                        callCheckAccidentAPI(accidentID, 1);//diagrammapping
                    } else {
                        adapter.updateModelAt(localID, "Sending DiagramMapping Data " + localID, "Failed");
                        adapter.notifyDataSetChanged();
                        callCheckAccidentAPI(accidentID, 0);
                        Log.d("Lihat onResponse SyncActivity", ((SetDiagramMappingResponseModel) response.body()).getMessage());
                    }
                } catch (Exception e) {
                    adapter.updateModelAt(localID, "Error when upload DiagramMapping Data " + localID, "Please check data & Reupload data again");
                    adapter.notifyDataSetChanged();
                    callCheckAccidentAPI(accidentID, 0);
                    Log.d("Lihat onResponse SyncActivity witness", e.getMessage());
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                adapter.updateModelAt(localID, "Sending DiagramMapping Data " + localID + " were failed : ", t.getMessage());
                adapter.notifyDataSetChanged();
                callCheckAccidentAPI(accidentID, 0);
            }
        });
        if (!realm.isClosed()) {
            realm.close();
        }
    }

    private void callCheckAccidentAPI(String accidentID, int state) {
        SetCheckAccidentRequestModel checkAccidentRequestModel = new SetCheckAccidentRequestModel();
        checkAccidentRequestModel.setAccident_id(accidentID);
        checkAccidentRequestModel.setState(String.valueOf(state));
        API.docheckAccident(SyncActivity.this, checkAccidentRequestModel, new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                try {
                    if (((SetCheckAccidentResponseModel) response.body()).getStatus().equals("success")) {
                        Log.i("Lihat onResponse SyncActivity", ((SetCheckAccidentResponseModel) response.body()).getStatus());
                        Log.i("Lihat onResponse SyncActivity", ((SetCheckAccidentResponseModel) response.body()).getMessage());
                    } else {
                        Log.i("Lihat onResponse SyncActivity", ((SetCheckAccidentResponseModel) response.body()).getStatus());
                        Log.i("Lihat onResponse SyncActivity", ((SetCheckAccidentResponseModel) response.body()).getMessage());
                    }
                } catch (Exception e) {
                    Log.i("Lihat onResponse SyncActivity", String.valueOf(e.getMessage()));
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.i("Lihat onResponse SyncActivity", t.getMessage());
            }
        });
    }

    private String getObjectID(String data, SparseArray<String> driverIDstore, SparseArray<String> pedestrianIDstore) {
        realm = Realm.getDefaultInstance();

        String r = "";
        if (data == null) {
            return r;

        } else if (data.substring(0, 3).equals("vec")) {
            r = driverIDstore.get(Integer.parseInt(data.substring(3)));
        } else if (data.substring(0, 3).equals("ped")) {
            r = pedestrianIDstore.get(Integer.parseInt(data.substring(3)));
        }
        return r;
    }

    private void deleteLocalDBat(final int localID) {
        adapter.updateModelAt(localID, "Deleting item from realm on localid " + localID, " On progress");
        adapter.notifyDataSetChanged();
        realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(DriverDBModel.class).equalTo("Local_ID", localID).findAll().deleteAllFromRealm();
                realm.where(AddDriverStateModel.class).equalTo("Local_ID", localID).findAll().deleteAllFromRealm();

                realm.where(PassangerDBModel.class).equalTo("Local_ID", localID).findAll().deleteAllFromRealm();
                realm.where(AddPassangerStateModel.class).equalTo("Local_ID", localID).findAll().deleteAllFromRealm();

                realm.where(PedestrianDBModel.class).equalTo("Local_ID", localID).findAll().deleteAllFromRealm();
                realm.where(AddPedestrianStateModel.class).equalTo("Local_ID", localID).findAll().deleteAllFromRealm();

                realm.where(SetWitnessDBModel.class).equalTo("Local_ID", localID).findAll().deleteAllFromRealm();
                realm.where(AddWitnessStateModel.class).equalTo("Local_ID", localID).findAll().deleteAllFromRealm();

                realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localID).findFirst().getSetAccidentRequestModel().deleteAllFromRealm();
                realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localID).findFirst().getAddAccidentStateModel().deleteAllFromRealm();
                realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localID).findFirst().getAddDiagramMappingStateModel().deleteAllFromRealm();
                realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localID).findFirst().deleteFromRealm();
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                adapter.updateModelAt(localID, "Deleting item from realm on localid " + localID, " Success");
                adapter.notifyDataSetChanged();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                adapter.updateModelAt(localID, "Deleting item from realm on localid " + localID, " Success");
                adapter.notifyDataSetChanged();
                Log.d("Lihat onError SyncActivity", error.getMessage());
            }
        });
    }

    private void deleteLocalDBatError(final int localID) {
        adapter.updateModelAt(localID, "Deleting item from realm on localid " + localID, " On progress");
        adapter.notifyDataSetChanged();
        realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(DriverDBModel.class).equalTo("Local_ID", localID).findAll().deleteAllFromRealm();
                realm.where(AddDriverStateModel.class).equalTo("Local_ID", localID).findAll().deleteAllFromRealm();

                realm.where(PassangerDBModel.class).equalTo("Local_ID", localID).findAll().deleteAllFromRealm();
                realm.where(AddPassangerStateModel.class).equalTo("Local_ID", localID).findAll().deleteAllFromRealm();

                realm.where(PedestrianDBModel.class).equalTo("Local_ID", localID).findAll().deleteAllFromRealm();
                realm.where(AddPedestrianStateModel.class).equalTo("Local_ID", localID).findAll().deleteAllFromRealm();

                realm.where(SetWitnessDBModel.class).equalTo("Local_ID", localID).findAll().deleteAllFromRealm();
                realm.where(AddWitnessStateModel.class).equalTo("Local_ID", localID).findAll().deleteAllFromRealm();

                realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localID).findFirst().getSetAccidentRequestModel().deleteAllFromRealm();
                realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localID).findFirst().getAddAccidentStateModel().deleteAllFromRealm();
                realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localID).findFirst().getAddDiagramMappingStateModel().deleteAllFromRealm();
                realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localID).findFirst().deleteFromRealm();
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                adapter.updateModelAt(localID, "Deleting item from realm on localid " + localID, " Success");
                adapter.notifyDataSetChanged();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                adapter.updateModelAt(localID, "Deleting item from realm on localid " + localID, " Success");
                adapter.notifyDataSetChanged();
                Log.d("Lihat onError SyncActivity", error.getMessage());
            }
        });
    }

    private void generateErrorDialog(String title, String subtitle, String content) {
        TextView tv_content, tv_subtitle, tv_title;
        ImageView ellipse, dialog_image;
        View customView = LayoutInflater.from(this).inflate(com.gandsoft.irsms.R.layout.dialog_imei, null);
        ellipse = (ImageView) customView.findViewById(com.gandsoft.irsms.R.id.ellipse);
        dialog_image = (ImageView) customView.findViewById(com.gandsoft.irsms.R.id.dialog_image);
        tv_content = (TextView) customView.findViewById(com.gandsoft.irsms.R.id.tv_content);
        tv_subtitle = (TextView) customView.findViewById(com.gandsoft.irsms.R.id.tv_subtitle);
        tv_title = (TextView) customView.findViewById(com.gandsoft.irsms.R.id.tv_title);
        tv_content.setText(content);

        dialog_image.setImageResource(com.gandsoft.irsms.R.drawable.ic_error_outline_88dp);
        ellipse.setImageResource(com.gandsoft.irsms.R.drawable.vector_ellipse_yellow);
        tv_title.setText(title);
        tv_title.setBackgroundColor(ContextCompat.getColor(this, com.gandsoft.irsms.R.color.colorYellow));
        tv_subtitle.setText(subtitle);
        DialogUtil.generateCustomDialogInfo(this, customView);
    }

    private void generateInfoUpdateDialog(BaseResponse br) {
        TextView tv_content, tv_subtitle, tv_title;
        ImageView ellipse, dialog_image;
        View customView = LayoutInflater.from(this).inflate(com.gandsoft.irsms.R.layout.dialog_imei, null);

        tv_title = (TextView) customView.findViewById(com.gandsoft.irsms.R.id.tv_title);
        ellipse = (ImageView) customView.findViewById(com.gandsoft.irsms.R.id.ellipse);
        dialog_image = (ImageView) customView.findViewById(com.gandsoft.irsms.R.id.dialog_image);
        tv_subtitle = (TextView) customView.findViewById(com.gandsoft.irsms.R.id.tv_subtitle);
        tv_content = (TextView) customView.findViewById(com.gandsoft.irsms.R.id.tv_content);

        tv_title.setText("Ada perubahan data!!!");
        ellipse.setImageResource(com.gandsoft.irsms.R.drawable.vector_ellipse_yellow);
        dialog_image.setImageResource(com.gandsoft.irsms.R.drawable.ic_error_outline_88dp);
        tv_title.setBackgroundColor(ContextCompat.getColor(this, R.color.colorYellow));
        tv_subtitle.setText("Aplikasi akan di restart untuk memperbaharui data,\n silahkan tunggu.");
        tv_subtitle.setVisibility(View.GONE);
        tv_content.setText("OK");

        tv_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResponseUtil.replaceFormData(br, getBaseContext());
            }
        });

        DialogUtil.generateCustomDialogInfo(this, customView);
    }

    @Override
    public void nextStep(int localID) {
        /*realm = Realm.getDefaultInstance();
        List<RequestRealmDbObject> realmObjects = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localID).findAll();

        for (RequestRealmDbObject realmObject : realmObjects) {
            callSetAccidentAPI(realmObject.getLocal_ID(), realmObject);
            adapter.addModel(new SyncViewModel(realmObject.getLocal_ID(), "processing bundled data " + realmObject.getLocal_ID(), ""));
            adapter.notifyDataSetChanged();
        }*/
    }

    @Override
    protected void onApiResponseCallback(final BaseResponse br, int responseCode, Response response) {
        super.onApiResponseCallback(br, responseCode, response);
        if (response.isSuccessful()) {
            if (responseCode == IConfig.API_CALLER_LOGIN_CODE) {
                if (br.getStatus().equals(IConfig.RETURN_STATUS_DONE)) {
                    Toast.makeText(this, R.string.sycn_start, Toast.LENGTH_SHORT).show();
                    doSync();
                } else {
                    Toast.makeText(this, R.string.sync_pass_fail, Toast.LENGTH_SHORT).show();
                }
            } else if (responseCode == IConfig.API_CALLER_UIITEM_CODE) {
                if (br.getStatus().equals(IConfig.RETURN_STATUS_DONE)) {

                    SessionManager.saveValidateData(((BaseResponseModel) br));

                    int totalUi = ((BaseResponseModel) br).getTotal();
                    int versionUi = ((BaseResponseModel) br).getVersion();
                    if (SessionManager.checkIfExist(ISeasonConfig.KEY_UITEM)) {
                        if (SessionManager.checkIfExist(ISeasonConfig.KEY_UITEM_VERSION) && !SessionManager.isVersionDataUpdate(br)) {
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_TOTAL, totalUi);
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_VERSION, versionUi);
                            doLogin();
                        } else if (!SessionManager.checkIfExist(ISeasonConfig.KEY_UITEM_VERSION) && SessionManager.isVersionDataUpdate(br)) {
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_TOTAL, totalUi);
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_VERSION, versionUi);
                            ResponseUtil.replaceFormData(br, getBaseContext());
                        } else if (SessionManager.checkIfExist(ISeasonConfig.KEY_UITEM_VERSION) && SessionManager.isVersionDataUpdate(br)) {
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_TOTAL, totalUi);
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_VERSION, versionUi);
                            ResponseUtil.replaceFormData(br, getBaseContext());
                        } else if (!SessionManager.checkIfExist(ISeasonConfig.KEY_UITEM_VERSION) && !SessionManager.isVersionDataUpdate(br)) {
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_TOTAL, totalUi);
                            SessionManager.saveInt(ISeasonConfig.KEY_UITEM_VERSION, versionUi);
                        }
                    } else {
                        ResponseUtil.addFormData(br, SyncActivity.this);
                    }
                }
            }
        }
    }

    private void changeDataDialog(BaseResponse br) {
        new android.support.v7.app.AlertDialog.Builder(SyncActivity.this)
                .setTitle("")
                .setMessage("")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //code here
                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}

