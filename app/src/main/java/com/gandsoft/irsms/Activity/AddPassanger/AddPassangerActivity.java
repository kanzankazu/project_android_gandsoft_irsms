package com.gandsoft.irsms.Activity.AddPassanger;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gandsoft.irsms.Activity.LocalBaseActivity;
import com.gandsoft.irsms.ISeasonConfig;
import com.gandsoft.irsms.Presenter.SeasonManager.SessionManager;
import com.gandsoft.irsms.Presenter.witget.ToggleRadioButton;
import com.gandsoft.irsms.R;
import com.gandsoft.irsms.Support.Util.SystemUtil;
import com.gandsoft.irsms.Support.ViewGenerator.ViewGenerator;
import com.gandsoft.irsms.Support.ViewGenerator.ViewGeneratorUtil;
import com.gandsoft.irsms.model.RealmDbModel.PassengerForm.AddPassangerStateModel;
import com.gandsoft.irsms.model.RealmDbModel.PassengerForm.PassangerDBModel;
import com.gandsoft.irsms.model.RealmDbModel.RequestRealmDbObject;
import com.gandsoft.irsms.model.RealmDbModel.VehicleForm.AddDriverStateModel;
import com.gandsoft.irsms.model.RealmDbModel.VehicleForm.DriverDBModel;
import com.gandsoft.irsms.model.uiModel.FormdataLoader;

import java.util.Arrays;
import java.util.List;

import io.realm.Realm;

import static com.gandsoft.irsms.Support.Util.SystemUtil.convertListIntegertToIntegerArray;
import static com.gandsoft.irsms.Support.Util.SystemUtil.getAllSizeStringList;
import static com.gandsoft.irsms.Support.Util.SystemUtil.getPosListStringIn2List;
import static com.gandsoft.irsms.Support.Util.SystemUtil.isKitkat;
import static com.gandsoft.irsms.Support.Util.SystemUtil.isListContainString;
import static com.gandsoft.irsms.Support.Util.SystemUtil.isListContainStringArray;

/**
 * Created by kanzan on 2/6/2018.
 */

public class AddPassangerActivity extends LocalBaseActivity {
    private LinearLayout llDataPenumpangTitlefvbi;

    private View cvDataPenumpangKeluarfvbi, cvDataPenumpangSimpanfvbi;
    private LinearLayout spDataPenumpangJenisIdentitasfvbi, spDataPenumpangAgamafvbi, spDataPenumpangPekerjaanfvbi, spDataPenumpangPendidikanfvbi, spDataPenumpangKecamatanfvbi, spDataPenumpangKelurahanfvbi, spDataPenumpangProvinsifvbi, spDataPenumpangKebangsaanfvbi, spDataPenumpangKotafvbi;
    private LinearLayout llviewDataPenumpangPenggunaanAlatKeselamatanfvbi;
    private LinearLayout llviewDataPenumpangJenisKelaminfvbi, llviewDataPenumpangTingkatLukafvbi, llviewDataPenumpangPerilakufvbi, llviewDataPenumpangDiTahanfvbi;
    private EditText etDataPenumpangJalanfvbi, etDataPenumpangNamaBelakangfvbi, etDataPenumpangUmurfvbi, etDataPenumpangNamaDepanfvbi, etDataPenumpangNoIdentitasfvbi, etDataPenumpangRTfvbi, etDataPenumpangRWfvbi;
    private TextView tvDataPenumpangJenisMerkKendaraanfvbi, tvDataPenumpangNoTNKBfvbi;

    private RadioGroup llviewDataPenumpangTingkatLukaRadioGroup;
    private Realm realm;
    private int localId, localSubId, itemId;

    private RadioGroup llviewDataPenumpangDeadSpot;
    private boolean isLoadedFromState = false;
    private boolean chckr = true;

    //private List<String> typeVehicleR2 = Arrays.asList("V02A05", "V02A14", "V02B15");
    //private List<String> typeVehicleOther = Arrays.asList("V02A00", "V02A01", "V02A03", "V02A06", "V02A19", "V02A20", "V02A31", "V02A34");
    private List<String> typeVehicleR4 = SessionManager.loadListString(ISeasonConfig.KEY_UITEM_CAR_DEFAULT);
    private List<String> typeVehicleR2 = SessionManager.loadListString(ISeasonConfig.KEY_UITEM_MOTOR_DEFAULT);

    //private List<String> r2PenggunaAlatKeselamatan = Arrays.asList("PS1001","PS1002");
    //private List<String> r4PenggunaAlatKeselamatan = Arrays.asList("PS1003","PS1004");
    private List<String> r4PenggunaAlatKeselamatan = SessionManager.loadListString(ISeasonConfig.KEY_UITEM_SAFETY_PASS_CAR_DEFAULT);
    private List<String> r2PenggunaAlatKeselamatan = SessionManager.loadListString(ISeasonConfig.KEY_UITEM_SAFETY_PASS_MOTOR_DEFAULT);

    private String vehicleTypeIdValue;
    private Integer vehicleTypeId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_passenger);

        Intent bundle = getIntent();
        if (bundle.hasExtra("Local_ID")) {
            localId = bundle.getIntExtra("Local_ID", 0);
        }
        if (bundle.hasExtra("Local_SubID")) {
            localSubId = bundle.getIntExtra("Local_SubID", 0);
        }
        if (bundle.hasExtra("Item_ID")) {
            itemId = bundle.getIntExtra("Item_ID", 0);
            isLoadedFromState = true;
        }
        initComponent();
        initContent();

        if (isLoadedFromState) {
            loadState();
        }
    }

    private void initComponent() {
        llDataPenumpangTitlefvbi = findViewById(R.id.llDataPenumpangTitle);

        cvDataPenumpangKeluarfvbi = findViewById(R.id.cvDataPenumpangKeluar);
        cvDataPenumpangSimpanfvbi = findViewById(R.id.cvDataPenumpangSimpan);

        etDataPenumpangJalanfvbi = findViewById(R.id.etDataPenumpangJalan);
        etDataPenumpangNamaBelakangfvbi = findViewById(R.id.etDataPenumpangNamaBelakang);
        etDataPenumpangNamaDepanfvbi = findViewById(R.id.etDataPenumpangNamaDepan);
        etDataPenumpangUmurfvbi = findViewById(R.id.etDataPenumpangUmur);
        etDataPenumpangNoIdentitasfvbi = findViewById(R.id.etDataPenumpangNoIdentitas);
        etDataPenumpangRTfvbi = findViewById(R.id.etDataPenumpangRT);
        etDataPenumpangRWfvbi = findViewById(R.id.etDataPenumpangRW);

        tvDataPenumpangNoTNKBfvbi = findViewById(R.id.tvDataPenumpangNoTNKB);
        tvDataPenumpangJenisMerkKendaraanfvbi = findViewById(R.id.tvDataPenumpangJenisMerkKendaraan);

        spDataPenumpangAgamafvbi = findViewById(R.id.spDataPenumpangAgama);
        spDataPenumpangJenisIdentitasfvbi = findViewById(R.id.spDataPenumpangJenisIdentitas);
        spDataPenumpangPekerjaanfvbi = findViewById(R.id.spDataPenumpangPekerjaan);
        spDataPenumpangPendidikanfvbi = findViewById(R.id.spDataPenumpangPendidikan);
        spDataPenumpangKebangsaanfvbi = findViewById(R.id.spDataPenumpangKebangsaan);
        //
        spDataPenumpangProvinsifvbi = findViewById(R.id.spDataPenumpangProvinsi);
        spDataPenumpangKotafvbi = findViewById(R.id.spDataPenumpangKota);
        spDataPenumpangKecamatanfvbi = findViewById(R.id.spDataPenumpangKecamatan);
        spDataPenumpangKelurahanfvbi = findViewById(R.id.spDataPenumpangelurahan);

        llviewDataPenumpangJenisKelaminfvbi = findViewById(R.id.llviewDataPenumpangJenisKelamin);
        llviewDataPenumpangTingkatLukafvbi = findViewById(R.id.llviewDataPenumpangTingkatLuka);
        llviewDataPenumpangPerilakufvbi = findViewById(R.id.llviewDataPenumpangPerilaku);
        llviewDataPenumpangDiTahanfvbi = findViewById(R.id.llviewDataPenumpangDiTahan);

        llviewDataPenumpangPenggunaanAlatKeselamatanfvbi = findViewById(R.id.llviewDataPenumpangPenggunaanAlatKeselamatan);
    }

    private void initContent() {
        if (isKitkat()) {
            llDataPenumpangTitlefvbi.bringToFront();
        }

        realm = Realm.getDefaultInstance();
        realm.refresh();

        cvDataPenumpangKeluarfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        cvDataPenumpangSimpanfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveOverwriteData();
            }
        });

        /*SP*/
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getReligion_id(), spDataPenumpangAgamafvbi);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getNationality_id(), spDataPenumpangKebangsaanfvbi);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getIdentity_type_id(), spDataPenumpangJenisIdentitasfvbi);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getProfession_id(), spDataPenumpangPekerjaanfvbi);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getEducation_id(), spDataPenumpangPendidikanfvbi);

        /*RB*/
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getGender_id(), llviewDataPenumpangJenisKelaminfvbi);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getPassenger_movement_id(), llviewDataPenumpangPerilakufvbi);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getArrested_id(), llviewDataPenumpangDiTahanfvbi);

        /*CB*/
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getPassenger_safety(), llviewDataPenumpangPenggunaanAlatKeselamatanfvbi);

        View tot = LayoutInflater.from(this).inflate(R.layout.widget_item_injury, null, false);
        llviewDataPenumpangTingkatLukafvbi.addView(tot);
        llviewDataPenumpangTingkatLukaRadioGroup = llviewDataPenumpangTingkatLukafvbi.findViewById(R.id.kucing);
        llviewDataPenumpangTingkatLukaRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.meong4) {
                    group.findViewById(R.id.hundx).setVisibility(View.VISIBLE);
                } else {
                    group.findViewById(R.id.hundx).setVisibility(View.GONE);
                }
            }
        });
        llviewDataPenumpangDeadSpot = llviewDataPenumpangTingkatLukafvbi.findViewById(R.id.lukasub);

        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getProvince(), spDataPenumpangProvinsifvbi);
        FormdataLoader.generateDynamicDBitems(0, ViewGeneratorUtil.getSelectedSpinnerValue(spDataPenumpangProvinsifvbi));
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getKabupaten(), spDataPenumpangKotafvbi);
        FormdataLoader.generateDynamicDBitems(1, ViewGeneratorUtil.getSelectedSpinnerValue(spDataPenumpangKotafvbi));
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getKecamatan(), spDataPenumpangKecamatanfvbi);
        FormdataLoader.generateDynamicDBitems(2, ViewGeneratorUtil.getSelectedSpinnerValue(spDataPenumpangKecamatanfvbi));
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getKelurahan(), spDataPenumpangKelurahanfvbi);
        ((Spinner) ((ViewGroup) spDataPenumpangProvinsifvbi.getChildAt(0)).getChildAt(1)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FormdataLoader.generateDynamicDBitems(0, ViewGeneratorUtil.getSelectedSpinnerValue(spDataPenumpangProvinsifvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(spDataPenumpangKotafvbi).replaceModels(FormdataLoader.getKabupaten().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(spDataPenumpangKotafvbi);

                FormdataLoader.generateDynamicDBitems(1, ViewGeneratorUtil.getSelectedSpinnerValue(spDataPenumpangKotafvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(spDataPenumpangKecamatanfvbi).replaceModels(FormdataLoader.getKecamatan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(spDataPenumpangKecamatanfvbi);

                FormdataLoader.generateDynamicDBitems(2, ViewGeneratorUtil.getSelectedSpinnerValue(spDataPenumpangKecamatanfvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(spDataPenumpangKelurahanfvbi).replaceModels(FormdataLoader.getKelurahan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(spDataPenumpangKelurahanfvbi);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ((Spinner) ((ViewGroup) spDataPenumpangKotafvbi.getChildAt(0)).getChildAt(1)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FormdataLoader.generateDynamicDBitems(1, ViewGeneratorUtil.getSelectedSpinnerValue(spDataPenumpangKotafvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(spDataPenumpangKecamatanfvbi).replaceModels(FormdataLoader.getKecamatan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(spDataPenumpangKecamatanfvbi);

                FormdataLoader.generateDynamicDBitems(2, ViewGeneratorUtil.getSelectedSpinnerValue(spDataPenumpangKecamatanfvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(spDataPenumpangKelurahanfvbi).replaceModels(FormdataLoader.getKelurahan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(spDataPenumpangKelurahanfvbi);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ((Spinner) ((ViewGroup) spDataPenumpangKecamatanfvbi.getChildAt(0)).getChildAt(1)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FormdataLoader.generateDynamicDBitems(2, ViewGeneratorUtil.getSelectedSpinnerValue(spDataPenumpangKecamatanfvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(spDataPenumpangKelurahanfvbi).replaceModels(FormdataLoader.getKelurahan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(spDataPenumpangKelurahanfvbi);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        String vehicleCompoundKey = realm.where(DriverDBModel.class).equalTo("Local_ID", localId).equalTo("vehicle_Dbid", localSubId).findFirst().getVehicleCompoundKey();
        String[] compoundKeyComponent = vehicleCompoundKey.split("/")[1].split("\\|");

        tvDataPenumpangNoTNKBfvbi.setText(compoundKeyComponent[0]);
        tvDataPenumpangJenisMerkKendaraanfvbi.setText(compoundKeyComponent[2] + " - " + compoundKeyComponent[1]);

        ViewGeneratorUtil.selectSpinnerView(spDataPenumpangJenisIdentitasfvbi).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SystemUtil.validationIdentityByType(etDataPenumpangNoIdentitasfvbi,
                        ViewGeneratorUtil.getSelectedSpinnerValue(spDataPenumpangJenisIdentitasfvbi),
                        FormdataLoader.getId_identity_type_id_lv(),
                        FormdataLoader.getId_identity_type_max_lenght_lv());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etDataPenumpangJalanfvbi.setFilters(new InputFilter[]{new InputFilter.LengthFilter(255)});

        vehicleTypeId = realm.where(AddDriverStateModel.class).equalTo("Local_ID", localId).equalTo("vehicle_Dbid", localSubId).findFirst().getVehicle_type_id();
        vehicleTypeIdValue = realm.where(DriverDBModel.class).equalTo("Local_ID", localId).equalTo("vehicle_Dbid", localSubId).findFirst().getVehicle_type_id();
        if (vehicleTypeId != null) {
            if (SystemUtil.isListContainString(typeVehicleR4, vehicleTypeIdValue)) {
                llviewDataPenumpangPenggunaanAlatKeselamatanfvbi.setVisibility(View.VISIBLE);
                ViewGeneratorUtil.setCheckboxDisabledAt(llviewDataPenumpangPenggunaanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getPosListStringIn2List(FormdataLoader.getId_passenger_safety_lv(), r2PenggunaAlatKeselamatan)));
                ViewGeneratorUtil.setCheckboxEnabledAt(llviewDataPenumpangPenggunaanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getPosListStringIn2List(FormdataLoader.getId_passenger_safety_lv(), r4PenggunaAlatKeselamatan)));
                //isMotorMobil = "mobil";
            } else if (SystemUtil.isListContainString(typeVehicleR2, vehicleTypeIdValue)) {
                llviewDataPenumpangPenggunaanAlatKeselamatanfvbi.setVisibility(View.VISIBLE);
                ViewGeneratorUtil.setCheckboxDisabledAt(llviewDataPenumpangPenggunaanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getPosListStringIn2List(FormdataLoader.getId_passenger_safety_lv(), r4PenggunaAlatKeselamatan)));
                ViewGeneratorUtil.setCheckboxEnabledAt(llviewDataPenumpangPenggunaanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getPosListStringIn2List(FormdataLoader.getId_passenger_safety_lv(), r2PenggunaAlatKeselamatan)));
                //isMotorMobil = "motor";
            } else {
                llviewDataPenumpangPenggunaanAlatKeselamatanfvbi.setVisibility(View.INVISIBLE);
                ViewGeneratorUtil.setCheckboxEnabledAt(llviewDataPenumpangPenggunaanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getAllSizeStringList(FormdataLoader.getId_passenger_safety_lv())));
                ViewGeneratorUtil.setCheckboxUnCheckedAt(llviewDataPenumpangPenggunaanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getAllSizeStringList(FormdataLoader.getId_passenger_safety_lv())));
                //isMotorMobil = "other";
            }
        }
    }

    private void loadState() {
        realm = Realm.getDefaultInstance();

        AddPassangerStateModel state = realm.where(AddPassangerStateModel.class).equalTo("Local_ID", localId).equalTo("item_id", itemId).findFirst();

        checkData(state);

        /*SP*//*
        ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPenumpangAgamafvbi, state.getReligion_id());
        ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPenumpangJenisIdentitasfvbi, state.getIdentity_type_id());
        ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPenumpangKebangsaanfvbi, state.getNationality_id());
        ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPenumpangPekerjaanfvbi, state.getProfession_id());
        ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPenumpangPendidikanfvbi, state.getEducation_id());

        *//*RB*//*
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataPenumpangJenisKelaminfvbi, state.getGender_id());
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataPenumpangTingkatLukafvbi, state.getInjury_id());
        if (state.getDead_spot_id() >= 0) {
            ((ToggleRadioButton) llviewDataPenumpangDeadSpot.getChildAt(state.getDead_spot_id())).setChecked(true);
        }
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataPenumpangPerilakufvbi, state.getBehavior_id());
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataPenumpangDiTahanfvbi, state.getArrested_id());

        *//*CB*//*
        passengerSafetyHandler(state);*/

        /*ET*/
        etDataPenumpangJalanfvbi.setText(state.getRoad_name());
        etDataPenumpangNamaDepanfvbi.setText(state.getFirst_name());
        etDataPenumpangNamaBelakangfvbi.setText(state.getLast_name());
        etDataPenumpangUmurfvbi.setText(state.getAge());
        etDataPenumpangNoIdentitasfvbi.setText(state.getIdentity_no());
        if (state.getIdentity_type_id() == 0) {
            etDataPenumpangNoIdentitasfvbi.setVisibility(View.INVISIBLE);
        } else if (state.getIdentity_type_id() == 4) {
            etDataPenumpangNoIdentitasfvbi.setVisibility(View.INVISIBLE);
        }
        etDataPenumpangRTfvbi.setText(state.getRt());
        etDataPenumpangRWfvbi.setText(state.getRw());

        ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPenumpangProvinsifvbi, state.getPropinsi());
        new Handler().postDelayed(() -> {
            ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPenumpangKotafvbi, state.getKabupaten());
            new Handler().postDelayed(() -> {
                ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPenumpangKecamatanfvbi, state.getKecamatan());
                new Handler().postDelayed(() -> ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPenumpangKelurahanfvbi, state.getKelurahan())
                        , 100);
            }, 100);
        }, 100);
    }

    private void passengerSafetyHandler(AddPassangerStateModel state) {
        if (vehicleTypeId != null) {
            if (SystemUtil.isListContainString(typeVehicleR4, vehicleTypeIdValue)) {//jika selain mobil dan motor
                if (Arrays.asList(state.getPassenger_safety()).contains(2) || Arrays.asList(state.getPassenger_safety()).contains(3)) {
                    ViewGeneratorUtil.setCheckboxCheckedAt(llviewDataPenumpangPenggunaanAlatKeselamatanfvbi, state.getPassenger_safety());
                } else {
                    ViewGeneratorUtil.setCheckboxUnCheckedAt(llviewDataPenumpangPenggunaanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getAllSizeStringList(FormdataLoader.getId_passenger_safety_lv())));
                }
                //isMotorMobil = "mobil";
            } else if (SystemUtil.isListContainString(typeVehicleR2, vehicleTypeIdValue)) {//jika sepeda & sepeda motor
                if (Arrays.asList(state.getPassenger_safety()).contains(0) || Arrays.asList(state.getPassenger_safety()).contains(1)) {//jika berisini opsi roda 2
                    ViewGeneratorUtil.setCheckboxCheckedAt(llviewDataPenumpangPenggunaanAlatKeselamatanfvbi, state.getPassenger_safety());
                } else {//jika tidak
                    ViewGeneratorUtil.setCheckboxUnCheckedAt(llviewDataPenumpangPenggunaanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getAllSizeStringList(FormdataLoader.getId_passenger_safety_lv())));
                }
                //isMotorMobil = "motor";
            } else {// jika kendaraan lebih dari mobil
                ViewGeneratorUtil.setCheckboxUnCheckedAt(llviewDataPenumpangPenggunaanAlatKeselamatanfvbi, convertListIntegertToIntegerArray(getAllSizeStringList(FormdataLoader.getId_passenger_safety_lv())));
                //isMotorMobil = "other";
            }
        }
    }

    private PassangerDBModel generateRequestModel(@Nullable PassangerDBModel requestModel) {
        if (requestModel == null) {
            requestModel = new PassangerDBModel();
            requestModel.setLocal_id(localId);
            requestModel.setVehicle_SubID(localSubId);

            int itemCount = realm.where(PassangerDBModel.class).equalTo("Local_ID", localId).findAll().size();
            if (itemCount > 0) {
                requestModel.setItem_id(realm.where(PassangerDBModel.class).equalTo("Local_ID", localId).findAll().get(itemCount - 1).getItem_id() + 1);
            } else {
                requestModel.setItem_id(1);
            }
        }

        /*SP*/
        requestModel.setReligion_id(ViewGeneratorUtil.getSelectedSpinnerValue(spDataPenumpangAgamafvbi));
        requestModel.setNationality_id(ViewGeneratorUtil.getSelectedSpinnerValue(spDataPenumpangKebangsaanfvbi));
        requestModel.setIdentity_type_id(ViewGeneratorUtil.getSelectedSpinnerValue(spDataPenumpangJenisIdentitasfvbi));
        requestModel.setProfession_id(ViewGeneratorUtil.getSelectedSpinnerValue(spDataPenumpangPekerjaanfvbi));
        requestModel.setEducation_id(ViewGeneratorUtil.getSelectedSpinnerValue(spDataPenumpangPendidikanfvbi));
        requestModel.setPropinsi(ViewGeneratorUtil.getSelectedSpinnerValue(spDataPenumpangProvinsifvbi));
        requestModel.setKabupaten(ViewGeneratorUtil.getSelectedSpinnerValue(spDataPenumpangKotafvbi));
        requestModel.setKecamatan(ViewGeneratorUtil.getSelectedSpinnerValue(spDataPenumpangKecamatanfvbi));
        requestModel.setKelurahan(ViewGeneratorUtil.getSelectedSpinnerValue(spDataPenumpangKelurahanfvbi));

        /*ET*/
        requestModel.setRoad_name(etDataPenumpangJalanfvbi.getText().toString());
        requestModel.setFirst_name(etDataPenumpangNamaDepanfvbi.getText().toString());
        requestModel.setLast_name(etDataPenumpangNamaBelakangfvbi.getText().toString());
        requestModel.setAge(etDataPenumpangUmurfvbi.getText().toString());
        requestModel.setIdentity_no(etDataPenumpangNoIdentitasfvbi.getText().toString());
        requestModel.setRt(etDataPenumpangRTfvbi.getText().toString());
        requestModel.setRw(etDataPenumpangRWfvbi.getText().toString());

        /*RB*/
        requestModel.setGender_id(ViewGeneratorUtil.getSelectedRadioValue(llviewDataPenumpangJenisKelaminfvbi));
        requestModel.setInjury_id(ViewGeneratorUtil.getSelectedRadioValue(llviewDataPenumpangTingkatLukafvbi));
        Log.wtf("Lihat generateRequestModel AddPassangerActivity", ViewGeneratorUtil.getSelectedRadioValue(llviewDataPenumpangTingkatLukafvbi));
        try {
            requestModel.setDead_spot_id((llviewDataPenumpangDeadSpot.findViewById(llviewDataPenumpangDeadSpot.getCheckedRadioButtonId())).getTag().toString());
            requestModel.setDead_spot_id_text((llviewDataPenumpangDeadSpot.findViewById(llviewDataPenumpangDeadSpot.getCheckedRadioButtonId())).getTag().toString());
        } catch (Exception e) {
            //Log.e("DeadSpot: ", e.getMessage());
            requestModel.setDead_spot_id("");
        }
        requestModel.setBehavior_id(ViewGeneratorUtil.getSelectedRadioValue(llviewDataPenumpangPerilakufvbi));
        requestModel.setArrested_id(ViewGeneratorUtil.getSelectedRadioValue(llviewDataPenumpangDiTahanfvbi));

        requestModel.setInjury_id_text(ViewGeneratorUtil.getSelectedRadioTextValue(llviewDataPenumpangTingkatLukafvbi));
        requestModel.setDead_spot_id_text(ViewGeneratorUtil.getSelectedRadioTextValue(llviewDataPenumpangDeadSpot));

        /*CB*/
        requestModel.setPassenger_safety(ViewGeneratorUtil.getSelectedCheckboxValue(llviewDataPenumpangPenggunaanAlatKeselamatanfvbi));


        return requestModel;
    }

    private AddPassangerStateModel generateStateModel(@Nullable AddPassangerStateModel stateModel) {
        if (stateModel == null) {
            stateModel = new AddPassangerStateModel();
            stateModel.setLocal_id(localId);
            stateModel.setVehicle_SubID(localSubId);

            int itemCount = realm.where(AddPassangerStateModel.class).equalTo("Local_ID", localId).findAll().size();
            if (itemCount > 0) {
                stateModel.setItem_id(realm.where(AddPassangerStateModel.class).equalTo("Local_ID", localId).findAll().get(itemCount - 1).getItem_id() + 1);
            } else {
                stateModel.setItem_id(1);
            }
        }

        stateModel.setPlate_no(tvDataPenumpangNoTNKBfvbi.getText().toString());

        /*SP*/
        stateModel.setReligion_id(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataPenumpangAgamafvbi));
        stateModel.setNationality_id(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataPenumpangKebangsaanfvbi));
        stateModel.setIdentity_type_id(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataPenumpangJenisIdentitasfvbi));
        stateModel.setProfession_id(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataPenumpangPekerjaanfvbi));
        stateModel.setEducation_id(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataPenumpangPendidikanfvbi));
        stateModel.setPropinsi(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataPenumpangProvinsifvbi));
        stateModel.setKabupaten(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataPenumpangKotafvbi));
        stateModel.setKecamatan(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataPenumpangKecamatanfvbi));
        stateModel.setKelurahan(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataPenumpangKelurahanfvbi));

        /*ET*/
        stateModel.setRoad_name(etDataPenumpangJalanfvbi.getText().toString());
        stateModel.setFirst_name(etDataPenumpangNamaDepanfvbi.getText().toString());
        stateModel.setLast_name(etDataPenumpangNamaBelakangfvbi.getText().toString());
        stateModel.setAge(etDataPenumpangUmurfvbi.getText().toString());
        stateModel.setIdentity_no(etDataPenumpangNoIdentitasfvbi.getText().toString());
        stateModel.setRt(etDataPenumpangRTfvbi.getText().toString());
        stateModel.setRw(etDataPenumpangRWfvbi.getText().toString());

        /*RB*/
        stateModel.setGender_id(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataPenumpangJenisKelaminfvbi));
        stateModel.setInjury_id(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataPenumpangTingkatLukafvbi));
        Log.d("Lihat generateStateModel AddPassangerActivity", String.valueOf(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataPenumpangTingkatLukafvbi)));
        stateModel.setDead_spot_id(llviewDataPenumpangDeadSpot.indexOfChild(llviewDataPenumpangDeadSpot.findViewById(llviewDataPenumpangDeadSpot.getCheckedRadioButtonId())));
        stateModel.setBehavior_id(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataPenumpangPerilakufvbi));
        stateModel.setArrested_id(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataPenumpangDiTahanfvbi));

        stateModel.setInjury_id_text(ViewGeneratorUtil.getSelectedRadioTextValue(llviewDataPenumpangTingkatLukafvbi));
        stateModel.setDead_spot_id_text(ViewGeneratorUtil.getSelectedRadioTextValue(llviewDataPenumpangDeadSpot));

        /*CB*/
        stateModel.setPassenger_safety(ViewGeneratorUtil.getSelectedCheckboxIndex(llviewDataPenumpangPenggunaanAlatKeselamatanfvbi));

        return stateModel;
    }

    private void saveOverwriteData() {
        if (!isLoadedFromState) {
            if (saveData()) {
                Toast.makeText(getApplicationContext(), "Save Success", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Save Failed", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (overwriteData()) {
                Toast.makeText(getApplicationContext(), "Overwrite Success", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Overwrite Failed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean saveData() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                RequestRealmDbObject poolRequestModel = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst();
                poolRequestModel.getSetPassangerRequestModels().add(generateRequestModel(null));
                poolRequestModel.getSetPassangerStateModels().add(generateStateModel(null));
                realm.copyToRealmOrUpdate(poolRequestModel);
            }
        });
        realm.refresh();
        realm.close();
        return true;
    }

    private boolean overwriteData() {
        realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                generateRequestModel(realm.where(PassangerDBModel.class).equalTo("Local_ID", localId).equalTo("item_id", itemId).findFirst());
                generateStateModel(realm.where(AddPassangerStateModel.class).equalTo("Local_ID", localId).equalTo("item_id", itemId).findFirst());
            }
        });
        realm.refresh();
        realm.close();
        return true;
    }

    private void checkData(AddPassangerStateModel passangerStateModel) {
        realm = Realm.getDefaultInstance();
        PassangerDBModel passangerDBModel = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst().getSetPassangerRequestModels().get(0);

        List<String> idModel1 = FormdataLoader.getIdModel();
        Log.d("Lihat checkData AddAccidentActivity1", String.valueOf(passangerDBModel));
        Log.d("Lihat checkData AddAccidentActivity4", String.valueOf(idModel1));

        if (passangerDBModel.getReligion_id() != null) {
            if (isListContainString(idModel1, passangerDBModel.getReligion_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPenumpangAgamafvbi, passangerStateModel.getReligion_id());
            }
        }
        if (passangerDBModel.getIdentity_type_id() != null) {
            if (isListContainString(idModel1, passangerDBModel.getIdentity_type_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPenumpangJenisIdentitasfvbi, passangerStateModel.getIdentity_type_id());
            }
        }
        if (passangerDBModel.getNationality_id() != null) {
            if (isListContainString(idModel1, passangerDBModel.getNationality_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPenumpangKebangsaanfvbi, passangerStateModel.getNationality_id());
            }
        }
        if (passangerDBModel.getEducation_id() != null) {
            if (isListContainString(idModel1, passangerDBModel.getEducation_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPenumpangPendidikanfvbi, passangerStateModel.getEducation_id());
            }
        }
        if (passangerDBModel.getProfession_id() != null) {
            if (isListContainString(idModel1, passangerDBModel.getProfession_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPenumpangPekerjaanfvbi, passangerStateModel.getProfession_id());
            }
        }
        //rb
        if (passangerDBModel.getGender_id() != null) {
            if (isListContainString(idModel1, passangerDBModel.getGender_id())) {
                ViewGeneratorUtil.setRadioCheckedAt(llviewDataPenumpangJenisKelaminfvbi, passangerStateModel.getGender_id());
            }
        }
        if (passangerDBModel.getInjury_id() != null) {
            if (isListContainString(idModel1, passangerDBModel.getInjury_id())) {
                ViewGeneratorUtil.setRadioCheckedAt(llviewDataPenumpangTingkatLukafvbi, passangerStateModel.getInjury_id());
            }
        }
        if (passangerDBModel.getDead_spot_id() != null) {
            if (isListContainString(idModel1, passangerDBModel.getDead_spot_id())) {
                ((ToggleRadioButton) llviewDataPenumpangDeadSpot.getChildAt(passangerStateModel.getDead_spot_id())).setChecked(true);
            }
        }
        if (passangerDBModel.getBehavior_id() != null) {
            if (isListContainString(idModel1, passangerDBModel.getBehavior_id())) {
                ViewGeneratorUtil.setRadioCheckedAt(llviewDataPenumpangPerilakufvbi, passangerStateModel.getBehavior_id());
            }
        }
        if (passangerDBModel.getArrested_id() != null) {
            if (isListContainString(idModel1, passangerDBModel.getArrested_id())) {
                ViewGeneratorUtil.setRadioCheckedAt(llviewDataPenumpangDiTahanfvbi, passangerStateModel.getArrested_id());
            }
        }
        //cb
        if (passangerDBModel.getPassenger_safety() != null) {
            if (isListContainStringArray(idModel1, passangerDBModel.getPassenger_safety())) {
                passengerSafetyHandler(passangerStateModel);
            }
        }
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        saveOverwriteData();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        finish();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(AddPassangerActivity.this);
        builder.setMessage("Simpan data sebelum keluar?").setPositiveButton("Ya", dialogClickListener)
                .setNegativeButton("Tidak", dialogClickListener).show();
    }

}
