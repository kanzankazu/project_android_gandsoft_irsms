package com.gandsoft.irsms.Activity.AddAccident.sub;

/**
 * Created by glenn on 2/6/18.
 */

public interface SetAccidentTypeInterface {

    void setSelectedMainTypeItem(int i);

    void closeDialogForResult(int res, int res2, String resid, boolean ismain);
}
