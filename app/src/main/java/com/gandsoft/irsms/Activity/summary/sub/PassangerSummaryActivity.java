package com.gandsoft.irsms.Activity.summary.sub;

import android.app.AlertDialog;
import com.gandsoft.irsms.Activity.AddPassanger.AddPassangerActivity;
import com.gandsoft.irsms.Activity.LocalBaseActivity;
import com.gandsoft.irsms.R;
import com.gandsoft.irsms.Support.Util.SystemUtil;
import com.gandsoft.irsms.model.RealmDbModel.PassengerForm.PassangerDBModel;
import com.gandsoft.irsms.model.RealmDbModel.RequestRealmDbObject;
import com.gandsoft.irsms.model.RealmDbModel.PassengerForm.AddPassangerStateModel;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by kanzan on 2/6/2018.
 */

public class PassangerSummaryActivity extends LocalBaseActivity implements PassengerObjectHookInteface {
    private View cvSummaryDataPenumpangLanjutanKeluarfvbi, cvSummaryDataPenumpangLanjutanTambahPenumpangfvbi;
    private RecyclerView rvSummaryDataPenumpangLanjutanfvbi;
    private PassengerItemRecyclerviewAdapter rvAdaptSummaryPassenger;
    private int localId;
    private int localSubId;
    private Realm realm;
    private LinearLayout llSummaryPassengerTitlefvbi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.gandsoft.irsms.R.layout.activity_summary_passenger);

        Intent bundle = getIntent();
        if (bundle.hasExtra("Local_ID")) {
            localId = bundle.getIntExtra("Local_ID", 0);
        }
        if (bundle.hasExtra("Local_SubID")) {
            localSubId = bundle.getIntExtra("Local_SubID", 0);
        }
        initComponent();
        initContent();
    }

    private void initComponent() {
        llSummaryPassengerTitlefvbi = findViewById(R.id.llSummaryPassengerTitle);

        cvSummaryDataPenumpangLanjutanKeluarfvbi = findViewById(com.gandsoft.irsms.R.id.cvSummaryDataPenumpangLanjutanKeluar);
        cvSummaryDataPenumpangLanjutanTambahPenumpangfvbi = findViewById(com.gandsoft.irsms.R.id.cvSummaryDataPenumpangLanjutanTambahPenumpang);
        rvSummaryDataPenumpangLanjutanfvbi = findViewById(com.gandsoft.irsms.R.id.rvSummaryDataPenumpangLanjutan);

    }

    private void initContent() {
        if (SystemUtil.isKitkat()){
            llSummaryPassengerTitlefvbi.bringToFront();
        }

        realm = Realm.getDefaultInstance();
        realm.refresh();
        cvSummaryDataPenumpangLanjutanKeluarfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        cvSummaryDataPenumpangLanjutanTambahPenumpangfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PassangerSummaryActivity.this, AddPassangerActivity.class);
                intent.putExtra("Local_ID", localId);
                intent.putExtra("Local_SubID", localSubId);
                startActivityForResult(intent, 8);
            }
        });

        RequestRealmDbObject passangerModels = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst();
        RealmList<PassangerDBModel> passangerDBModels = passangerModels.getSetPassangerRequestModels();

        rvAdaptSummaryPassenger = new PassengerItemRecyclerviewAdapter(this, passangerDBModels.where().equalTo("vehicle_SubID", localSubId).findAll());
        rvSummaryDataPenumpangLanjutanfvbi.setLayoutManager(new LinearLayoutManager(this, LinearLayout.VERTICAL, false));
        rvSummaryDataPenumpangLanjutanfvbi.setAdapter(rvAdaptSummaryPassenger);
        if(!realm.isClosed()){
            realm.close();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        initContent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        rvAdaptSummaryPassenger.notifyDataSetChanged();
    }

    @Override
    public void deletePassengerRealmModelAt(int id) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        try {
                            realm = Realm.getDefaultInstance();
                            realm.executeTransactionAsync(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.where(PassangerDBModel.class).equalTo("Local_ID",localId).equalTo("item_id", id).findFirst().deleteFromRealm();
                                    realm.where(AddPassangerStateModel.class).equalTo("Local_ID",localId).equalTo("item_id", id).findFirst().deleteFromRealm();
                                }
                            }, new Realm.Transaction.OnSuccess() {
                                @Override
                                public void onSuccess() {
                                    rvAdaptSummaryPassenger.notifyDataSetChanged();
                                }
                            });
                            realm.refresh();
                        } finally {
                            if (realm != null) {
                                realm.close();
                            }
                        }
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Hapus laporan ini?").
                setPositiveButton("Ya", dialogClickListener)
                .setNegativeButton("Tidak", dialogClickListener).show();
    }

    @Override
    public void editPassengerRealmModelAt(int id) {
        Intent editIntent = new Intent(this, AddPassangerActivity.class);
        editIntent.putExtra("Local_ID", localId);
        editIntent.putExtra("Item_ID", id);
        editIntent.putExtra("Local_SubID", localSubId);
        startActivityForResult(editIntent, 8);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
