package com.gandsoft.irsms.Activity.summary;

import android.app.Activity;

import com.gandsoft.irsms.model.RealmDbModel.PedestrianForm.PedestrianDBModel;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import io.realm.RealmList;

/**
 * Created by glenn on 2/6/18.
 */

public class AccidentRecyclerviewPedestrianAdapter extends RecyclerView.Adapter<AccidentRecyclerviewPedestrianAdapter.viewHolder> {

    private List<PedestrianDBModel> models;
    private Activity parent;

    public AccidentRecyclerviewPedestrianAdapter(Activity parent, RealmList<PedestrianDBModel> models) {
        this.models = models;
        this.parent = parent;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(com.gandsoft.irsms.R.layout.list_layout_summary_pedestrian, parent, false);
        return new AccidentRecyclerviewPedestrianAdapter.viewHolder(itemView);
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        private final TextView ivSummaryDataPejalanKakifvbi, tvSummaryDataPejalanKakiNamafvbi, tvSummaryDataPejalanKakiUmurfvbi, tvSummaryDataPejalanKakiLukafvbi, tvSummaryDataPejalanKakiPergerakanfvbi;
        private final ImageButton ibSummaryDataPejalanKakiDeletefvbi, ibSummaryDataPejalanKakiEditfvbi;

        public viewHolder(View view) {
            super(view);
            ivSummaryDataPejalanKakifvbi = view.findViewById(com.gandsoft.irsms.R.id.ivSummaryDataPejalanKaki);
            tvSummaryDataPejalanKakiNamafvbi = view.findViewById(com.gandsoft.irsms.R.id.tvSummaryDataPejalanKakiNama);
            tvSummaryDataPejalanKakiUmurfvbi = view.findViewById(com.gandsoft.irsms.R.id.tvSummaryDataPejalanKakiUmur);
            tvSummaryDataPejalanKakiLukafvbi = view.findViewById(com.gandsoft.irsms.R.id.tvSummaryDataPejalanKakiLuka);
            tvSummaryDataPejalanKakiPergerakanfvbi = view.findViewById(com.gandsoft.irsms.R.id.tvSummaryDataPejalanKakiPergerakan);
            ibSummaryDataPejalanKakiDeletefvbi = view.findViewById(com.gandsoft.irsms.R.id.ibSummaryDataPejalanKakiDelete);
            ibSummaryDataPejalanKakiEditfvbi = view.findViewById(com.gandsoft.irsms.R.id.ibSummaryDataPejalanKakiEdit);
        }
    }

    @Override
    public void onBindViewHolder(AccidentRecyclerviewPedestrianAdapter.viewHolder holder, final int position) {
        final PedestrianDBModel model = models.get(position);
        holder.tvSummaryDataPejalanKakiNamafvbi.setText(model.getFirst_name() + " " + model.getLast_name());
        holder.tvSummaryDataPejalanKakiUmurfvbi.setText(model.getAge());
        holder.tvSummaryDataPejalanKakiLukafvbi.setText(model.getInjury_id_text());
        holder.tvSummaryDataPejalanKakiPergerakanfvbi.setText(model.getMovement_id_text());
        holder.ivSummaryDataPejalanKakifvbi.setText((position + 1) + ".");

        holder.ibSummaryDataPejalanKakiEditfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SummaryHookInterface) parent).editPedestrianRealmModelAt(position);
            }
        });

        holder.ibSummaryDataPejalanKakiDeletefvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SummaryHookInterface) parent).deletePedestrianRealmModelAt(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public void updateModel(List<PedestrianDBModel> models) {
        this.models = models;
    }

}