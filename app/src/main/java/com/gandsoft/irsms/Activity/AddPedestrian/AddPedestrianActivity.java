package com.gandsoft.irsms.Activity.AddPedestrian;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gandsoft.irsms.Activity.LocalBaseActivity;
import com.gandsoft.irsms.Presenter.witget.ToggleRadioButton;
import com.gandsoft.irsms.R;
import com.gandsoft.irsms.Support.Util.SystemUtil;
import com.gandsoft.irsms.Support.ViewGenerator.ViewGenerator;
import com.gandsoft.irsms.Support.ViewGenerator.ViewGeneratorUtil;
import com.gandsoft.irsms.model.RealmDbModel.PedestrianForm.AddPedestrianStateModel;
import com.gandsoft.irsms.model.RealmDbModel.PedestrianForm.PedestrianDBModel;
import com.gandsoft.irsms.model.RealmDbModel.RequestRealmDbObject;
import com.gandsoft.irsms.model.RealmDbModel.VehicleForm.DriverDBModel;
import com.gandsoft.irsms.model.uiModel.FormdataLoader;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

import static com.gandsoft.irsms.Support.Util.SystemUtil.isKitkat;
import static com.gandsoft.irsms.Support.Util.SystemUtil.isListContainString;

/**
 * Created by kanzan on 2/6/2018.
 */

public class AddPedestrianActivity extends LocalBaseActivity {

    private View cvDataPejalanKakiKeluarfvbi, cvDataPejalanKakiStartfvbi;
    /*Spinner*/
    private LinearLayout spDataPejalanKakiKebangsaanfvbi, spDataPejalanKakiAgamafvbi, spDataPejalanKakiJenisIdentitasfvbi, spDataPejalanKakiPekerjaanfvbi, spDataPejalanKakiPendidikanfvbi, spDataPejalanKakiKecamatanfvbi, spDataPejalanKakiKelurahanfvbi, spDataPejalanKakiKabupatenfvbi, spDataPejalanKakiProvinsifvbi;
    private EditText etDataPejalanKakiJalanfvbi, etDataPejalanKakiUmurfvbi, etDataPejalanKakiNamaBelakangfvbi, etDataPejalanKakiNamaDepanfvbi, etDataPejalanKakiNoIdentitasfvbi, etDataPejalanKakiRTfvbi, etDataPejalanKakiRWfvbi;
    /*Radio*/
    private LinearLayout llviewDataPejalanKakiDiTahanfvbi, llviewDataPejalanKakiJenisKelaminfvbi, llviewDataPejalanKakiPergerakanfvbi, llviewDataPejalanKakiTingkatLukafvbi;
    private TextView tvDataPejalanKakiJenisMerkKendaraanfvbi;
    private Spinner spDataPejalanKakiNoTNKBfvbi;
    private int localId;
    private int orderId;
    private int selectedVehicleID;
    private Realm realm;
    private boolean loadedFromState;
    private RadioGroup llviewDataPejalankakiTingkatLukaRadioGroup;
    private RadioGroup llviewDataPejalanKakiDeadSpot;
    private LinearLayout llPedestrianTitlefvbi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_pedestrian);

        Intent bundle = getIntent();
        if (bundle.hasExtra("Local_ID")) {
            localId = bundle.getIntExtra("Local_ID", 0);
        }
        if (bundle.hasExtra("Order_ID")) {
            orderId = bundle.getIntExtra("Order_ID", 0);
            loadedFromState = true;
        }

        initComponent();
        initContent();

        if (loadedFromState) {
            loadState();
        }
        Log.d("Lihat", localId + " , " + orderId);
    }

    private void initComponent() {
        llPedestrianTitlefvbi = findViewById(R.id.llPedestrianTitle);

        cvDataPejalanKakiKeluarfvbi = findViewById(R.id.cvDataPejalanKakiKeluar);
        cvDataPejalanKakiStartfvbi = findViewById(R.id.cvDataPejalanKakiStart);

        spDataPejalanKakiNoTNKBfvbi = findViewById(R.id.spDataPejalanKakiNoTNKB);
        tvDataPejalanKakiJenisMerkKendaraanfvbi = findViewById(R.id.tvDataPejalanKakiJenisMerkKendaraan);

        etDataPejalanKakiNamaDepanfvbi = findViewById(R.id.etDataPejalanKakiNamaDepan);
        etDataPejalanKakiNamaBelakangfvbi = findViewById(R.id.etDataPejalanKakiNamaBelakang);
        etDataPejalanKakiUmurfvbi = findViewById(R.id.etDataPejalanKakiUmur);
        etDataPejalanKakiJalanfvbi = findViewById(R.id.etDataPejalanKakiJalan);
        etDataPejalanKakiNoIdentitasfvbi = findViewById(R.id.etDataPejalanKakiNoIdentitas);
        etDataPejalanKakiRTfvbi = findViewById(R.id.etDataPejalanKakiRT);
        etDataPejalanKakiRWfvbi = findViewById(R.id.etDataPejalanKakiRW);

        spDataPejalanKakiAgamafvbi = findViewById(R.id.spDataPejalanKakiAgama);
        spDataPejalanKakiJenisIdentitasfvbi = findViewById(R.id.spDataPejalanKakiJenisIdentitas);
        spDataPejalanKakiKebangsaanfvbi = findViewById(R.id.spDataPejalanKakiKebangsaan);
        spDataPejalanKakiPekerjaanfvbi = findViewById(R.id.spDataPejalanKakiPekerjaan);
        spDataPejalanKakiPendidikanfvbi = findViewById(R.id.spDataPejalanKakiPendidikan);
        //
        spDataPejalanKakiProvinsifvbi = findViewById(R.id.spDataPejalanKakiProvinsi);
        spDataPejalanKakiKabupatenfvbi = findViewById(R.id.spDataPejalanKakiKota);
        spDataPejalanKakiKecamatanfvbi = findViewById(R.id.spDataPejalanKakiKecamatan);
        spDataPejalanKakiKelurahanfvbi = findViewById(R.id.spDataPejalanKakiKelurahan);

        llviewDataPejalanKakiDiTahanfvbi = findViewById(R.id.llviewDataPejalanKakiDiTahan);
        llviewDataPejalanKakiJenisKelaminfvbi = findViewById(R.id.llviewDataPejalanKakiJenisKelamin);
        llviewDataPejalanKakiPergerakanfvbi = findViewById(R.id.llviewDataPejalanKakiPergerakan);
        llviewDataPejalanKakiTingkatLukafvbi = findViewById(R.id.llviewDataPejalanKakiTingkatLuka);
    }

    private void initContent() {

        if (isKitkat()){
            llPedestrianTitlefvbi.bringToFront();
        }

        cvDataPejalanKakiKeluarfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        cvDataPejalanKakiStartfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveOverwriteData();
            }
        });

        /*SP*/
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getReligion_id(), spDataPejalanKakiAgamafvbi);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getIdentity_type_id(), spDataPejalanKakiJenisIdentitasfvbi);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getNationality_id(), spDataPejalanKakiKebangsaanfvbi);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getProfession_id(), spDataPejalanKakiPekerjaanfvbi);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getEducation_id(), spDataPejalanKakiPendidikanfvbi);

        /*RB*/
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getArrested_id(), llviewDataPejalanKakiDiTahanfvbi);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getGender_id(), llviewDataPejalanKakiJenisKelaminfvbi);
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getPedestrian_movement_id(), llviewDataPejalanKakiPergerakanfvbi);

        View tot = LayoutInflater.from(this).inflate(R.layout.widget_item_injury, null, false);
        llviewDataPejalanKakiTingkatLukafvbi.addView(tot);
        llviewDataPejalankakiTingkatLukaRadioGroup = llviewDataPejalanKakiTingkatLukafvbi.findViewById(R.id.kucing);
        llviewDataPejalankakiTingkatLukaRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.meong4) {
                    group.findViewById(R.id.hundx).setVisibility(View.VISIBLE);
                } else {
                    group.findViewById(R.id.hundx).setVisibility(View.GONE);
                }
            }
        });
        llviewDataPejalanKakiDeadSpot = llviewDataPejalanKakiTingkatLukafvbi.findViewById(R.id.lukasub);

        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getProvince(), spDataPejalanKakiProvinsifvbi);
        FormdataLoader.generateDynamicDBitems(0, ViewGeneratorUtil.getSelectedSpinnerValue(spDataPejalanKakiProvinsifvbi));
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getKabupaten(), spDataPejalanKakiKabupatenfvbi);
        FormdataLoader.generateDynamicDBitems(1, ViewGeneratorUtil.getSelectedSpinnerValue(spDataPejalanKakiKabupatenfvbi));
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getKecamatan(), spDataPejalanKakiKecamatanfvbi);
        FormdataLoader.generateDynamicDBitems(2, ViewGeneratorUtil.getSelectedSpinnerValue(spDataPejalanKakiKecamatanfvbi));
        ViewGenerator.generateViewBasedOnModel(this, FormdataLoader.getKelurahan(), spDataPejalanKakiKelurahanfvbi);
        ((Spinner) ((ViewGroup) spDataPejalanKakiProvinsifvbi.getChildAt(0)).getChildAt(1)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FormdataLoader.generateDynamicDBitems(0, ViewGeneratorUtil.getSelectedSpinnerValue(spDataPejalanKakiProvinsifvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(spDataPejalanKakiKabupatenfvbi).replaceModels(FormdataLoader.getKabupaten().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(spDataPejalanKakiKabupatenfvbi);

                FormdataLoader.generateDynamicDBitems(1, ViewGeneratorUtil.getSelectedSpinnerValue(spDataPejalanKakiKabupatenfvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(spDataPejalanKakiKecamatanfvbi).replaceModels(FormdataLoader.getKecamatan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(spDataPejalanKakiKecamatanfvbi);

                FormdataLoader.generateDynamicDBitems(2, ViewGeneratorUtil.getSelectedSpinnerValue(spDataPejalanKakiKecamatanfvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(spDataPejalanKakiKelurahanfvbi).replaceModels(FormdataLoader.getKelurahan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(spDataPejalanKakiKelurahanfvbi);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ((Spinner) ((ViewGroup) spDataPejalanKakiKabupatenfvbi.getChildAt(0)).getChildAt(1)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FormdataLoader.generateDynamicDBitems(1, ViewGeneratorUtil.getSelectedSpinnerValue(spDataPejalanKakiKabupatenfvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(spDataPejalanKakiKecamatanfvbi).replaceModels(FormdataLoader.getKecamatan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(spDataPejalanKakiKecamatanfvbi);

                FormdataLoader.generateDynamicDBitems(2, ViewGeneratorUtil.getSelectedSpinnerValue(spDataPejalanKakiKecamatanfvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(spDataPejalanKakiKelurahanfvbi).replaceModels(FormdataLoader.getKelurahan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(spDataPejalanKakiKelurahanfvbi);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ((Spinner) ((ViewGroup) spDataPejalanKakiKecamatanfvbi.getChildAt(0)).getChildAt(1)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FormdataLoader.generateDynamicDBitems(2, ViewGeneratorUtil.getSelectedSpinnerValue(spDataPejalanKakiKecamatanfvbi));
                ViewGeneratorUtil.getSelectedSpinnerAdapter(spDataPejalanKakiKelurahanfvbi).replaceModels(FormdataLoader.getKelurahan().getViewModelGroup());
                ViewGeneratorUtil.notifyDatasetChangeOnSelectedSpinnerAdapter(spDataPejalanKakiKelurahanfvbi);
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        realm = Realm.getDefaultInstance();
        List<DriverDBModel> driverLists = realm.where(DriverDBModel.class).sort("vehicle_Dbid").equalTo("Local_ID", localId).findAll();
        List<String> noTnkbLists = new ArrayList<>();
        for (DriverDBModel mdls : driverLists) {
            noTnkbLists.add(mdls.getPlate_no());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, noTnkbLists);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDataPejalanKakiNoTNKBfvbi.setAdapter(dataAdapter);
        spDataPejalanKakiNoTNKBfvbi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                realm = Realm.getDefaultInstance();
                DriverDBModel model = realm.where(DriverDBModel.class).equalTo("Local_ID", localId).findAll().get(position);
                tvDataPejalanKakiJenisMerkKendaraanfvbi.setText(model.getVehicle_type_text() + " - " + model.getVehicle_brand_text());
                selectedVehicleID = model.getVehicle_Dbid();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ViewGeneratorUtil.selectSpinnerView(spDataPejalanKakiJenisIdentitasfvbi).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SystemUtil.validationIdentityByType(etDataPejalanKakiNoIdentitasfvbi,
                        ViewGeneratorUtil.getSelectedSpinnerValue(spDataPejalanKakiJenisIdentitasfvbi),
                        FormdataLoader.getId_identity_type_id_lv(),
                        FormdataLoader.getId_identity_type_max_lenght_lv());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etDataPejalanKakiJalanfvbi.setFilters(new InputFilter[]{new InputFilter.LengthFilter(255)});
    }

    private void loadState() {
        realm = Realm.getDefaultInstance();

        final AddPedestrianStateModel stateModel = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst().getSetPedestrianStateModels().get(orderId);

        checkData(stateModel);

        /*SP*//*
        ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPejalanKakiJenisIdentitasfvbi, stateModel.getIdentity_type_id());
        ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPejalanKakiAgamafvbi, stateModel.getReligion_id());
        ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPejalanKakiKebangsaanfvbi, stateModel.getNationality_id());
        ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPejalanKakiPekerjaanfvbi, stateModel.getProfession_id());
        ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPejalanKakiPendidikanfvbi, stateModel.getEducation_id());

        *//*RB*//*
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataPejalanKakiJenisKelaminfvbi, stateModel.getGender_id());
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataPejalanKakiPergerakanfvbi, stateModel.getPedestrian_movement_id());
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataPejalanKakiDiTahanfvbi, stateModel.getArrested_id());
        ViewGeneratorUtil.setRadioCheckedAt(llviewDataPejalanKakiTingkatLukafvbi, stateModel.getInjury_id());
        if (!stateModel.getDead_spot_id().equals(-1)) {
            ((ToggleRadioButton) llviewDataPejalanKakiDeadSpot.getChildAt(stateModel.getDead_spot_id())).setChecked(true);
        }*/

        /*ET*/
        etDataPejalanKakiNamaDepanfvbi.setText(stateModel.getFirst_name());
        etDataPejalanKakiNamaBelakangfvbi.setText(stateModel.getLast_name());
        etDataPejalanKakiUmurfvbi.setText(stateModel.getAge());
        etDataPejalanKakiNoIdentitasfvbi.setText(stateModel.getIdentity_no());
        etDataPejalanKakiJalanfvbi.setText(stateModel.getRoad_name());
        etDataPejalanKakiRTfvbi.setText(stateModel.getRt());
        etDataPejalanKakiRWfvbi.setText(stateModel.getRw());

        ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPejalanKakiProvinsifvbi, stateModel.getPropinsi());
        new Handler().postDelayed(() -> {
            ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPejalanKakiKabupatenfvbi, stateModel.getKabupaten());
            new Handler().postDelayed(() -> {
                ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPejalanKakiKecamatanfvbi, stateModel.getKecamatan());
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPejalanKakiKelurahanfvbi, stateModel.getKelurahan());
                    }
                }, 100);
            }, 100);
        }, 100);

        if (stateModel.getPlate_no() != null) {
            spDataPejalanKakiNoTNKBfvbi.setSelection(stateModel.getPlate_no(), true);
        }


    }

    private PedestrianDBModel generateRequestModel(@Nullable PedestrianDBModel requestModel) {
        if (requestModel == null) {
            requestModel = new PedestrianDBModel();
            requestModel.setLocal_id(localId);
            int itemCount = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst().getSetPedestrianRequestModels().where().findAll().size();
            if (itemCount > 0) {
                requestModel.setPedestrian_Dbid(realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst().getSetPedestrianRequestModels().where().equalTo("Local_ID", localId).findAll().get(itemCount - 1).getPedestrian_Dbid() + 1);
            } else {
                requestModel.setPedestrian_Dbid(1);
            }
        }
        try {
            requestModel.setPlate_no(spDataPejalanKakiNoTNKBfvbi.getSelectedItem().toString());
            requestModel.setVehicle_id(selectedVehicleID + "");
        } catch (Exception e) {
            //iz null bb
        }

        /*SP*/
        requestModel.setIdentity_type_id(ViewGeneratorUtil.getSelectedSpinnerValue(spDataPejalanKakiJenisIdentitasfvbi));
        requestModel.setReligion_id(ViewGeneratorUtil.getSelectedSpinnerValue(spDataPejalanKakiAgamafvbi));
        requestModel.setNationality_id(ViewGeneratorUtil.getSelectedSpinnerValue(spDataPejalanKakiKebangsaanfvbi));
        requestModel.setProfession_id(ViewGeneratorUtil.getSelectedSpinnerValue(spDataPejalanKakiPekerjaanfvbi));
        requestModel.setEducation_id(ViewGeneratorUtil.getSelectedSpinnerValue(spDataPejalanKakiPendidikanfvbi));

        requestModel.setPropinsi(ViewGeneratorUtil.getSelectedSpinnerValue(spDataPejalanKakiProvinsifvbi));
        requestModel.setKabupaten(ViewGeneratorUtil.getSelectedSpinnerValue(spDataPejalanKakiKabupatenfvbi));
        requestModel.setKecamatan(ViewGeneratorUtil.getSelectedSpinnerValue(spDataPejalanKakiKecamatanfvbi));
        requestModel.setKelurahan(ViewGeneratorUtil.getSelectedSpinnerValue(spDataPejalanKakiKelurahanfvbi));

        /*ET*/
        requestModel.setFirst_name(etDataPejalanKakiNamaDepanfvbi.getText().toString());
        requestModel.setLast_name(etDataPejalanKakiNamaBelakangfvbi.getText().toString());
        requestModel.setAge(etDataPejalanKakiUmurfvbi.getText().toString());
        requestModel.setIdentity_no(etDataPejalanKakiNoIdentitasfvbi.getText().toString());
        requestModel.setRoad_name(etDataPejalanKakiJalanfvbi.getText().toString());
        requestModel.setRt(etDataPejalanKakiRTfvbi.getText().toString());
        requestModel.setRw(etDataPejalanKakiRWfvbi.getText().toString());

        /*RB*/
        requestModel.setGender_id(ViewGeneratorUtil.getSelectedRadioValue(llviewDataPejalanKakiJenisKelaminfvbi));
        requestModel.setInjury_id(ViewGeneratorUtil.getSelectedRadioValue(llviewDataPejalanKakiTingkatLukafvbi));
        try {
            requestModel.setDead_spot_id((llviewDataPejalanKakiDeadSpot.findViewById(llviewDataPejalanKakiDeadSpot.getCheckedRadioButtonId())).getTag().toString());
            requestModel.setDead_spot_id_text((llviewDataPejalanKakiDeadSpot.findViewById(llviewDataPejalanKakiDeadSpot.getCheckedRadioButtonId())).getTag().toString());
        } catch (Exception e) {
            //Log.e("DeadSpot: ", e.getMessage());
            requestModel.setDead_spot_id("");
        }
        requestModel.setPedestrian_movement_id(ViewGeneratorUtil.getSelectedRadioValue(llviewDataPejalanKakiPergerakanfvbi));
        requestModel.setArrested_id(ViewGeneratorUtil.getSelectedRadioValue(llviewDataPejalanKakiDiTahanfvbi));

        requestModel.setInjury_id_text(ViewGeneratorUtil.getSelectedRadioTextValue(llviewDataPejalanKakiTingkatLukafvbi));
        requestModel.setDead_spot_id_text(ViewGeneratorUtil.getSelectedRadioTextValue(llviewDataPejalanKakiDeadSpot));
        requestModel.setMovement_id_text(ViewGeneratorUtil.getSelectedRadioTextValue(llviewDataPejalanKakiPergerakanfvbi));

        return requestModel;
    }

    private AddPedestrianStateModel generateStateModel(@Nullable AddPedestrianStateModel stateModel) {
        if (stateModel == null) {
            stateModel = new AddPedestrianStateModel();
            stateModel.setLocal_id(localId);
        }

        try {
            stateModel.setPlate_no(spDataPejalanKakiNoTNKBfvbi.getSelectedItemPosition());
        } catch (Exception e) {
            //if item is null, dont fill it
        }
        /*SP*/
        stateModel.setIdentity_type_id(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataPejalanKakiJenisIdentitasfvbi));
        stateModel.setReligion_id(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataPejalanKakiAgamafvbi));
        stateModel.setNationality_id(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataPejalanKakiKebangsaanfvbi));
        stateModel.setProfession_id(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataPejalanKakiPekerjaanfvbi));
        stateModel.setEducation_id(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataPejalanKakiPendidikanfvbi));

        stateModel.setPropinsi(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataPejalanKakiProvinsifvbi));
        stateModel.setKabupaten(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataPejalanKakiKabupatenfvbi));
        stateModel.setKecamatan(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataPejalanKakiKecamatanfvbi));
        stateModel.setKelurahan(ViewGeneratorUtil.getSelectedSpinnerIndex(spDataPejalanKakiKelurahanfvbi));

        /*ET*/
        stateModel.setFirst_name(etDataPejalanKakiNamaDepanfvbi.getText().toString());
        stateModel.setLast_name(etDataPejalanKakiNamaBelakangfvbi.getText().toString());
        stateModel.setAge(etDataPejalanKakiUmurfvbi.getText().toString());
        stateModel.setIdentity_no(etDataPejalanKakiNoIdentitasfvbi.getText().toString());
        stateModel.setRoad_name(etDataPejalanKakiJalanfvbi.getText().toString());
        stateModel.setRt(etDataPejalanKakiRTfvbi.getText().toString());
        stateModel.setRw(etDataPejalanKakiRWfvbi.getText().toString());

        /*TV*/
        stateModel.setVehicle_id(selectedVehicleID + "");

        /*RB*/
        stateModel.setGender_id(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataPejalanKakiJenisKelaminfvbi));
        stateModel.setInjury_id(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataPejalanKakiTingkatLukafvbi));
        stateModel.setDead_spot_id(llviewDataPejalanKakiDeadSpot.indexOfChild(llviewDataPejalanKakiDeadSpot.findViewById(llviewDataPejalanKakiDeadSpot.getCheckedRadioButtonId())));
        stateModel.setPedestrian_movement_id(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataPejalanKakiPergerakanfvbi));
        stateModel.setArrested_id(ViewGeneratorUtil.getSelectedRadioIndex(llviewDataPejalanKakiDiTahanfvbi));

        return stateModel;
    }

    private void saveOverwriteData() {
        if (!loadedFromState) {
            if (saveData()) {
                Toast.makeText(getApplicationContext(), "Save Success", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Save Failed", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (overwriteData()) {
                Toast.makeText(getApplicationContext(), "Overwrite Success", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Overwrite Failed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean saveData() {
        realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                RequestRealmDbObject poolRequestModel = realm.where(RequestRealmDbObject.class).sort("Local_ID").equalTo("Local_ID", localId).findFirst();
                poolRequestModel.getSetPedestrianRequestModels().add(generateRequestModel(null));
                poolRequestModel.getSetPedestrianStateModels().add(generateStateModel(null));
                realm.copyToRealmOrUpdate(poolRequestModel);

            }
        });
        realm.refresh();
        realm.close();
        return true;
    }

    private boolean overwriteData() {
        realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RequestRealmDbObject poolRequestModel = realm.where(RequestRealmDbObject.class).sort("Local_ID").equalTo("Local_ID", localId).findFirst();
                generateRequestModel(poolRequestModel.getSetPedestrianRequestModels().get(orderId));
                generateStateModel(poolRequestModel.getSetPedestrianStateModels().get(orderId));
            }
        });
        realm.refresh();
        realm.close();
        return true;
    }

    private void checkData(AddPedestrianStateModel pedestrianStateModel) {
        realm = Realm.getDefaultInstance();
        PedestrianDBModel pedestrianDBModel = realm.where(RequestRealmDbObject.class).equalTo("Local_ID", localId).findFirst().getSetPedestrianRequestModels().get(0);

        List<String> idModel1 = FormdataLoader.getIdModel();
        Log.d("Lihat checkData AddAccidentActivity1", String.valueOf(pedestrianDBModel));
        Log.d("Lihat checkData AddAccidentActivity4", String.valueOf(idModel1));

        //sp
        if (pedestrianDBModel.getIdentity_type_id() != null) {
            if (isListContainString(idModel1, pedestrianDBModel.getIdentity_type_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPejalanKakiJenisIdentitasfvbi, pedestrianStateModel.getIdentity_type_id());
            }
        }
        if (pedestrianDBModel.getReligion_id() != null) {
            if (isListContainString(idModel1, pedestrianDBModel.getReligion_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPejalanKakiAgamafvbi, pedestrianStateModel.getReligion_id());
            }
        }
        if (pedestrianDBModel.getNationality_id() != null) {
            if (isListContainString(idModel1, pedestrianDBModel.getNationality_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPejalanKakiKebangsaanfvbi, pedestrianStateModel.getNationality_id());
            }
        }
        if (pedestrianDBModel.getProfession_id() != null) {
            if (isListContainString(idModel1, pedestrianDBModel.getProfession_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPejalanKakiPekerjaanfvbi, pedestrianStateModel.getProfession_id());
            }
        }
        if (pedestrianDBModel.getEducation_id() != null) {
            if (isListContainString(idModel1, pedestrianDBModel.getEducation_id())) {
                ViewGeneratorUtil.selectSpinnerDropdownAt(spDataPejalanKakiPendidikanfvbi, pedestrianStateModel.getEducation_id());
            }
        }
        //rb
        if (pedestrianDBModel.getGender_id() != null) {
            if (isListContainString(idModel1, pedestrianDBModel.getGender_id())) {
                ViewGeneratorUtil.setRadioCheckedAt(llviewDataPejalanKakiJenisKelaminfvbi, pedestrianStateModel.getGender_id());
            }
        }
        if (pedestrianDBModel.getInjury_id() != null) {
            if (isListContainString(idModel1, pedestrianDBModel.getInjury_id())) {
                ViewGeneratorUtil.setRadioCheckedAt(llviewDataPejalanKakiTingkatLukafvbi, pedestrianStateModel.getInjury_id());
            }
        }
        if (pedestrianDBModel.getDead_spot_id() != null) {
            if (isListContainString(idModel1, pedestrianDBModel.getDead_spot_id())) {
                ((ToggleRadioButton) llviewDataPejalanKakiDeadSpot.getChildAt(pedestrianStateModel.getDead_spot_id())).setChecked(true);
            }
        }
        if (pedestrianDBModel.getPedestrian_movement_id() != null) {
            if (isListContainString(idModel1, pedestrianDBModel.getPedestrian_movement_id())) {
                ViewGeneratorUtil.setRadioCheckedAt(llviewDataPejalanKakiPergerakanfvbi, pedestrianStateModel.getPedestrian_movement_id());
            }
        }
        if (pedestrianDBModel.getArrested_id() != null) {
            if (isListContainString(idModel1, pedestrianDBModel.getArrested_id())) {
                ViewGeneratorUtil.setRadioCheckedAt(llviewDataPejalanKakiDiTahanfvbi, pedestrianStateModel.getArrested_id());
            }
        }
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        saveOverwriteData();
                        finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        finish();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(AddPedestrianActivity.this);
        builder.setMessage("Simpan data sebelum keluar?").setPositiveButton("Ya", dialogClickListener)
                .setNegativeButton("Tidak", dialogClickListener).show();
    }
}
