package com.gandsoft.irsms.model.RequestModel.VehicleForm;

/**
 * Created by glenn on 1/25/18.
 */

public class GetVehicleIDRequestModel {
    private String accident_id;

    public String getAccident_id() {
        return accident_id;
    }

    public void setAccident_id(String accident_id) {
        this.accident_id = accident_id;
    }
}
