package com.gandsoft.irsms.model.ObjectModel.DetailModel;

/**
 * Created by glenn on 1/25/18.
 */

public class DriverDetailModel {

    private String first_name;
    private String last_name;
    private String vehicle_type;
    private String no_tnkb;
    private int age;
    private String tingkat_luka;
    private String ditahan;

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getVehicle_type() {
        return vehicle_type;
    }

    public void setVehicle_type(String vehicle_type) {
        this.vehicle_type = vehicle_type;
    }

    public String getNo_tnkb() {
        return no_tnkb;
    }

    public void setNo_tnkb(String no_tnkb) {
        this.no_tnkb = no_tnkb;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getTingkat_luka() {
        return tingkat_luka;
    }

    public void setTingkat_luka(String tingkat_luka) {
        this.tingkat_luka = tingkat_luka;
    }

    public String getDitahan() {
        return ditahan;
    }

    public void setDitahan(String ditahan) {
        this.ditahan = ditahan;
    }
}
