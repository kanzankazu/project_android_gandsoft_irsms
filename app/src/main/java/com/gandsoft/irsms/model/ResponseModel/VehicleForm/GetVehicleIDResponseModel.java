package com.gandsoft.irsms.model.ResponseModel.VehicleForm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by glenn on 1/25/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetVehicleIDResponseModel {
    private List<String> vehicle_id;

    public List<String> getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(List<String> vehicle_id) {
        this.vehicle_id = vehicle_id;
    }
}
