package com.gandsoft.irsms.model.ObjectModel.DetailModel;

/**
 * Created by glenn on 1/25/18.
 */

public class PassengerDetailModel {

    private String passenger_first_name;
    private String passenger_last_name;
    private int passenger_age;
    private String passenger_luka;
    private String passenger_arrested;

    public String getPassenger_first_name() {
        return passenger_first_name;
    }

    public void setPassenger_first_name(String passenger_first_name) {
        this.passenger_first_name = passenger_first_name;
    }

    public String getPassenger_last_name() {
        return passenger_last_name;
    }

    public void setPassenger_last_name(String passenger_last_name) {
        this.passenger_last_name = passenger_last_name;
    }

    public int getPassenger_age() {
        return passenger_age;
    }

    public void setPassenger_age(int passenger_age) {
        this.passenger_age = passenger_age;
    }

    public String getPassenger_luka() {
        return passenger_luka;
    }

    public void setPassenger_luka(String passenger_luka) {
        this.passenger_luka = passenger_luka;
    }

    public String getPassenger_arrested() {
        return passenger_arrested;
    }

    public void setPassenger_arrested(String passenger_arrested) {
        this.passenger_arrested = passenger_arrested;
    }
}
