package com.gandsoft.irsms.model.ResponseModel.VehicleForm;

import com.gandsoft.irsms.model.ResponseModel.BaseResponseModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by glenn on 1/25/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class SetPedestrianResponseModel extends BaseResponseModel {

    private String pedestrian_person_id;

    public String getPedestrian_person_id() {
        return pedestrian_person_id;
    }

    public void setPedestrian_person_id(String pedestrian_person_id) {
        this.pedestrian_person_id = pedestrian_person_id;
    }
}
