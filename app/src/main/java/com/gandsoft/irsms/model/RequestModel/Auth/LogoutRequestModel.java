package com.gandsoft.irsms.model.RequestModel.Auth;

/**
 * Created by gleen on 12/02/18.
 */

public class LogoutRequestModel {
    private String imei;

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }
}
