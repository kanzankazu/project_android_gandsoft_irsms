package com.gandsoft.irsms.model.ResponseModel.MainPage;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gandsoft.irsms.model.ObjectModel.SearchAccidentDataModel;
import com.gandsoft.irsms.model.ResponseModel.BaseResponseModel;

import java.util.List;

/**
 * Created by glenn on 1/25/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchAccidentResponseModel extends BaseResponseModel {

    List<SearchAccidentDataModel> search_accident;

    public List<SearchAccidentDataModel> getSearch_accident() {
        return search_accident;
    }

    public void setSearch_accident(List<SearchAccidentDataModel> search_accident) {
        this.search_accident = search_accident;
    }
}
