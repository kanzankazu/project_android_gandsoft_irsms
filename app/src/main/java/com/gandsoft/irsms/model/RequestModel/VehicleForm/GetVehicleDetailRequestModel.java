package com.gandsoft.irsms.model.RequestModel.VehicleForm;

/**
 * Created by glenn on 1/25/18.
 */

public class GetVehicleDetailRequestModel {

    private String vehicle_id;

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }
}
