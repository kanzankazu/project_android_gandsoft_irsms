package com.gandsoft.irsms.model.RealmDbModel.WitnessForm;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Created by gleen on 08/02/18.
 */

public class AddWitnessStateModel extends RealmObject implements Serializable{

    private Integer religion_id;
    private Integer profession_id;
    private Integer identity_type_id;

    private String first_name;
    private String last_name;
    private String age;
    private String road_name;
    private String identity_no;

    private Integer gender_id;

    private int Local_ID;

    public Integer getReligion_id() {
        return religion_id;
    }

    public void setReligion_id(Integer religion_id) {
        this.religion_id = religion_id;
    }

    public Integer getProfession_id() {
        return profession_id;
    }

    public void setProfession_id(Integer profession_id) {
        this.profession_id = profession_id;
    }

    public Integer getIdentity_type_id() {
        return identity_type_id;
    }

    public void setIdentity_type_id(Integer identity_type_id) {
        this.identity_type_id = identity_type_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getRoad_name() {
        return road_name;
    }

    public void setRoad_name(String road_name) {
        this.road_name = road_name;
    }

    public Integer getGender_id() {
        return gender_id;
    }

    public void setGender_id(Integer gender_id) {
        this.gender_id = gender_id;
    }

    public String getIdentity_no() {
        return identity_no;
    }

    public void setIdentity_no(String identity_no) {
        this.identity_no = identity_no;
    }

    public int getLocal_id() {
        return Local_ID;
    }

    public void setLocal_id(int Local_ID) {
        this.Local_ID = Local_ID;
    }
}
