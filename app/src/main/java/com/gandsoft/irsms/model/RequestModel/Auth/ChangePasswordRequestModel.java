package com.gandsoft.irsms.model.RequestModel.Auth;

/**
 * Created by glenn on 1/25/18.
 */

public class ChangePasswordRequestModel {
    String password_confirmation;
    String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword_confirmation() {
        return password_confirmation;
    }

    public void setPassword_confirmation(String password_confirmation) {
        this.password_confirmation = password_confirmation;
    }
}
