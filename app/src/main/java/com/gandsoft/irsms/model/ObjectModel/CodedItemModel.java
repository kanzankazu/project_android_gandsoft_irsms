package com.gandsoft.irsms.model.ObjectModel;

import java.io.Serializable;

/**
 * Created by glenn on 1/25/18.
 */

public class CodedItemModel implements Serializable {

    private String id;
    private String name;
    private String grp_id;
    private String max_length;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGrp_id() {
        return grp_id;
    }

    public void setGrp_id(String grp_id) {
        this.grp_id = grp_id;
    }

    public String getMax_length() {
        return max_length;
    }

    public void setMax_length(String max_length) {
        this.max_length = max_length;
    }
}
