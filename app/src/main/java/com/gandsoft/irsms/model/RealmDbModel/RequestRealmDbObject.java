package com.gandsoft.irsms.model.RealmDbModel;

import com.gandsoft.irsms.model.RealmDbModel.AccidentForm.AccidentDBModel;
import com.gandsoft.irsms.model.RealmDbModel.DiagramMappingForm.DiagramMappingDBModel;
import com.gandsoft.irsms.model.RealmDbModel.PassengerForm.PassangerDBModel;
import com.gandsoft.irsms.model.RealmDbModel.PedestrianForm.PedestrianDBModel;
import com.gandsoft.irsms.model.RealmDbModel.VehicleForm.DriverDBModel;
import com.gandsoft.irsms.model.RealmDbModel.WitnessForm.SetWitnessDBModel;
import com.gandsoft.irsms.model.RealmDbModel.AccidentForm.AddAccidentStateModel;
import com.gandsoft.irsms.model.RealmDbModel.DiagramMappingForm.AddDiagraMappingStateModel;
import com.gandsoft.irsms.model.RealmDbModel.VehicleForm.AddDriverStateModel;
import com.gandsoft.irsms.model.RealmDbModel.PassengerForm.AddPassangerStateModel;
import com.gandsoft.irsms.model.RealmDbModel.PedestrianForm.AddPedestrianStateModel;
import com.gandsoft.irsms.model.RealmDbModel.WitnessForm.AddWitnessStateModel;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by gleen on 09/02/18.
 */


public class RequestRealmDbObject extends RealmObject {
    @PrimaryKey
    private int Local_ID;
    private String AccidentID;
    private int user_id;

    private RealmList<AccidentDBModel> setAccidentRequestModel;
    private RealmList<AddAccidentStateModel> AddAccidentStateModel;

    private RealmList<DiagramMappingDBModel> setDiagramMappingRequestModel;
    private RealmList<AddDiagraMappingStateModel> AddDiagramMappingStateModel;

    private RealmList<DriverDBModel> setDriverRequestModels;
    private RealmList<AddDriverStateModel> setDriverStateModels;

    private RealmList<PassangerDBModel> setPassangerRequestModels;
    private RealmList<AddPassangerStateModel> setPassangerStateModels;

    private RealmList<PedestrianDBModel> setPedestrianRequestModels;
    private RealmList<AddPedestrianStateModel> setPedestrianStateModels;

    private RealmList<SetWitnessDBModel> setWitnessRequestModels;
    private RealmList<AddWitnessStateModel> setWitnessStateModels;

    public int getLocal_ID() {
        return Local_ID;
    }

    public void setLocal_ID(int local_ID) {
        Local_ID = local_ID;
    }

    public String getAccidentID() {
        return AccidentID;
    }

    public void setAccidentID(String accidentID) {
        AccidentID = accidentID;
    }

    public RealmList<AddPedestrianStateModel> getSetPedestrianStateModels() {
        return setPedestrianStateModels;
    }

    public void setSetPedestrianStateModels(RealmList<AddPedestrianStateModel> setPedestrianStateModels) {
        this.setPedestrianStateModels = setPedestrianStateModels;
    }

    public RealmList<AddWitnessStateModel> getSetWitnessStateModels() {
        return setWitnessStateModels;
    }

    public void setSetWitnessStateModels(RealmList<AddWitnessStateModel> setWitnessStateModels) {
        this.setWitnessStateModels = setWitnessStateModels;
    }

    public RealmList<AccidentDBModel> getSetAccidentRequestModel() {
        return setAccidentRequestModel;
    }

    public void setSetAccidentRequestModel(RealmList<AccidentDBModel> setAccidentRequestModel) {
        this.setAccidentRequestModel = setAccidentRequestModel;
    }

    public RealmList<com.gandsoft.irsms.model.RealmDbModel.AccidentForm.AddAccidentStateModel> getAddAccidentStateModel() {
        return AddAccidentStateModel;
    }

    public void setAddAccidentStateModel(RealmList<com.gandsoft.irsms.model.RealmDbModel.AccidentForm.AddAccidentStateModel> addAccidentStateModel) {
        AddAccidentStateModel = addAccidentStateModel;
    }

    public RealmList<DiagramMappingDBModel> getSetDiagramMappingRequestModel() {
        return setDiagramMappingRequestModel;
    }

    public void setSetDiagramMappingRequestModel(RealmList<DiagramMappingDBModel> setDiagramMappingRequestModel) {
        this.setDiagramMappingRequestModel = setDiagramMappingRequestModel;
    }

    public RealmList<DriverDBModel> getSetDriverRequestModels() {
        return setDriverRequestModels;
    }

    public void setSetDriverRequestModels(RealmList<DriverDBModel> setDriverRequestModels) {
        this.setDriverRequestModels = setDriverRequestModels;
    }

    public RealmList<AddDriverStateModel> getSetDriverStateModels() {
        return setDriverStateModels;
    }

    public void setSetDriverStateModels(RealmList<AddDriverStateModel> setDriverStateModels) {
        this.setDriverStateModels = setDriverStateModels;
    }

    public RealmList<AddPassangerStateModel> getSetPassangerStateModels() {
        return setPassangerStateModels;
    }

    public void setSetPassangerStateModels(RealmList<AddPassangerStateModel> setPassangerStateModels) {
        this.setPassangerStateModels = setPassangerStateModels;
    }

    public RealmList<PassangerDBModel> getSetPassangerRequestModels() {
        return setPassangerRequestModels;
    }

    public void setSetPassangerRequestModels(RealmList<PassangerDBModel> setPassangerRequestModels) {
        this.setPassangerRequestModels = setPassangerRequestModels;
    }

    public RealmList<PedestrianDBModel> getSetPedestrianRequestModels() {
        return setPedestrianRequestModels;
    }

    public void setSetPedestrianRequestModels(RealmList<PedestrianDBModel> setPedestrianRequestModels) {
        this.setPedestrianRequestModels = setPedestrianRequestModels;
    }

    public RealmList<SetWitnessDBModel> getSetWitnessRequestModels() {
        return setWitnessRequestModels;
    }

    public void setSetWitnessRequestModels(RealmList<SetWitnessDBModel> setWitnessRequestModels) {
        this.setWitnessRequestModels = setWitnessRequestModels;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public RealmList<AddDiagraMappingStateModel> getAddDiagramMappingStateModel() {
        return AddDiagramMappingStateModel;
    }

    public void setAddDiagramMappingStateModel(RealmList<AddDiagraMappingStateModel> addDiagramMappingStateModel) {
        AddDiagramMappingStateModel = addDiagramMappingStateModel;
    }
}
