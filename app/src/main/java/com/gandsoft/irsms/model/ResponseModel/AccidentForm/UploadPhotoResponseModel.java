package com.gandsoft.irsms.model.ResponseModel.AccidentForm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gandsoft.irsms.model.ResponseModel.BaseResponseModel;

/**
 * Created by gleen on 09/02/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class UploadPhotoResponseModel extends BaseResponseModel {
}
