package com.gandsoft.irsms.model.RequestModel.AccidentForm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gandsoft.irsms.model.RealmDbModel.WitnessForm.SetWitnessDBModel;

/**
 * Created by glenn on 1/25/18.
 */

public class SetCheckAccidentRequestModel {

    private String accident_id;
    private String state;

    public String getAccident_id() {
        return accident_id;
    }

    public void setAccident_id(String accident_id) {
        this.accident_id = accident_id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
