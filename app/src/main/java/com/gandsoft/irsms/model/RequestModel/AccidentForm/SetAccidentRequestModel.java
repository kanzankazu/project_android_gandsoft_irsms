package com.gandsoft.irsms.model.RequestModel.AccidentForm;


import com.gandsoft.irsms.model.RealmDbModel.AccidentForm.AccidentDBModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by gleen on 19/02/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class SetAccidentRequestModel {

    private String imei;
    private String no_laka;
    private String petugas_pelapor;
    private String polres;
    private String tanggal_kejadian;
    private String tanggal_dilaporkan;
    private String jam_kejadian;
    private String jam_dilaporkan;
    private String informasi_khusus;
    private String tipe_kecelakaan;
    private String kondisi_cahaya;
    private String cuaca;
    private String kecelakaan_menonjol;
    private String nama_jalan;
    private String nomor_jalan;
    private String titik_dilaporkan;
    private String ref_spot_desc;
    private String latitude;
    private String longitude;
    private String jarak_lokasi;
    private String titik_acuan;
    private String arah_titik;
    private String fungsi_jalan;
    private String kelas_jalan;
    private String tipe_jalan;
    private String bentuk_geometri;
    private String kondisi_permukaan;
    private String batas_kecepatan;
    private String kemiringan_jalan;
    private String pengaturan_simpang;
    private String status_jalan;
    private String kerusakan_material;
    private String total_nilai_rugi_kendaraan;
    private String total_nilai_rugi_non_kendaraan;
    private String penyebab;
    private String uraian_polisi;
    private String created_by;

    private int req_data;

    public SetAccidentRequestModel(AccidentDBModel model) {
        this.setImei(model.getImei());
        this.setNo_laka(model.getNo_laka());
        this.setPetugas_pelapor(model.getPetugas_pelapor());
        this.setPolres(model.getPolres());
        this.setTanggal_kejadian(model.getTanggal_kejadian());
        this.setTanggal_dilaporkan(model.getTanggal_dilaporkan());
        this.setJam_kejadian(model.getJam_kejadian());
        this.setJam_dilaporkan(model.getJam_dilaporkan());
        this.setInformasi_khusus(model.getInformasi_khusus());
        this.setTipe_kecelakaan(model.getTipe_kecelakaan());
        this.setKondisi_cahaya(model.getKondisi_cahaya());
        this.setCuaca(model.getCuaca());
        this.setKecelakaan_menonjol(model.getKecelakaan_menonjol());
        this.setNama_jalan(model.getNama_jalan());
        this.setNomor_jalan(model.getNomor_jalan());
        this.setTitik_dilaporkan(model.getTitik_dilaporkan());
        this.setRef_spot_desc(model.getRef_spot_desc());
        this.setLatitude(model.getLatitude());
        this.setLongitude(model.getLongitude());
        this.setJarak_lokasi(model.getJarak_lokasi());
        this.setTitik_acuan(model.getTitik_acuan());
        this.setArah_titik(model.getArah_titik());
        this.setFungsi_jalan(model.getFungsi_jalan());
        this.setKelas_jalan(model.getKelas_jalan());
        this.setTipe_jalan(model.getTipe_jalan());
        this.setBentuk_geometri(model.getBentuk_geometri());
        this.setKondisi_permukaan(model.getKondisi_permukaan());
        this.setBatas_kecepatan(model.getBatas_kecepatan());
        this.setKemiringan_jalan(model.getKemiringan_jalan());
        this.setStatus_jalan(model.getStatus_jalan());
        this.setKerusakan_material(model.getKerusakan_material());
        this.setTotal_nilai_rugi_kendaraan(model.getTotal_nilai_rugi_kendaraan());
        this.setTotal_nilai_rugi_non_kendaraan(model.getTotal_nilai_rugi_non_kendaraan());
        this.setPenyebab(model.getPenyebab());
        this.setUraian_polisi(model.getUraian_polisi());
        this.setPengaturan_simpang(model.getPengaturan_simpang());
        this.setCreated_by(model.getCreated_by());
    }

    public String getImei() {
        return imei != null ? imei : "";
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getNo_laka() {
        return no_laka != null ? imei : "";
    }

    public void setNo_laka(String no_laka) {
        this.no_laka = no_laka;
    }

    public String getPetugas_pelapor() {
        return petugas_pelapor != null ? petugas_pelapor : "";
    }

    public void setPetugas_pelapor(String petugas_pelapor) {
        this.petugas_pelapor = petugas_pelapor;
    }

    public String getPolres() {
        return polres != null ? polres : "";
    }

    public void setPolres(String polres) {
        this.polres = polres;
    }

    public String getTanggal_kejadian() {
        return tanggal_kejadian != null ? tanggal_kejadian : "";
    }

    public void setTanggal_kejadian(String tanggal_kejadian) {
        String jing[] = tanggal_kejadian.split("-");
        this.tanggal_kejadian = jing[2] + "-" + jing[1] + "-" + jing[0];
    }

    public String getTanggal_dilaporkan() {
        return tanggal_dilaporkan != null ? tanggal_dilaporkan : "";
    }

    public void setTanggal_dilaporkan(String tanggal_dilaporkan) {
        String jing[] = tanggal_dilaporkan.split("-");
        this.tanggal_dilaporkan = jing[2] + "-" + jing[1] + "-" + jing[0];
    }

    public String getJam_kejadian() {
        return jam_kejadian != null ? jam_kejadian : "";
    }

    public void setJam_kejadian(String jam_kejadian) {
        this.jam_kejadian = jam_kejadian + ":00";
    }

    public String getJam_dilaporkan() {
        return jam_dilaporkan != null ? jam_dilaporkan : "";
    }

    public void setJam_dilaporkan(String jam_dilaporkan) {
        this.jam_dilaporkan = jam_dilaporkan + ":00";
    }

    public String getInformasi_khusus() {
        return informasi_khusus != null ? informasi_khusus : "";
    }

    public void setInformasi_khusus(String informasi_khusus) {
        this.informasi_khusus = informasi_khusus;
    }

    public String getTipe_kecelakaan() {
        return tipe_kecelakaan != null ? tipe_kecelakaan : "";
    }

    public void setTipe_kecelakaan(String tipe_kecelakaan) {
        this.tipe_kecelakaan = tipe_kecelakaan;
    }

    public String getKondisi_cahaya() {
        return kondisi_cahaya != null ? kondisi_cahaya : "";
    }

    public void setKondisi_cahaya(String kondisi_cahaya) {
        this.kondisi_cahaya = kondisi_cahaya;
    }

    public String getCuaca() {
        return cuaca != null ? cuaca : "";
    }

    public void setCuaca(String cuaca) {
        this.cuaca = cuaca;
    }

    public String getKecelakaan_menonjol() {
        return kecelakaan_menonjol != null ? kecelakaan_menonjol : "";
    }

    public void setKecelakaan_menonjol(String kecelakaan_menonjol) {
        this.kecelakaan_menonjol = kecelakaan_menonjol;
    }

    public String getNama_jalan() {
        return nama_jalan != null ? nama_jalan : "";
    }

    public void setNama_jalan(String nama_jalan) {
        this.nama_jalan = nama_jalan;
    }

    public String getNomor_jalan() {
        return nomor_jalan != null ? nomor_jalan : "";
    }

    public void setNomor_jalan(String nomor_jalan) {
        this.nomor_jalan = nomor_jalan;
    }

    public String getTitik_dilaporkan() {
        return titik_dilaporkan != null ? titik_dilaporkan : "";
    }

    public void setTitik_dilaporkan(String titik_dilaporkan) {
        this.titik_dilaporkan = titik_dilaporkan;
    }

    public String getRef_spot_desc() {
        return ref_spot_desc != null ? ref_spot_desc : "";
    }

    public void setRef_spot_desc(String ref_spot_desc) {
        this.ref_spot_desc = ref_spot_desc;
    }

    public String getLatitude() {
        return latitude != null ? latitude : "";
    }

    public void setLatitude(String latitude) {
        if (latitude != null && !latitude.equals("-")) {
            this.latitude = latitude;
        } else {
            this.latitude = 0 + "";
        }
    }

    public String getLongitude() {
        return longitude != null ? longitude : "";
    }

    public void setLongitude(String longitude) {
        if (longitude != null && !longitude.equals("-")) {
            this.longitude = longitude;
        } else {
            this.longitude = 0 + "";
        }
    }

    public String getJarak_lokasi() {
        return jarak_lokasi != null ? jarak_lokasi : "";
    }

    public void setJarak_lokasi(String jarak_lokasi) {
        this.jarak_lokasi = jarak_lokasi;
    }

    public String getTitik_acuan() {
        return titik_acuan != null ? titik_acuan : "";
    }

    public void setTitik_acuan(String titik_acuan) {
        this.titik_acuan = titik_acuan;
    }

    public String getArah_titik() {
        return arah_titik != null ? arah_titik : "";
    }

    public void setArah_titik(String arah_titik) {
        this.arah_titik = arah_titik;
    }

    public String getFungsi_jalan() {
        return fungsi_jalan != null ? fungsi_jalan : "";
    }

    public void setFungsi_jalan(String fungsi_jalan) {
        this.fungsi_jalan = fungsi_jalan;
    }

    public String getKelas_jalan() {
        return kelas_jalan != null ? kelas_jalan : "";
    }

    public void setKelas_jalan(String kelas_jalan) {
        this.kelas_jalan = kelas_jalan;
    }

    public String getTipe_jalan() {
        return tipe_jalan != null ? tipe_jalan : "";
    }

    public void setTipe_jalan(String tipe_jalan) {
        this.tipe_jalan = tipe_jalan;
    }

    public String getBentuk_geometri() {
        return bentuk_geometri != null ? bentuk_geometri : "";
    }

    public void setBentuk_geometri(String bentuk_geometri) {
        this.bentuk_geometri = bentuk_geometri;
    }

    public String getKondisi_permukaan() {
        return kondisi_permukaan != null ? kondisi_permukaan : "";
    }

    public void setKondisi_permukaan(String kondisi_permukaan) {
        this.kondisi_permukaan = kondisi_permukaan;
    }

    public String getBatas_kecepatan() {
        return batas_kecepatan != null ? batas_kecepatan : "";
    }

    public void setBatas_kecepatan(String batas_kecepatan) {
        this.batas_kecepatan = batas_kecepatan;
    }

    public String getKemiringan_jalan() {
        return kemiringan_jalan != null ? kemiringan_jalan : "";
    }

    public void setKemiringan_jalan(String kemiringan_jalan) {
        this.kemiringan_jalan = kemiringan_jalan;
    }

    public String getPengaturan_simpang() {
        return pengaturan_simpang != null ? pengaturan_simpang : "";
    }

    public void setPengaturan_simpang(String pengaturan_simpang) {
        this.pengaturan_simpang = pengaturan_simpang;
    }

    public String getStatus_jalan() {
        return status_jalan != null ? status_jalan : "";
    }

    public void setStatus_jalan(String status_jalan) {
        this.status_jalan = status_jalan;
    }

    public String getKerusakan_material() {
        return kerusakan_material != null ? kerusakan_material : "";
    }

    public void setKerusakan_material(String kerusakan_material) {
        this.kerusakan_material = kerusakan_material;
    }

    public String getTotal_nilai_rugi_kendaraan() {
        return total_nilai_rugi_kendaraan != null ? total_nilai_rugi_kendaraan : "";
    }

    public void setTotal_nilai_rugi_kendaraan(String total_nilai_rugi_kendaraan) {
        this.total_nilai_rugi_kendaraan = total_nilai_rugi_kendaraan;
    }

    public String getTotal_nilai_rugi_non_kendaraan() {
        return total_nilai_rugi_non_kendaraan;
    }

    public void setTotal_nilai_rugi_non_kendaraan(String total_nilai_rugi_non_kendaraan) {
        this.total_nilai_rugi_non_kendaraan = total_nilai_rugi_non_kendaraan;
    }

    public String getPenyebab() {
        return penyebab != null ? penyebab : "";
    }

    public void setPenyebab(String penyebab) {
        this.penyebab = penyebab;
    }

    public String getUraian_polisi() {
        return uraian_polisi != null ? uraian_polisi : "";
    }

    public void setUraian_polisi(String uraian_polisi) {
        this.uraian_polisi = uraian_polisi;
    }

    public int getReq_data() {
        return req_data;
    }

    public void setReq_data(int req_data) {
        this.req_data = req_data;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }
}
