package com.gandsoft.irsms.model.ResponseModel.UserInfo;

import com.gandsoft.irsms.model.ObjectModel.UserPoliceDataModel;

/**
 * Created by glenn on 1/25/18.
 */

public class GETUserInfoResponseModel {
    UserPoliceDataModel info_user;

    public UserPoliceDataModel getInfo_user() {
        return info_user;
    }

    public void setInfo_user(UserPoliceDataModel info_user) {
        this.info_user = info_user;
    }
}
