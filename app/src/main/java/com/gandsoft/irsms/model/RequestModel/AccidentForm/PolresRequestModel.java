package com.gandsoft.irsms.model.RequestModel.AccidentForm;

/**
 * Created by glenn on 1/25/18.
 */

public class PolresRequestModel {

    private String polda_id;

    public String getPolda_id() {
        return polda_id;
    }

    public void setPolda_id(String polda_id) {
        this.polda_id = polda_id;
    }

}
