package com.gandsoft.irsms.model.ResponseModel.Auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gandsoft.irsms.model.ObjectModel.UserDataModel;
import com.gandsoft.irsms.model.ResponseModel.BaseResponseModel;

/**
 * Created by glenn on 1/25/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponseModel extends BaseResponseModel {

    UserDataModel data_user;

    public UserDataModel getData_user() {
        return data_user;
    }

    public void setData_user(UserDataModel data_user) {
        this.data_user = data_user;
    }
}
