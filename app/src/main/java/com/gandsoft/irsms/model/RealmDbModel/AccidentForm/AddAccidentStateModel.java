package com.gandsoft.irsms.model.RealmDbModel.AccidentForm;

import java.io.Serializable;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by gleen on 08/02/18.
 */


public class AddAccidentStateModel extends RealmObject implements Serializable {

    //String no_laka;
//    String petugas_pelapor;
    String polres;
    RealmList<String> photoBase64;
    String tanggal_kejadian;
    String tanggal_dilaporkan;
    String jam_kejadian;
    String jam_dilaporkan;
    RealmList<Integer> informasi_khusus;
    String tipe_kecelakaan;
    Integer tipe_kecelakaan_id;
    Integer kondisi_cahaya;
    Integer cuaca;
    Integer kecelakaan_menonjol;
    RealmList<Double> gps;
    String nama_jalan;
    Double latitude;
    Double longitude;
    Integer fungsi_jalan;
    Integer kelas_jalan;
    Integer tipe_jalan;
    Integer bentuk_geometri;
    Integer kondisi_permukaan;
    Integer batas_kecepatan;
    Integer kemiringan_jalan;
    RealmList<Integer> pengaturan_simpang;
    Integer status_jalan;
    RealmList<Integer> kerusakan_material;
    String total_nilai_rugi_kendaraan;
    String total_nilai_rugi_non_kendaraan;
    Integer penyebab;
    Integer tipe_kecelakaan_mainid;
    Integer tipe_kecelakaan_posid;

    public void setPhotoBase64(RealmList<String> photoBase64) {
        this.photoBase64 = photoBase64;
    }

    public void setInformasi_khusus(RealmList<Integer> informasi_khusus) {
        this.informasi_khusus = informasi_khusus;
    }

    public void setPengaturan_simpang(RealmList<Integer> pengaturan_simpang) {
        this.pengaturan_simpang = pengaturan_simpang;
    }

    public void setKerusakan_material(RealmList<Integer> kerusakan_material) {
        this.kerusakan_material = kerusakan_material;
    }

    public String getPolres() {
        return polres != null ? polres : "-";
    }

    public void setPolres(String polres) {
        this.polres = polres;
    }


    public String[] getPhotoBase64() {
        if (photoBase64 != null) {
            return photoBase64.toArray(new String[photoBase64.size()]);
        } else {
            return null;
        }
    }

    public void setPhotoBase64(String[] photoBase64) {
        if (photoBase64 != null && photoBase64.length > 0) {
            this.photoBase64 = new RealmList<>();
            for (String item :
                    photoBase64) {
                this.photoBase64.add(item);
            }
        }
    }


    public String getTanggal_kejadian() {
        return tanggal_kejadian;
    }

    public void setTanggal_kejadian(String tanggal_kejadian) {
        this.tanggal_kejadian = tanggal_kejadian;
    }

    public String getTanggal_dilaporkan() {
        return tanggal_dilaporkan;
    }

    public void setTanggal_dilaporkan(String tanggal_dilaporkan) {
        this.tanggal_dilaporkan = tanggal_dilaporkan;
    }

    public String getJam_kejadian() {
        return jam_kejadian;
    }

    public void setJam_kejadian(String jam_kejadian) {
        this.jam_kejadian = jam_kejadian;
    }

    public String getJam_dilaporkan() {
        return jam_dilaporkan;
    }

    public void setJam_dilaporkan(String jam_dilaporkan) {
        this.jam_dilaporkan = jam_dilaporkan;
    }

    public String getTipe_kecelakaan() {
        return tipe_kecelakaan;
    }

    public void setTipe_kecelakaan(String tipe_kecelakaan) {
        this.tipe_kecelakaan = tipe_kecelakaan;
    }

    public Integer getTipe_kecelakaan_id() {
        return tipe_kecelakaan_id;
    }

    public void setTipe_kecelakaan_id(Integer tipe_kecelakaan_id) {
        this.tipe_kecelakaan_id = tipe_kecelakaan_id;
    }

    public Integer getKondisi_cahaya() {
        return kondisi_cahaya;
    }

    public void setKondisi_cahaya(Integer kondisi_cahaya) {
        this.kondisi_cahaya = kondisi_cahaya;
    }

    public Integer getCuaca() {
        return cuaca;
    }

    public void setCuaca(Integer cuaca) {
        this.cuaca = cuaca;
    }

    public Integer getKecelakaan_menonjol() {
        return kecelakaan_menonjol;
    }

    public void setKecelakaan_menonjol(Integer kecelakaan_menonjol) {
        this.kecelakaan_menonjol = kecelakaan_menonjol;
    }

    public Double[] getGps() {
        return gps.toArray(new Double[gps.size()]);
    }

    public void setGps(Double[] gps) {
        this.gps = new RealmList<>();
        for (Double item :
                gps) {
            this.gps.add(item);
        }
    }

    public void setGps(RealmList<Double> gps) {
        this.gps = gps;
    }

    public String getNama_jalan() {
        return nama_jalan;
    }

    public void setNama_jalan(String nama_jalan) {
        this.nama_jalan = nama_jalan;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getFungsi_jalan() {
        return fungsi_jalan;
    }

    public void setFungsi_jalan(Integer fungsi_jalan) {
        this.fungsi_jalan = fungsi_jalan;
    }

    public Integer getKelas_jalan() {
        return kelas_jalan;
    }

    public void setKelas_jalan(Integer kelas_jalan) {
        this.kelas_jalan = kelas_jalan;
    }

    public Integer getTipe_jalan() {
        return tipe_jalan;
    }

    public void setTipe_jalan(Integer tipe_jalan) {
        this.tipe_jalan = tipe_jalan;
    }

    public Integer getBentuk_geometri() {
        return bentuk_geometri;
    }

    public void setBentuk_geometri(Integer bentuk_geometri) {
        this.bentuk_geometri = bentuk_geometri;
    }

    public Integer getKondisi_permukaan() {
        return kondisi_permukaan;
    }

    public void setKondisi_permukaan(Integer kondisi_permukaan) {
        this.kondisi_permukaan = kondisi_permukaan;
    }

    public Integer getBatas_kecepatan() {
        return batas_kecepatan;
    }

    public void setBatas_kecepatan(Integer batas_kecepatan) {
        this.batas_kecepatan = batas_kecepatan;
    }

    public Integer getKemiringan_jalan() {
        return kemiringan_jalan;
    }

    public void setKemiringan_jalan(Integer kemiringan_jalan) {
        this.kemiringan_jalan = kemiringan_jalan;
    }

    public Integer[] getPengaturan_simpang() {
        if (pengaturan_simpang != null) {
            return pengaturan_simpang.toArray(new Integer[pengaturan_simpang.size()]);
        }
        return null;
    }

    public void setPengaturan_simpang(Integer[] pengaturan_simpang) {

        if (pengaturan_simpang != null && pengaturan_simpang.length > 0) {
            this.pengaturan_simpang = new RealmList<>();

            for (Integer item :
                    pengaturan_simpang) {
                this.pengaturan_simpang.add(item);
            }
        }else {
            this.pengaturan_simpang = new RealmList<>();
            for (Integer item : pengaturan_simpang) {
                this.pengaturan_simpang.remove(item);
            }
        }
    }

    public Integer[] getKerusakan_material() {
        if (kerusakan_material != null) {
            return kerusakan_material.toArray(new Integer[kerusakan_material.size()]);
        }
        return null;
    }

    public void setKerusakan_material(Integer[] kerusakan_material) {
        if (kerusakan_material != null && kerusakan_material.length > 0) {
            this.kerusakan_material = new RealmList<>();
            for (Integer item :
                    kerusakan_material) {
                this.kerusakan_material.add(item);
            }
        }else {
            this.kerusakan_material = new RealmList<>();
            for (Integer item : kerusakan_material) {
                this.kerusakan_material.remove(item);
            }
        }
    }

    public Integer[] getInformasi_khusus() {
        if (informasi_khusus != null) {
            return informasi_khusus.toArray(new Integer[informasi_khusus.size()]);
        }
        return null;
    }

    public void setInformasi_khusus(Integer[] informasi_khusus) {
        if (informasi_khusus != null && informasi_khusus.length > 0) {
            this.informasi_khusus = new RealmList<>();
            for (Integer item :
                    informasi_khusus) {
                this.informasi_khusus.add(item);
            }
        }else {
            this.informasi_khusus = new RealmList<>();
            for (Integer item : informasi_khusus) {
                this.informasi_khusus.remove(item);
            }
        }
    }

    public Integer getStatus_jalan() {
        return status_jalan;
    }

    public void setStatus_jalan(Integer status_jalan) {
        this.status_jalan = status_jalan;
    }

    public String getTotal_nilai_rugi_kendaraan() {
        return total_nilai_rugi_kendaraan;
    }

    public void setTotal_nilai_rugi_kendaraan(String total_nilai_rugi_kendaraan) {
        this.total_nilai_rugi_kendaraan = total_nilai_rugi_kendaraan;
    }

    public String getTotal_nilai_rugi_non_kendaraan() {
        return total_nilai_rugi_non_kendaraan;
    }

    public void setTotal_nilai_rugi_non_kendaraan(String total_nilai_rugi_non_kendaraan) {
        this.total_nilai_rugi_non_kendaraan = total_nilai_rugi_non_kendaraan;
    }

    public Integer getPenyebab() {
        return penyebab;
    }

    public void setPenyebab(Integer penyebab) {
        this.penyebab = penyebab;
    }

    public Integer getTipe_kecelakaan_mainid() {
        return tipe_kecelakaan_mainid;
    }

    public void setTipe_kecelakaan_mainid(Integer tipe_kecelakaan_mainid) {
        this.tipe_kecelakaan_mainid = tipe_kecelakaan_mainid;
    }

    public Integer getTipe_kecelakaan_posid() {
        return tipe_kecelakaan_posid;
    }

    public void setTipe_kecelakaan_posid(Integer tipe_kecelakaan_posid) {
        this.tipe_kecelakaan_posid = tipe_kecelakaan_posid;
    }
}
