package com.gandsoft.irsms.model.ResponseModel.VehicleForm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gandsoft.irsms.model.ResponseModel.BaseResponseModel;

/**
 * Created by glenn on 1/25/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class SetDriverResponseModel extends BaseResponseModel {

    private String vehicle_id;
    private String vehicle_type;
    private String first_name;
    private String last_name;
    private String no_tnkb;
    private String plate_no;
    private String umur;
    private String tingkat_luka;
    private String ditahan;

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getVehicle_type() {
        return vehicle_type;
    }

    public void setVehicle_type(String vehicle_type) {
        this.vehicle_type = vehicle_type;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getNo_tnkb() {
        return no_tnkb;
    }

    public void setNo_tnkb(String no_tnkb) {
        this.no_tnkb = no_tnkb;
    }

    public String getUmur() {
        return umur;
    }

    public void setUmur(String umur) {
        this.umur = umur;
    }

    public String getTingkat_luka() {
        return tingkat_luka;
    }

    public void setTingkat_luka(String tingkat_luka) {
        this.tingkat_luka = tingkat_luka;
    }

    public String getDitahan() {
        return ditahan;
    }

    public void setDitahan(String ditahan) {
        this.ditahan = ditahan;
    }

    public String getPlate_no() {
        return plate_no;
    }

    public void setPlate_no(String plate_no) {
        this.plate_no = plate_no;
    }
}
