package com.gandsoft.irsms.model.ObjectModel;

/**
 * Created by glenn on 1/25/18.
 */

public class UserPoliceDataModel extends UserDataModel {
    private String pangkat;
    private String phone_number;
    private String nrp;
    private String role;

    public String getPangkat() {
        return pangkat;
    }

    public void setPangkat(String pangkat) {
        this.pangkat = pangkat;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getNrp() {
        return nrp;
    }

    public void setNrp(String nrp) {
        this.nrp = nrp;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
