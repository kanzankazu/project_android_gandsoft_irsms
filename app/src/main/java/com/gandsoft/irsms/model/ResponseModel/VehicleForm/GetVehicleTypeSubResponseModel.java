package com.gandsoft.irsms.model.ResponseModel.VehicleForm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gandsoft.irsms.model.ObjectModel.CodedItemModel;

import java.util.List;

/**
 * Created by glenn on 1/25/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetVehicleTypeSubResponseModel {

    private String status;
    private String pic;
    private List<CodedItemModel> message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public List<CodedItemModel> getMessage() {
        return message;
    }

    public void setMessage(List<CodedItemModel> message) {
        this.message = message;
    }
}
