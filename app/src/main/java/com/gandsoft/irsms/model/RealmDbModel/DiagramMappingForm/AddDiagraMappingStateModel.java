package com.gandsoft.irsms.model.RealmDbModel.DiagramMappingForm;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Created by kanzan on 3/22/2018.
 */

public class AddDiagraMappingStateModel extends RealmObject implements Serializable {

    private Integer accident_id;
    private Integer accident_a;
    private Integer accident_b;
    private Integer accident_direction;
    private String noTnkb;

    public String getNoTnkb() {
        return noTnkb;
    }

    public void setNoTnkb(String noTnkb) {
        this.noTnkb = noTnkb;
    }

    public Integer getAccident_id() {
        return accident_id;
    }

    public void setAccident_id(Integer accident_id) {
        this.accident_id = accident_id;
    }

    public Integer getAccident_a() {
        return accident_a;
    }

    public void setAccident_a(Integer accident_a) {
        this.accident_a = accident_a;
    }

    public Integer getAccident_b() {
        return accident_b;
    }

    public void setAccident_b(Integer accident_b) {
        this.accident_b = accident_b;
    }

    public Integer getAccident_direction() {
        return accident_direction;
    }

    public void setAccident_direction(Integer accident_direction) {
        this.accident_direction = accident_direction;
    }
}
