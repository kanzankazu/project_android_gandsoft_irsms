package com.gandsoft.irsms.model.ResponseModel;

import java.io.Serializable;

import app.beelabs.com.codebase.base.response.BaseResponse;

/**
 * Created by glenn on 1/25/18.
 */

public class BaseResponseModel extends BaseResponse implements Serializable {

    private String message;
    private int total;
    private int version;

    private String[] car_default;
    private String[] car_relation;
    private String[] motor_default;
    private String[] motor_relation;

    private String[] safety_vehicle_car_default;
    private String[] safety_vehicle_car_relation;
    private String[] safety_vehicle_motor_default;
    private String[] safety_vehicle_motor_relation;

    private String[] safety_driver_car_default;
    private String[] safety_driver_car_relation;
    private String[] safety_driver_motor_default;
    private String[] safety_driver_motor_relation;

    private String[] safety_pass_car_default;
    private String[] safety_pass_car_relation;
    private String[] safety_pass_motor_default;
    private String[] safety_pass_motor_relation;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    /**/

    public String[] getCar_default() {
        return car_default;
    }

    public void setCar_default(String[] car_default) {
        this.car_default = car_default;
    }

    public String[] getCar_relation() {
        return car_relation;
    }

    public void setCar_relation(String[] car_relation) {
        this.car_relation = car_relation;
    }

    public String[] getMotor_default() {
        return motor_default;
    }

    public void setMotor_default(String[] motor_default) {
        this.motor_default = motor_default;
    }

    public String[] getMotor_relation() {
        return motor_relation;
    }

    public void setMotor_relation(String[] motor_relation) {
        this.motor_relation = motor_relation;
    }

    public String[] getSafety_vehicle_car_default() {
        return safety_vehicle_car_default;
    }

    public void setSafety_vehicle_car_default(String[] safety_vehicle_car_default) {
        this.safety_vehicle_car_default = safety_vehicle_car_default;
    }

    public String[] getSafety_vehicle_car_relation() {
        return safety_vehicle_car_relation;
    }

    public void setSafety_vehicle_car_relation(String[] safety_vehicle_car_relation) {
        this.safety_vehicle_car_relation = safety_vehicle_car_relation;
    }

    public String[] getSafety_vehicle_motor_default() {
        return safety_vehicle_motor_default;
    }

    public void setSafety_vehicle_motor_default(String[] safety_vehicle_motor_default) {
        this.safety_vehicle_motor_default = safety_vehicle_motor_default;
    }

    public String[] getSafety_vehicle_motor_relation() {
        return safety_vehicle_motor_relation;
    }

    public void setSafety_vehicle_motor_relation(String[] safety_vehicle_motor_relation) {
        this.safety_vehicle_motor_relation = safety_vehicle_motor_relation;
    }

    public String[] getSafety_driver_car_default() {
        return safety_driver_car_default;
    }

    public void setSafety_driver_car_default(String[] safety_driver_car_default) {
        this.safety_driver_car_default = safety_driver_car_default;
    }

    public String[] getSafety_driver_car_relation() {
        return safety_driver_car_relation;
    }

    public void setSafety_driver_car_relation(String[] safety_driver_car_relation) {
        this.safety_driver_car_relation = safety_driver_car_relation;
    }

    public String[] getSafety_driver_motor_default() {
        return safety_driver_motor_default;
    }

    public void setSafety_driver_motor_default(String[] safety_driver_motor_default) {
        this.safety_driver_motor_default = safety_driver_motor_default;
    }

    public String[] getSafety_driver_motor_relation() {
        return safety_driver_motor_relation;
    }

    public void setSafety_driver_motor_relation(String[] safety_driver_motor_relation) {
        this.safety_driver_motor_relation = safety_driver_motor_relation;
    }

    public String[] getSafety_pass_car_default() {
        return safety_pass_car_default;
    }

    public void setSafety_pass_car_default(String[] safety_pass_car_default) {
        this.safety_pass_car_default = safety_pass_car_default;
    }

    public String[] getSafety_pass_car_relation() {
        return safety_pass_car_relation;
    }

    public void setSafety_pass_car_relation(String[] safety_pass_car_relation) {
        this.safety_pass_car_relation = safety_pass_car_relation;
    }

    public String[] getSafety_pass_motor_default() {
        return safety_pass_motor_default;
    }

    public void setSafety_pass_motor_default(String[] safety_pass_motor_default) {
        this.safety_pass_motor_default = safety_pass_motor_default;
    }

    public String[] getSafety_pass_motor_relation() {
        return safety_pass_motor_relation;
    }

    public void setSafety_pass_motor_relation(String[] safety_pass_motor_relation) {
        this.safety_pass_motor_relation = safety_pass_motor_relation;
    }
}
