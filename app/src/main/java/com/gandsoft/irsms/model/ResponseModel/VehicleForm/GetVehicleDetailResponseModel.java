package com.gandsoft.irsms.model.ResponseModel.VehicleForm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gandsoft.irsms.model.ObjectModel.DetailModel.DriverDetailModel;
import com.gandsoft.irsms.model.ObjectModel.DetailModel.PassengerDetailModel;
import com.gandsoft.irsms.model.ObjectModel.DetailModel.PedestrianDetailModel;
import com.gandsoft.irsms.model.ResponseModel.BaseResponseModel;

import java.util.List;

/**
 * Created by glenn on 1/25/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetVehicleDetailResponseModel extends BaseResponseModel {

    private List<DriverDetailModel> driver_detail;
    private List<PassengerDetailModel> passenger_detail;
    private List<PedestrianDetailModel> pedestrian_detail;

    public List<DriverDetailModel> getDriver_detail() {
        return driver_detail;
    }

    public void setDriver_detail(List<DriverDetailModel> driver_detail) {
        this.driver_detail = driver_detail;
    }

    public List<PassengerDetailModel> getPassenger_detail() {
        return passenger_detail;
    }

    public void setPassenger_detail(List<PassengerDetailModel> passenger_detail) {
        this.passenger_detail = passenger_detail;
    }

    public List<PedestrianDetailModel> getPedestrian_detail() {
        return pedestrian_detail;
    }

    public void setPedestrian_detail(List<PedestrianDetailModel> pedestrian_detail) {
        this.pedestrian_detail = pedestrian_detail;
    }

}
