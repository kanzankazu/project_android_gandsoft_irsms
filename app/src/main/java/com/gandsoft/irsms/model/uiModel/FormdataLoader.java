package com.gandsoft.irsms.model.uiModel;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.gandsoft.irsms.Database.DatabaseHelper;
import com.gandsoft.irsms.ISeasonConfig;
import com.gandsoft.irsms.IrsmsApp;
import com.gandsoft.irsms.Presenter.SeasonManager.SessionManager;
import com.gandsoft.irsms.model.ObjectModel.CodedItemModel;
import com.gandsoft.irsms.model.ResponseModel.FormDataLoader.FormDataResponseModel;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by glenn on 2/1/18.
 */

public class FormdataLoader {

    private static List<CodedItemModel> listedModels = new ArrayList<>();

    private static List<String> idModel = new ArrayList<>();

    private static SQLiteDatabase locationCodex;

    private static Boolean initiated = false;

    private static ViewModel arah = new ViewModel();
    private static ViewModel batas_kecepatan = new ViewModel();
    private static ViewModel kemiringan_jalan = new ViewModel();
    private static ViewModel pengaturan_simpang = new ViewModel();
    private static ViewModel status_jalan = new ViewModel();
    private static ViewModel kondisi_permukaan = new ViewModel();
    private static ViewModel bentuk_geometri = new ViewModel();
    private static ViewModel tipe_jalan = new ViewModel();
    private static ViewModel kelas_jalan = new ViewModel();
    private static ViewModel fungsi_jalan = new ViewModel();
    private static ViewModel kecelakaan_menonjol = new ViewModel();
    private static ViewModel titik_acuan = new ViewModel();
    private static ViewModel cuaca = new ViewModel();
    private static ViewModel kondisi_cahaya = new ViewModel();
    private static ViewModel informasi_khusus = new ViewModel();
    private static ViewModel tipe_kecelakaan = new ViewModel();
    private static ViewModel license_class_id = new ViewModel();
    private static ViewModel injury_id = new ViewModel();
    private static ViewModel alcohol_id = new ViewModel();
    private static ViewModel arrested_id = new ViewModel();
    private static ViewModel suspected_id = new ViewModel();
    private static ViewModel plate_color_id = new ViewModel();
    private static ViewModel vehicle_type_id = new ViewModel();
    private static ViewModel vehicle_design_id = new ViewModel();
    private static ViewModel accident_before_id = new ViewModel();
    private static ViewModel accident_after_id = new ViewModel();
    private static ViewModel accident_end_id = new ViewModel();
    private static ViewModel special_id = new ViewModel();
    private static ViewModel vehicle_brand_id = new ViewModel();
    private static ViewModel vehicle_brand_motor_id = new ViewModel();
    private static ViewModel vehicle_color_id = new ViewModel();
    private static ViewModel identity_type_id = new ViewModel();
    private static ViewModel religion_id = new ViewModel();
    private static ViewModel profession_id = new ViewModel();
    private static ViewModel education_id = new ViewModel();
    private static ViewModel gender_id = new ViewModel();
    private static ViewModel safety = new ViewModel();
    private static ViewModel passenger_safety = new ViewModel();
    private static ViewModel safety_device = new ViewModel();
    private static ViewModel nationality_id = new ViewModel();
    private static ViewModel titik_kerusakan = new ViewModel();
    private static ViewModel passenger_movement_id = new ViewModel();
    private static ViewModel pedestrian_movement_id = new ViewModel();
    private static ViewModel kerusakan = new ViewModel();
    private static ViewModel disita = new ViewModel();
    private static ViewModel driver_behavior = new ViewModel();
    private static ViewModel driver_law = new ViewModel();
    private static ViewModel penyebab = new ViewModel();
    private static ViewModel dead_spot_id = new ViewModel();
    private static ViewModel pemudik = new ViewModel();
    private static ViewModel kermat = new ViewModel();
    /*new*/
    private static ViewModel propinsi_stnk = new ViewModel();
    private static ViewModel kabupaten_stnk = new ViewModel();
    private static ViewModel kecamatan_stnk = new ViewModel();
    private static ViewModel kelurahan_stnk = new ViewModel();

    private static ViewModel province = new ViewModel();
    private static ViewModel kabupaten = new ViewModel();
    private static ViewModel kecamatan = new ViewModel();
    private static ViewModel kelurahan = new ViewModel();

    private static ArrayList<String> id_informasi_khusus_lv = new ArrayList<>();
    private static ArrayList<String> id_kondisi_cahaya_lv = new ArrayList<>();
    private static ArrayList<String> id_cuaca_lv = new ArrayList<>();
    private static ArrayList<String> id_fungsi_jalan_lv = new ArrayList<>();
    private static ArrayList<String> id_kelas_jalan_lv = new ArrayList<>();
    private static ArrayList<String> id_tipe_jalan_lv = new ArrayList<>();
    private static ArrayList<String> id_bentuk_geometri_lv = new ArrayList<>();
    private static ArrayList<String> id_kondisi_permukaan_lv = new ArrayList<>();
    private static ArrayList<String> id_batas_kecepatan_lv = new ArrayList<>();
    private static ArrayList<String> id_kemiringan_jalan_lv = new ArrayList<>();
    private static ArrayList<String> id_pengaturan_simpang_lv = new ArrayList<>();
    private static ArrayList<String> id_status_jalan_lv = new ArrayList<>();
    private static ArrayList<String> id_penyebab_lv = new ArrayList<>();
    private static ArrayList<String> id_kermat_lv = new ArrayList<>();
    private static ArrayList<String> id_gender_id_lv = new ArrayList<>();
    private static ArrayList<String> id_religion_id_lv = new ArrayList<>();
    private static ArrayList<String> id_education_id_lv = new ArrayList<>();
    private static ArrayList<String> id_identity_type_id_lv = new ArrayList<>();
    private static ArrayList<String> id_identity_type_max_lenght_lv = new ArrayList<>();
    private static ArrayList<String> id_nationality_id_lv = new ArrayList<>();
    private static ArrayList<String> id_injury_id_lv = new ArrayList<>();
    private static ArrayList<String> id_profession_id_lv = new ArrayList<>();
    private static ArrayList<String> id_passenger_safety_lv = new ArrayList<>();
    private static ArrayList<String> id_disita_lv = new ArrayList<>();
    private static ArrayList<String> id_vehicle_type_id_lv = new ArrayList<>();
    private static ArrayList<String> id_vehicle_design_id_lv = new ArrayList<>();
    private static ArrayList<String> id_safety_device_lv = new ArrayList<>();
    private static ArrayList<String> id_kerusakan_lv = new ArrayList<>();
    private static ArrayList<String> id_accident_before_id_lv = new ArrayList<>();
    private static ArrayList<String> id_accident_after_id_lv = new ArrayList<>();
    private static ArrayList<String> id_titik_kerusakan_lv = new ArrayList<>();
    private static ArrayList<String> id_special_id_lv = new ArrayList<>();
    private static ArrayList<String> id_accident_end_id_lv = new ArrayList<>();
    private static ArrayList<String> id_pemudik_lv = new ArrayList<>();
    private static ArrayList<String> id_safety_lv = new ArrayList<>();
    private static ArrayList<String> id_plate_color_id_lv = new ArrayList<>();
    private static ArrayList<String> id_vehicle_brand_id_lv = new ArrayList<>();
    private static ArrayList<String> id_vehicle_brand_motor_id_lv = new ArrayList<>();
    private static ArrayList<String> id_vehicle_color_id_lv = new ArrayList<>();
    private static ArrayList<String> id_license_class_id_lv = new ArrayList<>();
    private static ArrayList<String> id_passanger_movement_id_lv = new ArrayList<>();
    private static ArrayList<String> id_pedestrian_movement_id_lv = new ArrayList<>();
    private static ArrayList<String> id_driver_behavior_lv = new ArrayList<>();
    private static ArrayList<String> id_driver_law_lv = new ArrayList<>();
    private static ArrayList<String> id_arah_lv = new ArrayList<>();

    public FormdataLoader() {

    }


    public static void initFormData() {
        if (!initiated) {
            generateStorageObject();
            generateUiModel();
            generateYaTidakItems();
            generateStaticDBitems();
            generateTheRest();
            initiated = true;
        } else {
            Log.w("Bitte Warnung", "form data already initiated");
        }
    }

    private static void generateStorageObject() {
        kelas_jalan = new ViewModel();
    }

    private static void generateDbObject() {
        DatabaseHelper toyong = new DatabaseHelper(IrsmsApp.getContext());
        try {
            toyong.createDatabase();
            locationCodex = toyong.openDatabase();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Cursor querry(String querry) {
        generateDbObject();
        return locationCodex.rawQuery(querry, null);
    }

    private static Cursor querry(String querry, String[] x) {
        generateDbObject();
        return locationCodex.rawQuery(querry, x);
    }

    private static void generateYaTidakItems() {

        ViewModel yaItem = new ViewModel();
        yaItem.setViewType(4);
        yaItem.setName(IrsmsApp.getContext().getString(com.gandsoft.irsms.R.string.form_positive));
        yaItem.setTextContent(IrsmsApp.getContext().getString(com.gandsoft.irsms.R.string.form_positive));
        yaItem.setViewID("G0101");
        yaItem.setItemResourceID(com.gandsoft.irsms.R.layout.list_widget_radiobutton);

        ViewModel tidakItem = new ViewModel();
        tidakItem.setViewType(4);
        tidakItem.setName(IrsmsApp.getContext().getString(com.gandsoft.irsms.R.string.form_negative));
        tidakItem.setTextContent(IrsmsApp.getContext().getString(com.gandsoft.irsms.R.string.form_negative));
        tidakItem.setViewID("G0102");
        tidakItem.setItemResourceID(com.gandsoft.irsms.R.layout.list_widget_radiobutton);

        List<ViewModel> yatidak = new ArrayList<>();
        yatidak.add(yaItem);
        yatidak.add(tidakItem);

        kecelakaan_menonjol.setName("kecelakaan menonjol");
        kecelakaan_menonjol.setViewType(5);
        kecelakaan_menonjol.setViewID("hund1");
        kecelakaan_menonjol.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        kecelakaan_menonjol.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        kecelakaan_menonjol.setViewModelGroup(yatidak);

        alcohol_id.setName("Di Pengaruhi Obat/Alkohol");
        alcohol_id.setViewType(5);
        alcohol_id.setViewID("hund2");
        alcohol_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        alcohol_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        alcohol_id.setViewModelGroup(yatidak);

        arrested_id.setName("Di tahan?");
        arrested_id.setViewType(5);
        arrested_id.setViewID("hund3");
        arrested_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        arrested_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        arrested_id.setViewModelGroup(yatidak);

        suspected_id.setName("Pelaku di Duga Sementara?");
        suspected_id.setViewType(5);
        suspected_id.setViewID("hund4");
        suspected_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        suspected_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        suspected_id.setViewModelGroup(yatidak);
    }

    private static void generateStaticDBitems() {
        Cursor pointer;

        province.setName("Provinsi");
        province.setViewType(7);//spinner
        province.setViewID("Provinsi");
        province.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        province.setItemResourceID(com.gandsoft.irsms.R.layout.component_spinner);
        List<ViewModel> province_lv = new ArrayList<>();

        pointer = querry("SELECT DISTINCT province FROM ms_subdistrict");

        try {
            CodedItemModel temp;
            while (pointer.moveToNext()) {
                temp = new CodedItemModel();
                temp.setName(pointer.getString(0));
                temp.setId(pointer.getString(0));
                temp.setGrp_id("Provinsi");
                province_lv.add(transformToViewModel(temp));
            }
        } finally {
            pointer.close();
        }
        province.setViewModelGroup(province_lv);

        kabupaten.setName("kabupaten");
        kabupaten.setViewType(7);//spinner
        kabupaten.setViewID("kabupaten");
        kabupaten.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        kabupaten.setItemResourceID(com.gandsoft.irsms.R.layout.component_spinner);

        kecamatan.setName("kecamatan");
        kecamatan.setViewType(7);//spinner
        kecamatan.setViewID("kecamatan");
        kecamatan.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        kecamatan.setItemResourceID(com.gandsoft.irsms.R.layout.component_spinner);

        kelurahan.setName("kelurahan");
        kelurahan.setViewType(7);//spinner
        kelurahan.setViewID("kelurahan");
        kelurahan.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        kelurahan.setItemResourceID(com.gandsoft.irsms.R.layout.component_spinner);
    }

    /**
     * @param selector choose which type of picked item
     *                 0 is city, 1 district , 2 subdistrict, else null
     * @param querry
     */
    public static void generateDynamicDBitems(int selector, String querry) {
        Cursor pointer;
        List<ViewModel> itemContainer_lv;
        CodedItemModel temp;
        String[] params;
        switch (selector) {
            case 0:
                itemContainer_lv = new ArrayList<>();
                params = new String[]{querry};
                pointer = querry("SELECT DISTINCT city FROM ms_subdistrict WHERE province = ?", params);
                try {
                    while (pointer.moveToNext()) {
                        temp = new CodedItemModel();
                        temp.setName(pointer.getString(0));
                        temp.setId(pointer.getString(0));
                        temp.setGrp_id("kabupaten");
                        itemContainer_lv.add(transformToViewModel(temp));
                    }
                } finally {
                    pointer.close();
                }
                kabupaten.setViewModelGroup(itemContainer_lv);
                break;
            case 1:
                itemContainer_lv = new ArrayList<>();
                params = new String[]{querry};
                pointer = querry("SELECT DISTINCT district FROM ms_subdistrict WHERE city = ?", params);
                try {
                    while (pointer.moveToNext()) {
                        temp = new CodedItemModel();
                        temp.setName(pointer.getString(0));
                        temp.setId(pointer.getString(0));
                        temp.setGrp_id("kecamatan");
                        itemContainer_lv.add(transformToViewModel(temp));
                    }
                } finally {
                    pointer.close();
                }
                kecamatan.setViewModelGroup(itemContainer_lv);
                break;
            case 2:
                itemContainer_lv = new ArrayList<>();
                params = new String[]{querry};
                pointer = querry("SELECT DISTINCT subdistrict FROM ms_subdistrict WHERE district = ?", params);
                try {
                    while (pointer.moveToNext()) {
                        temp = new CodedItemModel();
                        temp.setName(pointer.getString(0));
                        temp.setId(pointer.getString(0));
                        temp.setGrp_id("kelurahan");
                        itemContainer_lv.add(transformToViewModel(temp));
                    }
                    kelurahan.setViewModelGroup(itemContainer_lv);
                } finally {
                    pointer.close();
                }
                break;
            default:
                break;
        }


    }

    private static void generateTheRest() {
        /*SPINNER7*/
        religion_id.setName("Agama");
        religion_id.setViewType(7);
        religion_id.setViewID("G03");
        religion_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        religion_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_spinner);
        List<ViewModel> religion_id_lv = new ArrayList<>();

        vehicle_type_id.setName("Jenis Kendaraan");
        vehicle_type_id.setViewType(7);
        vehicle_type_id.setViewID("V02A");
        vehicle_type_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        vehicle_type_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_spinner);
        List<ViewModel> vehicle_type_id_lv = new ArrayList<>();

        plate_color_id.setViewType(7);
        plate_color_id.setViewID("V01B");
        plate_color_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        plate_color_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_spinner);
        List<ViewModel> plate_color_id_lv = new ArrayList<>();

        vehicle_brand_id.setViewType(7);
        vehicle_brand_id.setViewID("V02AC");
        vehicle_brand_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        vehicle_brand_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_spinner);
        List<ViewModel> vehicle_brand_id_lv = new ArrayList<>();

        vehicle_brand_motor_id.setViewType(7);
        vehicle_brand_motor_id.setViewID("V02AD");
        vehicle_brand_motor_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        vehicle_brand_motor_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_spinner);
        List<ViewModel> vehicle_brand_motor_id_lv = new ArrayList<>();

        vehicle_color_id.setViewType(7);
        vehicle_color_id.setViewID("V02ACC");
        vehicle_color_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        vehicle_color_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_spinner);
        List<ViewModel> vehicle_color_id_lv = new ArrayList<>();

        license_class_id.setViewType(7);
        license_class_id.setViewID("D08");
        license_class_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        license_class_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_spinner);
        List<ViewModel> license_class_id_lv = new ArrayList<>();

        identity_type_id.setViewType(7);
        identity_type_id.setViewID("G04");
        identity_type_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        identity_type_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_spinner);
        List<ViewModel> identity_type_id_lv = new ArrayList<>();

        nationality_id.setViewType(7);
        nationality_id.setViewID("G05");
        nationality_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        nationality_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_spinner);
        List<ViewModel> nationality_id_lv = new ArrayList<>();


        profession_id.setViewType(7);
        profession_id.setViewID("G07");
        profession_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        profession_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_spinner);
        List<ViewModel> profession_id_lv = new ArrayList<>();

        education_id.setViewType(7);
        education_id.setViewID("G08");
        education_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        education_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_spinner);
        List<ViewModel> education_id_lv = new ArrayList<>();

        /*RADIO5*/
        arah.setViewType(5);
        arah.setViewID("G09");
        arah.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        arah.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> arah_lv = new ArrayList<>();

        kondisi_cahaya.setName("Kondisi Cahaya");
        kondisi_cahaya.setViewType(5);
        kondisi_cahaya.setViewID("A08");
        kondisi_cahaya.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        kondisi_cahaya.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> kondisi_cahaya_lv = new ArrayList<>();

        cuaca.setName("Cuaca");
        cuaca.setViewType(5);
        cuaca.setViewID("A09");
        cuaca.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        cuaca.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> cuaca_lv = new ArrayList<>();

        fungsi_jalan.setName("Fungsi Jalan");
        fungsi_jalan.setViewType(5);
        fungsi_jalan.setViewID("R02");
        fungsi_jalan.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        fungsi_jalan.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> fungsi_jalan_lv = new ArrayList<>();

        kelas_jalan.setName("Kelas Jalan");
        kelas_jalan.setViewType(5);
        kelas_jalan.setViewID("R03");
        kelas_jalan.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        kelas_jalan.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> kelas_jalan_lv = new ArrayList<>();

        tipe_jalan.setName("Tipe Jalan");
        tipe_jalan.setViewType(5);
        tipe_jalan.setViewID("R04");
        tipe_jalan.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        tipe_jalan.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> tipe_jalan_lv = new ArrayList<>();

        bentuk_geometri.setName("Bentuk Geometri");
        bentuk_geometri.setViewType(5);
        bentuk_geometri.setViewID("R05");
        bentuk_geometri.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        bentuk_geometri.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> bentuk_geometri_lv = new ArrayList<>();

        kondisi_permukaan.setName("Kondisi Permukaan Jalan");
        kondisi_permukaan.setViewType(5);
        kondisi_permukaan.setViewID("R06");
        kondisi_permukaan.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        kondisi_permukaan.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> kondisi_permukaan_lv = new ArrayList<>();

        batas_kecepatan.setName("Batas Kecepatan di Lokasi");
        batas_kecepatan.setViewType(5);
        batas_kecepatan.setViewID("R07");
        batas_kecepatan.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        batas_kecepatan.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> batas_kecepatan_lv = new ArrayList<>();

        kemiringan_jalan.setName("Kemiringan Jalan");
        kemiringan_jalan.setViewType(5);
        kemiringan_jalan.setViewID("R08");
        kemiringan_jalan.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        kemiringan_jalan.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> kemiringan_jalan_lv = new ArrayList<>();

        pengaturan_simpang.setName("Pengaturan Simpang");
        pengaturan_simpang.setViewType(3);
        pengaturan_simpang.setViewID("R09");
        pengaturan_simpang.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        pengaturan_simpang.setItemResourceID(com.gandsoft.irsms.R.layout.component_checkboxgroup);
        List<ViewModel> pengaturan_simpang_lv = new ArrayList<>();

        status_jalan.setName("Status Jalan");
        status_jalan.setViewType(5);
        status_jalan.setViewID("R010");
        status_jalan.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        status_jalan.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> status_jalan_lv = new ArrayList<>();

        penyebab.setName("Faktor Utama Penyebab Laka");
        penyebab.setViewType(5);
        penyebab.setViewID("N01");
        penyebab.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        penyebab.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> penyebab_lv = new ArrayList<>();

        kermat.setName("Kerusakan Material / Infrastruktur");
        kermat.setViewType(3);
        kermat.setViewID("PRP1");
        kermat.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        kermat.setItemResourceID(com.gandsoft.irsms.R.layout.component_checkboxgroup);
        List<ViewModel> kermat_lv = new ArrayList<>();


        injury_id.setViewType(5);//radiobutton
        injury_id.setViewID("G06");
        injury_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        injury_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> injury_id_lv = new ArrayList<>();

        passenger_safety.setViewType(3);
        passenger_safety.setViewID("PS10");
        passenger_safety.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        passenger_safety.setItemResourceID(com.gandsoft.irsms.R.layout.component_checkboxgroup);
        List<ViewModel> passenger_safety_lv = new ArrayList<>();

        passenger_movement_id.setViewType(5);
        passenger_movement_id.setViewID("PS13");
        passenger_movement_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        passenger_movement_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> passanger_movement_id_lv = new ArrayList<>();

     /*KEndaraan*/
        pedestrian_movement_id.setViewType(5);
        pedestrian_movement_id.setViewID("PD10");
        pedestrian_movement_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        pedestrian_movement_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> pedestrian_movement_id_lv = new ArrayList<>();

        vehicle_design_id.setViewType(5);
        vehicle_design_id.setViewID("V03");
        vehicle_design_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        vehicle_design_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> vehicle_design_id_lv = new ArrayList<>();

        accident_before_id.setViewType(5);
        accident_before_id.setViewID("V06A");
        accident_before_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        accident_before_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> accident_before_id_lv = new ArrayList<>();

        accident_after_id.setViewType(5);
        accident_after_id.setViewID("V06B");
        accident_after_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        accident_after_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> accident_after_id_lv = new ArrayList<>();

        special_id.setViewType(5);
        special_id.setViewID("V09");
        special_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        special_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> special_id_lv = new ArrayList<>();

        accident_end_id.setViewType(5);
        accident_end_id.setViewID("V08");
        accident_end_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        accident_end_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> accident_end_id_lv = new ArrayList<>();

        pemudik.setViewType(5);
        pemudik.setViewID("P01");
        pemudik.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        pemudik.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> pemudik_lv = new ArrayList<>();

        /*CHECKBOX3*/
        driver_law.setViewType(3);
        driver_law.setViewID("D13");
        driver_law.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        driver_law.setItemResourceID(com.gandsoft.irsms.R.layout.component_checkboxgroup);
        List<ViewModel> driver_law_lv = new ArrayList<>();

        informasi_khusus.setName("Informasi Khusus");
        informasi_khusus.setViewType(3);
        informasi_khusus.setViewID("A06");
        informasi_khusus.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        informasi_khusus.setItemResourceID(com.gandsoft.irsms.R.layout.component_checkboxgroup);
        List<ViewModel> informasi_khusus_lv = new ArrayList<>();

        gender_id.setViewType(5);
        gender_id.setViewID("G02");
        gender_id.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        gender_id.setItemResourceID(com.gandsoft.irsms.R.layout.component_radiogroup);
        List<ViewModel> gender_id_lv = new ArrayList<>();

        safety_device.setViewType(3);
        safety_device.setViewID("V04");
        safety_device.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        safety_device.setItemResourceID(com.gandsoft.irsms.R.layout.component_checkboxgroup);
        List<ViewModel> safety_device_lv = new ArrayList<>();

        kerusakan.setViewType(3);
        kerusakan.setViewID("V05");
        kerusakan.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        kerusakan.setItemResourceID(com.gandsoft.irsms.R.layout.component_checkboxgroup);
        List<ViewModel> kerusakan_lv = new ArrayList<>();


        titik_kerusakan.setViewType(3);
        titik_kerusakan.setViewID("V07");
        titik_kerusakan.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        titik_kerusakan.setItemResourceID(com.gandsoft.irsms.R.layout.component_checkboxgroup);
        List<ViewModel> titik_kerusakan_lv = new ArrayList<>();

        disita.setViewType(3);
        disita.setViewID("V01A");
        disita.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        disita.setItemResourceID(com.gandsoft.irsms.R.layout.component_checkboxgroup);
        List<ViewModel> disita_lv = new ArrayList<>();

        safety.setViewType(3);
        safety.setViewID("D10");
        safety.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        safety.setItemResourceID(com.gandsoft.irsms.R.layout.component_checkboxgroup);
        List<ViewModel> safety_lv = new ArrayList<>();


        driver_behavior.setViewType(3);
        driver_behavior.setViewID("D12");
        driver_behavior.setItemContainerID(com.gandsoft.irsms.R.layout.component_primary_container);
        driver_behavior.setItemResourceID(com.gandsoft.irsms.R.layout.component_checkboxgroup);
        List<ViewModel> driver_behavior_lv = new ArrayList<>();

        for (CodedItemModel model : listedModels) {
            idModel.add(model.getId());
            switch (model.getGrp_id()) {
                /*DataLaka*/
                case "A06":
                    informasi_khusus_lv.add(transformToViewModel(model, false));
                    id_informasi_khusus_lv.add(model.getId());
                    break;
                case "A08":
                    kondisi_cahaya_lv.add(transformToViewModel(model, true));
                    id_kondisi_cahaya_lv.add(model.getId());
                    break;
                case "A09":
                    cuaca_lv.add(transformToViewModel(model, true));
                    id_cuaca_lv.add(model.getId());
                    break;
                case "R02":
                    fungsi_jalan_lv.add(transformToViewModel(model, true));
                    id_fungsi_jalan_lv.add(model.getId());
                    break;
                case "R03":
                    kelas_jalan_lv.add(transformToViewModel(model, true));
                    id_kelas_jalan_lv.add(model.getId());
                    break;
                case "R04":
                    tipe_jalan_lv.add(transformToViewModel(model, true));
                    id_tipe_jalan_lv.add(model.getId());
                    break;
                case "R05":
                    bentuk_geometri_lv.add(transformToViewModel(model, true));
                    id_bentuk_geometri_lv.add(model.getId());
                    break;
                case "R06":
                    kondisi_permukaan_lv.add(transformToViewModel(model, true));
                    id_kondisi_permukaan_lv.add(model.getId());
                    break;
                case "R07":
                    batas_kecepatan_lv.add(transformToViewModel(model, true));
                    id_batas_kecepatan_lv.add(model.getId());
                    break;
                case "R08":
                    kemiringan_jalan_lv.add(transformToViewModel(model, true));
                    id_kemiringan_jalan_lv.add(model.getId());
                    break;
                case "R09":
                    pengaturan_simpang_lv.add(transformToViewModel(model, false));
                    id_pengaturan_simpang_lv.add(model.getId());
                    break;
                case "R10":
                    status_jalan_lv.add(transformToViewModel(model, true));
                    id_status_jalan_lv.add(model.getId());
                    break;
                case "N01":
                    penyebab_lv.add(transformToViewModel(model, true));
                    id_penyebab_lv.add(model.getId());
                    break;
                case "PRP1":
                    kermat_lv.add(transformToViewModel(model, false));
                    id_kermat_lv.add(model.getId());
                    break;
                case "G02":
                    gender_id_lv.add(transformToViewModel(model, true));
                    id_gender_id_lv.add(model.getId());
                    break;
                case "G03":
                    religion_id_lv.add(transformToViewModel(model));
                    id_religion_id_lv.add(model.getId());
                    break;
                case "G08":
                    education_id_lv.add(transformToViewModel(model));
                    id_education_id_lv.add(model.getId());
                    break;
                case "G04":
                    identity_type_id_lv.add(transformToViewModel(model));
                    id_identity_type_id_lv.add(model.getId());
                    id_identity_type_max_lenght_lv.add(model.getMax_length());
                    break;
                case "G05":
                    nationality_id_lv.add(transformToViewModel(model));
                    id_nationality_id_lv.add(model.getId());
                    break;
                case "G06":
                    injury_id_lv.add(transformToViewModel(model, true));
                    id_injury_id_lv.add(model.getId());
                    break;
                case "G07":
                    profession_id_lv.add(transformToViewModel(model));
                    id_profession_id_lv.add(model.getId());
                    break;
                case "PS10":
                    passenger_safety_lv.add(transformToViewModel(model, false));
                    id_passenger_safety_lv.add(model.getId());
                    break;
                /*kendaraan*/
                case "V01A":
                    disita_lv.add(transformToViewModel(model, false));
                    id_disita_lv.add(model.getId());
                    break;
                case "V02A":
                    vehicle_type_id_lv.add(transformToViewModel(model));
                    id_vehicle_type_id_lv.add(model.getId());
                    break;
                case "V03":
                    vehicle_design_id_lv.add(transformToViewModel(model, true));
                    id_vehicle_design_id_lv.add(model.getId());
                    break;
                case "V04":
                    safety_device_lv.add(transformToViewModel(model, false));
                    id_safety_device_lv.add(model.getId());
                    break;
                case "V05":
                    kerusakan_lv.add(transformToViewModel(model, false));
                    id_kerusakan_lv.add(model.getId());
                    break;
                case "V06A":
                    accident_before_id_lv.add(transformToViewModel(model, true));
                    id_accident_before_id_lv.add(model.getId());
                    break;
                case "V06B":
                    accident_after_id_lv.add(transformToViewModel(model, true));
                    id_accident_after_id_lv.add(model.getId());
                    break;
                case "V07":
                    titik_kerusakan_lv.add(transformToViewModel(model, false));
                    id_titik_kerusakan_lv.add(model.getId());
                    break;
                case "V09":
                    special_id_lv.add(transformToViewModel(model, true));
                    id_special_id_lv.add(model.getId());
                    break;
                case "V08":
                    accident_end_id_lv.add(transformToViewModel(model, true));
                    id_accident_end_id_lv.add(model.getId());
                    break;
                case "P01":
                    pemudik_lv.add(transformToViewModel(model, true));
                    id_pemudik_lv.add(model.getId());
                    break;
                case "D10":
                    safety_lv.add(transformToViewModel(model, false));
                    id_safety_lv.add(model.getId());
                    break;
                case "V01B":
                    plate_color_id_lv.add(transformToViewModel(model));
                    id_plate_color_id_lv.add(model.getId());
                    break;
                case "V02AC":
                    vehicle_brand_id_lv.add(transformToViewModel(model));
                    id_vehicle_brand_id_lv.add(model.getId());
                    break;
                case "V02AD":
                    vehicle_brand_motor_id_lv.add(transformToViewModel(model));
                    id_vehicle_brand_motor_id_lv.add(model.getId());
                    break;
                case "V02ACC":
                    vehicle_color_id_lv.add(transformToViewModel(model));
                    id_vehicle_color_id_lv.add(model.getId());
                    break;
                case "D08":
                    license_class_id_lv.add(transformToViewModel(model));
                    id_license_class_id_lv.add(model.getId());
                    break;
                case "PS13":
                    passanger_movement_id_lv.add(transformToViewModel(model, true));
                    id_passanger_movement_id_lv.add(model.getId());
                    break;
                case "PD10":
                    pedestrian_movement_id_lv.add(transformToViewModel(model, true));
                    id_pedestrian_movement_id_lv.add(model.getId());
                    break;
                case "D12":
                    driver_behavior_lv.add(transformToViewModel(model, false));
                    id_driver_behavior_lv.add(model.getId());
                    break;
                case "D13":
                    driver_law_lv.add(transformToViewModel(model, false));
                    id_driver_law_lv.add(model.getId());
                    break;
                case "G3":
                    arah_lv.add(transformToViewModel(model, true));
                    id_arah_lv.add(model.getId());
                    break;
            }
            /*SP*/
            religion_id.setViewModelGroup(religion_id_lv);
            plate_color_id.setViewModelGroup(plate_color_id_lv);
            vehicle_brand_id.setViewModelGroup(vehicle_brand_id_lv);
            vehicle_brand_motor_id.setViewModelGroup(vehicle_brand_motor_id_lv);
            vehicle_color_id.setViewModelGroup(vehicle_color_id_lv);
            license_class_id.setViewModelGroup(license_class_id_lv);
            vehicle_type_id.setViewModelGroup(vehicle_type_id_lv);
            identity_type_id.setViewModelGroup(identity_type_id_lv);
            nationality_id.setViewModelGroup(nationality_id_lv);
            profession_id.setViewModelGroup(profession_id_lv);
            education_id.setViewModelGroup(education_id_lv);

            /*RB*/
            arah.setViewModelGroup(arah_lv);
            driver_behavior.setViewModelGroup(driver_behavior_lv);
            kondisi_cahaya.setViewModelGroup(kondisi_cahaya_lv);
            cuaca.setViewModelGroup(cuaca_lv);
            fungsi_jalan.setViewModelGroup(fungsi_jalan_lv);
            kelas_jalan.setViewModelGroup(kelas_jalan_lv);
            tipe_jalan.setViewModelGroup(tipe_jalan_lv);
            bentuk_geometri.setViewModelGroup(bentuk_geometri_lv);
            kondisi_permukaan.setViewModelGroup(kondisi_permukaan_lv);
            batas_kecepatan.setViewModelGroup(batas_kecepatan_lv);
            kemiringan_jalan.setViewModelGroup(kemiringan_jalan_lv);
            pengaturan_simpang.setViewModelGroup(pengaturan_simpang_lv);
            status_jalan.setViewModelGroup(status_jalan_lv);
            penyebab.setViewModelGroup(penyebab_lv);
            kermat.setViewModelGroup(kermat_lv);
            injury_id.setViewModelGroup(injury_id_lv);
            passenger_movement_id.setViewModelGroup(passanger_movement_id_lv);
            pedestrian_movement_id.setViewModelGroup(pedestrian_movement_id_lv);
            vehicle_design_id.setViewModelGroup(vehicle_design_id_lv);
            accident_before_id.setViewModelGroup(accident_before_id_lv);
            accident_after_id.setViewModelGroup(accident_after_id_lv);
            special_id.setViewModelGroup(special_id_lv);
            accident_end_id.setViewModelGroup(accident_end_id_lv);
            pemudik.setViewModelGroup(pemudik_lv);

            /*CB*/
            driver_law.setViewModelGroup(driver_law_lv);
            informasi_khusus.setViewModelGroup(informasi_khusus_lv);
            kermat.setViewModelGroup(kermat_lv);
            gender_id.setViewModelGroup(gender_id_lv);
            pengaturan_simpang.setViewModelGroup(pengaturan_simpang_lv);
            passenger_safety.setViewModelGroup(passenger_safety_lv);
            safety_device.setViewModelGroup(safety_device_lv);
            kerusakan.setViewModelGroup(kerusakan_lv);
            titik_kerusakan.setViewModelGroup(titik_kerusakan_lv);
            disita.setViewModelGroup(disita_lv);
            safety.setViewModelGroup(safety_lv);
        }
    }

    /**
     * for spinner
     *
     * @param model
     * @return
     */
    private static ViewModel transformToViewModel(CodedItemModel model) {
        ViewModel itemViewModel = new ViewModel();
        itemViewModel.setName(model.getName());
        itemViewModel.setTextContent(model.getName());
        itemViewModel.setViewID(model.getId());
        itemViewModel.setItemResourceID(com.gandsoft.irsms.R.layout.component_spinneritem_dropdown);
        itemViewModel.setItemContainerID(com.gandsoft.irsms.R.layout.component_spinneritem_display);
        return itemViewModel;
    }

    private static ViewModel transformToViewModel(CodedItemModel model, Boolean isRadio) {
        ViewModel itemViewModel = new ViewModel();
        itemViewModel.setName(model.getName());
        itemViewModel.setTextContent(model.getName());
        itemViewModel.setViewID(model.getId());
        if (isRadio) {
            itemViewModel.setItemResourceID(com.gandsoft.irsms.R.layout.list_widget_radiobutton);
        } else {
            itemViewModel.setItemResourceID(com.gandsoft.irsms.R.layout.list_widget_checkbox);
        }
        return itemViewModel;
    }

    public static void generateUiModel() {
        FormDataResponseModel models = null;
        try {
            models = new Gson().fromJson(SessionManager.loadData(ISeasonConfig.KEY_UITEM), FormDataResponseModel.class);
        } catch (Exception e) {
            //Log.e("uiloading fail:", e.getMessage());
        }
        if (models == null) {
            //Log.e("uiloading fail: ", " load aborted, no data!");
            return;
        }

        listedModels.addAll(models.getArah());
        listedModels.addAll(models.getBatas_kecepatan());
        listedModels.addAll(models.getKemiringan_jalan());
        listedModels.addAll(models.getPengaturan_simpang());
        listedModels.addAll(models.getStatus_jalan());
        listedModels.addAll(models.getKondisi_permukaan());
        listedModels.addAll(models.getBentuk_geometri());
        listedModels.addAll(models.getTipe_jalan());
        listedModels.addAll(models.getKelas_jalan());
        listedModels.addAll(models.getFungsi_jalan());
        listedModels.addAll(models.getKecelakaan_menonjol());
        listedModels.addAll(models.getTitik_acuan());
        listedModels.addAll(models.getCuaca());
        listedModels.addAll(models.getKondisi_cahaya());
        listedModels.addAll(models.getInformasi_khusus());
        listedModels.addAll(models.getTipe_kecelakaan());
        listedModels.addAll(models.getLicense_class_id());
        listedModels.addAll(models.getInjury_id());
        listedModels.addAll(models.getAlcohol_id());

        listedModels.addAll(models.getArrested_id());
        listedModels.addAll(models.getSuspected_id());
        listedModels.addAll(models.getPlate_color_id());
        listedModels.addAll(models.getVehicle_type_id());
        listedModels.addAll(models.getVehicle_design_id());
        listedModels.addAll(models.getAccident_before_id());
        listedModels.addAll(models.getAccident_after_id());
        listedModels.addAll(models.getAccident_end_id());
        listedModels.addAll(models.getSpecial_id());
        listedModels.addAll(models.getVehicle_brand_id());
        listedModels.addAll(models.getVehicle_brand_motor_id());
        listedModels.addAll(models.getVehicle_color_id());
        listedModels.addAll(models.getIdentity_type_id());
        listedModels.addAll(models.getReligion_id());
        listedModels.addAll(models.getProfession_id());
        listedModels.addAll(models.getEducation_id());

        listedModels.addAll(models.getGender_id());
        listedModels.addAll(models.getSafety());
        listedModels.addAll(models.getPassenger_safety());
        listedModels.addAll(models.getSafety_device());
        listedModels.addAll(models.getNationality_id());
        listedModels.addAll(models.getTitik_kerusakan());
        listedModels.addAll(models.getPassenger_movement_id());
        listedModels.addAll(models.getPedestrian_movement_id());
        listedModels.addAll(models.getKerusakan());
        listedModels.addAll(models.getDisita());
        listedModels.addAll(models.getDriver_behavior());
        listedModels.addAll(models.getDriver_law());
        listedModels.addAll(models.getPenyebab());
        listedModels.addAll(models.getDead_spot_id());
        listedModels.addAll(models.getPemudik());
        listedModels.addAll(models.getKermat());


    }

    public static ViewModel getArah() {
        return arah;
    }

    public static ViewModel getBatas_kecepatan() {
        return batas_kecepatan;
    }

    public static ViewModel getKemiringan_jalan() {
        return kemiringan_jalan;
    }

    public static ViewModel getPengaturan_simpang() {
        return pengaturan_simpang;
    }

    public static ViewModel getStatus_jalan() {
        return status_jalan;
    }

    public static ViewModel getKondisi_permukaan() {
        return kondisi_permukaan;
    }

    public static ViewModel getBentuk_geometri() {
        return bentuk_geometri;
    }

    public static ViewModel getTipe_jalan() {
        return tipe_jalan;
    }

    public static ViewModel getKelas_jalan() {
        return kelas_jalan;
    }

    public static ViewModel getFungsi_jalan() {
        return fungsi_jalan;
    }

    public static ViewModel getKecelakaan_menonjol() {
        return kecelakaan_menonjol;
    }

    public static ViewModel getTitik_acuan() {
        return titik_acuan;
    }

    public static ViewModel getCuaca() {
        return cuaca;
    }

    public static ViewModel getKondisi_cahaya() {
        return kondisi_cahaya;
    }

    public static ViewModel getInformasi_khusus() {
        return informasi_khusus;
    }

    public static ViewModel getTipe_kecelakaan() {
        return tipe_kecelakaan;
    }

    public static ViewModel getLicense_class_id() {
        return license_class_id;
    }

    public static ViewModel getInjury_id() {
        return injury_id;
    }

    public static ViewModel getAlcohol_id() {
        return alcohol_id;
    }

    public static ViewModel getArrested_id() {
        return arrested_id;
    }

    public static ViewModel getSuspected_id() {
        return suspected_id;
    }

    public static ViewModel getPlate_color_id() {
        return plate_color_id;
    }

    public static ViewModel getVehicle_type_id() {
        return vehicle_type_id;
    }

    public static ViewModel getVehicle_design_id() {
        return vehicle_design_id;
    }

    public static ViewModel getAccident_before_id() {
        return accident_before_id;
    }

    public static ViewModel getAccident_after_id() {
        return accident_after_id;
    }

    public static ViewModel getAccident_end_id() {
        return accident_end_id;
    }

    public static ViewModel getSpecial_id() {
        return special_id;
    }

    public static ViewModel getVehicle_brand_id() {
        return vehicle_brand_id;
    }

    public static ViewModel getVehicle_brand_motor_id() {
        return vehicle_brand_motor_id;
    }

    public static ViewModel getVehicle_color_id() {
        return vehicle_color_id;
    }

    public static ViewModel getIdentity_type_id() {
        return identity_type_id;
    }

    public static ViewModel getReligion_id() {
        return religion_id;
    }

    public static ViewModel getProfession_id() {
        return profession_id;
    }

    public static ViewModel getEducation_id() {
        return education_id;
    }

    public static ViewModel getGender_id() {
        return gender_id;
    }

    public static ViewModel getSafety() {
        return safety;
    }

    public static ViewModel getPassenger_safety() {
        return passenger_safety;
    }

    public static ViewModel getSafety_device() {
        return safety_device;
    }

    public static ViewModel getNationality_id() {
        return nationality_id;
    }

    public static ViewModel getTitik_kerusakan() {
        return titik_kerusakan;
    }

    public static ViewModel getKerusakan() {
        return kerusakan;
    }

    public static ViewModel getDisita() {
        return disita;
    }

    public static ViewModel getDriver_behavior() {
        return driver_behavior;
    }

    public static ViewModel getDriver_law() {
        return driver_law;
    }

    public static ViewModel getPenyebab() {
        return penyebab;
    }

    public static ViewModel getDead_spot_id() {
        return dead_spot_id;
    }

    public static ViewModel getPemudik() {
        return pemudik;
    }

    public static ViewModel getKermat() {
        return kermat;
    }

    public static ViewModel getProvince() {
        return province;
    }

    public static ViewModel getKabupaten() {
        return kabupaten;
    }

    public static ViewModel getKecamatan() {
        return kecamatan;
    }

    public static ViewModel getKelurahan() {
        return kelurahan;
    }

    public static ViewModel getPropinsi_stnk() {
        return propinsi_stnk;
    }

    public static ViewModel getKabupaten_stnk() {
        return kabupaten_stnk;
    }

    public static ViewModel getKecamatan_stnk() {
        return kecamatan_stnk;
    }

    public static ViewModel getKelurahan_stnk() {
        return kelurahan_stnk;
    }

    public static ViewModel getPassenger_movement_id() {
        return passenger_movement_id;
    }

    public static ViewModel getPedestrian_movement_id() {
        return pedestrian_movement_id;
    }

    public static List<CodedItemModel> getListedModels() {
        return listedModels;
    }

    public static SQLiteDatabase getLocationCodex() {
        return locationCodex;
    }

    public static List<String> getIdModel() {
        return idModel;
    }

    public static ArrayList<String> getId_informasi_khusus_lv() {
        return id_informasi_khusus_lv;
    }

    public static ArrayList<String> getId_kondisi_cahaya_lv() {
        return id_kondisi_cahaya_lv;
    }

    public static ArrayList<String> getId_cuaca_lv() {
        return id_cuaca_lv;
    }

    public static ArrayList<String> getId_fungsi_jalan_lv() {
        return id_fungsi_jalan_lv;
    }

    public static ArrayList<String> getId_kelas_jalan_lv() {
        return id_kelas_jalan_lv;
    }

    public static ArrayList<String> getId_tipe_jalan_lv() {
        return id_tipe_jalan_lv;
    }

    public static ArrayList<String> getId_bentuk_geometri_lv() {
        return id_bentuk_geometri_lv;
    }

    public static ArrayList<String> getId_kondisi_permukaan_lv() {
        return id_kondisi_permukaan_lv;
    }

    public static ArrayList<String> getId_batas_kecepatan_lv() {
        return id_batas_kecepatan_lv;
    }

    public static ArrayList<String> getId_kemiringan_jalan_lv() {
        return id_kemiringan_jalan_lv;
    }

    public static ArrayList<String> getId_pengaturan_simpang_lv() {
        return id_pengaturan_simpang_lv;
    }

    public static ArrayList<String> getId_status_jalan_lv() {
        return id_status_jalan_lv;
    }

    public static ArrayList<String> getId_penyebab_lv() {
        return id_penyebab_lv;
    }

    public static ArrayList<String> getId_kermat_lv() {
        return id_kermat_lv;
    }

    public static ArrayList<String> getId_gender_id_lv() {
        return id_gender_id_lv;
    }

    public static ArrayList<String> getId_religion_id_lv() {
        return id_religion_id_lv;
    }

    public static ArrayList<String> getId_education_id_lv() {
        return id_education_id_lv;
    }

    public static ArrayList<String> getId_identity_type_id_lv() {
        return id_identity_type_id_lv;
    }

    public static ArrayList<String> getId_nationality_id_lv() {
        return id_nationality_id_lv;
    }

    public static ArrayList<String> getId_injury_id_lv() {
        return id_injury_id_lv;
    }

    public static ArrayList<String> getId_profession_id_lv() {
        return id_profession_id_lv;
    }

    public static ArrayList<String> getId_passenger_safety_lv() {
        return id_passenger_safety_lv;
    }

    public static ArrayList<String> getId_disita_lv() {
        return id_disita_lv;
    }

    public static ArrayList<String> getId_vehicle_type_id_lv() {
        return id_vehicle_type_id_lv;
    }

    public static ArrayList<String> getId_vehicle_design_id_lv() {
        return id_vehicle_design_id_lv;
    }

    public static ArrayList<String> getId_safety_device_lv() {
        return id_safety_device_lv;
    }

    public static ArrayList<String> getId_kerusakan_lv() {
        return id_kerusakan_lv;
    }

    public static ArrayList<String> getId_accident_before_id_lv() {
        return id_accident_before_id_lv;
    }

    public static ArrayList<String> getId_accident_after_id_lv() {
        return id_accident_after_id_lv;
    }

    public static ArrayList<String> getId_titik_kerusakan_lv() {
        return id_titik_kerusakan_lv;
    }

    public static ArrayList<String> getId_special_id_lv() {
        return id_special_id_lv;
    }

    public static ArrayList<String> getId_accident_end_id_lv() {
        return id_accident_end_id_lv;
    }

    public static ArrayList<String> getId_pemudik_lv() {
        return id_pemudik_lv;
    }

    public static ArrayList<String> getId_safety_lv() {
        return id_safety_lv;
    }

    public static ArrayList<String> getId_plate_color_id_lv() {
        return id_plate_color_id_lv;
    }

    public static ArrayList<String> getId_vehicle_brand_id_lv() {
        return id_vehicle_brand_id_lv;
    }

    public static ArrayList<String> getId_vehicle_brand_motor_id_lv() {
        return id_vehicle_brand_motor_id_lv;
    }

    public static ArrayList<String> getId_vehicle_color_id_lv() {
        return id_vehicle_color_id_lv;
    }

    public static ArrayList<String> getId_license_class_id_lv() {
        return id_license_class_id_lv;
    }

    public static ArrayList<String> getId_passanger_movement_id_lv() {
        return id_passanger_movement_id_lv;
    }

    public static ArrayList<String> getId_pedestrian_movement_id_lv() {
        return id_pedestrian_movement_id_lv;
    }

    public static ArrayList<String> getId_driver_behavior_lv() {
        return id_driver_behavior_lv;
    }

    public static ArrayList<String> getId_driver_law_lv() {
        return id_driver_law_lv;
    }

    public static ArrayList<String> getId_arah_lv() {
        return id_arah_lv;
    }

    public static ArrayList<String> getId_identity_type_max_lenght_lv() {
        return id_identity_type_max_lenght_lv;
    }
}



