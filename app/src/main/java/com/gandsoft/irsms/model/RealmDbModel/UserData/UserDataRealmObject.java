package com.gandsoft.irsms.model.RealmDbModel.UserData;

import io.realm.RealmObject;

/**
 * Created by gleen on 23/02/18.
 */

public class UserDataRealmObject extends RealmObject {

    private String username;
    private String password;
    private UserData userDetailData;

    public UserDataRealmObject() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserData getUserDetailData() {
        return userDetailData;
    }

    public void setUserDetailData(UserData userDetailData) {
        this.userDetailData = userDetailData;
    }
}
