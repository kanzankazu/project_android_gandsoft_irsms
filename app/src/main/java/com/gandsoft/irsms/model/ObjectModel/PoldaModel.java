package com.gandsoft.irsms.model.ObjectModel;

import java.io.Serializable;

/**
 * Created by glenn on 1/25/18.
 */

public class PoldaModel implements Serializable {
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
