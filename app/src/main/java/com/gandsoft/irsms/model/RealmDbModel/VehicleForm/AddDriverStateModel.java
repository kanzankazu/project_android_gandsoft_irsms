package com.gandsoft.irsms.model.RealmDbModel.VehicleForm;

import java.io.Serializable;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by gleen on 08/02/18.
 */

public class AddDriverStateModel extends RealmObject implements Serializable {

    private Integer vehicle_color_id;
    private Integer plate_color_id;
    private Integer vehicle_type_id;
    private Integer vehicle_design_id;
    private Integer accident_before_id;
    private Integer accident_after_id;
    private Integer vehicle_brand_id;
    private Integer gender_id;
    private Integer religion_id;
    private Integer accident_end_id;
    private Integer special_damage_id;
    private Integer license_class_id;
    private Integer nationality_id;
    private Integer identity_type;
    private Integer injury_id;
    private Integer alcohol_id;
    private Integer propinsi_stnk;
    private Integer kabupaten_stnk;
    private Integer kecamatan_stnk;
    private Integer kelurahan_stnk;
    private Integer arrested_id;
    private Integer suspected_id;
    private Integer Pemudik;
    private Integer identity_type_id;
    private Integer propinsi;
    private Integer kabupaten;
    private Integer kecamatan;
    private Integer kelurahan;
    private Integer profession_id;
    private Integer education_id;

    private RealmList<Integer> safety_device;
    private RealmList<Integer> kerusakan;
    private RealmList<Integer> disita;
    private RealmList<Integer> safety;
    private RealmList<Integer> driver_law;
    private RealmList<Integer> driver_behavior;

    private RealmList<Integer> titik_kerusakan;

    private String plate_no;
    private String stnk_no;
    private String road_name_stnk;
    private String road_number_stnk;
    private String rt_stnk;
    private String rw_stnk;
    private String frame_no;
    private String engine_no;

    private String engine_capacity;
    private String brake_footprint;
    private String odometer;
    private String total_penumpang;
    private String first_name;
    private String last_name;
    private String birth_date;
    private String age;
    private String identity_no;
    private String road_name;
    private String road_number;
    private String rt;
    private String rw;
    private String license_pub_date;
    private String accident_id;
    private String statement_desc;
    private String no_laka;
    private Integer dead_spot_id;
    private String address;
    private String license_no;
    private String driver_experience;
    private String person_id;
    private String informasi_khusus;

    private String dehavior_text;
    private String daw_text;

    private int Local_ID;
    private int vehicle_Dbid;

    public Integer getVehicle_color_id() {
        return vehicle_color_id;
    }

    public void setVehicle_color_id(Integer vehicle_color_id) {
        this.vehicle_color_id = vehicle_color_id;
    }

    public Integer getPlate_color_id() {
        return plate_color_id;
    }

    public void setPlate_color_id(Integer plate_color_id) {
        this.plate_color_id = plate_color_id;
    }

    public Integer getVehicle_type_id() {
        return vehicle_type_id;
    }

    public void setVehicle_type_id(Integer vehicle_type_id) {
        this.vehicle_type_id = vehicle_type_id;
    }

    public Integer getVehicle_design_id() {
        return vehicle_design_id;
    }

    public void setVehicle_design_id(Integer vehicle_design_id) {
        this.vehicle_design_id = vehicle_design_id;
    }

    public Integer getAccident_before_id() {
        return accident_before_id;
    }

    public void setAccident_before_id(Integer accident_before_id) {
        this.accident_before_id = accident_before_id;
    }

    public Integer getAccident_after_id() {
        return accident_after_id;
    }

    public void setAccident_after_id(Integer accident_after_id) {
        this.accident_after_id = accident_after_id;
    }

    public Integer getVehicle_brand_id() {
        return vehicle_brand_id;
    }

    public void setVehicle_brand_id(Integer vehicle_brand_id) {
        this.vehicle_brand_id = vehicle_brand_id;
    }

    public Integer getGender_id() {
        return gender_id;
    }

    public void setGender_id(Integer gender_id) {
        this.gender_id = gender_id;
    }

    public Integer getReligion_id() {
        return religion_id;
    }

    public void setReligion_id(Integer religion_id) {
        this.religion_id = religion_id;
    }

    public Integer getAccident_end_id() {
        return accident_end_id;
    }

    public void setAccident_end_id(Integer accident_end_id) {
        this.accident_end_id = accident_end_id;
    }

    public Integer getSpecial_damage_id() {
        return special_damage_id;
    }

    public void setSpecial_damage_id(Integer special_damage_id) {
        this.special_damage_id = special_damage_id;
    }

    public Integer getLicense_class_id() {
        return license_class_id;
    }

    public void setLicense_class_id(Integer license_class_id) {
        this.license_class_id = license_class_id;
    }

    public Integer getNationality_id() {
        return nationality_id;
    }

    public void setNationality_id(Integer nationality_id) {
        this.nationality_id = nationality_id;
    }

    public Integer getIdentity_type() {
        return identity_type;
    }

    public void setIdentity_type(Integer identity_type) {
        this.identity_type = identity_type;
    }

    public Integer getInjury_id() {
        return injury_id;
    }

    public void setInjury_id(Integer injury_id) {
        this.injury_id = injury_id;
    }

    public Integer getAlcohol_id() {
        return alcohol_id;
    }

    public void setAlcohol_id(Integer alcohol_id) {
        this.alcohol_id = alcohol_id;
    }

    public Integer getPropinsi_stnk() {
        return propinsi_stnk;
    }

    public void setPropinsi_stnk(Integer propinsi_stnk) {
        this.propinsi_stnk = propinsi_stnk;
    }

    public Integer getKabupaten_stnk() {
        return kabupaten_stnk;
    }

    public void setKabupaten_stnk(Integer kabupaten_stnk) {
        this.kabupaten_stnk = kabupaten_stnk;
    }

    public Integer getKecamatan_stnk() {
        return kecamatan_stnk;
    }

    public void setKecamatan_stnk(Integer kecamatan_stnk) {
        this.kecamatan_stnk = kecamatan_stnk;
    }

    public Integer getKelurahan_stnk() {
        return kelurahan_stnk;
    }

    public void setKelurahan_stnk(Integer kelurahan_stnk) {
        this.kelurahan_stnk = kelurahan_stnk;
    }

    public Integer getArrested_id() {
        return arrested_id;
    }

    public void setArrested_id(Integer arrested_id) {
        this.arrested_id = arrested_id;
    }

    public Integer getSuspected_id() {
        return suspected_id;
    }

    public void setSuspected_id(Integer suspected_id) {
        this.suspected_id = suspected_id;
    }

    public Integer getPemudik() {
        return Pemudik;
    }

    public void setPemudik(Integer pemudik) {
        Pemudik = pemudik;
    }

    public void setIdentity_type_id(Integer identity_type_id) {
        this.identity_type_id = identity_type_id;
    }

    public Integer getPropinsi() {
        return propinsi;
    }

    public void setPropinsi(Integer propinsi) {
        this.propinsi = propinsi;
    }

    public Integer getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(Integer kabupaten) {
        this.kabupaten = kabupaten;
    }

    public Integer getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(Integer kecamatan) {
        this.kecamatan = kecamatan;
    }

    public Integer getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(Integer kelurahan) {
        this.kelurahan = kelurahan;
    }

    public Integer getProfession_id() {
        return profession_id;
    }

    public void setProfession_id(Integer profession_id) {
        this.profession_id = profession_id;
    }

    public Integer getEducation_id() {
        return education_id;
    }

    public void setEducation_id(Integer education_id) {
        this.education_id = education_id;
    }

    public void setSafety_device(RealmList<Integer> safety_device) {
        this.safety_device = safety_device;
    }

    public void setKerusakan(RealmList<Integer> kerusakan) {
        this.kerusakan = kerusakan;
    }

    public void setDisita(RealmList<Integer> disita) {
        this.disita = disita;
    }

    public void setSafety(RealmList<Integer> safety) {
        this.safety = safety;
    }

    public Integer[] getTitik_kerusakan() {
        return titik_kerusakan.toArray(new Integer[titik_kerusakan.size()]);
    }

    public void setTitik_kerusakan(Integer[] titik_kerusakan) {
        if (titik_kerusakan != null && titik_kerusakan.length > 0) {
            this.titik_kerusakan = new RealmList<>();

            for (Integer item :
                    titik_kerusakan) {
                this.titik_kerusakan.add(item);
            }
        }else {
            this.titik_kerusakan = new RealmList<>();
            for (Integer item : titik_kerusakan) {
                this.titik_kerusakan.remove(item);
            }
        }
    }

    public Integer[] getSafety_device() {
        return safety_device.toArray(new Integer[safety_device.size()]);
    }

    public void setSafety_device(Integer[] safety_device) {
        if (safety_device != null && safety_device.length > 0) {
            this.safety_device = new RealmList<>();

            for (Integer item :
                    safety_device) {
                this.safety_device.add(item);
            }
        }else {
            this.safety_device = new RealmList<>();
            for (Integer item : safety_device) {
                this.safety_device.remove(item);
            }
        }
    }

    public Integer[] getKerusakan() {
        return kerusakan.toArray(new Integer[kerusakan.size()]);
    }

    public void setKerusakan(Integer[] kerusakan) {
        if (kerusakan != null && kerusakan.length > 0) {
            this.kerusakan = new RealmList<>();

            for (Integer item :
                    kerusakan) {
                this.kerusakan.add(item);
            }
        }else {
            this.kerusakan = new RealmList<>();
            for (Integer item : kerusakan) {
                this.kerusakan.remove(item);
            }
        }
    }

    public Integer[] getDisita() {
        return disita.toArray(new Integer[disita.size()]);
    }

    public void setDisita(Integer[] disita) {
        if (disita != null && disita.length > 0) {
            this.disita = new RealmList<>();

            for (Integer item :
                    disita) {
                this.disita.add(item);
            }
        }else {
            this.disita = new RealmList<>();
            for (Integer item : disita) {
                this.disita.remove(item);
            }
        }
    }

    public Integer[] getSafety() {
        return safety.toArray(new Integer[safety.size()]);
    }

    public void setSafety(Integer[] safety) {
        if (safety != null && safety.length > 0) {
            this.safety = new RealmList<>();
            for (Integer item :
                    safety) {
                this.safety.add(item);
            }
        }else {
            this.safety = new RealmList<>();
            for (Integer item : safety) {
                this.safety.remove(item);
            }
        }
    }

    public String getPlate_no() {
        return plate_no;
    }

    public void setPlate_no(String plate_no) {
        this.plate_no = plate_no;
    }

    public String getStnk_no() {
        return stnk_no;
    }

    public void setStnk_no(String stnk_no) {
        this.stnk_no = stnk_no;
    }

    public String getRoad_name_stnk() {
        return road_name_stnk;
    }

    public void setRoad_name_stnk(String road_name_stnk) {
        this.road_name_stnk = road_name_stnk;
    }

    public String getRoad_number_stnk() {
        return road_number_stnk;
    }

    public void setRoad_number_stnk(String road_number_stnk) {
        this.road_number_stnk = road_number_stnk;
    }

    public String getRt_stnk() {
        return rt_stnk;
    }

    public void setRt_stnk(String rt_stnk) {
        this.rt_stnk = rt_stnk;
    }

    public String getRw_stnk() {
        return rw_stnk;
    }

    public void setRw_stnk(String rw_stnk) {
        this.rw_stnk = rw_stnk;
    }

    public String getFrame_no() {
        return frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public String getEngine_no() {
        return engine_no;
    }

    public void setEngine_no(String engine_no) {
        this.engine_no = engine_no;
    }

    public String getEngine_capacity() {
        return engine_capacity;
    }

    public void setEngine_capacity(String engine_capacity) {
        this.engine_capacity = engine_capacity;
    }

    public String getBrake_footprint() {
        return brake_footprint;
    }

    public void setBrake_footprint(String brake_footprint) {
        this.brake_footprint = brake_footprint;
    }

    public String getOdometer() {
        return odometer;
    }

    public void setOdometer(String odometer) {
        this.odometer = odometer;
    }

    public String getTotal_penumpang() {
        return total_penumpang;
    }

    public void setTotal_penumpang(String total_penumpang) {
        this.total_penumpang = total_penumpang;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getIdentity_no() {
        return identity_no;
    }

    public void setIdentity_no(String identity_no) {
        this.identity_no = identity_no;
    }

    public String getRoad_name() {
        return road_name;
    }

    public void setRoad_name(String road_name) {
        this.road_name = road_name;
    }

    public String getRoad_number() {
        return road_number;
    }

    public void setRoad_number(String road_number) {
        this.road_number = road_number;
    }

    public String getRt() {
        return rt;
    }

    public void setRt(String rt) {
        this.rt = rt;
    }

    public String getRw() {
        return rw;
    }

    public void setRw(String rw) {
        this.rw = rw;
    }

    public String getLicense_pub_date() {
        return license_pub_date;
    }

    public void setLicense_pub_date(String license_pub_date) {
        this.license_pub_date = license_pub_date;
    }

    public RealmList<Integer> getDriver_behavior() {
        return driver_behavior;
    }

    public Integer[] getDriver_behavior_asArray() {
        Integer[] guk = new Integer[driver_behavior.size()];
        driver_behavior.toArray(guk);
        return guk;
    }

    public void setDriver_behavior(RealmList<Integer> driver_behavior) {
        this.driver_behavior = driver_behavior;
    }

    public RealmList<Integer> getDriver_law() {
        return driver_law;
    }

    public Integer[] getDriver_law_asArray() {
        Integer[] guk = new Integer[driver_law.size()];
        driver_law.toArray(guk);
        return guk;
    }

    public void setDriver_law(RealmList<Integer> driver_law) {
        this.driver_law = driver_law;
    }

    public String getAccident_id() {
        return accident_id;
    }

    public void setAccident_id(String accident_id) {
        this.accident_id = accident_id;
    }

    public String getStatement_desc() {
        return statement_desc;
    }

    public void setStatement_desc(String statement_desc) {
        this.statement_desc = statement_desc;
    }

    public String getNo_laka() {
        return no_laka;
    }

    public void setNo_laka(String no_laka) {
        this.no_laka = no_laka;
    }

    public Integer getDead_spot_id() {
        return dead_spot_id;
    }

    public void setDead_spot_id(Integer dead_spot_id) {
        this.dead_spot_id = dead_spot_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLicense_no() {
        return license_no;
    }

    public void setLicense_no(String license_no) {
        this.license_no = license_no;
    }

    public String getDriver_experience() {
        return driver_experience;
    }

    public void setDriver_experience(String driver_experience) {
        this.driver_experience = driver_experience;
    }

    public String getPerson_id() {
        return person_id;
    }

    public void setPerson_id(String person_id) {
        this.person_id = person_id;
    }

    public String getInformasi_khusus() {
        return informasi_khusus;
    }

    public void setInformasi_khusus(String informasi_khusus) {
        this.informasi_khusus = informasi_khusus;
    }

    public int getIdentity_type_id() {
        return identity_type_id;
    }

    public void setIdentity_type_id(int identity_type_id) {
        this.identity_type_id = identity_type_id;
    }

    public void setTitik_kerusakan(RealmList<Integer> titik_kerusakan) {
        this.titik_kerusakan = titik_kerusakan;
    }

    public int getVehicle_Dbid() {
        return vehicle_Dbid;
    }

    public void setVehicle_Dbid(int vehicle_Dbid) {
        this.vehicle_Dbid = vehicle_Dbid;
    }

    public String getDehavior_text() {
        return dehavior_text;
    }

    public void setDehavior_text(String dehavior_text) {
        this.dehavior_text = dehavior_text;
    }

    public String getDaw_text() {
        return daw_text;
    }

    public void setDaw_text(String daw_text) {
        this.daw_text = daw_text;
    }

    public int getLocal_id() {
        return Local_ID;
    }

    public void setLocal_id(int Local_ID) {
        this.Local_ID = Local_ID;
    }
}
