package com.gandsoft.irsms.model.ResponseModel.FormDataLoader;

import com.gandsoft.irsms.model.ObjectModel.CodedItemModel;
import com.gandsoft.irsms.model.ResponseModel.BaseResponseModel;
import android.widget.LinearLayout;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;

/**
 * Created by glenn on 1/25/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class FormDataResponseModel extends BaseResponseModel {

    private List<CodedItemModel> arah;
    private List<CodedItemModel> batas_kecepatan;
    private List<CodedItemModel> kemiringan_jalan;
    private List<CodedItemModel> pengaturan_simpang;
    private List<CodedItemModel> status_jalan;
    private List<CodedItemModel> kondisi_permukaan;
    private List<CodedItemModel> bentuk_geometri;
    private List<CodedItemModel> tipe_jalan;
    private List<CodedItemModel> kelas_jalan;
    private List<CodedItemModel> fungsi_jalan;
    private List<CodedItemModel> kecelakaan_menonjol;
    private List<CodedItemModel> titik_acuan;
    private List<CodedItemModel> cuaca;
    private List<CodedItemModel> kondisi_cahaya;
    private List<CodedItemModel> informasi_khusus;
    private List<CodedItemModel> tipe_kecelakaan;
    private List<CodedItemModel> license_class_id;
    private List<CodedItemModel> injury_id;
    private List<CodedItemModel> alcohol_id;
    private List<CodedItemModel> arrested_id;
    private List<CodedItemModel> suspected_id;
    private List<CodedItemModel> plate_color_id;
    private List<CodedItemModel> vehicle_type_id;
    private List<CodedItemModel> vehicle_design_id;
    private List<CodedItemModel> accident_before_id;
    private List<CodedItemModel> accident_after_id;
    private List<CodedItemModel> accident_end_id;
    private List<CodedItemModel> special_id;
    private List<CodedItemModel> vehicle_brand_id;
    private List<CodedItemModel> vehicle_brand_motor_id;
    private List<CodedItemModel> vehicle_color_id;
    private List<CodedItemModel> identity_type_id;
    private List<CodedItemModel> religion_id;
    private List<CodedItemModel> profession_id;
    private List<CodedItemModel> education_id;
    private List<CodedItemModel> gender_id;
    private List<CodedItemModel> safety;
    private List<CodedItemModel> passenger_safety;
    private List<CodedItemModel> safety_device;
    private List<CodedItemModel> nationality_id;
    private List<CodedItemModel> titik_kerusakan;
    private List<CodedItemModel> passenger_movement_id;
    private List<CodedItemModel> kerusakan;
    private List<CodedItemModel> disita;
    private List<CodedItemModel> driver_behavior;
    private List<CodedItemModel> driver_law;
    private List<CodedItemModel> penyebab;
    private List<CodedItemModel> dead_spot_id;
    private List<CodedItemModel> pemudik;
    private List<CodedItemModel> kermat;
    private List<CodedItemModel> pedestrian_movement_id;


    public List<CodedItemModel> getArah() {
        return arah;
    }

    public void setArah(List<CodedItemModel> arah) {
        this.arah = arah;
    }

    public List<CodedItemModel> getBatas_kecepatan() {
        return batas_kecepatan;
    }

    public void setBatas_kecepatan(List<CodedItemModel> batas_kecepatan) {
        this.batas_kecepatan = batas_kecepatan;
    }

    public List<CodedItemModel> getKemiringan_jalan() {
        return kemiringan_jalan;
    }

    public void setKemiringan_jalan(List<CodedItemModel> kemiringan_jalan) {
        this.kemiringan_jalan = kemiringan_jalan;
    }

    public List<CodedItemModel> getPengaturan_simpang() {
        return pengaturan_simpang;
    }

    public void setPengaturan_simpang(List<CodedItemModel> pengaturan_simpang) {
        this.pengaturan_simpang = pengaturan_simpang;
    }

    public List<CodedItemModel> getStatus_jalan() {
        return status_jalan;
    }

    public void setStatus_jalan(List<CodedItemModel> status_jalan) {
        this.status_jalan = status_jalan;
    }

    public List<CodedItemModel> getKondisi_permukaan() {
        return kondisi_permukaan;
    }

    public void setKondisi_permukaan(List<CodedItemModel> kondisi_permukaan) {
        this.kondisi_permukaan = kondisi_permukaan;
    }

    public List<CodedItemModel> getBentuk_geometri() {
        return bentuk_geometri;
    }

    public void setBentuk_geometri(List<CodedItemModel> bentuk_geometri) {
        this.bentuk_geometri = bentuk_geometri;
    }

    public List<CodedItemModel> getTipe_jalan() {
        return tipe_jalan;
    }

    public void setTipe_jalan(List<CodedItemModel> tipe_jalan) {
        this.tipe_jalan = tipe_jalan;
    }

    public List<CodedItemModel> getKelas_jalan() {
        return kelas_jalan;
    }

    public void setKelas_jalan(List<CodedItemModel> kelas_jalan) {
        this.kelas_jalan = kelas_jalan;
    }

    public List<CodedItemModel> getFungsi_jalan() {
        return fungsi_jalan;
    }

    public void setFungsi_jalan(List<CodedItemModel> fungsi_jalan) {
        this.fungsi_jalan = fungsi_jalan;
    }

    public List<CodedItemModel> getKecelakaan_menonjol() {
        return kecelakaan_menonjol;
    }

    public void setKecelakaan_menonjol(List<CodedItemModel> kecelakaan_menonjol) {
        this.kecelakaan_menonjol = kecelakaan_menonjol;
    }

    public List<CodedItemModel> getTitik_acuan() {
        return titik_acuan;
    }

    public void setTitik_acuan(List<CodedItemModel> titik_acuan) {
        this.titik_acuan = titik_acuan;
    }

    public List<CodedItemModel> getCuaca() {
        return cuaca;
    }

    public void setCuaca(List<CodedItemModel> cuaca) {
        this.cuaca = cuaca;
    }

    public List<CodedItemModel> getKondisi_cahaya() {
        return kondisi_cahaya;
    }

    public void setKondisi_cahaya(List<CodedItemModel> kondisi_cahaya) {
        this.kondisi_cahaya = kondisi_cahaya;
    }

    public List<CodedItemModel> getInformasi_khusus() {
        return informasi_khusus;
    }

    public void setInformasi_khusus(List<CodedItemModel> informasi_khusus) {
        this.informasi_khusus = informasi_khusus;
    }

    public List<CodedItemModel> getTipe_kecelakaan() {
        return tipe_kecelakaan;
    }

    public void setTipe_kecelakaan(List<CodedItemModel> tipe_kecelakaan) {
        this.tipe_kecelakaan = tipe_kecelakaan;
    }

    public List<CodedItemModel> getLicense_class_id() {
        return license_class_id;
    }

    public void setLicense_class_id(List<CodedItemModel> license_class_id) {
        this.license_class_id = license_class_id;
    }

    public List<CodedItemModel> getInjury_id() {
        return injury_id;
    }

    public void setInjury_id(List<CodedItemModel> injury_id) {
        this.injury_id = injury_id;
    }

    public List<CodedItemModel> getAlcohol_id() {
        return alcohol_id;
    }

    public void setAlcohol_id(List<CodedItemModel> alcohol_id) {
        this.alcohol_id = alcohol_id;
    }

    public List<CodedItemModel> getArrested_id() {
        return arrested_id;
    }

    public void setArrested_id(List<CodedItemModel> arrested_id) {
        this.arrested_id = arrested_id;
    }

    public List<CodedItemModel> getSuspected_id() {
        return suspected_id;
    }

    public void setSuspected_id(List<CodedItemModel> suspected_id) {
        this.suspected_id = suspected_id;
    }

    public List<CodedItemModel> getPlate_color_id() {
        return plate_color_id;
    }

    public void setPlate_color_id(List<CodedItemModel> plate_color_id) {
        this.plate_color_id = plate_color_id;
    }

    public List<CodedItemModel> getVehicle_type_id() {
        return vehicle_type_id;
    }

    public void setVehicle_type_id(List<CodedItemModel> vehicle_type_id) {
        this.vehicle_type_id = vehicle_type_id;
    }

    public List<CodedItemModel> getVehicle_design_id() {
        return vehicle_design_id;
    }

    public void setVehicle_design_id(List<CodedItemModel> vehicle_design_id) {
        this.vehicle_design_id = vehicle_design_id;
    }

    public List<CodedItemModel> getAccident_before_id() {
        return accident_before_id;
    }

    public void setAccident_before_id(List<CodedItemModel> accident_before_id) {
        this.accident_before_id = accident_before_id;
    }

    public List<CodedItemModel> getAccident_after_id() {
        return accident_after_id;
    }

    public void setAccident_after_id(List<CodedItemModel> accident_after_id) {
        this.accident_after_id = accident_after_id;
    }

    public List<CodedItemModel> getAccident_end_id() {
        return accident_end_id;
    }

    public void setAccident_end_id(List<CodedItemModel> accident_end_id) {
        this.accident_end_id = accident_end_id;
    }

    public List<CodedItemModel> getSpecial_id() {
        return special_id;
    }

    public void setSpecial_id(List<CodedItemModel> special_id) {
        this.special_id = special_id;
    }

    public List<CodedItemModel> getVehicle_brand_id() {
        return vehicle_brand_id;
    }

    public void setVehicle_brand_id(List<CodedItemModel> vehicle_brand_id) {
        this.vehicle_brand_id = vehicle_brand_id;
    }

    public List<CodedItemModel> getVehicle_color_id() {
        return vehicle_color_id;
    }

    public void setVehicle_color_id(List<CodedItemModel> vehicle_color_id) {
        this.vehicle_color_id = vehicle_color_id;
    }

    public List<CodedItemModel> getIdentity_type_id() {
        return identity_type_id;
    }

    public void setIdentity_type_id(List<CodedItemModel> identity_type_id) {
        this.identity_type_id = identity_type_id;
    }

    public List<CodedItemModel> getReligion_id() {
        return religion_id;
    }

    public void setReligion_id(List<CodedItemModel> religion_id) {
        this.religion_id = religion_id;
    }

    public List<CodedItemModel> getProfession_id() {
        return profession_id;
    }

    public void setProfession_id(List<CodedItemModel> profession_id) {
        this.profession_id = profession_id;
    }

    public List<CodedItemModel> getEducation_id() {
        return education_id;
    }

    public void setEducation_id(List<CodedItemModel> education_id) {
        this.education_id = education_id;
    }

    public List<CodedItemModel> getGender_id() {
        return gender_id;
    }

    public void setGender_id(List<CodedItemModel> gender_id) {
        this.gender_id = gender_id;
    }

    public List<CodedItemModel> getSafety() {
        return safety;
    }

    public void setSafety(List<CodedItemModel> safety) {
        this.safety = safety;
    }

    public List<CodedItemModel> getPassenger_safety() {
        return passenger_safety;
    }

    public void setPassenger_safety(List<CodedItemModel> passenger_safety) {
        this.passenger_safety = passenger_safety;
    }

    public List<CodedItemModel> getSafety_device() {
        return safety_device;
    }

    public void setSafety_device(List<CodedItemModel> safety_device) {
        this.safety_device = safety_device;
    }

    public List<CodedItemModel> getNationality_id() {
        return nationality_id;
    }

    public void setNationality_id(List<CodedItemModel> nationality_id) {
        this.nationality_id = nationality_id;
    }

    public List<CodedItemModel> getTitik_kerusakan() {
        return titik_kerusakan;
    }

    public void setTitik_kerusakan(List<CodedItemModel> titik_kerusakan) {
        this.titik_kerusakan = titik_kerusakan;
    }

    public List<CodedItemModel> getPassenger_movement_id() {
        return passenger_movement_id;
    }

    public void setPassenger_movement_id(List<CodedItemModel> passenger_movement_id) {
        this.passenger_movement_id = passenger_movement_id;
    }

    public List<CodedItemModel> getKerusakan() {
        return kerusakan;
    }

    public void setKerusakan(List<CodedItemModel> kerusakan) {
        this.kerusakan = kerusakan;
    }

    public List<CodedItemModel> getDisita() {
        return disita;
    }

    public void setDisita(List<CodedItemModel> disita) {
        this.disita = disita;
    }

    public List<CodedItemModel> getDriver_behavior() {
        return driver_behavior;
    }

    public void setDriver_behavior(List<CodedItemModel> driver_behavior) {
        this.driver_behavior = driver_behavior;
    }

    public List<CodedItemModel> getDriver_law() {
        return driver_law;
    }

    public void setDriver_law(List<CodedItemModel> driver_law) {
        this.driver_law = driver_law;
    }

    public List<CodedItemModel> getPenyebab() {
        return penyebab;
    }

    public void setPenyebab(List<CodedItemModel> penyebab) {
        this.penyebab = penyebab;
    }

    public List<CodedItemModel> getDead_spot_id() {
        return dead_spot_id;
    }

    public void setDead_spot_id(List<CodedItemModel> dead_spot_id) {
        this.dead_spot_id = dead_spot_id;
    }

    public List<CodedItemModel> getPemudik() {
        return pemudik;
    }

    public void setPemudik(List<CodedItemModel> pemudik) {
        this.pemudik = pemudik;
    }

    public List<CodedItemModel> getKermat() {
        return kermat;
    }

    public void setKermat(List<CodedItemModel> kermat) {
        this.kermat = kermat;
    }

    public List<CodedItemModel> getPedestrian_movement_id() {
        return pedestrian_movement_id;
    }

    public void setPedestrian_movement_id(List<CodedItemModel> pedestrian_movement_id) {
        this.pedestrian_movement_id = pedestrian_movement_id;
    }

    public List<CodedItemModel> getVehicle_brand_motor_id() {
        return vehicle_brand_motor_id;
    }

    public void setVehicle_brand_motor_id(List<CodedItemModel> vehicle_brand_motor_id) {
        this.vehicle_brand_motor_id = vehicle_brand_motor_id;
    }
}
