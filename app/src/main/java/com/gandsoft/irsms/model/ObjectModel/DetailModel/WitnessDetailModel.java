package com.gandsoft.irsms.model.ObjectModel.DetailModel;

/**
 * Created by glenn on 1/25/18.
 */

public class WitnessDetailModel {

    private String witness_first_name;
    private String witness_last_name;
    private int witness_age;
    private String witness_gender;

    public String getWitness_first_name() {
        return witness_first_name;
    }

    public void setWitness_first_name(String witness_first_name) {
        this.witness_first_name = witness_first_name;
    }

    public String getWitness_last_name() {
        return witness_last_name;
    }

    public void setWitness_last_name(String witness_last_name) {
        this.witness_last_name = witness_last_name;
    }

    public int getWitness_age() {
        return witness_age;
    }

    public void setWitness_age(int witness_age) {
        this.witness_age = witness_age;
    }

    public String getWitness_gender() {
        return witness_gender;
    }

    public void setWitness_gender(String witness_gender) {
        this.witness_gender = witness_gender;
    }
}