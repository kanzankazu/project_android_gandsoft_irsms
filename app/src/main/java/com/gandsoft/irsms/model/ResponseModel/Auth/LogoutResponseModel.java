package com.gandsoft.irsms.model.ResponseModel.Auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gandsoft.irsms.model.ObjectModel.UserDataModel;
import com.gandsoft.irsms.model.ResponseModel.BaseResponseModel;

/**
 * Created by glenn on 1/25/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class LogoutResponseModel extends BaseResponseModel {
}
