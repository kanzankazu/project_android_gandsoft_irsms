package com.gandsoft.irsms.model.RequestModel.VehicleForm;

import com.gandsoft.irsms.model.RealmDbModel.VehicleForm.DriverDBModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by glenn on 1/25/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class SetDriverRequestModel {

    private String accident_id;
    private String license_no;
    private String license_pub_date;
    private String driver_experience;
    private String license_class_id;
    private String injury_id;
    private String alcohol_id;
    private String arrested_id;
    private String suspected_id;
    private String statement_desc;
    private String plate_no;
    private String total_penumpang;
    private String plate_color_id;
    private String stnk_no;
    private String vehicle_type_id;
    private String frame_no;
    private String engine_no;
    private String engine_capacity;
    private String vehicle_design_id;
    private String accident_before_id;
    private String accident_after_id;
    private String accident_end_id;
    private String special_damage_id;
    private String brake_footprint;
    private String odometer;
    private String vehicle_brand_id;
    private String vehicle_color_id;
    private String no_laka;
    private String informasi_khusus;
    private String first_name;
    private String last_name;
    private String identity_type_id;
    private String identity_no;
    private String nationality_id;
    private String profession_id;
    private String education_id;
    private String birth_date;
    private String age;
    private String address;
    private String person_id;
    private String gender_id;
    private String safety_device;
    private String kerusakan;
    private String disita;
    private String safety;
    private String titik_kerusakan;
    private String religion_id;
    private String driver_law;
    private String driver_behavior;
    private String dead_spot_id;
    /*new add by faisal*/
    private String Pemudik;
    private String road_name;
    private String road_number;
    private String rt;
    private String rw;
    private String propinsi;
    private String kabupaten;
    private String kecamatan;
    private String kelurahan;
    /*stnk*/
    private String road_name_stnk;
    private String road_number_stnk;
    private String rt_stnk;
    private String rw_stnk;
    private String propinsi_stnk;
    private String kabupaten_stnk;
    private String kecamatan_stnk;
    private String kelurahan_stnk;

    public SetDriverRequestModel(DriverDBModel model) {
        license_no = model.getLicense_no();
        license_pub_date = model.getLicense_pub_date();
        driver_experience = model.getDriver_experience();
        license_class_id = model.getLicense_class_id();
        injury_id = model.getInjury_id();
        alcohol_id = model.getAlcohol_id();
        arrested_id = model.getArrested_id();
        suspected_id = model.getSuspected_id();
        statement_desc = model.getStatement_desc();
        plate_no = model.getPlate_no();
        total_penumpang = model.getTotal_penumpang();
        plate_color_id = model.getPlate_color_id();
        stnk_no = model.getStnk_no();
        vehicle_type_id = model.getVehicle_type_id();
        frame_no = model.getFrame_no();
        engine_no = model.getEngine_no();
        engine_capacity = model.getEngine_capacity();
        vehicle_design_id = model.getVehicle_design_id();
        accident_before_id = model.getAccident_before_id();
        accident_after_id = model.getAccident_after_id();
        accident_end_id = model.getAccident_end_id();
        special_damage_id = model.getSpecial_damage_id();
        brake_footprint = model.getBrake_footprint();
        odometer = model.getOdometer();
        vehicle_brand_id = model.getVehicle_brand_id();
        vehicle_color_id = model.getVehicle_color_id();
        no_laka = model.getNo_laka();
        informasi_khusus = model.getInformasi_khusus();
        first_name = model.getFirst_name();
        last_name = model.getLast_name();
        identity_type_id = model.getIdentity_type_id();
        identity_no = model.getIdentity_no();
        nationality_id = model.getNationality_id();
        profession_id = model.getProfession_id();
        education_id = model.getEducation_id();
        birth_date = model.getBirth_date();
        age = model.getAge();
        address = model.getAddress();
        person_id = model.getPerson_id();
        gender_id = model.getGender_id();
        safety_device = model.getSafety_device();
        kerusakan = model.getKerusakan();
        disita = model.getDisita();
        safety = model.getSafety();
        titik_kerusakan = model.getTitik_kerusakan();
        religion_id = model.getReligion_id();
        driver_law = model.getDriver_law();
        driver_behavior = model.getDriver_behavior();
        dead_spot_id = model.getDead_spot_id();
        Pemudik = model.getPemudik();
        road_name = model.getRoad_name();
        road_number = model.getRoad_number();
        rt = model.getRt();
        rw = model.getRw();
        propinsi = model.getPropinsi();
        kabupaten = model.getKabupaten();
        kecamatan = model.getKecamatan();
        kelurahan = model.getKelurahan();
        road_name_stnk = model.getRoad_name_stnk();
        //road_number_stnk = model.getRoad_number_stnk();
        stnk_no = model.getRoad_number_stnk();
        rt_stnk = model.getRt_stnk();
        rw_stnk = model.getRw_stnk();
        propinsi_stnk = model.getPropinsi_stnk();
        kabupaten_stnk = model.getKabupaten_stnk();
        kecamatan_stnk = model.getKecamatan_stnk();
        kelurahan_stnk = model.getKelurahan_stnk();
    }

    public String getRoad_name() {
        return road_name != null ? road_name : "";
    }

    public void setRoad_name(String road_name) {
        this.road_name = road_name;
    }

    public String getRoad_number() {
        return road_number != null ? road_number : "";
    }

    public void setRoad_number(String road_number) {
        this.road_number = road_number;
    }

    public String getRoad_name_stnk() {
        return road_name_stnk != null ? road_name_stnk : "";
    }

    public void setRoad_name_stnk(String road_name_stnk) {
        this.road_name_stnk = road_name_stnk;
    }

    public String getRoad_number_stnk() {
        return road_number_stnk != null ? road_number_stnk : "";
    }

    public void setRoad_number_stnk(String road_number_stnk) {
        this.road_number_stnk = road_number_stnk;
    }

    public String getRt() {
        return rt != null ? rt : "";
    }

    public void setRt(String rt) {
        this.rt = rt;
    }

    public String getRw() {
        return rw != null ? rw : "";
    }

    public void setRw(String rw) {
        this.rw = rw;
    }

    public String getRt_stnk() {
        return rt_stnk != null ? rt_stnk : "";
    }

    public void setRt_stnk(String rt_stnk) {
        this.rt_stnk = rt_stnk;
    }

    public String getRw_stnk() {
        return rw_stnk != null ? rw_stnk : "";
    }

    public void setRw_stnk(String rw_stnk) {
        this.rw_stnk = rw_stnk;
    }

    public String getAccident_id() {
        return accident_id != null ? accident_id : "";
    }

    public void setAccident_id(String accident_id) {
        this.accident_id = accident_id;
    }

    public String getLicense_no() {
        return license_no != null ? license_no : "";
    }

    public void setLicense_no(String license_no) {
        this.license_no = license_no;
    }

    public String getLicense_pub_date() {
        return license_pub_date != null ? license_pub_date : "";
    }

    public void setLicense_pub_date(String license_pub_date) {
        this.license_pub_date = license_pub_date;
    }

    public String getDriver_experience() {
        return driver_experience != null ? driver_experience : "";
    }

    public void setDriver_experience(String driver_experience) {
        this.driver_experience = driver_experience;
    }

    public String getLicense_class_id() {
        return license_class_id != null ? license_class_id : "";
    }

    public void setLicense_class_id(String license_class_id) {
        this.license_class_id = license_class_id;
    }

    public String getInjury_id() {
        return injury_id != null ? injury_id : "";
    }

    public void setInjury_id(String injury_id) {
        this.injury_id = injury_id;
    }

    public String getAlcohol_id() {
        return alcohol_id != null ? alcohol_id : "";
    }

    public void setAlcohol_id(String alcohol_id) {
        this.alcohol_id = alcohol_id;
    }

    public String getArrested_id() {
        return arrested_id != null ? arrested_id : "";
    }

    public void setArrested_id(String arrested_id) {
        this.arrested_id = arrested_id;
    }

    public String getSuspected_id() {
        return suspected_id != null ? suspected_id : "";
    }

    public void setSuspected_id(String suspected_id) {
        this.suspected_id = suspected_id;
    }

    public String getStatement_desc() {
        return statement_desc != null ? statement_desc : "";
    }

    public void setStatement_desc(String statement_desc) {
        this.statement_desc = statement_desc;
    }

    public String getPlate_no() {
        return plate_no != null ? plate_no : "";
    }

    public void setPlate_no(String plate_no) {
        this.plate_no = plate_no;
    }

    public String getPlate_color_id() {
        return plate_color_id != null ? plate_color_id : "";
    }

    public void setPlate_color_id(String plate_color_id) {
        this.plate_color_id = plate_color_id;
    }

    public String getStnk_no() {
        return stnk_no != null ? stnk_no : "";
    }

    public void setStnk_no(String stnk_no) {
        this.stnk_no = stnk_no;
    }

    public String getVehicle_type_id() {
        return vehicle_type_id != null ? vehicle_type_id : "";
    }

    public void setVehicle_type_id(String vehicle_type_id) {
        this.vehicle_type_id = vehicle_type_id;
    }

    public String getFrame_no() {
        return frame_no != null ? frame_no : "";
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public String getEngine_no() {
        return engine_no != null ? engine_no : "";
    }

    public void setEngine_no(String engine_no) {
        this.engine_no = engine_no;
    }

    public String getEngine_capacity() {
        return engine_capacity != null ? engine_capacity : "";
    }

    public void setEngine_capacity(String engine_capacity) {
        this.engine_capacity = engine_capacity;
    }

    public String getVehicle_design_id() {
        return vehicle_design_id != null ? vehicle_design_id : "";
    }

    public void setVehicle_design_id(String vehicle_design_id) {
        this.vehicle_design_id = vehicle_design_id;
    }

    public String getAccident_before_id() {
        return accident_before_id != null ? accident_before_id : "";
    }

    public void setAccident_before_id(String accident_before_id) {
        this.accident_before_id = accident_before_id;
    }

    public String getAccident_after_id() {
        return accident_after_id != null ? accident_after_id : "";
    }

    public void setAccident_after_id(String accident_after_id) {
        this.accident_after_id = accident_after_id;
    }

    public String getAccident_end_id() {
        return accident_end_id != null ? accident_end_id : "";
    }

    public void setAccident_end_id(String accident_end_id) {
        this.accident_end_id = accident_end_id;
    }

    public String getSpecial_damage_id() {
        return special_damage_id != null ? special_damage_id : "";
    }

    public void setSpecial_damage_id(String special_damage_id) {
        this.special_damage_id = special_damage_id;
    }

    public String getBrake_footprint() {
        return brake_footprint != null ? brake_footprint : "";
    }

    public void setBrake_footprint(String brake_footprint) {
        this.brake_footprint = brake_footprint;
    }

    public String getOdometer() {
        return odometer != null ? odometer : "";
    }

    public void setOdometer(String odometer) {
        this.odometer = odometer;
    }

    public String getVehicle_brand_id() {
        return vehicle_brand_id != null ? vehicle_brand_id : "";
    }

    public void setVehicle_brand_id(String vehicle_brand_id) {
        this.vehicle_brand_id = vehicle_brand_id;
    }

    public String getVehicle_color_id() {
        return vehicle_color_id != null ? vehicle_color_id : "";
    }

    public void setVehicle_color_id(String vehicle_color_id) {
        this.vehicle_color_id = vehicle_color_id;
    }

    public String getNo_laka() {
        return no_laka != null ? no_laka : "";
    }

    public void setNo_laka(String no_laka) {
        this.no_laka = no_laka;
    }

    public String getInformasi_khusus() {
        return informasi_khusus != null ? informasi_khusus : "";
    }

    public void setInformasi_khusus(String informasi_khusus) {
        this.informasi_khusus = informasi_khusus;
    }

    public String getFirst_name() {
        return first_name != null ? first_name : "";
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name != null ? last_name : "";
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getIdentity_type_id() {
        return identity_type_id != null ? identity_type_id : "";
    }

    public void setIdentity_type_id(String identity_type_id) {
        this.identity_type_id = identity_type_id;
    }

    public String getIdentity_no() {
        return identity_no != null ? identity_no : "";
    }

    public void setIdentity_no(String identity_no) {
        this.identity_no = identity_no;
    }

    public String getNationality_id() {
        return nationality_id != null ? nationality_id : "";
    }

    public void setNationality_id(String nationality_id) {
        this.nationality_id = nationality_id;
    }

    public String getProfession_id() {
        return profession_id != null ? profession_id : "";
    }

    public void setProfession_id(String profession_id) {
        this.profession_id = profession_id;
    }

    public String getEducation_id() {
        return education_id != null ? education_id : "";
    }

    public void setEducation_id(String education_id) {
        this.education_id = education_id;
    }

    public String getBirth_date() {
        return birth_date != null ? birth_date : "";
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getAge() {
        return age != null ? age : "";
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAddress() {
        return address != null ? address : "";
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPerson_id() {
        return person_id != null ? person_id : "";
    }

    public void setPerson_id(String person_id) {
        this.person_id = person_id;
    }

    public String getGender_id() {
        return gender_id != null ? gender_id : "";
    }

    public void setGender_id(String gender_id) {
        this.gender_id = gender_id;
    }

    public String getSafety_device() {
        return safety_device != null ? safety_device : "";
    }

    public void setSafety_device(String safety_device) {
        this.safety_device = safety_device;
    }

    public String getKerusakan() {
        return kerusakan != null ? kerusakan : "";
    }

    public void setKerusakan(String kerusakan) {
        this.kerusakan = kerusakan;
    }

    public String getDisita() {
        return disita != null ? disita : "";
    }

    public void setDisita(String disita) {
        this.disita = disita;
    }

    public String getSafety() {
        return safety != null ? safety : "";
    }

    public void setSafety(String safety) {
        this.safety = safety;
    }

    public String getTitik_kerusakan() {
        return titik_kerusakan != null ? titik_kerusakan : "";
    }

    public void setTitik_kerusakan(String titik_kerusakan) {
        this.titik_kerusakan = titik_kerusakan;
    }

    public String getReligion_id() {
        return religion_id != null ? religion_id : "";
    }

    public void setReligion_id(String religion_id) {
        this.religion_id = religion_id;
    }

    public String getDriver_law() {
        return driver_law != null ? driver_law : "";
    }

    public void setDriver_law(String driver_law) {
        this.driver_law = driver_law;
    }

    public String getDriver_behavior() {
        return driver_behavior != null ? driver_behavior : "";
    }

    public void setDriver_behavior(String driver_behavior) {
        this.driver_behavior = driver_behavior;
    }

    public String getDead_spot_id() {
        return dead_spot_id != null ? dead_spot_id : "";
    }

    public void setDead_spot_id(String dead_spot_id) {
        this.dead_spot_id = dead_spot_id;
    }

    public String getPemudik() {
        return Pemudik != null ? Pemudik : "";
    }

    public void setPemudik(String pemudik) {
        Pemudik = pemudik;
    }

    public String getKelurahan_stnk() {
        return kelurahan_stnk != null ? kelurahan_stnk : "";
    }

    public void setKelurahan_stnk(String kelurahan_stnk) {
        this.kelurahan_stnk = kelurahan_stnk;
    }

    public String getKecamatan_stnk() {
        return kecamatan_stnk != null ? kecamatan_stnk : "";
    }

    public void setKecamatan_stnk(String kecamatan_stnk) {
        this.kecamatan_stnk = kecamatan_stnk;
    }

    public String getKabupaten_stnk() {
        return kabupaten_stnk != null ? kabupaten_stnk : "";
    }

    public void setKabupaten_stnk(String kabupaten_stnk) {
        this.kabupaten_stnk = kabupaten_stnk;
    }

    public String getPropinsi_stnk() {
        return propinsi_stnk != null ? propinsi_stnk : "";
    }

    public void setPropinsi_stnk(String propinsi_stnk) {
        this.propinsi_stnk = propinsi_stnk;
    }

    public String getKelurahan() {
        return kelurahan != null ? kelurahan : "";
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getKecamatan() {
        return kecamatan != null ? kecamatan : "";
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKabupaten() {
        return kabupaten != null ? kabupaten : "";
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public String getPropinsi() {
        return propinsi != null ? propinsi : "";
    }

    public void setPropinsi(String propinsi) {
        this.propinsi = propinsi;
    }

    public String getTotal_penumpang() {
        return total_penumpang != null ? total_penumpang : "";
    }

    public void setTotal_penumpang(String total_penumpang) {
        this.total_penumpang = total_penumpang;
    }
}
