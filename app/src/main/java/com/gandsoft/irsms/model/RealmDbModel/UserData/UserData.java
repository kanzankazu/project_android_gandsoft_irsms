package com.gandsoft.irsms.model.RealmDbModel.UserData;

import com.gandsoft.irsms.model.ObjectModel.UserDataModel;

import io.realm.RealmObject;

/**
 * Created by gleen on 23/02/18.
 */

public class UserData extends RealmObject {

    private String username;
    private String first_name;
    private String last_name;
    private String email;
    private int role_id;
    private String officer_id;
    private String polda_id;
    private String polda_name;
    private String polres_id;
    private String polres_name;
    private String kanit_id;
    private int user_id;
    private String pangkat;
    private String phone_number;
    private String nrp;
    private String role;

    public UserData() {
    }

    public void consume(UserDataModel model) {
        this.username = model.getUsername();
        this.first_name = model.getFirst_name();
        this.last_name = model.getLast_name();
        this.email = model.getEmail();
        this.role_id = model.getRole_id();
        this.officer_id = model.getOfficer_id();
        this.polda_id = model.getPolda_id();
        this.polda_name = model.getPolda_name();
        this.polres_id = model.getPolres_id();
        this.polres_name = model.getPolres_name();
        this.kanit_id = model.getKanit_id();
        this.user_id = model.getUser_id();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public String getOfficer_id() {
        return officer_id;
    }

    public void setOfficer_id(String officer_id) {
        this.officer_id = officer_id;
    }

    public String getPolda_id() {
        return polda_id;
    }

    public void setPolda_id(String polda_id) {
        this.polda_id = polda_id;
    }

    public String getPolda_name() {
        return polda_name;
    }

    public void setPolda_name(String polda_name) {
        this.polda_name = polda_name;
    }

    public String getPolres_id() {
        return polres_id;
    }

    public void setPolres_id(String polres_id) {
        this.polres_id = polres_id;
    }

    public String getPolres_name() {
        return polres_name;
    }

    public void setPolres_name(String polres_name) {
        this.polres_name = polres_name;
    }

    public String getKanit_id() {
        return kanit_id;
    }

    public void setKanit_id(String kanit_id) {
        this.kanit_id = kanit_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getPangkat() {
        return pangkat;
    }

    public void setPangkat(String pangkat) {
        this.pangkat = pangkat;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getNrp() {
        return nrp;
    }

    public void setNrp(String nrp) {
        this.nrp = nrp;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}