package com.gandsoft.irsms.model.RealmDbModel.PedestrianForm;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Created by gleen on 08/02/18.
 */

public class AddPedestrianStateModel extends RealmObject implements Serializable {


    private Integer plate_no;
    private Integer identity_type_id;
    private Integer religion_id;
    private Integer propinsi;
    private Integer kabupaten;
    private Integer kecamatan;
    private Integer kelurahan;
    private Integer nationality_id;
    private Integer profession_id;
    private Integer education_id;

    private String first_name;
    private String last_name;
    private String age;
    private String identity_no;
    private String road_name;
    private String rt;
    private String rw;

    private String vehicle_id;

    private Integer gender_id;
    private Integer injury_id;
    private Integer dead_spot_id;
    private Integer pedestrian_movement_id;
    private Integer arrested_id;

    private int Local_ID;

    public Integer getPlate_no() {
        return plate_no;
    }

    public void setPlate_no(Integer plate_no) {
        this.plate_no = plate_no;
    }

    public Integer getIdentity_type_id() {
        return identity_type_id;
    }

    public void setIdentity_type_id(Integer identity_type_id) {
        this.identity_type_id = identity_type_id;
    }

    public Integer getReligion_id() {
        return religion_id;
    }

    public void setReligion_id(Integer religion_id) {
        this.religion_id = religion_id;
    }

    public Integer getPropinsi() {
        return propinsi;
    }

    public void setPropinsi(Integer propinsi) {
        this.propinsi = propinsi;
    }

    public Integer getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(Integer kabupaten) {
        this.kabupaten = kabupaten;
    }

    public Integer getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(Integer kecamatan) {
        this.kecamatan = kecamatan;
    }

    public Integer getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(Integer kelurahan) {
        this.kelurahan = kelurahan;
    }

    public Integer getNationality_id() {
        return nationality_id;
    }

    public void setNationality_id(Integer nationality_id) {
        this.nationality_id = nationality_id;
    }

    public Integer getProfession_id() {
        return profession_id;
    }

    public void setProfession_id(Integer profession_id) {
        this.profession_id = profession_id;
    }

    public Integer getEducation_id() {
        return education_id;
    }

    public void setEducation_id(Integer education_id) {
        this.education_id = education_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getIdentity_no() {
        return identity_no;
    }

    public void setIdentity_no(String identity_no) {
        this.identity_no = identity_no;
    }

    public String getRoad_name() {
        return road_name;
    }

    public void setRoad_name(String road_name) {
        this.road_name = road_name;
    }

    public String getRt() {
        return rt;
    }

    public void setRt(String rt) {
        this.rt = rt;
    }

    public String getRw() {
        return rw;
    }

    public void setRw(String rw) {
        this.rw = rw;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public Integer getGender_id() {
        return gender_id;
    }

    public void setGender_id(Integer gender_id) {
        this.gender_id = gender_id;
    }

    public Integer getInjury_id() {
        return injury_id;
    }

    public void setInjury_id(Integer injury_id) {
        this.injury_id = injury_id;
    }

    public Integer getPedestrian_movement_id() {
        return pedestrian_movement_id;
    }

    public void setPedestrian_movement_id(Integer pedestrian_movement_id) {
        this.pedestrian_movement_id = pedestrian_movement_id;
    }

    public Integer getArrested_id() {
        return arrested_id;
    }

    public void setArrested_id(Integer arrested_id) {
        this.arrested_id = arrested_id;
    }

    public Integer getDead_spot_id() {
        return dead_spot_id;
    }

    public void setDead_spot_id(Integer dead_spot_id) {
        this.dead_spot_id = dead_spot_id;
    }

    public int getLocal_id() {
        return Local_ID;
    }

    public void setLocal_id(int Local_ID) {
        this.Local_ID = Local_ID;
    }
}
