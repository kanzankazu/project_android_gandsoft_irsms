package com.gandsoft.irsms.model.RequestModel.MainPage;

/**
 * Created by glenn on 1/25/18.
 */

public class SearchAccidentRequestModel {
    String no_laka;
    String tanggal_kejadian;

    public String getNo_laka() {
        return no_laka;
    }

    public void setNo_laka(String no_laka) {
        this.no_laka = no_laka;
    }

    public String getTanggal_kejadian() {
        return tanggal_kejadian;
    }

    public void setTanggal_kejadian(String tanggal_kejadian) {
        this.tanggal_kejadian = tanggal_kejadian;
    }
}
