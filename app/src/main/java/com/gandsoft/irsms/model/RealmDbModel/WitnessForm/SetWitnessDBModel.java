package com.gandsoft.irsms.model.RealmDbModel.WitnessForm;

import io.realm.RealmObject;

/**
 * Created by glenn on 1/25/18.
 */

public class SetWitnessDBModel extends RealmObject {

    private String religion_id;
    private String profession_id;
    private String identity_type_id;

    private String first_name;
    private String last_name;
    private String age;
    private String road_name;
    private String identity_no;

    private String gender_id;
    private String gender_id_text;

    private int Local_ID;

    public String getGender_id_text() {
        return gender_id_text;
    }

    public void setGender_id_text(String gender_id_text) {
        this.gender_id_text = gender_id_text;
    }

    public String getRoad_name() {
        return road_name;
    }

    public void setRoad_name(String road_name) {
        this.road_name = road_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getIdentity_type_id() {
        return identity_type_id;
    }

    public void setIdentity_type_id(String identity_type_id) {
        this.identity_type_id = identity_type_id;
    }

    public String getReligion_id() {
        return religion_id;
    }

    public void setReligion_id(String religion_id) {
        this.religion_id = religion_id;
    }

    public String getProfession_id() {
        return profession_id;
    }

    public void setProfession_id(String profession_id) {
        this.profession_id = profession_id;
    }

    public String getGender_id() {
        return gender_id;
    }

    public void setGender_id(String gender_id) {
        this.gender_id = gender_id;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getIdentity_no() {
        return identity_no;
    }

    public void setIdentity_no(String identity_no) {
        this.identity_no = identity_no;
    }

    public int getLocal_id() {
        return Local_ID;
    }

    public void setLocal_id(int Local_ID) {
        this.Local_ID = Local_ID;
    }
}
