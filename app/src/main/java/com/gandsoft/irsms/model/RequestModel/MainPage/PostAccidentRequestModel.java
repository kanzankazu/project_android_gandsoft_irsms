package com.gandsoft.irsms.model.RequestModel.MainPage;

/**
 * Created by glenn on 1/25/18.
 */

public class PostAccidentRequestModel {

    public String page;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }
}
