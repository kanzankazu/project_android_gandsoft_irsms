package com.gandsoft.irsms.model.RequestModel.Auth;

/**
 * Created by glenn on 1/25/18.
 */

public class LoginRequestModel {
    String username;
    String password;
    String imei;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }
}
