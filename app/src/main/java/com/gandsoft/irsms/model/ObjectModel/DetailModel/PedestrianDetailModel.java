package com.gandsoft.irsms.model.ObjectModel.DetailModel;

/**
 * Created by glenn on 1/25/18.
 */

public class PedestrianDetailModel {

    private String pedestrian_hit_by;
    private String pedestrian_first_name;
    private String pedestrian_last_name;
    private int pedestrian_age;
    private String pedestrian_luka;
    private String pedestrian_movement;

    public String getPedestrian_hit_by() {
        return pedestrian_hit_by;
    }

    public void setPedestrian_hit_by(String pedestrian_hit_by) {
        this.pedestrian_hit_by = pedestrian_hit_by;
    }

    public String getPedestrian_first_name() {
        return pedestrian_first_name;
    }

    public void setPedestrian_first_name(String pedestrian_first_name) {
        this.pedestrian_first_name = pedestrian_first_name;
    }

    public String getPedestrian_last_name() {
        return pedestrian_last_name;
    }

    public void setPedestrian_last_name(String pedestrian_last_name) {
        this.pedestrian_last_name = pedestrian_last_name;
    }

    public int getPedestrian_age() {
        return pedestrian_age;
    }

    public void setPedestrian_age(int pedestrian_age) {
        this.pedestrian_age = pedestrian_age;
    }

    public String getPedestrian_luka() {
        return pedestrian_luka;
    }

    public void setPedestrian_luka(String pedestrian_luka) {
        this.pedestrian_luka = pedestrian_luka;
    }

    public String getPedestrian_movement() {
        return pedestrian_movement;
    }

    public void setPedestrian_movement(String pedestrian_movement) {
        this.pedestrian_movement = pedestrian_movement;
    }
}
