package com.gandsoft.irsms.model.ResponseModel.AccidentForm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gandsoft.irsms.model.ResponseModel.BaseResponseModel;

/**
 * Created by glenn on 1/25/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class SetCheckAccidentResponseModel extends BaseResponseModel {
}
