package com.gandsoft.irsms.model.RequestModel.Auth;

/**
 * Created by glenn on 2/2/18.
 */

public class ResetPasswordRequestModel {
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
