package com.gandsoft.irsms.model.ResponseModel.VehicleForm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gandsoft.irsms.model.ObjectModel.DetailModel.WitnessDetailModel;
import com.gandsoft.irsms.model.ResponseModel.BaseResponseModel;

import java.util.List;

/**
 * Created by glenn on 1/25/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetWitnessDetailResponseModel extends BaseResponseModel {

    private List<WitnessDetailModel> witness_detail;

    public List<WitnessDetailModel> getWitness_detail() {
        return witness_detail;
    }

    public void setWitness_detail(List<WitnessDetailModel> witness_detail) {
        this.witness_detail = witness_detail;
    }
}

