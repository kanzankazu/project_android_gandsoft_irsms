package com.gandsoft.irsms.model.RequestModel.DiagramMappingForm;

import com.gandsoft.irsms.model.RealmDbModel.DiagramMappingForm.DiagramMappingDBModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.realm.RealmObject;

/**
 * Created by gleen on 09/02/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SetDiagramMappingRequestModel {
    private String accident_id;
    private String accident_a;
    private String accident_b;
    private String accident_direction;

    public SetDiagramMappingRequestModel() {
    }

    public void consumeDBModel(DiagramMappingDBModel model) {
        this.accident_id = model.getAccident_id();
        this.accident_a = model.getAccident_a();
        this.accident_b = model.getAccident_b();
        this.accident_direction = model.getAccident_direction();
    }

    public String getAccident_id() {
        return accident_id != null ? accident_id : "";
    }

    public void setAccident_id(String accident_id) {
        this.accident_id = accident_id;
    }

    public String getAccident_a() {
        return accident_a != null ? accident_a : "";
    }

    public void setAccident_a(String accident_a) {
        this.accident_a = accident_a;
    }

    public String getAccident_b() {
        return accident_b != null ? accident_b : "";
    }

    public void setAccident_b(String accident_b) {
        this.accident_b = accident_b;
    }

    public String getAccident_direction() {
        return accident_direction != null ? accident_direction : "";
    }

    public void setAccident_direction(String accident_direction) {
        this.accident_direction = accident_direction;
    }
}
