package com.gandsoft.irsms.model.uiModel;

import java.io.Serializable;

/**
 * Created by glenn on 1/29/18.
 */

public class InfoDialogModel implements Serializable {
    private String title;
    private int imgResID;
    private String subTitle;
    private String content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImgResID() {
        return imgResID;
    }

    public void setImgResID(int imgResID) {
        this.imgResID = imgResID;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
