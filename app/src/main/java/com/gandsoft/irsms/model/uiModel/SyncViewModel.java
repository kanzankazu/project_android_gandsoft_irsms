package com.gandsoft.irsms.model.uiModel;

/**
 * Created by gleen on 19/02/18.
 */

public class SyncViewModel {

    private int localID;
    private int color;
    private String content;
    private String status;


    public SyncViewModel(int localID, String content, String status) {
        this.content = content;
        this.status = status;
        this.localID = localID;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getLocalID() {
        return localID;
    }

    public void setLocalID(int localID) {
        this.localID = localID;
    }
}
