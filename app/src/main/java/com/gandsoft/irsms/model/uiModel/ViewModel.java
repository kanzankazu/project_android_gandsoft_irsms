package com.gandsoft.irsms.model.uiModel;

import java.util.List;

/**
 * ViewModel for view autogenerator
 * some data are mandatory dependant on viewType
 * Created by glenn on 1/17/18.
 * @version 1.0
 */

public class ViewModel {
    String name, Title, viewID, textContent;
    int viewType, EditTextInputType, itemResourceID, itemDecoratorID, itemContainerID;

    boolean isHasSub  = false, mainHaveDisplayWithSub = false;

    List<ViewModel> viewModelGroup;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * get title for id:title at either container of child
     *
     * @return
     */
    public String getTitle() {
        return Title;
    }

    /**
     * @param title set title for container or text
     */
    public void setTitle(String title) {
        Title = title;
    }

    /**
     * get what kind of view will be created
     *
     * @return
     */
    public int getViewType() {
        return viewType;
    }

    /**
     * customize what they type of the autgenerated View
     *
     * @param viewType type of this current view
     *                 0. is Container
     *                 1. is textview
     *                 2. is EditText
     *                 3. is CheckBoxGroup
     *                 4. is CheckBox
     *                 5. is RadioGroup
     *                 6. is RadioBtn
     *                 7. is spinner
     */
    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public String getViewID() {
        return viewID;
    }

    /**
     * set the "id" of the view on tag
     *
     * @param viewID id of the view, will be placed as a Tag on the view
     */
    public void setViewID(String viewID) {
        this.viewID = viewID;
    }

    public List<ViewModel> getViewModelGroup() {
        return viewModelGroup;
    }

    /**
     * set a child object of the view
     *
     * @param viewModelGroup MANDATORY if the view had children,
     *                       for eg. if the view were nested like checkBoxGroup, RadioGroup, or a Container
     */
    public void setViewModelGroup(List<ViewModel> viewModelGroup) {
        this.viewModelGroup = viewModelGroup;
    }

    public int getItemResourceID() {
        return itemResourceID;
    }

    /**
     * @param itemResourceID set a layout for view,
     *                       this will replace "buttonDrawable" on checkbox and radio button
     */
    public void setItemResourceID(int itemResourceID) {
        this.itemResourceID = itemResourceID;
    }

    public void setItemContainerID(int itemContainerID) {
        this.itemContainerID = itemContainerID;
    }

    public int getItemContainerID() {
        return itemContainerID;
    }


    /**
     * check if it has a child item
     *
     * @return
     */
    public boolean isHasSub() {
        return isHasSub;
    }

    /**
     * set if it has a child item
     *
     * @param hasSub
     */
    public void setHasSub(boolean hasSub) {
        isHasSub = hasSub;
    }

    /**
     * @return
     */
    public boolean isMainHaveDisplayWithSub() {
        return mainHaveDisplayWithSub;
    }

    /**
     * @param mainHaveDisplayWithSub
     */
    public void setMainHaveDisplayWithSub(boolean mainHaveDisplayWithSub) {
        this.mainHaveDisplayWithSub = mainHaveDisplayWithSub;
    }

    public String getTextContent() {
        return textContent;
    }

    /**
     * set Text content of the child item, only fill this if it is a child item
     *
     * @param textContent for TextView it will filled as is,
     *                    on EditText it will filled as hint,
     */
    public void setTextContent(String textContent) {
        this.textContent = textContent;
    }

    public int getItemDecoratorID() {
        return itemDecoratorID;
    }

    /**
     * @param itemDecoratorID set a custom drawable for certain view,
     *                        this will replace "buttonDrawable" on checkbox and radio button
     *                        this will replace "background" for edittext or textview
     */
    public void setItemDecoratorID(int itemDecoratorID) {
        this.itemDecoratorID = itemDecoratorID;
    }

    public int getEditTextInputType() {
        return EditTextInputType;
    }

    /**
     * set Input type if field is EditText
     *
     * @param editTextInputType only mandatory if field is inputText,
     *                          see link of input type value
     * @link https://developer.android.com/reference/android/text/InputType.html
     */
    public void setEditTextInputType(int editTextInputType) {
        EditTextInputType = editTextInputType;
    }
}
