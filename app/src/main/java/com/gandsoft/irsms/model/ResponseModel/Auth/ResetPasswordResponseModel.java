package com.gandsoft.irsms.model.ResponseModel.Auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gandsoft.irsms.model.ResponseModel.BaseResponseModel;

/**
 * Created by glenn on 2/2/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResetPasswordResponseModel extends BaseResponseModel {
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
