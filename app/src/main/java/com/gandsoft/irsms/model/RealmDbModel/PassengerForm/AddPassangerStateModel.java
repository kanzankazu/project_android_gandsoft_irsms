package com.gandsoft.irsms.model.RealmDbModel.PassengerForm;

import java.io.Serializable;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by gleen on 08/02/18.
 */

public class AddPassangerStateModel extends RealmObject implements Serializable {
    private Integer religion_id;
    private Integer identity_type_id;
    private Integer propinsi;
    private Integer kabupaten;
    private Integer kecamatan;
    private Integer kelurahan;
    private Integer nationality_id;
    private Integer profession_id;
    private Integer education_id;

    private Integer gender_id;
    private Integer injury_id;
    private Integer dead_spot_id;
    private Integer behavior_id;
    private Integer arrested_id;

    private RealmList<Integer> passenger_safety;

    private String plate_no;
    private String vehicle_type_id;
    private String vehicle_brand_id;
    private String first_name;
    private String last_name;
    private String age;
    private String identity_no;
    private String road_name;
    private String rt;
    private String rw;

    private String injury_id_text;
    private String dead_spot_id_text;
    private String vehicle_brand_text;
    private String vehicle_type_text;

    private int item_id;
    private int vehicle_SubID;
    private int Local_ID;

    public void setPassenger_safety(RealmList<Integer> passenger_safety) {
        this.passenger_safety = passenger_safety;
    }

    public Integer[] getPassenger_safety() {
        if (passenger_safety != null) {
            return passenger_safety.toArray(new Integer[passenger_safety.size()]);
        }
        return null;
    }

    public void setPassenger_safety(Integer[] passenger_safety) {
        if (passenger_safety != null && passenger_safety.length > 0) {
            this.passenger_safety = new RealmList<>();
            for (Integer item :
                    passenger_safety) {
                this.passenger_safety.add(item);
            }
        }else {
            this.passenger_safety = new RealmList<>();
            for (Integer item : passenger_safety) {
                this.passenger_safety.remove(item);
            }
        }
    }

    public Integer getReligion_id() {
        return religion_id;
    }

    public void setReligion_id(Integer religion_id) {
        this.religion_id = religion_id;
    }

    public Integer getIdentity_type_id() {
        return identity_type_id;
    }

    public void setIdentity_type_id(Integer identity_type_id) {
        this.identity_type_id = identity_type_id;
    }

    public Integer getNationality_id() {
        return nationality_id;
    }

    public void setNationality_id(Integer nationality_id) {
        this.nationality_id = nationality_id;
    }

    public Integer getPropinsi() {
        return propinsi;
    }

    public void setPropinsi(Integer propinsi) {
        this.propinsi = propinsi;
    }

    public Integer getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(Integer kabupaten) {
        this.kabupaten = kabupaten;
    }

    public Integer getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(Integer kecamatan) {
        this.kecamatan = kecamatan;
    }

    public Integer getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(Integer kelurahan) {
        this.kelurahan = kelurahan;
    }

    public Integer getProfession_id() {
        return profession_id;
    }

    public void setProfession_id(Integer profession_id) {
        this.profession_id = profession_id;
    }

    public Integer getEducation_id() {
        return education_id;
    }

    public void setEducation_id(Integer education_id) {
        this.education_id = education_id;
    }

    public String getRoad_name() {
        return road_name;
    }

    public void setRoad_name(String road_name) {
        this.road_name = road_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getIdentity_no() {
        return identity_no;
    }

    public void setIdentity_no(String identity_no) {
        this.identity_no = identity_no;
    }

    public String getRt() {
        return rt;
    }

    public void setRt(String rt) {
        this.rt = rt;
    }

    public String getRw() {
        return rw;
    }

    public void setRw(String rw) {
        this.rw = rw;
    }

    public Integer getGender_id() {
        return gender_id;
    }

    public void setGender_id(Integer gender_id) {
        this.gender_id = gender_id;
    }

    public Integer getInjury_id() {
        return injury_id;
    }

    public void setInjury_id(Integer injury_id) {
        this.injury_id = injury_id;
    }

    public Integer getBehavior_id() {
        return behavior_id;
    }

    public void setBehavior_id(Integer behavior_id) {
        this.behavior_id = behavior_id;
    }

    public Integer getArrested_id() {
        return arrested_id;
    }

    public void setArrested_id(Integer arrested_id) {
        this.arrested_id = arrested_id;
    }

    public Integer getDead_spot_id() {
        return dead_spot_id;
    }

    public void setDead_spot_id(Integer dead_spot_id) {
        this.dead_spot_id = dead_spot_id;
    }

    public String getPlate_no() {
        return plate_no;
    }

    public void setPlate_no(String plate_no) {
        this.plate_no = plate_no;
    }

    public String getVehicle_type_id() {
        return vehicle_type_id;
    }

    public void setVehicle_type_id(String vehicle_type_id) {
        this.vehicle_type_id = vehicle_type_id;
    }

    public String getVehicle_brand_id() {
        return vehicle_brand_id;
    }

    public void setVehicle_brand_id(String vehicle_brand_id) {
        this.vehicle_brand_id = vehicle_brand_id;
    }

    public String getVehicle_brand_text() {
        return vehicle_brand_text;
    }

    public void setVehicle_brand_text(String vehicle_brand_text) {
        this.vehicle_brand_text = vehicle_brand_text;
    }

    public String getVehicle_type_text() {
        return vehicle_type_text;
    }

    public void setVehicle_type_text(String vehicle_type_text) {
        this.vehicle_type_text = vehicle_type_text;
    }

    public String getInjury_id_text() {
        return injury_id_text;
    }

    public void setInjury_id_text(String injury_id_text) {
        this.injury_id_text = injury_id_text;
    }

    public String getDead_spot_id_text() {
        return dead_spot_id_text;
    }

    public void setDead_spot_id_text(String dead_spot_id_text) {
        this.dead_spot_id_text = dead_spot_id_text;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public int getVehicle_SubID() {
        return vehicle_SubID;
    }

    public void setVehicle_SubID(int vehicle_SubID) {
        this.vehicle_SubID = vehicle_SubID;
    }

    public int getLocal_id() {
        return Local_ID;
    }

    public void setLocal_id(int Local_ID) {
        this.Local_ID = Local_ID;
    }
}
