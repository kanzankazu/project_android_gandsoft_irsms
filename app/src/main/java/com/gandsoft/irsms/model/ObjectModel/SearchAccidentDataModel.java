package com.gandsoft.irsms.model.ObjectModel;

import java.io.Serializable;
import java.util.List;

/**
 * Created by glenn on 1/25/18.
 */

public class SearchAccidentDataModel implements Serializable {

    private String id;
    private String report_id;
    private String officer_first_name;
    private String officer_last_name;
    private String polres;
    private String polda;
    private String latitude;
    private String longitude;
    private String accident_date;
    private String accident_time;
    private String report_date;
    private String report_time;
    private String accident_type_id;
    private String created_at;
    private String updated_at;
    private String road_name;
    private boolean draft;
    private boolean editable;
    private boolean gps;
    private boolean urgent;
    private boolean verified;
    private String lb;
    private String lr;
    private String md;
    private String jumlah_kendaraan;
    private String jumlah_pejalan;
    private String tingkat_kecelakaan;
    List<?> nopol;

    public List<?> getNopol() {
        return nopol;
    }

    public void setNopol(List<?> nopol) {
        this.nopol = nopol;
    }

    public String getOfficer_first_name() {
        return officer_first_name;
    }

    public void setOfficer_first_name(String officer_first_name) {
        this.officer_first_name = officer_first_name;
    }

    public String getOfficer_last_name() {
        return officer_last_name;
    }

    public void setOfficer_last_name(String officer_last_name) {
        this.officer_last_name = officer_last_name;
    }

    public String getLb() {
        return lb;
    }

    public void setLb(String lb) {
        this.lb = lb;
    }

    public String getLr() {
        return lr;
    }

    public void setLr(String lr) {
        this.lr = lr;
    }

    public String getMd() {
        return md;
    }

    public void setMd(String md) {
        this.md = md;
    }

    public String getJumlah_kendaraan() {
        return jumlah_kendaraan;
    }

    public void setJumlah_kendaraan(String jumlah_kendaraan) {
        this.jumlah_kendaraan = jumlah_kendaraan;
    }

    public String getJumlah_pejalan() {
        return jumlah_pejalan;
    }

    public void setJumlah_pejalan(String jumlah_pejalan) {
        this.jumlah_pejalan = jumlah_pejalan;
    }

    public String getTingkat_kecelakaan() {
        return tingkat_kecelakaan;
    }

    public void setTingkat_kecelakaan(String tingkat_kecelakaan) {
        this.tingkat_kecelakaan = tingkat_kecelakaan;
    }

    public boolean isDraft() {
        return draft;
    }

    public void setDraft(boolean draft) {
        this.draft = draft;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isGps() {
        return gps;
    }

    public void setGps(boolean gps) {
        this.gps = gps;
    }

    public boolean isUrgent() {
        return urgent;
    }

    public void setUrgent(boolean urgent) {
        this.urgent = urgent;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReport_id() {
        return report_id;
    }

    public void setReport_id(String report_id) {
        this.report_id = report_id;
    }


    public String getPolres() {
        return polres;
    }

    public void setPolres(String polres) {
        this.polres = polres;
    }

    public String getPolda() {
        return polda;
    }

    public void setPolda(String polda) {
        this.polda = polda;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAccident_date() {
        return accident_date;
    }

    public void setAccident_date(String accident_date) {
        this.accident_date = accident_date;
    }

    public String getAccident_time() {
        return accident_time;
    }

    public void setAccident_time(String accident_time) {
        this.accident_time = accident_time;
    }

    public String getReport_date() {
        return report_date;
    }

    public void setReport_date(String report_date) {
        this.report_date = report_date;
    }

    public String getReport_time() {
        return report_time;
    }

    public void setReport_time(String report_time) {
        this.report_time = report_time;
    }

    public String getAccident_type_id() {
        return accident_type_id;
    }

    public void setAccident_type_id(String accident_type_id) {
        this.accident_type_id = accident_type_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getRoad_name() {
        return road_name;
    }

    public void setRoad_name(String road_name) {
        this.road_name = road_name;
    }


}
