package com.gandsoft.irsms.model.RequestModel.WitnessForm;

import com.gandsoft.irsms.model.RealmDbModel.WitnessForm.SetWitnessDBModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by glenn on 1/25/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class SetWitnessRequestModel {

    private String first_name;
    private String last_name;
    private String identity_type_id;
    private String identity_no;
    private String religion_id;
    private String profession_id;
    private String education_id;
    private String gender_id;
    private String birth_date;
    private String age;
    private String address;
    private String accident_id;
    private String statement_desc;
    /*new*/
    private String road_name;
    private String road_number;
    private String rt;
    private String rw;
    private String propinsi;
    private String kabupaten;
    private String kecamatan;
    private String kelurahan;

    public SetWitnessRequestModel() {
    }

    public void consumeDBModel(SetWitnessDBModel model) {
        this.first_name = model.getFirst_name();
        this.last_name = model.getLast_name();
        this.identity_type_id = model.getIdentity_type_id();
        this.identity_no = model.getIdentity_no();
        this.religion_id = model.getReligion_id();
        this.profession_id = model.getProfession_id();
        this.gender_id = model.getGender_id();
        this.age = model.getAge();
        this.address = model.getRoad_name();
        this.road_name = model.getRoad_name();
    }

    public String getKelurahan() {
        return kelurahan != null ? kelurahan : "";
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getRt() {
        return rt != null ? rt : "";
    }

    public void setRt(String rt) {
        this.rt = rt;
    }

    public String getRw() {
        return rw != null ? rw : "";
    }

    public void setRw(String rw) {
        this.rw = rw;
    }

    public String getRoad_name() {
        return road_name != null ? road_name : "";
    }

    public void setRoad_name(String road_name) {
        this.road_name = road_name;
    }

    public String getRoad_number() {
        return road_number != null ? road_number : "";
    }

    public void setRoad_number(String road_number) {
        this.road_number = road_number;
    }

    public String getPropinsi() {
        return propinsi != null ? propinsi : "";
    }

    public void setPropinsi(String propinsi) {
        this.propinsi = propinsi;
    }

    public String getKabupaten() {
        return kabupaten != null ? kabupaten : "";
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public String getKecamatan() {
        return kecamatan != null ? kecamatan : "";
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getFirst_name() {
        return first_name != null ? first_name : "";
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name != null ? last_name : "";
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getIdentity_type_id() {
        return identity_type_id != null ? identity_type_id : "";
    }

    public void setIdentity_type_id(String identity_type_id) {
        this.identity_type_id = identity_type_id;
    }

    public String getIdentity_no() {
        return identity_no != null ? identity_no : "";
    }

    public void setIdentity_no(String identity_no) {
        this.identity_no = identity_no;
    }

    public String getReligion_id() {
        return religion_id != null ? religion_id : "";
    }

    public void setReligion_id(String religion_id) {
        this.religion_id = religion_id;
    }

    public String getProfession_id() {
        return profession_id != null ? profession_id : "";
    }

    public void setProfession_id(String profession_id) {
        this.profession_id = profession_id;
    }

    public String getEducation_id() {
        return education_id != null ? education_id : "";
    }

    public void setEducation_id(String education_id) {
        this.education_id = education_id;
    }

    public String getGender_id() {
        return gender_id != null ? gender_id : "";
    }

    public void setGender_id(String gender_id) {
        this.gender_id = gender_id;
    }

    public String getBirth_date() {
        return birth_date != null ? birth_date : "";
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getAge() {
        return age != null ? age : "";
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAddress() {
        return address != null ? address : "";
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAccident_id() {
        return accident_id != null ? accident_id : "";
    }

    public void setAccident_id(String accident_id) {
        this.accident_id = accident_id;
    }

    public String getStatement_desc() {
        return statement_desc != null ? statement_desc : "";
    }

    public void setStatement_desc(String statement_desc) {
        this.statement_desc = statement_desc;
    }

}
