package com.gandsoft.irsms.model.RequestModel.VehicleForm;

/**
 * Created by glenn on 1/25/18.
 */

public class GetVehicleTypeSubRequestModel {

    private String vehicle_type_id;

    public String getVehicle_type_id() {
        return vehicle_type_id != null ? vehicle_type_id : "";
    }

    public void setVehicle_type_id(String vehicle_type_id) {
        this.vehicle_type_id = vehicle_type_id;
    }
}
