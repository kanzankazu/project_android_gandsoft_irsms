package com.gandsoft.irsms.model.ResponseModel.VehicleForm;

import com.gandsoft.irsms.model.ResponseModel.BaseResponseModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by glenn on 1/25/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class SetPassangerResponseModel extends BaseResponseModel {

    private String passenger_person_id;

    public String getPassenger_id() {
        return passenger_person_id;
    }

    public void setPassenger_id(String passenger_id) {
        this.passenger_person_id = passenger_id;
    }

    public String getPassenger_person_id() {
        return passenger_person_id;
    }

    public void setPassenger_person_id(String passenger_person_id) {
        this.passenger_person_id = passenger_person_id;
    }
}
