package com.gandsoft.irsms.model.ResponseModel.AccidentForm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gandsoft.irsms.model.ObjectModel.CodedItemModel;
import com.gandsoft.irsms.model.ResponseModel.BaseResponseModel;

import java.util.List;

import io.realm.annotations.Ignore;

/**
 * Created by glenn on 1/25/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class PolresResponseModel extends BaseResponseModel {

    private List<CodedItemModel> get_polres;

    public List<CodedItemModel> getGet_polres() {
        return get_polres;
    }

    public void setGet_polres(List<CodedItemModel> get_polres) {
        this.get_polres = get_polres;
    }

}
