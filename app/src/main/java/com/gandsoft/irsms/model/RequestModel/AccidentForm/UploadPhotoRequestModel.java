package com.gandsoft.irsms.model.RequestModel.AccidentForm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.realm.RealmObject;

/**
 * Created by gleen on 09/02/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UploadPhotoRequestModel {

    private String accident_id;
    private String pic;

    public String getAccident_id() {
        return accident_id;
    }

    public void setAccident_id(String accident_id) {
        this.accident_id = accident_id;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
