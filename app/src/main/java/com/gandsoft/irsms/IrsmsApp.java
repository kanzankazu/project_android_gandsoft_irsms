package com.gandsoft.irsms;

import android.content.Context;
import android.support.multidex.MultiDex;

import com.gandsoft.irsms.Support.Util.SystemUtil;
import com.gandsoft.irsms.Support.Util.TypefaceUtil;

import app.beelabs.com.codebase.base.BaseApp;
import app.beelabs.com.codebase.di.component.AppComponent;
import app.beelabs.com.codebase.di.component.DaggerAppComponent;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by glenn on 1/25/18.
 */

public class IrsmsApp extends BaseApp {

    private static Context context;
    private static IrsmsApp instance;

    public static Context getContext() {
        return context;
    }

    public static AppComponent getAppComponent() {
        if (context == null) return null;
        return getComponent();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        /*Class<?> rt = null;
        try {
            rt = Class.forName("dalvik.system.VMRuntime");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Object runtime = null;
        try {
            runtime = rt.getMethod("getRuntime").invoke(null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        try {
            rt.getMethod("setMinimumHeapSize", Long.TYPE).invoke(runtime, 32 * 1024 * 1024);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }*/

        if (SystemUtil.isKitkat()) {
            MultiDex.install(this);
        }
        instance = this;
        context = getApplicationContext();
        setupBuilder(DaggerAppComponent.builder(), this);
        TypefaceUtil.overrideFont(getApplicationContext(), "DEFAULT", "fonts/Nunito-Regular.ttf");
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Nunito-SemiBold.ttf");
        TypefaceUtil.overrideFont(getApplicationContext(), "MONOSPACE", "fonts/Nunito-Light.ttf");
        Realm.init(this);
        RealmConfiguration realmConfig = new RealmConfiguration.Builder()
                .name("awewe.realm")
                .deleteRealmIfMigrationNeeded()
                .schemaVersion(1)
                .build();

        Realm.setDefaultConfiguration(realmConfig);
    }

}
